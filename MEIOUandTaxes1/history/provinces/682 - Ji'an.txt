# 682 - jiangxi_area Jizhou

owner = YUA
controller = YUA
culture = hakka
religion = confucianism
capital = "Luling"
trade_goods = chinaware
hre = no
base_tax = 5
base_production = 5
base_manpower = 2
is_city = yes
discovered_by = chinese
discovered_by = steppestech

0985.1.1 = {
	owner = SNG
	controller = SNG
	add_core = SNG
}
1276.1.1 = {
	owner = YUA
	controller = YUA
	add_core = YUA
}
1320.1.1 = {
	remove_core = SNG
}
1351.1.1 = {
	owner = DAA
	controller = DAA
	add_core = DAA
                        remove_core = YUA
}
1365.1.1 = {
	owner = MNG
	controller = MNG
	add_core = MNG
}
1368.1.1 = {
	remove_core = DAA
}
1513.1.1 = { unrest = 5 } # Peasant rebellion, Jiangxi
1514.1.1 = { unrest = 0 }
1529.3.17 = { 
	marketplace = yes
	bailiff = yes
	courthouse = yes
	road_network = yes
	fine_arts_academy = yes
}

1645.6.25 = {
	owner = QNG
	controller = QNG
	add_core = QNG
} # The Qing Dynasty
#1662.1.1 = {
#	owner = QNG
#	controller = QNG
#	add_core = QNG
#	remove_core = MNG
#} # The Qing Dynasty
1662.1.1 = { remove_core = MNG }
