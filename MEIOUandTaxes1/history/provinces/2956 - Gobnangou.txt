# 2956 - Gobnangou

owner = GUR
controller = GUR
culture = mossi
religion = west_african_pagan_reformed
capital = "Konkobiri"
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
trade_goods = millet
hre = no

discovered_by = soudantech
discovered_by = sub_saharan

1356.1.1   = {
	add_core = GUR
}
