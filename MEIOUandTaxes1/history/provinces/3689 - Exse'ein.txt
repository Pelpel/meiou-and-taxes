# No previous file for Exse'ein

culture = yokut
religion = totemism
capital = "Exse'ein"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 25
native_ferocity = 3
native_hostileness = 6

1542.1.1   = { discovered_by = SPA } # Juan Rodr�guez Cabrillo
1579.6.16  = { discovered_by = ENG } # Sir Francis Drake
1707.5.12  = { discovered_by = GBR }
1770.1.1 = {	owner = SPA
	controller = SPA
	citysize = 355
	trade_goods = wool 
} # Don Gaspar de Portol�
1792.1.1 = {
	citysize = 1600
} # British settlement, Yerba Buena (San Francisco) founded by George Vancouver
1795.1.1 = {
	citysize = 1200
	culture = castillian
	religion = catholic
	add_core = SPA
}
1810.9.16  = {
	owner = MEX
} # Declaration of Independence
1821.8.24  = {
	controller = MEX
	remove_core = SPA
} # Treaty of Cordba
1832.10.1  = {
	unrest = 3
} # Convention of 1832
1833.4.1   = {
	unrest = 6
} # Convention of 1833
