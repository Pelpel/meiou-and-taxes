# 1291 - Galich-Merskiy

owner = GLC
controller = GLC
capital = "Merya"
culture = russian
religion = orthodox
trade_goods = salt
hre = no
base_tax = 4
base_production = 4
base_manpower = 5
is_city = yes
discovered_by = NOR
discovered_by = SWE
discovered_by = RSW
discovered_by = eastern
discovered_by = muslim

1356.1.1  = {
	add_core = GLC
	add_core = MOS
}
1364.1.1  = {
	owner = MOS
	controller = MOS
}
1433.1.1   = {
	revolt = { 
		type = pretender_rebels
		size = 2
		name = "Yuri Dmitrievich"
#		dynasty = "Rurikovich" #Does not work
	}
	controller = REB
}
1434.5.6   = {
	revolt = { }
	controller = MOS
}
1438.1.1  = {
	discovered_by = KAZ
}
1450.1.1  = {
	discovered_by = SIB
}
1453.1.1  = {
	discovered_by = western
	discovered_by = eastern
}
1530.1.4  = {
	bailiff = yes	
}
1547.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia
