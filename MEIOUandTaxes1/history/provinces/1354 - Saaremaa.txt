# 1354 - Saaremaa (�sel)

owner = LVO
controller = LVO
culture = estonian
religion = catholic
hre = no
base_tax = 2
base_production = 2
trade_goods = fish
base_manpower = 1
is_city = yes
capital = "Ahrensburg"
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech

1088.1.1 = { dock = yes }
1356.1.1 = {
	add_core = LVO
	add_core = EST
}
1530.1.4  = {
	bailiff = yes	
}
1560.4.15  = {
	owner = DAN
	controller = DAN
	add_core = DAN
	remove_core = LVO
} # Saaremaa is sold to Denmark
1645.8.13  = {
	owner = SWE
	controller = SWE
	add_core = SWE
	remove_core = DAN
} # Treaty of Br�msebro
1721.8.30  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = SWE
} #The Peace of Nystad
