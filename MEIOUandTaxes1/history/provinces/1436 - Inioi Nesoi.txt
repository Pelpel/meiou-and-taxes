
owner = VEN
controller = VEN
culture = greek
religion = orthodox
capital = "Agios Yiorgios"
trade_goods = olive
hre = no
base_tax = 7
base_production = 7
base_manpower = 2
is_city = yes
discovered_by = CIR
discovered_by = muslim
discovered_by = eastern
discovered_by = western
discovered_by = turkishtech

1088.1.1 = { dock = yes }
1267.1.1   = {
	owner = CEP
	controller = CEP
	add_core = CEP
}
1350.1.1   = {
	add_core = VEN
}
1364.1.1   = {
	add_core = GEN
}
1386.1.1  =  {
	remove_core = GEN
}	# vassal of venezia
1479.1.1  = {
	owner = VEN
	controller = VEN
}

1750.1.1  = {
	add_core = GRE
}
1757.1.1  = {
	capital = "Argostoli"
}
1797.1.1  = {
	owner = FRA
	controller = FRA
	add_core = FRA
	remove_core = VEN
}
1809.1.1  = {
	controller = GBR
}
1810.1.1  = {
	owner = GBR
	add_core = GBR
	remove_core = FRA
}
1863.1.1  = {
	owner = GRE
	controller= GRE
	remove_core = GBR
}
