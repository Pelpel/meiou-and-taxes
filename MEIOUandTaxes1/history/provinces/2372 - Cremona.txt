# 2372 - Cremona

owner = MLO    
controller = MLO
culture = lombard 
religion = catholic 
capital = "Cremona"
is_city = yes
base_tax = 9
base_production = 9 
base_manpower = 3 
trade_goods = linen
fort_14th = yes 
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
hre = yes 

1250.1.1 = { temple = yes }
1300.1.1 = { road_network = yes }
1356.1.1   = {
	add_core = MLO
} 
1499.1.1   = {
	controller = VEN
	owner = VEN
	add_core = VEN
}
1509.1.1   = {
	controller = MLO
	owner = MLO
	remove_core = VEN
}
1513.3.23  = {
	owner = SPA
	controller = SPA
	add_core = SPA
	bailiff = yes
} # Treaty of Noyons
1530.1.2 = {
	road_network = no paved_road_network = yes 
}
1530.2.27 = {
	hre = no
}
1618.1.1   =  { hre = no }
1707.4.10  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = SPA
}
1796.11.15 = {
	owner = ITC
	controller = ITC
	add_core = ITC
} # Transpadane Republic
1797.6.29  = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = ITC
} # Cisalpine Republic
#1802.6.26  = {
#	owner = ITA
#	controller = ITA
#	add_core = ITA
#	remove_core = ITE
#} # Italian Republic
# 1805.3.17 - Kingdom of Italy 
1814.4.11  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = ITE
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1860.3.20 = {    ##??
	owner = SPI
	controller = SPI
	add_core = SPI
}
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = SPI
}
