# 77 - Wirtemberg

culture = schwabisch
owner = WUR
controller = WUR
add_core = WUR
capital = "Stuttgart"
religion = catholic
trade_goods = wheat
base_tax = 8
base_production = 8
base_manpower = 2
is_city = yes
fort_14th = yes
hre = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim

#1111.1.1 = { post_system = yes }
1119.1.1 = { bailiff = yes }
1133.1.1 = { mill = yes }
1476.1.1 = { medieval_university = yes }
1489.1.1 = { temple = yes }
1500.1.1 = { road_network = yes }
1519.1.1  = { controller = HAB owner = HAB add_core = HAB unrest = 8  fort_14th = no fort_15th = yes } # After attacking the free town of Reutlingen the Duke of Würtemberg is sent fleeing and the country governed by  the Austrians.
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1534.1.1  = { unrest = 0 owner = WUR controller = WUR religion = protestant } # House Würtemberg reconquers the coutnry and installs the protestant religion
1670.1.1  = {  }

1724.1.1  = { capital = "Ludwigsburg" }
1733.1.1  = { capital = "Stuttgart" unrest = 2  } # Karl Alexander becomes Duke of Würtemberg. He is a catholic with a jewish  advisor, which is very much resented by the protestant nobility.
1737.3.12 = { unrest = 0   } # Death of the Duke, execution of the advisor after a set up process.
1770.1.1  = {
	early_modern_university = yes
} # Hohe Karlsschule
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
