# 2224 - Urfa

owner = JAI
controller = JAI
culture = shami
religion = sunni
capital = "Sanliurfa"	# formerly Edessa
trade_goods = cloth
hre = no
base_tax = 8
base_production = 8
base_manpower = 3
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1356.1.1 = {
	add_core = MAM
	add_core = JAI
}
1514.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = MAM
	remove_core = JAI
}
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1771.1.1  = { unrest = 4 } # Ali Bey gained control of Syria, reconstituting the Mamluk state
1772.1.1  = { unrest = 0 }
