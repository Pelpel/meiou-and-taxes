# No previous file for Yberaba

culture = ge
religion = pantheism
capital = "Yberaba"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 25
native_ferocity = 1
native_hostileness = 5

1550.1.1   = { discovered_by = POR }
1668.1.1   = {
	owner = POR
	controller = POR
	add_core = POR
	citysize = 460
	religion = catholic
	culture = portugese
	trade_goods = copper
	change_province_name = "Santo Antonio da Manga"
	rename_capital = "Santo Antonio da Manga"
}
1700.1.1   = {
	is_city = yes
}
1750.1.1   = {
	add_core = BRZ
	culture = brazilian
}
1822.9.7   = {
	owner = BRZ
	controller = BRZ
}
1825.1.1   = {
	remove_core = POR
}
