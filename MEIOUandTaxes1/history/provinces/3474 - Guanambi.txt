# 3474 - Guanambi

culture = tupinamba
religion = pantheism
capital = "Guanambi"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 20
native_ferocity = 1
native_hostileness = 3

1500.4.23 = { discovered_by = POR } # Pedro �lvares Cabral
1630.1.1 = {
	owner = POR
	controller = POR
	add_core = POR
	citysize = 300
	religion = catholic
	culture = portugese
	trade_goods = sugar
}
1745.1.1 = {
	citysize = 1000
	capital = "Santo Ant�nio do Urubu de Cima"
}
1750.1.1   = {
	add_core = BRZ
	culture = brazilian
}
1822.9.7   = {
	owner = BRZ
	controller = BRZ
}
1825.1.1   = {
	remove_core = POR
}
