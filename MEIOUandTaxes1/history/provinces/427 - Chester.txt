# 427 - Chester

owner = ENG
controller = ENG
culture = northern_e
religion = catholic
hre = no
base_tax = 5
base_production = 5
trade_goods = salt
base_manpower = 3
is_city = yes
capital = "Chester"
add_core = ENG
discovered_by = western
discovered_by = muslim
discovered_by = eastern

#1100.1.1 = { bailiff = yes }
1264.1.1   = {
	add_permanent_province_modifier = {
		name = "county_palatine"
		duration = -1
	}
}
1300.1.1  = { road_network = yes }
1453.1.1  = { unrest = 5 } #Start of the War of the Roses
1461.6.1  = { unrest = 2 } #Coronation of Edward IV
1467.1.1  = { unrest = 5 } #Rivalry between Edward IV & Warwick
1471.1.1  = { unrest = 8 } #Unpopularity of Warwick & War with Burgundy
1471.5.4  = { unrest = 2 } #Murder of Henry VI & Restoration of Edward IV
1483.6.26 = { unrest = 8 } #Revulsion at Supposed Murder of the Princes in the Tower
1485.8.23 = { unrest = 0 } #Battle of Bosworth Field & the End of the War of the Roses
1529.2.5  = { bailiff = yes road_network = no paved_road_network = yes }
1530.1.1  = { culture = english }
1600.1.1  = { fort_14th = yes } #Estimated
1644.1.1  = { controller = REB } #Estimated
1645.1.1  = { controller = ENG }
1654.1.1 = {
	remove_province_modifier = "county_palatine"
	add_permanent_province_modifier = {
		name = "county_palatine_reformed"
		duration = -1
	}
} # Now represented in Parliament
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
   	remove_core = ENG
}
1830.1.1 = {
	remove_province_modifier = "county_palatine_reformed"
} # Special privileges revoked
