# No previous file for Hargeisa

owner = ADA
controller = ADA
add_core = ADA
culture = somali
religion = sunni
capital = "Hargeisa"
citysize = 4500
base_manpower = 2
trade_goods = naval_supplies
hre = no
base_tax = 2
base_production = 2
discovered_by = ETH
discovered_by = ADA
discovered_by = ZAN
discovered_by = AJU
discovered_by = MBA
discovered_by = MLI
discovered_by = ZIM
discovered_by = indian
discovered_by = muslim
discovered_by = turkishtech
discovered_by = east_african

1499.1.1 = { discovered_by = POR }
1515.2.1 = { training_fields = yes }
1550.1.1 = { discovered_by = TUR }
1554.1.1 = { unrest = 9 } # Invasion by Oromo causes widespread destruction
1559.1.1 = { unrest = 8 } # Invasion by Galawdewos
1562.1.1 = { unrest = 5 } # Invasion by Oromo
1567.1.1 = { unrest = 5 } # Invasion by Oromo
1568.1.1 = {
	unrest = 0
	owner = HAR
	controller = HAR
}
1577.1.1 = { owner = WAR controller = WAR add_core = WAR }
