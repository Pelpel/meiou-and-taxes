# 1409 - Cantabria

owner = CAS		#Juan II of Castille
controller = CAS
add_core = CAS
culture = castillian
religion = catholic
hre = no
base_tax = 4
base_production = 4
trade_goods = naval_supplies
base_manpower = 4
is_city = yes
capital = "Santander"

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1088.1.1 = { dock = yes }
1250.1.1 = { temple = yes }
1356.1.1  = { 
	add_core = ENR
}
1369.3.23  = { 
	remove_core = ENR
}
1475.6.2   = { controller = POR }
1476.3.2   = { controller = CAS }
1516.1.23  = {
	controller = SPA
	owner = SPA
	add_core = SPA
	bailiff = yes
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille
1522.3.20 = { dock = no naval_arsenal = yes }
1713.4.11  = { remove_core = CAS }
 #Audiencia de Asturias
