# No previous file for Virumaa

owner = DEN
controller = DEN
culture = estonian
religion = catholic
capital = "Narva"
base_tax = 3
base_production = 3
base_manpower = 3
is_city = yes
fort_14th   = yes
trade_goods = salt
discovered_by = western
discovered_by = eastern
discovered_by = steppestech
hre = no

1200.1.1 = { road_network = yes }
1346.1.1  = { 
	owner = LVO 
	controller = LVO 
	add_core = LVO 
	add_core = EST
}

1356.1.1   = {
	add_local_autonomy = 25 # supression of St. George's Night Uprising
}

1530.1.4  = {
	bailiff = yes	
}
1542.1.1  = { religion = protestant} #Unknown date
1561.11.18 = {
	owner = SWE
	controller = SWE
	add_core = SWE
	remove_core = LVO
} # # Wilno Pact
1710.9.15 = { controller = RUS } #The Great Nordic War-Captured Reval
1721.8.30 = {
	owner = RUS
	add_core = RUS
	remove_core = SWE
} #The Peace of Nystad
#1814.5.17 = {
#	add_core = DEN
#	remove_core = DAN
#}
