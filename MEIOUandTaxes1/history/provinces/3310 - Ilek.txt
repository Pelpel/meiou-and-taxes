# No previous file for Ilek

owner = BLU
controller = BLU
capital = "Aktobe"
culture = tartar
religion = sunni
trade_goods = naval_supplies
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = turkishtech
discovered_by = steppestech
discovered_by = muslim

1356.1.1 = {
	add_core = KZH
	add_core = BLU
	add_core = NOG
	unrest = 3
}
1382.1.1   = {
	owner = GOL
	controller = GOL
	add_core = GOL
	remove_core = BLU
}
1419.1.1 = {
	owner = NOG
	controller = NOG
}
1444.1.1 = {
	remove_core = GOL
	remove_core = BLU
	remove_core = WHI
}
1515.1.1 = { training_fields = yes }
1520.1.1  = {
	owner = KZH
	controller = KZH
	culture = khazak
} # Qasim Khan conquers Nogai lands
