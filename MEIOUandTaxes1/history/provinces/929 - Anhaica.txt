# No previous file for Anhaica

culture = timicua
religion = totemism
capital = "Anhaica"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 25
native_ferocity = 1 
native_hostileness = 6

1513.4.2  = { discovered_by = CAS } # Juan Ponce de Leon
1516.1.23 = { discovered_by = SPA }
1623.1.1   = {	owner = SPA
		controller = SPA
		citysize = 150
		culture = castillian
		trade_goods = fish
		religion = catholic } #First Spanish missions to the Yustaga
1648.1.1   = {	add_core = SPA citysize = 1000 } 
1819.2.22 = {	owner = USA
		controller = USA
		remove_core = SPA
	   } # The Adams-On�s Treaty
