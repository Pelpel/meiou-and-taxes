# No previous file for Jaribaan

owner = AJU
controller = AJU
add_core = AJU
add_core = HOB
culture = somali
religion = sunni
capital = "Jaribaan"
trade_goods = millet
citysize = 3000
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
add_local_autonomy = 25
discovered_by = ETH
discovered_by = ADA
discovered_by = ZAN
discovered_by = AJU
discovered_by = MBA
discovered_by = MLI
discovered_by = ZIM
discovered_by = indian
discovered_by = muslim
discovered_by = turkishtech
discovered_by = east_african

1499.1.1 = { discovered_by = POR } 
1515.2.1 = { training_fields = yes }
1526.1.1 = { owner = MJE controller = MJE add_core = MJE } #Ahmad Gran secures control over Marehan
1550.1.1 = { discovered_by = TUR }
1555.1.1 = { owner = AJU controller = AJU } #Northern part of province no longer conrolled by ADA
1650.1.1 = { owner = HOB controller = HOB remove_core = AJU }
