# 1554 - Kano

owner = KNO
controller = KNO
culture = haussa		
religion = sunni		 
capital = "Kano"
base_tax = 10
base_production = 10
base_manpower = 6
is_city = yes
trade_goods = millet
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1356.1.1   = {
	add_core = KNO
}
1400.1.1 = { temple = yes }
1807.1.1 = {
	owner = SOK
	controller = SOK
	add_core = SOK
}
1810.1.1   = {
	remove_core = KNO
}
