# 337 - Ouahran

owner = TLE
controller = TLE
culture = algerian
religion = sunni
capital = "Oran"
base_tax = 9
base_production = 9
base_manpower = 2
is_city = yes
trade_goods = wine
discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech
hre = no

900.1.1    = { set_province_flag = barbary_port }
1000.1.1   = {
	add_permanent_province_modifier = { 
		name = "mediterranean_harbour" 
		duration = -1 
		}
}
1088.1.1 = { dock = yes }

1337.1.1 = {
	owner = FEZ
	controller = FEZ
	add_core = FEZ
	add_core = ALG
	add_core = TLE
}
1399.1.1  = {
	owner = TLE
	controller = TLE
}
1509.1.1 = {
	owner = CAS
	controller = CAS
	add_core = CAS
		
} # Taken over by the Spanish
1510.1.1 = { fort_14th = yes }
1516.1.23  = {
	controller = SPA
	owner = SPA
	add_core = SPA
	remove_core = CAS
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille
1522.3.20 = { dock = no naval_arsenal = yes }
1530.1.2 = {
	remove_core = FEZ
}
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1550.1.1 = { remove_core = FEZ }
1650.1.1 = {  }
1708.1.1 = {
	owner = ALG
	controller = ALG
	remove_core = SPA
} # The Spanish are expelled by Algerian forces, part of the Ottoman empire
1732.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	
	remove_core = TUR
} # Conquered by Spain
1792.1.1 = {
	owner = ALG
	controller = ALG
	remove_core = SPA
} # The Spanish surrender Oran to Algiers
1845.1.1 = {
	owner = FRA
	controller = FRA
}
