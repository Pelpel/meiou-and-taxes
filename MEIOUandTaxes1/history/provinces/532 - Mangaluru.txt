# 532 - Mangaluru

owner = ZAM
controller = ZAM
culture = tuluva
religion = hinduism
capital = "Mangaluru"
trade_goods = steel
hre = no
base_tax = 6
base_production = 6
base_manpower = 3
is_city = yes
discovered_by = indian
discovered_by = muslim

1000.1.1 = {
	add_permanent_province_modifier = {
		name = ideal_european_port
		duration = -1
	}
	dock = yes
}
1356.1.1 = {
	owner = VIJ
	controller = VIJ
	add_core = VIJ
	add_core = ZAM
	add_core = KLN
}
1376.1.1 = {
	owner = ZAM
	controller = ZAM
}
1450.1.1  = { add_core = ZAM }
1498.5.20 = { discovered_by = POR }		#FB was 1498.1.1
1530.1.1 = {
	owner = VIJ
	controller = VIJ
	add_core = VIJ
	add_core = KLN
	#remove_core = VIJ
}
1530.3.17 = {
	bailiff = yes
	marketplace = yes
	road_network = yes
}
1559.1.1  = {
	owner = VIJ
	controller = VIJ
	add_core = VIJ
} # Consolidation
1565.7.1  = {
	owner = KLN
	controller = KLN
} # The Vijayanagar empire collapses
1596.2.1  = { discovered_by = NED } # Cornelis de Houtman
1600.1.1  = { discovered_by = turkishtech discovered_by = ENG discovered_by = FRA }
1650.1.1 = {
	fort_16th = yes
}
1660.1.1  = {
	controller = BIJ
	owner = BIJ
}

1707.5.12 = { discovered_by = GBR }

1763.1.1 = {
	owner = MYS
	controller = MYS
	add_core = MYS
} # Mysore sultan conquers the province
1768.1.1 = {
	owner = GBR
	controller = GBR
}
1794.1.1 = {
	owner = MYS
	controller = MYS
}
1798.1.1  = {
	controller = GBR
}
1799.1.1 = {
	owner = GBR
}
