# No previous file for Heilong Jiang

culture = nivkh
religion = tengri_pagan_reformed
capital = "Rih�n"
trade_goods = fur
hre = no
base_tax = 2
base_production = 2
base_manpower = 1
native_size = 10
native_ferocity = 1
native_hostileness = 3
discovered_by = chinese
discovered_by = steppestech

1636.5.15 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = MCH
} # The Qing Dynasty
1858.1.1 = {
	discovered_by = RUS
   	owner = RUS
   	controller = RUS
   	religion = orthodox
   	culture = russian
} # Treaty of Aigun
