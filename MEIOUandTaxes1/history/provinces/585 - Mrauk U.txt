# 585 - Mrauk U
# TM-Smiles indochina-mod for meiou

owner = ARK
controller = ARK
culture = arakanese
religion = buddhism
capital = "Mrauk U"
trade_goods = rice
hre = no
base_tax = 10
base_production = 10
base_manpower = 5
is_city = yes
discovered_by = chinese
discovered_by = indian
discovered_by = muslim

1100.1.1 = { marketplace = yes }
1133.1.1 = { mill = yes }
1356.1.1 = {
	add_core = ARK
	fort_14th = yes
}
1404.1.1 = { 
	fort_14th = no 
	fort_15th = yes
}
1519.1.1 = { bailiff = yes }
1521.1.1 = { temple = yes }
1752.2.28 = { add_core = BRM }
1784.12.31 = {
	owner = BRM
	controller = BRM
	add_core = BRM
} # Annexed by Burma
1826.2.24 = { owner = GBR controller = GBR add_core = GBR citysize = 5405 }  #today's Sittwe
