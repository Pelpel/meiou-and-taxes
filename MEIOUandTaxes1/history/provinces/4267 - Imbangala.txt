# No previous file for Imbangala

owner = KSJ
controller = KSJ
add_core = KSJ
culture = lunda
religion = animism
capital = "Imbangala"
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
trade_goods = ivory
hre = no
discovered_by = central_african

1628.1.1   = { discovered_by = POR }
