# 461 - Alma Ata

owner = MGH
controller = MGH
culture = khazak
religion = sunni
capital = "Almatu"
trade_goods = lead
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes
discovered_by = KSH
discovered_by = mongol_tech
discovered_by = steppestech

#1111.1.1 = { post_system = yes }
1356.1.1 = {
	add_core = MGH
	add_core = KAS
}
#1465.1.1 = {
#	owner = KZH
#	controller = KZH
#	remove_core = SHY
#}
1515.1.1 = { training_fields = yes }
1520.1.1 = {
	owner = KZH
	controller = KZH
	add_core = KZH
	remove_core = SHY
}
1723.1.1 = { owner = OIR controller = OIR } # Dzungarian invasion
1755.1.1   = {
	owner = QNG
	controller = QNG
	add_core = QNG
}

