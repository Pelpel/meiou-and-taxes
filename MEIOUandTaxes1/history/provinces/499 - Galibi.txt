# 499 - Galibi

culture = carib
religion = pantheism
capital = "Galibi"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 25
native_ferocity = 2 
native_hostileness = 9

1502.6.15 = {
	discovered_by = CAS
	owner = CAS
	controller = CAS
	citysize = 450
	culture = castillian
	religion = catholic
	capital = "Martinique"
	trade_goods = sugar
	set_province_flag = trade_good_set
} # Christopher Columbus
1516.1.23 = {
	discovered_by = SPA
	owner = SPA
	controller = SPA
	citysize = 1100
}
1527.1.1  = {
	add_core = SPA
}
1635.1.1  = {
	owner = FRA 
	controller = FRA 
	remove_core = SPA
	capital = "Saint-Pierre"
	citysize = 1100
	culture = creole
	religion = catholic 
	trade_goods = sugar
	set_province_flag = trade_good_set
} # Colonized by France
1660.1.1  = {
	add_core = FRA
}
1794.1.1   = { controller = GBR }
1797.10.17 = { controller = FRA } # End of the First Coalition
1797.10.18 = { controller = GBR }
1802.10.1  = { controller = FRA } # End of the Second Coalition
1803.5.18  = { controller = GBR }
# 1805.12.26 = { controller = FRA } # End of the Third Coalition
# 1805.12.26 = { controller = GBR }
1807.10.24 = { controller = FRA } # End of the Fourth Coalition
1809.4.10  = { controller = GBR }
1809.10.14 = { controller = FRA } # End of the Fifth Coalition
1812.6.21  = { controller = GBR }
1814.4.11 = {
	controller = FRA
} # Treaty of Fontainebleu, Napoleon abdicates unconditionally
