#252 - Highlands

owner = SCO
controller = SCO
culture = highland_scottish
religion = catholic
hre = no
base_tax = 2
base_production = 2
trade_goods = livestock
base_manpower = 1
is_city = yes
capital = "Inbhir Aora" # Inveraray
add_core = SCO
#add_core = HIG
discovered_by = western
discovered_by = muslim
discovered_by = eastern

1356.1.1 = {
	add_permanent_province_modifier = {
		name = medieval_castle
		duration = -1
	}
}
1560.8.1  = { religion = reformed }
1644.1.1  = { controller = REB }
1647.1.1  = { controller = SCO } #Estimated
1689.3.1  = { controller = REB } #Jacobite Rising
1689.11.1 = { controller = SCO }
1690.1.1  = { fort_14th = yes }
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
}
 #marketplace, Regimental Camp Estimated
