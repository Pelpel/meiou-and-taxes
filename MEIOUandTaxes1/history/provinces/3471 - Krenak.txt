# No previous file for Kren�k

culture = ge
religion = pantheism
capital = "Kren�k"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 25
native_ferocity = 1
native_hostileness = 5

1550.1.1 = { discovered_by = POR }
1670.1.1 = {
	owner = POR
	controller = POR
	add_core = POR
	citysize = 460
	religion = catholic
	culture = portugese
	trade_goods = gold
	change_province_name = "Arraial do Tejuco"
	rename_capital = "Arraial do Tejuco"
}
1713.1.1 = {
	is_city = yes
}
1750.1.1   = {
	add_core = BRZ
	culture = brazilian
}
1822.9.7   = {
	owner = BRZ
	controller = BRZ
}
1825.1.1   = {
	remove_core = POR
}
