# 2804 - Ueosu

owner = KOR
controller = KOR
add_core = KOR
culture = korean
religion = mahayana #FB-ASSA become confucianism in 1392
capital = "Namwon"
base_tax = 6
base_production = 6
base_manpower = 4
is_city = yes
trade_goods = chinaware


discovered_by = chinese
discovered_by = steppestech
hre = no

1000.1.1   = {
	add_permanent_province_modifier = { 
		name = "natural_harbour" 
		duration = -1 
		}
}
1356.1.1  = {
	set_province_flag = korea_wokou_rebels
} # Wokou pirates effectively control south Korea
1360.1.1  = {
	revolt = { }
	controller = KOR
	unrest = 0
	clr_province_flag = korea_wokou_rebels
} # Pirates chased
1392.6.5  = {
	religion = confucianism
	owner = JOS
	controller = JOS
	add_core = JOS
	remove_core = KOR
}
1529.3.17 = { 
	dock = yes
	marketplace = yes
	public_baths = yes
	customs_house = yes
	road_network = yes
}
1593.1.1  = {
	controller = ODA
} # Japanese invasion
1593.2.12 = {
	controller = JOS
} # With Chinese help the Japanese troops are driven back
1597.8.16 = {
	controller = ODA
} # Second Japanese invasion
1597.9.1  = {
	controller = JOS
} # The Japanese are forced to retreat
1637.1.1  = {
	add_core = MNG
} # Tributary of Qing China
1644.1.1  = {
	add_core = QNG
	remove_core = MNG
} # Part of the Manchu empire
1653.1.1  = {
	discovered_by = NED
} # Hendrick Hamel
