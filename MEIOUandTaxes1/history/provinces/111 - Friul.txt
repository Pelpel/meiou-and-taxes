# 111 - Fri�l

owner = AQU      
controller = AQU
add_core = AQU
culture = friulian 
religion = catholic  
capital = "Udin"
base_tax = 7
base_production = 7
base_manpower = 4
is_city = yes               
trade_goods = wheat
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

hre = yes 

1088.1.1 = { dock = yes }
1133.1.1 = { mill = yes }

1300.1.1 = { road_network = yes }
1389.1.1 = { temple = yes }
1420.1.1   = {
	owner = VEN
	controller = VEN
	add_core = VEN	
} # To Venice
1530.1.2 = {
	road_network = no paved_road_network = yes 
	bailiff = yes
	hre = no 
}
1618.1.1  =  { hre = no }
1797.10.17 = {
	owner = HAB
	controller = HAB
	add_core = HAB
} # Treaty of Campo Formio
1805.12.26 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = HAB
} # Treaty of Pressburg
1814.4.11  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = ITE
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1866.1.1 = {
	owner = ITE
	controller = ITE
	add_core = ITE
}
