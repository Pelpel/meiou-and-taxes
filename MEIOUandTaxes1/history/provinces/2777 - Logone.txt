# 2777 - Ngambai

owner = YAO
controller = YAO
culture = kanouri
religion = animism
capital = "Moundou"
base_tax = 3
base_production = 3
base_manpower = 3
is_city = yes
trade_goods = ivory
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1356.1.1   = {
	add_core = BAG
	add_core = YAO
}
1522.1.1   = {
	owner = BAG
	controller = BAG
	remove_core = YAO
}
1568.1.1   = {
	religion = sunni
}
1580.1.1   = {
	owner = KBO
	controller = KBO
}
1617.1.1   = {
	owner = BAG
	controller = BAG
}
1800.1.1   = {
	add_core = WAD
}
