#1130 - Yatanga

owner = YAT
controller = YAT
culture = mossi
religion = west_african_pagan_reformed
capital = "Ouahiyouga"
base_tax = 7
base_production = 7
base_manpower = 5
is_city=  yes
trade_goods = ivory
hre = no
discovered_by = soudantech
discovered_by = sub_saharan

1356.1.1   = {
	add_core = YAT
}
