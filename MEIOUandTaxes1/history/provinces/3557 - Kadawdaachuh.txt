# No previous file for Kadawd�achuh

culture = caddo
religion = totemism
capital = "Kadawd�achuh"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 20
native_ferocity = 2
native_hostileness = 5

1714.1.1  = {
	owner = CAD
	controller = CAD
	add_core = CAD
	is_city = yes
	trade_goods = cotton
} # Founding of Natchitoches
