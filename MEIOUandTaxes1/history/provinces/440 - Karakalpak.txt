# 440 - Khawarazm

owner = BLU
controller = BLU
culture = kwarezhmi
religion = sunni
capital = "No�kis"
trade_goods = wool
hre = no
base_tax = 6
base_production = 6
base_manpower = 4
is_city = yes
discovered_by = steppestech
discovered_by = turkishtech
discovered_by = muslim

1356.1.1   = {
	add_core = BLU
	add_core = KHI
}
1359.1.1 = {
	owner = KHI
	controller = KHI
	remove_core = BLU
}
1379.1.1 = {
	owner = TIM
	controller = TIM
	add_core = TIM
}
1469.8.27 = {
	owner = KHI
	controller = KHI
	remove_core = TIM
}
1505.1.1 = {
	owner = SHY
	controller = SHY
} # Captured by the Shaybanid horde
1511.1.1 = {
	owner = KHI 
	controller = KHI
} # Khiva Independent
1515.1.1 = { training_fields = yes }
1677.1.1 = { discovered_by = FRA }
