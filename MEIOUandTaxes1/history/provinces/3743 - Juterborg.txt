# 3743 - J�terbog

owner = MAG
controller = MAG
culture = low_saxon
religion = catholic
capital = "Juterborg"
trade_goods = wheat
hre = yes
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes
add_core = MAG
discovered_by = eastern
discovered_by = western
discovered_by = muslim


1133.1.1 = { mill = yes }
1500.1.1 = { road_network = yes }
1524.1.1  = { religion = protestant  }
1530.1.4  = {
	bailiff = yes	
}
1648.10.24 = {
	owner = SAX
	controller = SAX
	add_core = SAX
} # Peace of Westphalia
1815.6.9  = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = SAX
} # Congress of Vienna
