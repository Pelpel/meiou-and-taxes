# No previous file for Amba Hinggan

owner = YUA
controller = YUA
add_core = YUA
culture = khorchin
religion = tengri_pagan_reformed
capital = "Hulunbuir"
trade_goods = wheat
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes
discovered_by = chinese
discovered_by = steppestech

1392.1.1  = {
	owner = KHR
	controller = KHR
	add_core = KHR
	remove_core = YUA
}
1515.1.1 = { training_fields = yes }
1586.1.1 = { religion = vajrayana } # State religion
1594.1.1 = {
	owner = MYR
	controller = MYR
} #Solon Khanate
1612.1.1 = {
	owner = MCH
	controller = MCH
} # Part of the Manchu empire
1636.5.15 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = MCH
} # The Qing Dynasty
1662.1.1 = { remove_core = MNG } # The government in Taiwan surrendered
