# 642 - Mandar

culture = bugis
religion = polynesian_religion
capital = "Mamuju"
trade_goods = unknown # cloth
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 60
native_ferocity = 2
native_hostileness = 7
discovered_by = chinese
discovered_by = indian
discovered_by = austranesian

1356.1.1 = {
	add_core = MKS
	add_core = BNE
}
1511.1.1 = { discovered_by = POR }
1608.1.1 = { religion = sunni }
1644.1.1 = {
	discovered_by = MKS
	owner = MKS
	controller = MKS
	is_city = yes
	trade_goods = cloth
}
1667.1.1  = {
	owner = NED
	controller = NED
} # Under Dutch control
1700.1.1 = { add_core = NED }
1811.9.1 = {
	owner = GBR
	controller = GBR
} # British take over
1816.1.1 = {
	owner = NED
	controller = NED
} # Returned to the Dutch
