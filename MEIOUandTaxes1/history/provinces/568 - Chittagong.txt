# 568 - Chittagong

owner = BNG
controller = BNG
culture = bengali
religion = hinduism
capital = "Chittagong"
trade_goods = rice
hre = no
base_tax = 10
base_production = 10
base_manpower = 4
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = chinese
discovered_by = steppestech

1100.1.1 = { marketplace = yes }
1200.1.1 = { road_network = yes }
1356.1.1  = {
	add_core = BNG
	add_core = ARK
	add_core = TPR
	fort_14th = yes
}
1444.1.1 = {
	add_core = ARK
}
1467.1.1 = {
	owner = ARK
	controller = ARK
	remove_core = BNG
	remove_core = TPR
} # Conquered by Arakan, soon becomes haven for Portuguese pirates
1473.1.1 = {
}
1500.1.1 = {
	discovered_by = POR
}
1520.1.1 = {
	base_tax = 6
base_production = 6
} #Land reclamation
1528.1.1 = {
	owner = POR
	controller = POR
	add_core = POR
	naval_arsenal = yes
	add_permanent_claim = MUG
} # Fallout of the internal Bengal conflict being won by Sher Shah
1666.1.1 = {
	owner = MUG
	controller = MUG
	add_core = MUG
} # Annexation by the Mughals
1707.3.15 = {
	owner = BNG
	controller = BNG
}
1760.1.1 = {
	owner = GBR
	controller = GBR
	remove_core = MUG
} # Given to GBR by Mir Qasim
1800.1.1 = { religion = sunni }
1810.1.1  = {
	add_core = GBR
}
