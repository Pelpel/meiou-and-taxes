# 3741 - Erfurt

owner = ERF
controller = ERF
culture = high_saxon
religion = catholic
capital = "Erfurt"
trade_goods = wheat
hre = yes
base_tax = 7
base_production = 7
base_manpower = 2
is_city = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim

#1111.1.1 = { post_system = yes }
1200.1.1 = { road_network = yes }
1250.1.1 = { temple = yes }
1356.1.1   = {
	add_core = ERF
	add_core = MAI
}
1392.1.1   = {
	medieval_university = yes
}
1483.1.1   = {
	fort_14th = yes
}
1500.1.1 = { road_network = yes }
1521.1.1   = {
	religion = protestant
}
1664.1.1   = {
	owner = MAI
	controller = MAI
	remove_core = ERF
} # City of Erfurt loses its autonomy
1726.1.1   = {
	fort_14th = no
	fort_17th = yes
}
1802.1.1   = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = MAI
}
1806.7.12  = {
	hre = no
} # The Holy Roman Empire is dissolved
1806.10.16 = {
	controller = FRA
} # Controlled by France
1807.7.9   = {
	owner = WES
	controller = WES
	add_core = WES
	remove_core = PRU
} # The Second Treaty of Tilsit
1810.12.13 = {
	owner = FRA
	controller = FRA
     add_core = FRA
     remove_core = WES
} # Annexed by France
1813.10.13 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = WES
} # Treaty of Paris
