
owner = TEU
controller = TEU
add_core = TEU
capital = "Königsberg"
culture = prussian
religion = catholic
trade_goods = gems
hre = no
base_tax = 9
base_production = 9
base_manpower = 4
is_city = yes
fort_14th = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = steppestech

1088.1.1 = { dock = yes }
1100.1.1 = { marketplace = yes }
#1111.1.1 = { post_system = yes }
1119.1.1 = { bailiff = yes }
1300.1.1 = { road_network = yes }
1356.1.1 = {
	add_permanent_province_modifier = {
		name = teutonic_castle
		duration = -1
	}
}

1380.1.1 = { temple = yes }
1453.1.1   = { add_core = POL }
1454.3.6   = { controller = REB } # Beginning of the "thirteen years war"
1466.10.19 = { controller = TEU culture = prussian } # "Peace of Torun", became a Polish fief

1515.1.1 = { training_fields = yes }
1522.2.15 = { shipyard = yes }
1522.3.20 = { dock = no naval_arsenal = yes }
1525.4.10   = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = TEU
	religion = protestant
	remove_core = POL
} # Albert of Prussia became a protestant
1529.12.17 = { merchant_guild = yes }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1560.1.1   = {  } # Important port for the southeastern Baltic region
1618.8.28  = {
	owner = BRA
	controller = BRA
	add_core = BRA
} # Prussia in a personal union with Brandenburg
1667.1.1   = {  }
1701.1.18  = {	owner = PRU
		controller = PRU
		remove_core = BRA
	     } # Kingdom of Prussia
1708.1.1   = { fort_14th = yes }
1758.1.22  = { controller = RUS } # Occupied by Russia
1758.8.25  = { controller = PRU }
