# 1220 - Oeddei

native_size = 10
native_ferocity = 2
native_hostileness = 2
culture = chadic 
religion = animism
capital = "Ubangi"
trade_goods = millet
hre = no
discovered_by = soudantech
discovered_by = sub_saharan

1635.1.1   = {
	owner = WAD
	controller = WAD
	add_core = WAD
	is_city = yes
		religion = sunni
}
