# 3516 - Tayisal

owner = ITZ
controller = ITZ
add_core = ITZ
culture = itza
religion = mesoamerican_religion
capital = "Nojpetén" 
base_tax = 3
base_production = 3
base_manpower = 2
trade_goods = maize
hre = no
is_city = yes

1502.1.1   = {
	discovered_by = CAS
}
1516.1.23  = {
	discovered_by = SPA
}
1528.2.12  = {
	owner = SPA
	controller = SPA
	#citysize = 200
	culture = castillian
	religion = catholic
	
}
1553.1.1   = {
	add_core = SPA
	citysize = 5000
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
}
1810.9.16  = {
	owner = MEX
} # Declaration of Independence
1821.8.24  = {
	controller = MEX
	remove_core = SPA
} # Treaty of Cordba
1823.7.10  = {
	owner = CAM
	controller = CAM
	remove_core = MEX
}
