# 4067 - Balikesir

owner = OTT
controller = OTT
culture = greek
religion = orthodox
capital = "Balikesir"
trade_goods = cloth
hre = no
base_tax = 5
base_production = 5
base_manpower = 2
is_city = yes
discovered_by = CIR
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
add_permanent_claim = BYZ

1300.1.1 = { road_network = yes }
1356.1.1 = {
	add_core = OTT
	add_permanent_province_modifier = {
		name = "beylik_of_karasi"
		duration = -1
	}
	set_province_flag = turkish_name
	fort_14th = yes
}
1360.1.1 = {
	remove_province_modifier = beylik_of_karasi
}
1390.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = OTT
	culture = turkish
	religion =  sunni
}
1481.6.1  = { controller = REB } # Civil war, Bayezid & Jem
1482.7.26 = { controller = TUR } # Jem escapes to Rhodes
1509.1.1  = { controller = REB } # Civil war
1513.1.1  = { controller = TUR }
1530.1.4  = {
	bailiff = yes	
}
1623.1.1  = { controller = REB } # The empire fell into anarchy, Janissaries stormed the palace
1625.1.1  = { controller = TUR } # Murad tries to quell the corruption
1656.1.1  = {
}
1718.1.1  = { unrest = 3 } # Lale Devri (the tulip age), not appreciated by everyone  
1720.1.1  = { unrest = 0 }
1795.1.1  = { unrest = 6 } # Reforms by Sultan Selim III, tried to replace the Janissary corps
