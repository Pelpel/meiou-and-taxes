# 2806 - Yeongdong

owner = KOR
controller = KOR
add_core = KOR
culture = korean
religion = mahayana #FB-ASSA become confucianism in 1392
capital = "Gangneung"

base_tax = 7
base_production = 7
#base_manpower = 2.0
base_manpower = 4.0
citysize = 30000
trade_goods = fish



discovered_by = chinese
discovered_by = steppestech

hre = no

1356.1.1 = {
}
1392.6.5  = {
	religion = confucianism
	owner = JOS
	controller = JOS
	add_core = JOS
	remove_core = KOR
}
1500.1.1  = {
	citysize = 30000
}
1529.3.17 = { 
	dock = yes
	marketplace = yes
	public_baths = yes
	customs_house = yes
	road_network = yes
}
1550.1.1  = {
	citysize = 30000
}
1592.4.24 = {
	controller = ODA
} # Japanese invasion
1593.5.18 = {
	controller = JOS
	add_core = ODA
} # The Japanese still retained a small foothold after their first invasion
1597.1.1  = {
	controller = ODA
}
1597.11.1 = {
	controller = JOS
	remove_core = ODA
}
1600.1.1  = {
	citysize = 15000
} # Great devastation as a result of the Japanese invasion
1637.1.1  = {
	add_core = MNG
} # Tributary of Qing China
1644.1.1  = {
	add_core = QNG
	remove_core = MNG
} # Part of the Manchu empire
1650.1.1  = {
	citysize = 30000
}
1653.1.1  = {
	discovered_by = NED
} # Hendrick Hamel
1680.1.1  = {
	
} # Center of herbal trade in Joseon
1700.1.1  = {
	citysize = 40000
}
1750.1.1  = {
	citysize = 50000
}
1760.1.1  = {
	
}
1800.1.1  = {
	citysize = 60000
}
