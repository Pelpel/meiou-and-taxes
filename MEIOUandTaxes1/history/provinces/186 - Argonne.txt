# 186 - Champagne - Principal cities: Reims

owner = FRA
controller = FRA
add_core = CHP
add_core = FRA
culture = francien
religion = catholic
capital = "Reims"
base_tax = 9
base_production = 9
base_manpower = 4
is_city = yes
trade_goods = lumber
estate = estate_church
discovered_by = eastern
discovered_by = western
discovered_by = muslim
hre = no


1250.1.1 = { temple = yes }
1300.1.1 = { road_network = yes }
1343.1.1 = { add_core = NAV}
1378.1.1 = { remove_core = NAV}
1419.1.19  = {
	owner = ENG
	controller = ENG
}
1429.7.17  = {
	owner = FRA
	controller = FRA
}
1466.2.10  = { add_core = BUR } # Treaty of Conflans, Champagne is promised to Charles the Bold as dowry
1477.1.5   = { remove_core = BUR } # Charles the Bold dies
1500.1.1 = { road_network = yes }
1519.1.1 = { bailiff = yes }
1530.1.1   = { fort_14th = yes }
1562.3.1   = { unrest = 7 } # Massacre at Wassy, the fuse is put in the French powder keg
1563.3.19  = { unrest = 0 } # Temporary truce after the assassination of de Guise
1588.12.1  = { unrest = 5 } # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
1594.1.1   = { unrest = 0 } # 'Paris vaut bien une messe!', Henri converts to Catholicism
1621.1.1   = { base_tax = 10
base_production = 10 } # Richelieu
1630.1.1   = { fort_14th = no fort_15th = yes }
1636.8.30  = { controller = HAB } # Habsburg forces ravage North Eastern France
1636.9.25  = { controller = FRA } # Bernhard of Saxe-Weimar defeats the invaders and gradually pushes them back
1650.1.14  = { unrest = 7 } # Mazarin arrests the Princes Cond�, Conti & Longueville, the beginning of the Second Fronde
1651.4.1   = { unrest = 4 } # An unstable peace is concluded
1651.12.1  = { unrest = 7 } # Mazarin returns from exile, Cond� sides with Spain, situation heats up again
1652.2.15  = { revolt = { type = noble_rebels size = 0 } controller = REB unrest = 3 } # Charles of Lorraine marches towards Paris to team up with Cond�, ravaging the Champagne
1652.10.21 = { revolt = { } controller = FRA unrest = 0 } # The King is allowed to enter Paris again, Mazarin leaves France for good. Second Fronde over.
1680.1.1   = { fort_15th = no fort_16th = yes }
1740.1.1   = { fort_16th = no fort_17th = yes  }
