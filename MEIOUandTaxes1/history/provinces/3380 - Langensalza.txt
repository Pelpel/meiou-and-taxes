# 3380 - Langensalza

owner = THU
controller = THU
culture = high_saxon
religion = catholic
capital = "Langensalza"
trade_goods = wool
hre = yes
base_tax = 7
base_production = 7
base_manpower = 3
is_city = yes
add_core = THU
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1200.1.1 = { road_network = yes }
1440.1.1   = { 
	owner = SAX
	controller = SAX 
	add_core = SAX
	remove_core = MEI
}#Duchy of Thuringia is inherited by Saxony
1485.11.11   = { 
	owner = MEI
	controller = MEI
	add_core = MEI
	remove_core = SAX
} #Treaty of Leipzig
1500.1.1 = { road_network = yes }
1520.12.10 = {
	religion = protestant
}
1530.1.4  = {
	bailiff = yes	
}
1547.5.19   = { 
	owner = SAX
	controller = SAX 
	add_core = SAX
	remove_core = MEI
} #Treaty of Wittenberg
1560.1.1  = { fort_15th = yes }
1790.8.1  = { unrest = 5 } # Peasant revolt
1791.1.1  = { unrest = 0 }
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
1815.6.9  = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = SAX
} #Ceded to Prussia