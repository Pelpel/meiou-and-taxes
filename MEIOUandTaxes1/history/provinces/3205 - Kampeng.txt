# No previous file for Kampeng

culture = sutiya
religion = hinduism
capital = "Tawang"
trade_goods = tea
hre = no
base_manpower = 1
native_size = 30
native_ferocity = 2
native_hostileness = 9
discovered_by = indian
discovered_by = chinese
discovered_by = muslim
discovered_by = steppestech

1858.1.1   = {
	owner = ASS
	controller = ASS
	is_city = yes
}
