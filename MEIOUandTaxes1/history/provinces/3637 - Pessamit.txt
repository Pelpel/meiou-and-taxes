# No previous file for Stadacona

culture = huron
religion = totemism
capital = "Stadacona"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 10
native_ferocity = 1
native_hostileness = 4
discovered_by = north_american

1000.1.1 = {
	add_permanent_province_modifier = {
		name = center_of_trade_modifier
		duration = -1
	}
	add_permanent_province_modifier = {
		name = saint_laurent_estuary_modifier
		duration = -1
	}
}
1534.1.1  = {
	discovered_by = FRA
} # Jacques Cartier
1608.7.3  = {
	owner = FRA
	controller = FRA		
	citysize = 200
	capital = "Qu�bec"
	religion = catholic
	culture = francien
	
	trade_goods = fur
	base_tax = 3
base_production = 3
#	base_manpower = 1.5
	base_manpower = 3.0
	set_province_flag = trade_good_set
	} 
1650.1.1   = {
	add_core = FRA
	citysize = 1000
	fort_14th = yes
}
1700.1.1   = {
	citysize = 3000
}
1750.1.1   = {
	add_core = QUE
	culture = canadian
	
	citysize = 10000
}
1759.9.12  = {
	controller = GBR
}
1763.2.10  = {
	owner = GBR
	add_core = GBR
	remove_core = FRA
}
1800.1.1   = {
	citysize = 25000
}
1819.1.1 = {  }
