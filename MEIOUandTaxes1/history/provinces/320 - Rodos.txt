# 320 - Rodos

owner = KNI 		# Knights of St. John Hospitaler of Jerusalem
controller = KNI
culture = greek
religion = orthodox
capital = "Rodos"
trade_goods = fish
hre = no
base_tax = 6
base_production = 6
base_manpower = 3
is_city = yes
discovered_by = CIR
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1000.1.1   = {
	add_permanent_province_modifier = { 
		name = "mediterranean_harbour" 
		duration = -1 
		}
	add_permanent_province_modifier = {
		name = medieval_castle
		duration = -1
	}
}
1088.1.1 = { dock = yes }

1356.1.1 = {
	add_core = KNI
	add_permanent_claim = BYZ
}
1400.1.1 = { remove_core = BYZ add_permanent_claim = BYZ }
1444.1.1 = {
	add_core = TUR
	remove_claim = BYZ
	fort_14th = yes
}
1522.3.20 = { dock = no naval_arsenal = yes }
1522.12.21 = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = KNI
} # Part of the Ottoman Empire
1580.1.1   = { fort_14th = yes  }
1720.1.1   = {  }
