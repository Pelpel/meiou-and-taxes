# 3936 - Ninh Binh

owner = DAI
controller = DAI
add_core = DAI
culture = vietnamese
religion = mahayana
capital = "Ninh Binh"
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
trade_goods = tea
discovered_by = DAI
discovered_by = chinese
discovered_by = indian
discovered_by = muslim
hre = no

1250.1.1 = { temple = yes }
1372.1.1 = { controller = CHA }
1390.1.1 = { controller = DAI }
1407.6.17 = { owner = MNG controller = MNG }
1425.1.1 = { religion = confucianism }
1427.1.1 = { owner = DAI controller = DAI }
1515.1.1 = { regimental_camp = yes }
1540.1.1 = { owner = ANN controller = ANN add_core = ANN } #Nguyen Kim dies
1545.1.1 = { owner = TOK controller = TOK add_core = TOK remove_core = ANN } # The kingdom is divided between the Nguyens & the Trinh line
1730.1.1 = { unrest = 5 }							# Peasant revolt
1731.1.1 = { unrest = 0 }
1769.1.1 = { unrest = 6 } # Tay Son Rebellion
1776.1.1 = {
	unrest = 0
	owner = DAI
	controller = DAI
	add_core = DAI
} # Tay Son Dynasty conquered the Nguyen Lords
1786.1.1 = { unrest = 5 } # Unsuccessful revolt
1787.1.1 = { unrest = 0 }
1802.7.22 = {
	owner = ANN
	controller = ANN
	remove_core = DAI
} # Nguyen Phuc Anh conquered the Tay Son Dynasty
1883.8.25 = { 
	owner = FRA
	controller = FRA
}
