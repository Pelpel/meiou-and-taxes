# 1253 - J�lich

owner = JUL
controller = JUL
culture = ripuarianfranconian
religion = catholic
hre = yes
base_tax = 5
base_production = 5
trade_goods = linen
base_manpower = 3
is_city = yes

capital = "J�lich"
discovered_by = eastern
discovered_by = western
discovered_by = muslim


1300.1.1 = { road_network = yes }
1356.1.1  = {
	add_core = JUL
}
#1500.1.1 = { road_network = yes }
1521.3.15 = {
	owner = JBC
	controller = JBC
	add_core = JBC
	remove_core = JUL
}
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1600.1.1  = {
	fort_14th = yes
}
1609.1.1    = {
	owner = PAL
	controller = PAL
	add_core = PAL
}
1742.1.1    = {
	owner = BAV
	controller = BAV
	add_core = BAV
	remove_core = PAL
}
1794.1.1  = {
	controller = FRA
	add_core = FRA
	remove_core = BAV
} # Controlled by France
1796.8.7  = {
	owner = FRA
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1815.6.9  = {
	owner = PRU
	controller = PRU
	add_core = PRU
   	remove_core = FRA
} # Congress of Vienna
