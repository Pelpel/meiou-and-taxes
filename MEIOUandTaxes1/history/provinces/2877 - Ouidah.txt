# 2877 - Ouidah

culture = fon
religion = west_african_pagan_reformed
capital = "Gl?xw�"
trade_goods = unknown # palm
hre = no
base_tax = 1
native_size = 80
native_ferocity = 4.5
native_hostileness = 9
discovered_by = soudantech
discovered_by = sub_saharan

1356.1.1 = {
    add_permanent_province_modifier = {
        name = trading_post_province
        duration = -1
    }
}
1471.1.1   = { discovered_by = POR } 
1580.1.1   = {
	owner = POR
	controller = POR
	add_core = POR
	citysize = 200
	capital = "Ajuda"
	trade_goods = slaves
}
