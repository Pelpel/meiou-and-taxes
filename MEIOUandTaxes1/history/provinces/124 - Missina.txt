# 124 - Missina

owner = SIC
controller = SIC
culture = sicilian 
religion = catholic 
hre = no 
base_tax = 11
base_production = 11
trade_goods = silk
base_manpower = 5
is_city = yes
fort_14th = yes 
capital = "Missina"
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech


1000.1.1   = {
	add_permanent_province_modifier = { 
		name = "mediterranean_harbour" 
		duration = -1 
		}
}
1088.1.1 = { dock = yes }
1300.1.1 = { road_network = yes }
1356.1.1  = {
	add_core = ARA
	add_core = SIC
	add_core = KNP
}
1409.1.1  = { owner = ARA controller = ARA }
1516.1.23  = {
	controller = SPA
	owner = SPA
	add_core = SPA
	remove_core = ARA
	road_network = no paved_road_network = yes 
	bailiff = yes
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille
1522.3.20 = { dock = no naval_arsenal = yes }
1530.1.1 = {
	owner = KNP
	controller = KNP
	remove_core = SPA
}
1531.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
}
1706.7.1  = { controller = SAV }
1713.4.11 = { owner = SIC controller = SIC remove_core = SPA }
1718.8.2  = {
	owner = HAB
	controller = HAB
	add_core = HAB
}
1734.6.2  = {
	owner = NAP
	controller = NAP
	add_core = NAP
	remove_core = HAB
}
#1815.5.3  = {
#	owner = SIC
#	controller = SIC
#	remove_core = NAP
#}
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
}
