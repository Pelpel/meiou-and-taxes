# 733 - Haeseo
# FL - Korea Universalis
# LS - Alpha 5

owner = KOR
controller = KOR
add_core = KOR
culture = korean
religion = mahayana #FB-ASSA become confucianism in 1392
capital = "Haeju"
base_tax = 7
base_production = 7
base_manpower = 3
is_city = yes
trade_goods = rice


discovered_by = chinese
discovered_by = steppestech
hre = no

1133.1.1 = { mill = yes }
1356.1.1 = {
}
1392.6.5  = {
	religion = confucianism
	owner = JOS
	controller = JOS
	add_core = JOS
	remove_core = KOR
}
1529.3.17 = { 
	dock = yes
	marketplace = yes
	public_baths = yes
	customs_house = yes
	road_network = yes
}
1593.1.1 = {
	unrest = 5
} # Japanese invasion
1593.5.1 = {
	controller = JOS
	unrest = 0
} # Japanese invasion ends
1637.1.1 = {
	add_core = MNG
} # Tributary of Qing China
1644.1.1 = {
	add_core = QNG
	remove_core = MNG
} # Part of the Manchu empire
1653.1.1 = {
	discovered_by = NED
} # Hendrick Hamel
