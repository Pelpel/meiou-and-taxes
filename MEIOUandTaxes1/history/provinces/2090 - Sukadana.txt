#Province: Sukadana
#file name: 2090 - Sukadana

owner = SKD
controller = SKD
culture = dayak
religion = vajrayana
capital = "Sukadana"
trade_goods = rice # gold		#FB this area was a major source of gold
hre = no
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
discovered_by = chinese
discovered_by = indian
discovered_by = austranesian

1356.1.1 = {
	add_core = SKD
	add_core = BKS
	add_core = PTN
}
1521.1.1 = { discovered_by = POR }
1550.1.1 = { religion = sunni trade_goods = gold }
1712.1.1 = {
	owner = NED
	controller = NED
	unrest = 3
}
1737.1.1 = { add_core = NED }
1811.9.1 = {
	owner = GBR
	controller = GBR
} # British take over
1816.1.1 = { owner = NED controller = NED } # Returned to the Dutch
