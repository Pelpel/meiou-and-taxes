# 2380 - Tulcea

owner = DOB
controller = DOB
add_core = DOB
culture = vlach
religion = orthodox
capital = "Tulcea"
base_tax = 6
base_production = 6
base_manpower = 3
is_city = yes
trade_goods = fish
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech
hre = no

1000.1.1 = {
	add_permanent_province_modifier = {
		name = danube_estuary_modifier
		duration = -1
	}
}
1356.1.1 = { 
	add_permanent_claim = WAL
}
1388.1.1  = {
	owner = WAL
	controller = WAL
	add_core = WAL
	add_core = BUL
	remove_core = DOB
}
1419.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = WAL
	add_permanent_claim = WAL
}
1481.6.1  = { controller = REB } # Civil war, Bayezid & Jem
1482.7.26 = { controller = TUR } # Jem escapes to Rhodes
1519.1.1 = { bailiff = yes }
1550.1.1  = { fort_14th = yes }
1555.1.1  = { unrest = 5 } # General discontent with the Janissaries' dominance
1556.1.1  = { unrest = 0  }
1688.1.1  = { unrest = 6 } # Rebellion against Ottoman rule, centered on Chiprovtzi
1689.1.1  = { unrest = 0 } # Brutally suppressed by Janissaries
1793.1.1  = { unrest = 5 } # Pasvanoglu  Rebellion, centered at Vidin
1798.1.1  = { unrest = 0 }
