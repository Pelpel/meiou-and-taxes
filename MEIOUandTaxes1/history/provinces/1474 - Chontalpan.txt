# 1474 - Chontalpan

# owner = MAY
# controller = MAY
# add_core = MAY
culture = cholan
religion = mesoamerican_religion
capital = "Comalcalco" 

base_tax = 2
base_production = 2
#base_manpower = 1.0
base_manpower = 2.0
# citysize = 5000
trade_goods = fish 

# 
discovered_by = mesoamerican

hre = no
1517.1.1   = {
	discovered_by = SPA
}
1522.1.1   = {
	owner = SPA
	controller = SPA
	add_core = SPA
	naval_arsenal = yes
	marketplace = yes
	bailiff = yes
	
	
}
1546.1.1   = {
	add_core = SPA
	capital = "Villahermosa" 
	citysize = 500
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
	citysize = 2000
}
1810.9.16  = {
	owner = MEX
} # Declaration of Independence
1821.8.24  = {
	controller = MEX
	remove_core = SPA
} # Treaty of Cordba
1832.10.1  = {
	unrest = 3
} # Convention of 1832
1833.4.1   = {
	unrest = 6
} # Convention of 1833
