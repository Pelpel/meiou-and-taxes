# 3467 - Tamoio

culture = ge
religion = pantheism
capital = "Tamoio"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 35
native_ferocity = 1
native_hostileness = 2

1500.5.1   = {
	discovered_by = POR
	add_permanent_claim = POR
} # Pedro �lvares Cabral 
1551.9.8   = {
	owner = POR
	controller = POR
	change_province_name = "Esp�rito Santo"
	rename_capital = "Vila Nova"
	citysize = 260
	culture = portugese
	religion = catholic
	trade_goods = sugar
	base_tax = 3
base_production = 3
}
1600.1.1   = {
	citysize = 1000
	rename_capital = "Vict�ria"
}
1750.1.1   = {
	add_core = BRZ
	culture = brazilian
}
1822.9.7   = {
	owner = BRZ
	controller = BRZ
}
1825.1.1   = {
	remove_core = POR
}
