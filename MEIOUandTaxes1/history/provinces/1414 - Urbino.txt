# 1414 - Urbino

owner = URB
controller = URB
culture = romagnol
religion = catholic 
hre = no 
base_tax = 6
base_production = 6        
trade_goods = wool      
base_manpower = 3
is_city = yes
fort_14th = yes 
capital = "Urb�n"
medieval_university = yes #University of Camerino
add_core = URB
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1250.1.1 = { temple = yes }
1300.1.1 = { road_network = yes }
1356.1.1   = {
	add_permanent_province_modifier = {
		name = "republic_of_san_marino"
		duration = -1
	}
}
1369.1.1   = {
	controller = PAP
	owner = PAP
	add_core = PAP
}
1375.1.1   = {
	controller = URB
	owner = URB
}
1502.12.8  = {
	owner = PAP
	controller = PAP
}
1503.8.28  = {
	owner = URB
	controller = URB
}
1523.1.1 = {
	rename_capital = "P�s're"
	base_tax = 8
base_production = 8
	base_manpower = 4
	road_network = no paved_road_network = yes 
	bailiff = yes
}
1626.1.1   = {
	controller = PAP
	owner = PAP
	remove_core = URB
} # Annexed to the Holy See
1700.1.1   = {
	
}
1805.3.17 = {
	owner = ITE
	controller = ITE
	add_core = ITE
} # Treaty of Pressburg
1814.4.11   = {
	owner = PAP
	controller = PAP
	remove_core = ITE
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
}
