# 163 - Crete

owner = VEN
controller = VEN
culture = greek
religion = orthodox
capital = "Rethymno"
trade_goods = wheat
hre = no
base_tax = 4
base_production = 4
base_manpower = 5
is_city = yes
discovered_by = CIR
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
add_permanent_claim = BYZ

1088.1.1 = { dock = yes }
1300.1.1 = { }
1356.1.1 = {
	add_core = VEN
	add_core = CRT
	set_province_flag = greek_name
}
1444.1.1 = {
	add_core = CRT
	road_network = yes
}
1470.1.1  = { controller = REB } # Cretan rebellion against Venetian rule
1471.1.1  = { controller = VEN } # Estimated
1530.1.3 = {
	road_network = no paved_road_network = yes 
	bailiff = yes
}

1555.1.1  = { unrest = 5 } # General discontent with the Janissaries' dominance
1556.1.1  = { unrest = 0 }
1570.1.1  = { revolt = { type = nationalist_rebels size = 2 } controller = REB } # Cretan rebellion against Venetian rule

1580.1.1  = { fort_14th = yes } # Rethymnon
1629.1.1  = {  } # Fort Firkas
1646.1.1  = {
	revolt = { }
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = VEN
	change_province_name = "Girit"
	rename_capital = "Resmo"
}
1690.1.1  = { base_tax = 3
base_production = 3 } #The Decentralizing Effect of the Provincial System
1692.1.1  = { controller = REB } # Insurrection
1693.1.1  = { controller = TUR } # Estimated
1750.1.1  = { add_core = GRE }
1770.3.9  = { controller = REB } # Revolutionary government
1770.6.1  = { controller = TUR }
