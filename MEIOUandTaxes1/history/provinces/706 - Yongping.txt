# 706 - Zhili Haigang

owner = YUA
controller = YUA
culture = hanyu
religion = confucianism
capital = "Lulong"
trade_goods = wheat
hre = no
base_tax = 6
base_production = 6
#base_manpower = 2.0
base_manpower = 4.0
citysize = 50000
discovered_by = chinese
discovered_by = steppestech

1000.1.1 = {
	add_permanent_province_modifier = { 
		name = great_wall_ruins
		duration = -1
	}
}
1200.1.1 = { paved_road_network = yes }
1271.12.18  = {#Proclamation of Yuan dynasty
	add_core = YUA
}
#1356.1.1 = {	remove_core = YUA } # Red Turbans
1368.1.1  = {
	owner = MNG
	controller = MNG
	add_core = MNG
}
1529.3.17 = { 
	dock = yes
	marketplace = yes
	bailiff = yes
	courthouse = yes
}
1530.1.1 = { fort_14th = no fort_15th = yes }
1551.1.1 = {
	remove_province_modifier = great_wall_ruins
	add_permanent_province_modifier = {
		name = great_wall_full
		duration = -1
	}
}
1640.1.1 = {
	controller = MCH
}
1644.4.29 = {
	controller = MCH
}
1644.6.6 = {
	owner = QNG
	controller = QNG
	add_core = QNG
#	remove_core = MNG
} # The Qing Dynasty
1662.1.1 = {
	remove_core = MNG
}
