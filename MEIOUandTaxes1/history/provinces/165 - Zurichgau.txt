#165 - Zurichgau
# Zurich

owner = SWI
controller = SWI 
add_core = SWI 
culture = high_alemanisch
religion = catholic
capital = "Z�rich"
base_tax = 6
base_production = 6
base_manpower = 5
is_city = yes
trade_goods = livestock
hre = yes
fort_14th = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim

1000.1.1 = {
	add_permanent_province_modifier = {
		name = minor_inland_center_of_trade_modifier
		duration = -1
	}
}
1100.1.1 = { marketplace = yes }
1200.1.1 = { road_network = yes }
1250.1.1 = { temple = yes }
1356.1.1 = {
}
#1500.1.1 = { road_network = yes }
1513.7.3 = { unrest = 7 } # Luzern Peasant War
1515.1.1 = { unrest = 0 }
1515.2.1 = { training_fields = yes }
1524.1.1 = { religion = reformed } #reformation in switzerland
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}

1648.10.24 = {
	hre = no
} # Treaty of Westphalia, ending the Thirty Years' War

1653.1.1 = { unrest = 5 } # Peasant rebellion against overtaxation
1654.1.1 = { unrest = 0 }
1656.1.1 = { unrest = 7 } # Protestants are expelled from Schwyz which lead to the First War of Villmergen
1657.1.1 = { unrest = 2 } # An agreement is concluded at Villmergren but tensions remain
1705.1.1 = {  }
1798.3.5  = { controller = FRA } # French occupation
1798.4.12 = { controller = SWI } # The establishment of the Helvetic Republic
1798.4.15 = { controller = REB } # The Nidwalden Revolt
1798.9.1  = { controller = SWI } # The revolt is supressed
1802.6.1  = { controller = REB } # Swiss rebellion
1802.9.18 = { controller = SWI }
