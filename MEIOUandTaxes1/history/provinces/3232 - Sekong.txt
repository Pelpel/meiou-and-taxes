# No previous file for Sekong

owner = LXA
controller = LXA
add_core = LXA
culture = laotian
religion = buddhism
capital = "Sekong"
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
trade_goods = rice
discovered_by = chinese
discovered_by = indian
hre = no

1707.1.1 = {
	owner = VIE
	controller = VIE
	add_core = VIE
	remove_core = LXA
	fort_14th = yes
} # Declared independent, Lan Xang was partitioned into 4 kingdoms; Vientiane, Champasak & Luang Prabang + Muang Phuan
1713.1.1 = {
	owner = CHK
	controller = CHK
	add_core = CHK
	remove_core = VIE
}
1811.1.1 = { unrest = 5 } # The Siamese-Cambodian Rebellion
1812.1.1 = { unrest = 0 }
1829.1.1 = {
	owner = SIA
	controller = SIA
    	add_core = SIA
	unrest = 0
} # Annexed by Siam
1893.1.1 = {
	owner = FRA
	controller = FRA
    	unrest = 0
}
