# 2497 - Qars Farafra

culture = bedouin
religion = sunni
capital = "Farafra"
trade_goods = wool
hre = no
base_manpower = 1
native_size = 30
native_ferocity = 4.5
native_hostileness = 9
discovered_by = muslim
discovered_by = soudantech
discovered_by = turkishtech
discovered_by = sub_saharan

1000.1.1 = {
	add_permanent_province_modifier = {
		name = oasis_route
		duration = -1
	}
}
1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1820.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
	is_city = yes
}
