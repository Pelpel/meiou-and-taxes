# 3945 - Dewa-Ugo

owner = ANO
controller = ANO
culture = tohoku
religion = mahayana
capital = "Kubota"
trade_goods = fish
hre = no
base_tax = 7
base_production = 7
base_manpower = 7
is_city = yes
discovered_by = chinese

1356.1.1 = {
	add_core = ANO
}
1367.1.1 = {
	remove_core = DTE
}
1542.1.1   = { discovered_by = POR }
1630.1.1   = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
