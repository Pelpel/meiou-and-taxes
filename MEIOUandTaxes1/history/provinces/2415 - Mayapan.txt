# 2415 - Mayapan

owner = KKM
controller = KKM
add_core = KKM
culture = yucatecan
religion = mesoamerican_religion
capital = "M�ayap�an"

base_tax = 9
base_production = 9
#base_manpower = 3.0
base_manpower = 6.0
citysize = 25000
trade_goods = cacao 




discovered_by = mesoamerican

hre = no

1517.1.1   = {
	discovered_by = SPA
}
1546.1.1   = {
	owner = SPA
	controller = SPA
	capital = "Mayap�n"
	citysize = 5000
	base_tax = 4
base_production = 4
#	base_manpower = 1.0
	base_manpower = 2.0
} #Pedro de Alvanado
1571.1.1   = {
	add_core = SPA
}
1596.1.1   = {
	religion = catholic
}
1750.1.1   = {
	add_core = MEX
	citysize = 2000
}
1810.9.16  = {
	owner = MEX
} # Declaration of Independence
1821.8.24  = {
	controller = MEX
	remove_core = SPA
} # Treaty of Cordba
1832.10.1  = {
	unrest = 3
} # Convention of 1832
1833.4.1   = {
	unrest = 6
} # Convention of 1833
