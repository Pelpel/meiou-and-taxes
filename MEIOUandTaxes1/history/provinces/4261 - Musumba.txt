# No previous file for Musumba

owner = LND
controller = LND
add_core = LND
culture = lunda
religion = animism
capital = "Musumba"
trade_goods = copper
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
hre = no
discovered_by = central_african
