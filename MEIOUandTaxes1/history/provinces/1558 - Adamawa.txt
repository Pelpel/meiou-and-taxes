#1558 - Adamawa

owner = KUB
controller = KUB
add_core = KUB
culture = kuba
religion = animism
capital = "Adamawa"
trade_goods = copper
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
hre = no
discovered_by = central_african

1000.1.1 = {
	add_permanent_province_modifier = {
		name = oasis_route
		duration = -1
	}
}
