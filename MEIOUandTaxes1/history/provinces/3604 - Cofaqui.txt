# No previous file for Cofaqui

owner = CHE
controller = CHE
add_core = CHE
is_city = yes
culture = cherokee
religion = totemism
capital = "cofaqui"
trade_goods = tobacco
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 25
native_ferocity = 1 
native_hostileness = 6

1785.11.2 = {
	owner = USA
	controller = USA
	add_core = USA
	culture = american
	religion = protestant
} #Treaty of Hopewell (with the Cherokee), define a new border
