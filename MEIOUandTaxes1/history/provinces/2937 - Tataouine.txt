# 2937 - Tataouine

owner = GHD
controller = GHD  
culture = berber
religion = sunni
capital = "Tataouine"
trade_goods = wool
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes
discovered_by = muslim
discovered_by = soudantech
discovered_by = sub_saharan

1356.1.1 = {
	add_core = GHD
}
1830.1.1 = {
	owner = TUN
	controller = TUN
	add_core = TUN
}
