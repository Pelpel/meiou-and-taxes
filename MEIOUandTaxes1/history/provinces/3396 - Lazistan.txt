# No previous file for Lazistan

owner = TRE
controller = TRE
culture = pontic
religion = orthodox
capital = "Rizaion"
trade_goods = wheat
hre = no
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech
add_permanent_claim = BYZ

1356.1.1  = {
	add_core = TRE
	set_province_flag = greek_name
}
1390.1.1  = {
	rename_capital = "Rize" 
}
#1444.1.1 = {
#	add_permanent_claim = TUR
#	add_core = SMT
#}
1470.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	#remove_core = GEO
}
1481.6.1  = { controller = REB } # Civil war, Bayezid & Jem
1509.1.1  = { controller = REB } # Civil war
1513.1.1  = { controller = TUR }
#1530.1.1 = { add_permanent_claim = TUR } #Ruled by local Turkish Beys
1530.1.4  = {
	bailiff = yes
}
1623.1.1  = { controller = REB } # The empire fell into anarchy, Janissaries stormed the palace
1625.1.1  = { controller = TUR } # Murad tries to quell the corruption
1658.1.1  = { controller = REB } # Revolt of Abaza Hasan Pasha, centered on Aleppo, extending into Anatolia
1659.1.1  = { controller = TUR }
1740.1.1  = {  }
