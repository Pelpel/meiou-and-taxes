# 2878 - Elmina

culture = akaa
religion = west_african_pagan_reformed
capital = "Anomansah"
trade_goods = unknown
hre = no
base_tax = 1
native_size = 80
native_ferocity = 4.5
native_hostileness = 9
discovered_by = soudantech
discovered_by = sub_saharan

1356.1.1 = {
    add_permanent_province_modifier = {
        name = trading_post_province
        duration = -1
    }
}
1456.1.1   = { discovered_by = POR } #Cadamosto
1482.1.1   = {
	owner = POR
	controller = POR
	add_core = POR
	citysize = 200
	trade_goods = gold
	capital = "Elmina"
	naval_arsenal = yes
	marketplace = yes 
	fort_14th = yes
	set_province_flag = TP_trading_post
	trading_post = yes
} # Sao Jorge da Mina Castle, built by Diogo de Azambuja
1637.1.1   = {
	owner = NED
	controller = NED
	add_core = NED
	trade_goods = slaves
} # Captured by the Dutch West India Company, shift toards slave trade
1807.1.1   = {
	trade_goods = fish
} # Slave trade abolished
1872.1.1   = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = NED
} # Sold to the British
