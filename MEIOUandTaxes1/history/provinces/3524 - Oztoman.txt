# 3524 - Oztoman

owner = TPQ
controller = TPQ
add_core = TPQ
culture = chontal
religion = nahuatl
capital = "Oztoman" 

base_tax = 3
base_production = 3
#base_manpower = 1.0
base_manpower = 2.0
citysize = 5000
trade_goods = maize


hre = no

discovered_by = mesoamerican

1428.1.1  = {
	owner = AZT
	controller = AZT
	add_core = AZT
	remove_core = TPQ
	road_network = yes
}

1522.1.1 = {
	discovered_by = SPA

} # Spanish keep the P'uh�pecha capital
1529.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	marketplace = yes
	bailiff = yes
	courthouse = yes
} # Spanish keep the P'uh�pecha capital
1540.1.1 = {
	culture = castillian
	religion = catholic
} # Capital moved to another indian town, because of turmoil
1547.1.1   = {
	add_core = SPA
	citysize = 1000
}
1580.1.1   = {
	# capital = "Valladolid"
} # Capital moved to a newly founded spanish city
1600.1.1   = {
	citysize = 2000
}
1650.1.1   = {
	citysize = 5000
}
1700.1.1   = {
	citysize = 10000
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
	citysize = 20000
}
1800.1.1   = {
	citysize = 50000
}
1810.9.16  = {
	owner = MEX
	controller = MEX
} # Declaration of Independence
1821.8.24  = {
	remove_core = SPA
} # Treaty of Cord�ba
