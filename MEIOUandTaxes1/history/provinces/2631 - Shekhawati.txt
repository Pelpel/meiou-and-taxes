# 2631 - Shekhawati

owner = JAU
controller = JAU
culture = jati
religion = hinduism
capital = "Jaipur"
trade_goods = copper
hre = no
base_tax = 4
base_production = 4
#base_manpower = 1.0
base_manpower = 2.0
citysize = 5000
add_local_autonomy = 25

discovered_by = indian
discovered_by = muslim
discovered_by = steppestech 
discovered_by = turkishtech

1356.1.1  = {
	add_core = JAU
}
1450.1.1  = { citysize = 7000 }
1500.1.1  = { citysize = 12000 }
1515.12.17 = { training_fields = yes }
1530.1.1  = {
	owner = MUG
	controller = MUG
	add_core = MUG
	add_core = TRT
} # Mughal Conquest
1530.3.17 = {
	bailiff = yes
	marketplace = yes
	road_network = yes
}
1540.1.1  = {
	owner = JAU
	controller = JAU
} # Independent again after Mughals lose Delhi
1544.1.1  = {
	owner = BNG
	controller = BNG
} # Sur expansion
1545.5.22 = {
	owner = JAU
	controller = JAU
} # Independent again after death of Shere Shah Sur
1550.1.1  = { citysize = 15000 }
1600.1.1  = { citysize = 16000 }
1650.1.1  = { citysize = 18000 }
1690.1.1  = { discovered_by = ENG }
1700.1.1  = { citysize = 20000 }
1707.5.12 = { discovered_by = GBR }
1750.1.1  = { citysize = 22000 }
1800.1.1  = { citysize = 26000 }
	   
