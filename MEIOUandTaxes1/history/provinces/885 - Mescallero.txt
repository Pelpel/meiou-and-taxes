# No previous file for Mescallero

owner = APA
controller = APA
add_core = APA
is_city = yes
culture = apache
religion = totemism
capital = "Mescallero"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 5
native_ferocity = 4
native_hostileness = 8
