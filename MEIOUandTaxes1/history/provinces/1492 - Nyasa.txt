#1492 - Nyassa

owner = MKA
controller = MKA
culture = makua
religion = animism
capital = "Nyassa"
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
hre = no
trade_goods = ivory
discovered_by = central_african
discovered_by = east_african

1356.1.1 = {
	add_core = MKA
}
