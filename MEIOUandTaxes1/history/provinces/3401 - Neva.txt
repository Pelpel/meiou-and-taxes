# No previous file for Neva

owner = NOV
controller = NOV
add_core = NOV
culture = ingrian
religion = orthodox
hre = no
base_tax = 2
base_production = 2
trade_goods = hemp
base_manpower = 1
is_city = yes
capital = "Nyen"
discovered_by = western
discovered_by = eastern
discovered_by = steppestech
 
1000.1.1   = {
	add_permanent_province_modifier = {
		name = neva_estuary_modifier
		duration = -1
	}
	add_permanent_province_modifier = { 
		name = "natural_harbour" 
		duration = -1 
		}
}
1478.1.14  = {
	owner = MOS
	controller = MOS
	add_core = MOS
	remove_core = NOV
} #Muscovite annectation of Novgorod
1530.1.4  = {
	bailiff = yes	
}

1547.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia
1583.8.10  = {
	owner = SWE
	controller = SWE
	add_core = SWE
	remove_core = RUS
} #The Armistice of Pliusa
1590.2.23  = {
	owner = RUS
     	controller = RUS
     	add_core = RUS
     	remove_core = SWE
} #The Armistice of Narva
1612.12.4  = { controller = SWE } #The Ingermanian War-Captured by Evert Horn
1617.2.17  = {
	owner = SWE
	controller = SWE
	add_core = SWE
	remove_core = RUS
	} #The Peace of Stolbova
1617.2.17  = { religion = protestant }
1704.8.16  = {
	controller = RUS
	capital = "Saint Petersburg"
	culture = russian
	fort_18th = yes
 } #The Great Nordic War-Captured all fortifications
1721.8.30  = {
	owner = RUS
	add_core = RUS
	remove_core = SWE
	
} #The Peace of Nystad
1733.1.1   = {
	
}
