# No previous file for Czerwonogrod

owner = GVO
controller = GVO
capital= "Czerwonogród"
culture = ruthenian
religion = orthodox
trade_goods = wheat
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

#1133.1.1 = { mill = yes }
1349.1.1 = {
	owner = POL
	controller = POL
	add_permanent_province_modifier = {
		name = polish_estates
		duration = -1
	}
}
1356.1.1 = {
	add_core = GVO
	add_core = POL
	add_claim = LIT
}
1366.9.1 = {
	owner = GVO
	controller = GVO
}
1370.11.5 = {
	owner = LIT
	controller = LIT
}
1377.1.1 = {
	owner = GVO
	controller = GVO
}
1430.10.27 = {
	owner = POL
	controller = POL
} # After the death of the Vytautas incorporated into Podolian Voivodeship of the Polish Crown

1490.1.1   = { unrest = 6 } # Uprising led by Mukha
1492.1.1   = { unrest = 0 }
1523.8.16 = { mill = yes }
1530.1.4  = {
	bailiff = yes	
	farm_estate = yes
}
1569.7.1  = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1588.1.1   = { controller = REB } # Civil war
1589.1.1   = { controller = PLC } # Coronation of Sigismund III

1596.1.1   = { unrest  = 4 } # Religious struggles, union of Brest
1597.1.1   = { unrest = 0 }
1733.1.1   = { controller = REB } # The war of Polish succession
1735.1.1   = { controller = PLC }
 # St. George temple in Lwow
1772.8.5   = {
	owner = HAB
	controller = HAB
	add_core = HAB
	add_core = POL
	remove_core = PLC
} # First partition of Poland
1781.1.1   = { unrest = 4 } # Struggle against serfdom
1794.3.24  = { unrest = 8 } # Kosciuszko uprising
1794.11.16 = { unrest = 0 } # The end of the uprising
