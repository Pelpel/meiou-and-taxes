
owner = OTT
controller = OTT
culture = turkish
religion = sunni
capital = "Bolu"
trade_goods = leather
hre = no
base_tax = 6
base_production = 6
base_manpower = 4
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech
add_permanent_claim = BYZ

1356.1.1 = {
	add_core = OTT
	add_core = CND
	set_province_flag = turkish_name
}
1359.1.1 = {					#AdL: revival of romaion
	add_core = BYZ
}
1359.1.2   = {
	remove_core = BYZ				#AdL: revival of romaion
}
1390.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = OTT
}
1481.6.1  = { controller = REB } # Civil war, Bayezid & Jem
1482.7.26 = { controller = TUR } # Jem escapes to Rhodes
1509.1.1  = { controller = REB } # Civil war

1513.1.1  = { controller = TUR }
1515.1.1 = { training_fields = yes }
 # Reign of Suleyman the magnificent, organizes the state
1519.1.1 = { bailiff = yes }
1621.1.1  = { unrest = 6 } # Osman II's reforms against the Janissaries
1622.5.20 = { unrest = 7 } # Osman II is murdered
1622.6.1  = { controller = TUR unrest = 0 } # Mustafa I, estimated
1623.1.1  = { controller = REB } # The empire fell into anarchy, Janissaries stormed the palace
1625.1.1  = { controller = TUR } # Murad tries to quell the corruption

1700.1.1  = {  }
1718.1.1  = { unrest = 3 } # Lale Devri (the tulip age), not appreciated by everyone  
1720.1.1  = { unrest = 0 }
1795.1.1  = { unrest = 6 } # Reforms by Sultan Selim III, tried to replace the Janissary corps
1807.5.30 = { controller = REB } # Janissary revolt
1808.6.29 = { controller = TUR } # Mahmud II takes the throne
