# 279 - Zyrian

culture = pomor
religion = tengri_pagan_reformed
capital = "Verkhnyaya Toyma"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 50
native_ferocity = 2
native_hostileness = 5
discovered_by = NOR
discovered_by = SWE
discovered_by = RSW
discovered_by = eastern
discovered_by = steppestech
discovered_by = muslim

1356.1.1 = {
	owner = NOV
	controller = NOV
	add_core = NOV
	citysize = 1100
	trade_goods = fur # subsistence
	set_province_flag = trade_good_set
}
1397.1.1  = {
	add_core = MOS
} # Dvina statutory charter
1472.1.14 = {
	owner = MOS
	controller = MOS
	add_core = MOS
	remove_core = NOV
	religion = orthodox
	culture = russian
	} # Stefan of Perm mission
1500.1.1  = { citysize = 1200 }
1530.1.4  = {
	bailiff = yes	
}
1547.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia
1550.1.1  = { citysize = 1500 }
 # Became a wealthy and important trade and transport center
1598.1.1  = { unrest = 5 } # "Time of troubles"
1600.1.1  = { citysize = 2000 }
1613.1.1  = { unrest = 0 } # Order returned, Romanov dynasty
1650.1.1  = { citysize = 2500 }
 # Dedicated to the Domition of Mary, rebuilt in the 17th century
1700.1.1  = { citysize = 3000 }
1750.1.1  = { citysize = 4000 }
1800.1.1  = { citysize = 5000 }
