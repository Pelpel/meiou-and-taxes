# 148 - Prilep

owner = MKO
controller = MKO
culture = bulgarian
religion = orthodox
capital = "Prilep"
trade_goods = wheat
hre = no
base_tax = 9
base_production = 9
base_manpower = 2
is_city = yes
discovered_by = CIR
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

#1133.1.1 = { mill = yes }
1356.1.1 = {
	add_core = MKO
	add_local_autonomy = 15
	add_permanent_claim = BYZ
	add_permanent_claim = SER
}
1371.2.17 = {
	owner = OTT
	controller = OTT
	add_core = OTT
	capital = "�sk�b"
	add_local_autonomy = -15
} # Battle of Kosovo
1390.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = OTT
}
1400.1.1 = { remove_core = BYZ add_permanent_claim = BYZ }
1444.1.1 = {
	remove_core = SER
}
1453.5.29 = {
	remove_claim = BYZ
}
1515.2.1 = { training_fields = yes }
1523.8.16 = { mill = yes }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1555.1.1  = { unrest = 5 } # General discontent with the Janissaries' dominance
1556.1.1  = { unrest = 0  }
1615.1.1  = {
	base_tax = 4
	base_production = 4
} # The Decentralizing Effect of the Provincial System
1623.1.1  = { controller = REB } # The empire fell into anarchy, Janissaries stormed the palace
1625.1.1  = { controller = TUR } # Murad tries to quell the corruption
1689.1.1  = { controller = REB } # Karposh uprising against Ottoman rule
1690.1.1  = { controller = TUR }
