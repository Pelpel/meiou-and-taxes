# Tsitsihar

owner = YUA
controller = YUA
culture = jurchen
religion = tengri_pagan_reformed
capital = "Tsitsihar"
trade_goods = wheat
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = chinese
discovered_by = steppestech



1356.1.1 = {
	add_core = YUA
}
1392.1.1 = {
	owner = KHR
	controller = KHR
	add_core = KHR
	remove_core = YUA
}
1420.1.1 = {
	owner = MHX
	controller = MHX
	add_core = MHX
	remove_core = KHR
}
1515.1.1 = { training_fields = yes }
1530.1.1 = { 
	marketplace = yes
	bailiff = yes
	mill = yes
}
1593.1.1 = {
	owner = MCH
	controller = MCH
	add_core = MCH
	remove_core = KHR
} # The Later Jin Khanate
1616.2.17  = {
	owner = JIN
	controller = JIN
	add_core = JIN
	remove_core = MCH
}
1635.1.1 = {
	culture = manchu
}
1636.5.15 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = JIN
} # The Qing Dynasty
