# No previous file for Hada

owner = YUA
controller = YUA
culture = jurchen
religion = tengri_pagan_reformed
capital = "Hada"
trade_goods = wheat
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = chinese
discovered_by = steppestech

1200.1.1 = { road_network = yes }
1356.1.1 = {
	add_core = YUA
	add_core = MXI
}
1392.1.1 = {
	owner = MXI
	controller = MXI
	remove_core = YUA
}
1450.1.1  = {
	owner = MHX
	controller = MHX
	add_core = MHX
	remove_core = MXI
} #Nara clan
#1543.1.1 = {
#	owner = MHD
#	controller = MHD
#	add_core = MHD
#	remove_core = MHX
#} # Nara splits into Hada and Ula
1515.1.1 = { training_fields = yes }
1530.1.1 = { 
	marketplace = yes
	bailiff = yes
	mill = yes
}
1601.1.1 = {
	owner = MCH
	controller = MCH
	add_core = MCH
} # Nurhaci's conquest
1616.2.17  = {
	owner = JIN
	controller = JIN
	remove_core = MCH
}
1635.1.1 = {
	culture = manchu
}
1636.5.15 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = MCH
} # The Qing Dynasty
1709.1.1 = { discovered_by = SPA }
