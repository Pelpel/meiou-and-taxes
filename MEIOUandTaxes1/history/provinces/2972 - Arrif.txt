# 2972 Arrif

owner = FEZ
controller = FEZ
culture = fassi
religion = sunni
capital = "Taounat"
base_tax = 8
base_production = 8
base_manpower = 3
is_city = yes
trade_goods = wool
discovered_by = western
discovered_by = muslim
discovered_by = turkishtech
hre = no

1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1250.1.1 = { temple = yes }
1356.1.1 = {
	add_core = FEZ
}
1530.1.1 = {
	add_core = MOR
}
1554.1.1 = {
	owner = MOR
	controller = MOR
	add_core = MOR
}
1603.1.1 = { unrest = 5 } # The death of the Saadita Ahmad I al-Mansur
1604.1.1 = { unrest = 0 }
1638.1.1 = {
	owner = FEZ
	controller = FEZ
	remove_core = MOR
}
1664.1.1 = {
	owner = TFL
	controller = TFL
	add_core = TFL
	remove_core = MOR
}
1668.8.2 = {
	owner = MOR
	controller = MOR
	remove_core = TFL
}
1672.1.1 = { unrest = 4 } # Oppositions against Ismail, & the idea of a unified state
1727.1.1 = { unrest = 0 }
