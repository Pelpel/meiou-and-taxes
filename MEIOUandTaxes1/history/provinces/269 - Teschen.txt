# 269 - Teschen

owner = TES
controller = TES
culture = silesian
religion = catholic
capital = "Cieszyn"
base_tax = 5
base_production = 5
base_manpower = 5
is_city = yes
trade_goods = hemp  
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
hre = yes

1356.1.1  = {
	add_core = BOH
	add_core = TES
	add_permanent_province_modifier = {
		name = bohemian_estates
		duration = -1
	}
}
1419.8.16 = {
#	owner = HUN
#	controller = HUN
	add_core = HUN
}
1437.12.9 = {
#	owner = BOH
#	controller = BOH
	remove_core = HUN
}
1457.1.1  = {
	unrest = 5
	
} # George of Podiebrand had to secure recognition from the German and Catholic towns
1464.1.1  = {
	unrest = 1
} # The Catholic nobility still undermines the crown.
1471.1.1  = {
	unrest = 0
}
1500.1.1 = { road_network = yes }
1526.8.30 = {
	add_permanent_claim = HAB
} #Battle of Mohac where Lajos II dies -> Habsburg succession
1530.1.4  = {
#	owner = HAB
#	controller = HAB
#	add_core = HAB
	bailiff = yes	
}
1550.1.1  = {
	
}
1576.1.1  = {
	religion = reformed
}
1600.1.1  = {
	
}
1618.4.23 = {
	revolt = {
		type = religious_rebels
		size = 2
	}
	controller = REB
} # Defenestration of Prague
1619.3.1  = {
	revolt = { }
	controller = TES
#	owner = PAL
#	controller = PAL
	add_core = PAL
}
1620.11.8 = {
#	owner = HAB
#	controller = HAB
	remove_core = PAL
}# Tilly beats the Winterking at White Mountain. Deus Vult!
1621.1.1 = {
	
} # ... and let us start this session by executing the most inconvenient nobles....
1627.1.1 = {
	religion = catholic
} # Order from Ferdinand II to reconvert to Catholicism, many Protestant leave the country  
1653.5.19  = {
	owner = HAB
	controller = HAB
}
1700.1.1 = {
	
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
