# 4029 - Bahdinan

owner = KRD
controller = KRD
culture = ge_armenian
religion = coptic
capital = "Amadia"
trade_goods = wool
hre = no
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
discovered_by = steppestech
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1356.1.1   = {
	add_core = KRD
	owner = QAR
	controller = QAR
	add_core = QAR
	add_permanent_province_modifier = {
		name = "kurdish_princelings"
		duration = -1
	}
}
1393.1.1   = {
	owner = TIM
	controller = TIM
}
1406.1.1   = {
	owner = AKK
	controller = AKK
} # Independance granted by Tamerlane
1444.1.1 = {
	remove_core = JAI
}
1453.1.1  = { discovered_by = western }
1501.1.1  = {
	controller = SAM
}
1508.1.1  = {
	owner = SAM
}
1512.1.1  = {
	owner = PER
	controller = PER
	add_core = PER
	remove_core = SAM
	estate = estate_nobles
	bailiff = yes
	courthouse = yes
} # Safawids "form persia"
#1514.8.23 = { add_core = TUR  } # Diyarbakir conquered by Ottomans, Van remains Persian

1515.1.1 = { training_fields = yes }
1530.1.1 = { add_permanent_claim = TUR } #As Caliph, duty to rescue Baghdad
1534.7.1  = { controller = TUR } # Wartime occupation
1535.1.1  = { controller = PER } # Persians recapture Van
1548.8.25 = { controller = TUR }
1549.12.1 = {
	owner = TUR
	remove_core = PER	
	add_core = TUR
	remove_claim = TUR	
} # Part of the Ottoman empire
1556.1.1 = {
	culture = kurdish
	religion = sunni
}
1722.1.1  = { unrest = 5 } # Rebellion against the Ottomans
1730.1.1  = { unrest = 0 }
