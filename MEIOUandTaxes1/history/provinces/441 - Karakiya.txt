# No previous file for Karakiya

owner = KHO
controller = KHO
culture = turkmeni
religion = sunni
capital = "Kuryk"
trade_goods = wool
hre = no
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
discovered_by = mongol_tech
discovered_by = steppestech
discovered_by = turkishtech
discovered_by = muslim

1356.1.1   = {
	add_core = TIM
	add_core = KHO
	add_core = KTD
}
1379.1.1   = {
	owner = TIM
	controller = TIM
}
1444.1.1 = {
	remove_core = KTD
	remove_core = TIM
}
1469.8.27 = {
	owner = KHO
	controller = KHO
	remove_core = TIM
}
1505.1.1 = {
	owner = SHY
	controller = SHY
} # Captured by the Shaybanid horde
1511.1.1 = {
	owner = KHI 
	controller = KHI
	add_core = KHI
} # Khiva Independent
1515.1.1 = { training_fields = yes }
1677.1.1 = { discovered_by = FRA }
