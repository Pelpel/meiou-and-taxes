# 3949 - Kazusa

owner = KYO
controller = KYO
culture = kanto
religion = mahayana
capital = "Oudaki"
trade_goods = rice
hre = no
base_tax = 2
base_production = 2
base_manpower = 5
is_city = yes
discovered_by = chinese

1133.1.1 = { mill = yes }
1356.1.1 = {
	#briefly held by Kyogoku
	add_core = CHB #home province
}
1362.1.1 = { #assigned to chiba
	owner = CHB
	controller = CHB
}
1364.1.1 = { #briefly conquered by Nitta for southern court ADD TAG
	controller = JAP
}
1364.6.6 = { #taken back by the Uyesugi
	add_core = USG
	owner = USG
	controller = USG
}
1418.1.1 = { #held by Utsunomiya
	add_core = UTN
	owner = UTN
	controller = UTN
}	
1420.1.1 = {
	owner = USG
	controller = USG
}
1448.1.1 = { #returned to Chiba, last Shugo on record
	owner = CHB
	controller = CHB
}
1490.1.1   = {
	owner = HJO
	controller = HJO
}
1542.1.1   = { discovered_by = POR }
1590.8.4 = {
	owner = ODA
	controller = ODA
} # Toyotomi Hideyoshi takes Odawara Castle, Hojo Ujimasa commits seppuku
1603.1.1   = { capital = "Mito" }
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
