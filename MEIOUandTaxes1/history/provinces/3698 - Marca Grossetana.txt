# No previous file for Marca Grossetana

owner = SIE
controller = SIE
culture = tuscan 
religion = catholic 
capital = "Grosseto" 
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
trade_goods = livestock

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
hre = yes							#AdL: was part of the HRE

1300.1.1 = { road_network = yes }
1356.1.1   = {
	add_core = SIE
	add_permanent_province_modifier = {
		name = "county_of_santa_fiora"
		duration = -1
	}
}
1399.1.1 = { owner = MLO controller = MLO } #Changes hands multiple times in the Italian wars
1403.1.1 = { owner = SIE controller = SIE }
1450.1.1 = { temple = yes }
1502.2.2   = {
	owner = PAP
	controller = PAP
}
1503.3.29  ={
	owner = SIE
	controller = SIE
	bailiff = yes
}
1530.1.1 = {
	road_network = no paved_road_network = yes 
}
1530.2.27 = {
	hre = no
}
1531.1.1   = { controller = SPA owner = SPA add_core = SPA }
1552.1.1   = { controller = FRA owner = FRA add_core = FRA }
1555.1.1   = { controller = SPA owner = SPA remove_core = FRA }
1557.1.1   = {
	controller = FIR
	owner = FIR
	add_core = FIR
	remove_core = SPA
}
1569.1.1   = {
	owner = TUS
	controller = TUS
	add_core = TUS
	remove_core = FIR
} # Pope Pius V declared Duke Cosimo I de' Medici  Grand Duke of Tuscany
 
1618.1.1  =  { hre = no }
1801.2.9   = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # The Treaty of LunÚville
1801.3.21  = {
	owner = ETR
	controller = ETR
	add_core = ETR
} # The Kingdom of Etruria
1807.12.10 = {
	owner = FRA
	controller = FRA
	remove_core = ETR
} # Etruria is annexed to France
1814.4.11   = {
	owner = TUS
	controller = TUS
	remove_core = FRA
} # Treaty of Fontainebleu, Napoleon abdicates and Tuscany is restored
1860.3.20 = {
	owner = SPI
	controller = SPI
	add_core = SPI
}
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = SPI
}
