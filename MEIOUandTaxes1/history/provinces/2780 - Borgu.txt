# 2780 - Borgu

owner = BOR
controller = BOR
culture = yorumba
religion = west_african_pagan_reformed
capital = "Bussa"
base_tax = 6
base_production = 6
base_manpower = 3
is_city = yes
trade_goods = ivory

discovered_by = soudantech
discovered_by = sub_saharan

hre = no

1356.1.1   = {
	add_core = BOR
}
1806.1.1 = {
	owner = SOK
	controller = SOK
	add_core = SOK
}
