# No previous file for Lankaran

owner = CHU
controller = CHU
culture = azerbadjani
religion = sunni
capital = "Lankaran"
trade_goods = rice
hre = no
base_tax = 3
base_production = 3
base_manpower = 1
is_city = yes
discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = steppestech
discovered_by = turkishtech

1133.1.1 = { mill = yes }
1356.1.1 = {
	add_core = CHU
	add_core = WHI
	add_core = QAR
	unrest = 4
}
1357.1.1   = {
	owner = WHI
	controller = WHI
	remove_core = CHU
}
1360.1.1   = {
	owner = JAI
	controller = JAI
	remove_core = WHI
}
1375.1.1   = {
	owner = QAR
	controller = QAR
} # Independance secured
1444.1.1 = {
	remove_core = JAI
	add_core = AKK
}	
1467.1.1 = {
	controller = AKK
}
1470.1.1 = {
	owner = AKK
	add_core = AKK
	remove_core = QAR
}
1493.1.1  = {
	owner = SAM
	controller = SAM
	add_core = SAM
	remove_core = AKK
} #The Safawid Order
1512.1.1  = {
	owner = PER
	controller = PER
	add_core = PER
	remove_core = SAM
	bailiff = yes
} # The Safavids took over
1515.1.1 = { training_fields = yes }
1588.1.1   = { controller = TUR } # Ottoman conquest
1590.3.21  = { owner = TUR add_core = TUR } # Peace of Istanbul
1605.1.1   = { controller = PER }
1612.11.20 = { owner = PER }
1722.1.1 = {
	controller = REB
	revolt = { type = pretender_rebels }
} # Anti Ghilzai
1730.1.1 = {
	controller = PER
	revolt = { }
} # Afghans Ousted
1747.1.1 = {
	controller = REB
	revolt = { type = pretender_rebels }
} # Anti Ghilzai
1760.1.1 = {
	controller = PER
	revolt = { }
} # Afghans Ousted
1805.1.1 = {
	controller = RUS
}
1813.10.24 = {
	owner = RUS
	add_core = RUS
	remove_core = PER
} # The Treaty of Gulistan
