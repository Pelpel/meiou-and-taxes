owner = BRA
controller = BRA
add_core = BRA
culture = low_saxon
religion = catholic
hre = yes
base_tax = 6
base_production = 6
trade_goods = hemp
base_manpower = 3
is_city = yes
capital = "Stendal"
discovered_by = eastern
discovered_by = western
discovered_by = muslim

 #Magdeburger Dom, the most important Gothic cathedral in eastern Germany
1500.1.1 = { road_network = yes }
1515.1.1 = { training_fields = yes }
1519.1.1 = { bailiff = yes }
1524.1.1  = { religion = protestant }
1530.1.3 = {
	road_network = no paved_road_network = yes 
	city_watch = yes
}

1550.1.1  = { fort_14th = yes }

1650.1.1  = { culture = prussian }
1701.1.18 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = BRA
} # Friedrich III becomes king of Prussia
1725.1.1  = {  }
1750.1.1  = {  }

1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
1806.11.1 = { controller = FRA } # Occupied by French troops
1807.7.9  = {
	owner = WES
	controller = WES
	add_core = WES
	remove_core = PRU
} # The Second Treaty of Tilsit, the kingdom of Westfalia
1813.10.13 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = WES
} # The kingdom is dissolved
