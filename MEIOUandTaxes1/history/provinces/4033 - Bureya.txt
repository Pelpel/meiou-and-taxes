# No previous file for Birija

owner = MHX
controller = MHX
add_core = MHX
culture = jurchen
religion = tengri_pagan_reformed
capital = "Birija"
trade_goods = fur
hre = no
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
discovered_by = chinese
discovered_by = steppestech

1420.1.1 = {
	owner = XXX
	controller = XXX
	remove_core = MHX
	culture = evenki
}

1643.1.1 = {
	discovered_by = RUS
   	owner = RUS
   	controller = RUS
   	religion = orthodox
   	culture = russian
} # Founded by Pyotr Beketov
1668.1.1 = {
	add_core = RUS
}
1689.8.27 = {
	discovered_by = QNG
	owner = QNG
	controller = QNG
	add_core = QNG
	culture = manchu
	religion = confucianism
} # Treaty of Nerchinsk
1858.1.1 = {
	discovered_by = RUS
   	owner = RUS
   	controller = RUS
   	religion = orthodox
   	culture = russian
} # Treaty of Aigun
