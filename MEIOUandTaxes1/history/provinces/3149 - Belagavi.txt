# No previous file for Belagavi

owner = BAH
controller = BAH
culture = marathi
religion = hinduism
capital = "Athani"
trade_goods = cotton
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = indian
discovered_by = muslim 

1100.1.1 = { plantations = yes }
1356.1.1  = {
	add_core = BAH
	add_core = BIJ
	fort_14th = yes
}
1465.1.1 = {
	add_permanent_province_modifier = {
		name = mudhol_state
		duration = -1
	}
}
1490.1.1  = {
	remove_core = BAH
	controller = BIJ
	owner = BIJ
	fort_14th = no
	fort_15th = yes
} # The Breakup of the Bahmani sultanate
1498.1.1  = { discovered_by = POR }
1515.12.17 = { training_fields = yes }
1530.3.17 = {
	bailiff = yes
	marketplace = yes
}
1650.1.1  = {
	add_core = MAR #Maratha Identity
}
1657.1.1  = {
	owner = MAR
	controller = MAR
	remove_core = MUG
} # Inheritance of Shivaj
