# V�rmland
# MEIOU - Gigau

add_core = SWE
owner = SWE
controller = SWE
culture = swedish
religion = catholic
hre = no
base_tax = 2
base_production = 2
trade_goods = iron
base_manpower = 1
is_city = yes
capital = "Karlstad"
discovered_by = eastern
discovered_by = western

1512.1.1  = { fort_14th = yes }
1515.1.1 = { training_fields = yes }
1527.6.1  = { religion = protestant}
 #controller of trade route to Denmark
 #N�rke-V�rmlands regemente
1529.10.7 = { bailiff = yes }
1529.12.17 = { merchant_guild = yes }
1625.1.1  = {  }
 #minor court belonging to G�ta Hovr�tt
 #Unknown date
1670.1.1  = { fort_14th = no fort_15th = yes } #Due to the "Staplepolicy act"
1680.1.1  = { fort_15th = no fort_16th = yes }
1730.1.1  = { fort_16th = no fort_17th = yes } 
 #Due to the support of manufactories
