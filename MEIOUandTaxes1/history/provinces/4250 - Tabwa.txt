# No previous file for Tabwa

owner = KZB
controller = KZB
add_core = KZB
culture = luba
religion = animism
capital = "Tabwa"
trade_goods = fish
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
hre = no
discovered_by = central_african
