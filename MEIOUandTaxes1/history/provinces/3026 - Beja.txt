# No previous file for Beja

native_size = 10
native_ferocity = 2
native_hostileness = 2
culture = nubian 
religion = coptic
capital = "Kassala"
trade_goods = unknown # wool
hre = no
discovered_by = ALW
discovered_by = MKU
discovered_by = ADA
discovered_by = muslim
discovered_by = turkishtech
discovered_by = east_african
discovered_by = YAO
discovered_by = ETH
discovered_by = DAR
discovered_by = KIT

1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1503.1.1 = {
	owner = SEN
	controller = SEN
	add_core = SEN
	discovered_by = SEN
	culture = funj 
	is_city = yes
	discovered_by = ETH
	trade_goods = wool
}
1515.2.1 = { training_fields = yes }
1530.1.1 = { religion = sunni } #Spread of Islam leads to shift in religion affiliation of region
1550.1.1 = { discovered_by = TUR }
1583.1.1 = { unrest = 6 } #Shaykh Ajib expelled from Sennar, Abdallabi discontent grows
1584.1.1 = { unrest = 0 } #Dakin and Ajib reach agreement to end conflict
1612.1.1 = { unrest = 5 } #Funj destroy Ajib at Karkoj
1613.1.1 = { unrest = 0 } #Funj restore autonomy to the Abdallabi
1706.1.1 = { unrest = 3 } #Badi III faces revolt over development of slave army at aristocrats expense
1709.1.1 = { unrest = 0 } #Badi III faces revolt over development of slave army at aristocrats expense
1718.1.1 = { unrest = 6 } #Unsa III comes into conflict with aristocrats
1720.1.1 = { unrest = 0 } #Unsa III deposed, new Funj dynasty set up by aristocrats
1820.1.1 = {
	owner = TUR
	controller = TUR
}
