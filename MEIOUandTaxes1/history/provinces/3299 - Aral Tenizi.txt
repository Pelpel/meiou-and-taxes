# No previous file for Aral Te�izi

owner = BLU
controller = BLU
culture = uzbehk
religion = sunni
capital = "Jankent"
trade_goods = wool
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = muslim
discovered_by = mongol_tech
discovered_by = steppestech
discovered_by = turkishtech

1356.1.1   = {
	add_core = BLU
	add_core = KHI
}
1359.1.1 = {
	owner = KHI
	controller = KHI
	remove_core = BLU
}
1379.1.1 = {
	owner = TIM
	controller = TIM
	add_core = TIM
}
1444.1.1 = {
	add_core = SHY
}
1446.1.1 = {
	owner = SHY
	controller = SHY
	add_core = SHY
	remove_core = GOL
}
#1465.1.1 = {
#	owner = KZH
#	controller = KZH
#}
1502.1.1 = {
	owner = SHY
	controller = SHY
	remove_core = TIM
}
1511.1.1 = {
	owner = KHI 
	controller = KHI
	add_core = KHI
	remove_core = SHY
	remove_core = KZH
} # Khiva Independent
1515.1.1 = { training_fields = yes }
1723.1.1 = { owner = OIR controller = OIR } # Dzungarian invasion
1728.1.1 = { owner = KZH controller = KZH }
1740.1.1 = { owner = OIR controller = OIR } # Dzungarian invasion
1755.1.1 = { owner = KZH controller = KZH }
