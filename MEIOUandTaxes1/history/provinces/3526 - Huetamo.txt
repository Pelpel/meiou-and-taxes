# 3526 - Huetamo

owner = HUE
controller = HUE
add_core = HUE
culture = purepechan
religion = nahuatl
capital = "Huetamo"

base_tax = 3
base_production = 3
#base_manpower = 1.0
base_manpower = 2.0
citysize = 1500
trade_goods = maize


discovered_by = mesoamerican

hre = no
1460.1.1   = {
	owner = PUR
	controller = PUR
	add_core = PUR
} 
1530.1.1   = {
	discovered_by = SPA
	owner = SPA
	controller = SPA
	add_core = SPA
	religion = catholic
	marketplace = yes
	bailiff = yes
	courthouse = yes
} # Francisco V�zquez de Coronado y Luj�n
1600.1.1   = {
	citysize = 3000
}
1608.1.1   = {
	add_core = SPA
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
}
1810.9.16  = {
	owner = MEX
	controller = MEX
} # Declaration of Independence
1821.8.24  = {
	remove_core = SPA
} # Treaty of Cord�ba
