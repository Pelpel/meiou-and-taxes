# 570 - Ahom

owner = DMS
controller = DMS
culture = naga
religion = adi_dharam
capital = "Dimapur"
trade_goods = rice
hre = no
base_tax = 5
base_production = 5
#base_manpower = 1.5
base_manpower = 3.0
citysize = 12000
discovered_by = indian
discovered_by = muslim
discovered_by = chinese
discovered_by = steppestech
1120.1.1 = { farm_estate = yes }
1133.1.1 = { mill = yes }
1200.1.1 = { road_network = yes }

1356.1.1 = { add_core = DMS }
1450.1.1 = { citysize = 13000 }
1500.1.1 = { citysize = 15000 }
1515.1.1 = { training_fields = yes }
1526.2.1 = { controller = ASS }
1530.1.1 = { owner = ASS add_core = ASS remove_core = DMS }
1550.1.1 = { citysize = 16000 }
1581.1.1 = { add_core = ASS }
1600.1.1 = { citysize = 18000 }
1650.1.1 = { citysize = 20000 }
1700.1.1 = { citysize = 23000 }
1750.1.1 = { citysize = 25000 }
1770.1.1 = { unrest = 8 } # Moamoria rebellion
1800.1.1 = { citysize = 28000 }
1806.1.1 = { unrest = 0 }
