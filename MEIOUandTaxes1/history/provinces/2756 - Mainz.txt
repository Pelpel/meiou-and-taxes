# 2756 - Mainz

capital = "Mainz"
culture = hessian
religion = catholic
trade_goods = beer
owner = MAI
base_tax = 7
base_production = 7
base_manpower = 4
is_city = yes
controller = MAI
add_core = MAI
hre = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim

 # The Kaiserdom in Speyer is on of the oldest and greatest in the HRE, built over the 11th and 12th century
 # The Reichskammergericht (1495-1806) is the highest court in the HRE situated in Worms and after 1527 Speyer

1100.1.1 = { marketplace = yes }
#1111.1.1 = { post_system = yes }
1119.1.1 = { bailiff = yes }
1250.1.1 = { temple = yes }
1476.1.1 = { medieval_university = yes }
1500.1.1 = { road_network = yes }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1546.4.19  = { religion = protestant  } # #Friedrich II converts the coutnry to protestant
1620.1.1   = { fort_14th = yes  }
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
1815.6.9  = {
	owner = HES
	controller = HES
	add_core = HES
} # Congress of Vienna
