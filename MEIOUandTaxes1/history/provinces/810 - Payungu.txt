# 810 - Payungu

culture = aboriginal
religion = polynesian_religion
capital = "Yamatji"
trade_goods = unknown #grain
hre = no
native_size = 10
native_ferocity = 0.5
native_hostileness = 9

1620.1.1 = {
	discovered_by = NED
} # Dutch navigator Willem Janszoon
