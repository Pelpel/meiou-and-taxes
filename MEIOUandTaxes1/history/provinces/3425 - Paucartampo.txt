# 3425 - Paucartampo

owner = CZC
controller = CZC 
add_core = CZC
culture = quechuan
religion = inti
capital = "Paucartampo"
trade_goods = cacao
hre = no
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
 
discovered_by = south_american

1356.1.1 = {
}
1480.1.1   = {
	owner = INC
	controller = INC
	add_core = INC
	remove_core = CZC
}
1529.1.1 = {
	owner = CZC
	controller = CZC
	add_core = QUI
	add_core = CZC
	remove_core = INC
	bailiff = yes
	constable = yes
	marketplace = yes
}
1535.1.1  = {
	discovered_by = SPA
	add_core = SPA
	owner = SPA
	controller = SPA
	religion = catholic
	culture = castillian
}
1750.1.1  = {
	add_core = PEU
	culture = peruvian
}
1810.9.18  = {
	owner = PEU
	controller = PEU
}
1818.2.12  = {
	remove_core = SPA
}
