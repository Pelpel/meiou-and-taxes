# No previous file for Vang Vieng

owner = LXA
controller = LXA
add_core = LXA
culture = laotian
religion = buddhism
capital = "Vang Vieng"
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
trade_goods = rice
discovered_by = chinese
discovered_by = indian
hre = no

1250.1.1 = { temple = yes }
1707.1.1 = {
	owner = VIE
	controller = VIE
	add_core = VIE
	remove_core = LXA
	fort_14th = yes
} # Declared independent, Lan Xang was partitioned into 4 kingdoms; Vientiane, Champasak & Luang Prabang + Muang Phuan
1713.1.1 = {
	owner = CHK
	controller = CHK
	add_core = CHK
	remove_core = VIE
}
1811.1.1 = { unrest = 5 } # The Siamese-Cambodian Rebellion
1812.1.1 = { unrest = 0 }
1829.1.1 = {
	owner = SIA
	controller = SIA
    	add_core = SIA
	unrest = 0
} # Annexed by Siam
1893.1.1 = {
	owner = FRA
	controller = FRA
    	unrest = 0
}
