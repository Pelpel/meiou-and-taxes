# 700 - henan_area Beiguan

owner = YUA
controller = YUA
culture = hanyu
religion = confucianism
capital = "Anyang"
trade_goods = chinaware
hre = no
base_tax = 11
base_production = 11
base_manpower = 5
is_city = yes


discovered_by = chinese
discovered_by = steppestech

#1111.1.1 = { post_system = yes }
1271.12.18  = {#Proclamation of Yuan dynasty
	add_core = YUA
}
1280.1.1 = {#Yu Viceroy
	owner = CYU
	controller = CYU
	add_core = CYU
}
1351.1.1  = {
	add_core = SNG
}
#1356.1.1 = {	remove_core = YUA } # Red Turbans
1357.8.1  = {
	owner = SNG
	controller = SNG
}
1361.1.1 = {
	owner = YUA
	controller = YUA
}
1368.1.1  = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = CYU
	remove_core = SNG
}
1529.3.17 = { 
	marketplace = yes
	bailiff = yes
	courthouse = yes
	road_network = yes
	fine_arts_academy = yes
}

1540.1.1  = { fort_14th = yes }

1630.1.1  = { unrest = 6 } # Li Zicheng rebellion
1640.1.1  = { controller = REB } # Financhial crisis
1641.1.1  = { controller = MNG } 
1644.4.29 = {
	controller = MCH
}
1644.6.6 = {
	owner = QNG
	controller = QNG
	add_core = QNG
#	remove_core = MNG
} # The Qing Dynasty

1645.5.27 = { unrest = 0 } # The rebellion is defeated
1662.1.1 = { remove_core = MNG }
1740.1.1  = {  }
