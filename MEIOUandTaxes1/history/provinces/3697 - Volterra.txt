# No previous file for Volterra

owner = FIR
controller = FIR
add_core = FIR
culture = tuscan
religion = catholic 
capital = "Volterra" 
base_tax = 5
base_production = 5 
base_manpower = 2
is_city = yes
trade_goods = lead
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
hre = yes

1300.1.1 = { road_network = yes }
#1348 Black Death kills 50-75%popul. in one year
1527.1.1   = { controller = SPA } # War of the League of Cognac
1529.8.3   = { controller = FIR
	bailiff = yes } # Treaty of Cambrai

1530.2.27 = {
	hre = no
}
1569.1.1   = {
	owner = TUS
	controller = TUS
	add_core = TUS
	remove_core = FIR
} # Pope Pius V declared Duke Cosimo I de' Medici  Grand Duke of Tuscany
1618.1.1  =  { hre = no }
1720.1.1   = {  } 
1801.2.9   = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # The Treaty of LunÚville
1801.3.21  = {
	owner = ETR
	controller = ETR
	add_core = ETR
} # The Kingdom of Etruria
1807.12.10 = {
	owner = FRA
	controller = FRA
	remove_core = ETR
} # Etruria is annexed to France
1814.4.11  = {
	owner = TUS
	controller = TUS
	remove_core = FRA
} # Napoleon abdicates and Tuscany is restored
1860.3.20 = {
	owner = SPI
	controller = SPI
	add_core = SPI
}
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = SPI
}
