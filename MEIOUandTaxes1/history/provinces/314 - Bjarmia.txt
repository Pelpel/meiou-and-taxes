# 314 - Bjarmia

owner = NOV
controller = NOV    
culture = pomor 
religion = orthodox
hre = no
trade_goods = fur # subsistence
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
capital = "Holmogory"
discovered_by = eastern
discovered_by = western
discovered_by = steppestech
discovered_by = muslim

1000.1.1 = {
	add_permanent_province_modifier = {
		name = dvina_estuary_modifier
		duration = -1
	}
}
1356.1.1  = {
	add_core = NOV
}
1397.1.1  = {
	add_core = MOS
} # Dvina statutory charter
1472.1.14 = {
	owner = MOS
	controller = MOS
	remove_core = NOV
} # Land strife in Novgorod. Dvinskaya zemlya submits to Muscowian Prince.
1530.1.4  = {
	bailiff = yes	
}

1547.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia
 # Became a wealthy and important trade and transport center
1584.1.1  = { capital = "Arkhangelsk" fort_14th = yes culture = russian } # Foundation of Arkhangelsk, also maybe 
1598.1.1  = { unrest = 5 } # "Time of troubles"
1613.1.1  = { unrest = 0 } # Order returned, Romanov dynasty
 # Dedicated to the Domition of Mary, rebuilt in the 17th century
 # Peter I ordered the creation of a state shipyard
