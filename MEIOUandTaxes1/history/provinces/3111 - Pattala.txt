# No previous file for Pattala

owner = MUL
controller = MUL
culture = panjabi
religion = hinduism
capital = "Patiala"
trade_goods = carpet
hre = no
base_tax = 10
base_production = 10
base_manpower = 5
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1000.1.1 = {
	add_permanent_province_modifier = {
		name = jaswan_state
		duration = -1
	}
}
1115.1.1 = { bailiff = yes }
1120.1.1 = { textile = yes }
#1180.1.1 = { post_system = yes }
1200.1.1 = { road_network = yes }
1356.1.1 = {
	add_core = MUL
	#add_core = PUN
	add_core = PTA
}
1443.1.1 = {
	#revolt = { type = pretender_rebels size = 4 leader = "Bahlul Lodi" }
	controller = PTA
	owner = PTA
	add_core = PTA
} #First revolt of Bahlul Lodi
1445.1.1 = {
	controller = DLH
	revolt = {  }
} #Bahlul Lodi returns to the Punjab
1447.1.1 = {
	#revolt = { type = pretender_rebels size = 5 leader = "Bahlul Lodi" }
	controller = PTA
} #Second revolt of Bahlul Lodi
1451.4.20 = {
	controller = DLH
	owner = DLH
	revolt = {  }
	#remove_core = PTA
} #Final triumph of the Lodi
1499.1.1 = {
	religion = sikhism
	add_permanent_province_modifier = {
	name = religious_center
	duration = -1
	}
}
1526.2.1 = { controller = TIM } # Babur's invasion
1526.4.21 = {
	owner = MUG
	controller = MUG
	add_core = MUG
	remove_core = DLH
	training_fields = yes
} # Battle of Panipat
1530.1.1 = { add_core = TRT }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1540.1.1 = {
	owner = BNG
	controller = BNG
	add_core = BNG
} #Conquered by Sher Shah Suri
1553.1.1 = {
	owner = PTA
	controller = PTA
	remove_core = BNG
} #Death of Islam Shah Sur, Suri empire split
1554.12.1 = {
	controller = MUG
	owner = MUG
} #Battle of Sirhind
1690.1.1  = { discovered_by = ENG }
1700.1.1 = {
	controller = REB
	revolt = { type = sikhism_rebels size = 1 }
}
1707.5.12 = { discovered_by = GBR }
1715.1.1 = {
	controller = MUG
	revolt = { }
}
1720.1.1 = {
	controller = REB
	revolt = { type = sikhism_rebels size = 1 }
}
1757.1.1 = {
	owner = DUR
	controller = DUR
	remove_core = MUG
	revolt = { }
}
1758.1.1 = {
	controller = MAR
	add_core = GWA
} # Held by the Peshawas
1760.1.1 = { controller = DUR }
1762.1.1 = {
	owner = PUN
	controller = PUN
	add_core = PUN
}
