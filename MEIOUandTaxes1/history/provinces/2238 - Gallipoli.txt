# 2238 - Gallipoli

owner = BYZ
controller = BYZ
culture = greek
religion = orthodox
capital = "Gallipoli"
trade_goods = wheat
hre = no
base_tax = 7
base_production = 7
base_manpower = 2
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

#1133.1.1 = { mill = yes }
1300.1.1   = { fort_14th = yes } #Ottomans managed to overcome the fort by surprise; will help them keep Byzantines at bay or be a reward for Byzantines to take it back

1301.1.1 = { road_network = yes }
1350.1.1  = {
	add_claim = OTT
}
1354.1.1  = {
	owner = OTT
	controller = OTT
	add_core = OTT
	add_core = BYZ
	capital = "Gelibolu"
	set_province_flag = turkish_name
}
1367.8.26 = {
	owner = BYZ
	controller = BYZ
	capital =  "Gallipoli"
} # Captured during the Savoyard Crusade
1376.1.1  = {
	owner = OTT
	controller = OTT
	capital = "Gelibolu"
} # Iannes V Palaiologos gives it back to Murad I in exchange of support in the Civil War
1390.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = OTT
}
1453.5.29 = {
	remove_core = BYZ
}
1515.1.1 = { training_fields = yes }
1519.1.1 = { bailiff = yes }
1523.8.16 = { mill = yes }
1555.1.1  = { unrest = 5 } # General discontent with the Janissaries' dominance
1556.1.1  = { unrest = 0  }

1623.1.1  = { controller = REB } # The empire fell into anarchy, Janissaries stormed the palace
1625.1.1  = { controller = TUR } # Murad tries to quell the corruption
1689.1.1  = { controller = REB } # Karposh uprising against Ottoman rule
1690.1.1  = { controller = TUR }
1715.1.1  = {  }
1750.1.1  = { add_core = GRE }
1821.3.1  = {
	controller = REB
}
1829.1.1  = {
	controller = TUR
}
