# 3448 - Chenque
# local name of the hill over the sole city of this province

culture = chon
religion = pantheism
capital = "Chenque"
trade_goods = unknown #fish
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 8
native_ferocity = 2
native_hostileness = 9

1516.1.23  = {
	discovered_by = SPA
	#add_core = SPA
} # Juan D�az de Sol�s
1750.1.1   = {
	unrest = 2
   	add_core = LAP
}
1776.1.1   = {
	owner = SPA
	controller = SPA
	religion = catholic
	culture = platean
	citysize = 500
	capital = "San Antonio"
	trade_goods = fish
	set_province_flag = trade_good_set
}
1780.1.1   = { unrest = 4 } # The desire for independence grew
1790.1.1   = { unrest = 6 }
1800.1.1   = { citysize = 1000 }
1810.5.25  = {
	owner = LAP
	controller = LAP
	unrest = 0
}
1816.7.9   = {
	remove_core = SPA
}
