#44 - Hamburg

owner = HAM
controller = HAM
add_core = HAM
culture = old_saxon
religion = catholic
hre = yes
base_tax = 9
base_production = 9
trade_goods = beer
#base_manpower = 2.5
base_manpower = 5.0

capital = "Hamburg"
citysize = 10500
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1000.1.1 = {
	add_permanent_province_modifier = {
		name = elbe_estuary_modifier
		duration = -1
	}
	add_permanent_province_modifier = {
		name = center_of_trade_modifier
		duration = -1
	}
	add_permanent_province_modifier = { 
		name = "natural_harbour" 
		duration = -1 
		}
}
1088.1.1 = { dock = yes shipyard = yes }
1100.1.1 = { merchant_guild = yes }
#1111.1.1 = { post_system = yes }
1200.1.1 = { road_network = yes }
1356.1.1 = { citysize = 11000 }
1399.1.1 = { citysize = 12000 }
1453.1.1 = { citysize = 13000 }
1499.1.1 = { road_network = yes }
1500.1.1 = { citysize = 14000 }
1510.1.1 = { fort_14th = yes }
1522.3.20 = { dock = no naval_arsenal = yes }
1525.1.1 = { citysize = 21500  }
1529.1.1 = { religion = protestant }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}

1550.1.1 = { citysize = 29000 base_tax = 11
base_production = 11 } #gradual shift in trading power from L�beck to Hamburg

1575.1.1 = { citysize = 34500   }
1600.1.1 = { citysize = 40000 base_tax = 12
base_production = 12 fort_14th = no fort_17th = yes } #gradual shift in trading power from L�beck to Hamburg
1625.1.1 = { citysize = 57500 }

1650.1.1 = { citysize = 75000  }
1675.1.1 = { citysize = 72500 }
1700.1.1 = { citysize = 70000 fort_17th = no fort_18th = yes }
1725.1.1 = { citysize = 72500  }
1750.1.1 = { citysize = 75000  }
1775.1.1 = { citysize = 87500 }
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
1810.12.13 = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # Annexed by France
1814.4.11 = {
	owner = HAM
	controller = HAM
	remove_core = FRA
} # Napoleon abdicates unconditionally
