# 2434 - Buton

culture = bungku
religion = polynesian_religion
capital = "Bau-Bau"
trade_goods = pepper
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 5
native_ferocity = 2
native_hostileness = 2
discovered_by = MKS
discovered_by = MPH
discovered_by = MTR
discovered_by = chinese
discovered_by = austranesian

1000.1.1   = {
	set_province_flag = sulawesi_natives
}
1500.1.1 = { religion = sunni }
1658.1.1 = {
	discovered_by = NED
	owner = NED
	controller = NED
   	citysize = 600
	base_tax = 2
	base_production = 2
} # Dutch control
1683.1.1 = { add_core = NED }
1700.1.1  = {
	is_city = yes
	culture = batavian
}
