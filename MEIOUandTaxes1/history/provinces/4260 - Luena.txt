# No previous file for Luena

culture = lunda
religion = animism
capital = "Luena"
native_size = 50
native_ferocity = 1
native_hostileness = 7
trade_goods = millet
hre = no
discovered_by = central_african
