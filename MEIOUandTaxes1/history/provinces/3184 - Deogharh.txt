# No previous file for Deogharh

owner = BNG
controller = BNG
culture = bihari
religion = hinduism #important center of Hinduism
capital = "Deogharh"
trade_goods = ivory
hre = no
base_tax = 10
base_production = 10
base_manpower = 3
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech 
discovered_by = turkishtech

#1180.1.1 = { post_system = yes }
1200.1.1 = { road_network = yes }
1356.1.1  = {
	add_core = BNG
	add_core = TRT
}
1515.12.17 = { training_fields = yes }
1530.1.1 = {
	owner = TRT
	controller = TRT
	add_permanent_claim = MUG
}
1530.1.2 = { add_core = TRT }
1538.6.1  = {
	controller = MUG
} # Mughal Invasion
1539.1.1  = {
	controller = BNG
	owner = BNG
} # Surs again in control
1587.1.1  = {
	owner = MUG
	controller = MUG
	add_core = MUG
} # Integrated into Mughal Empire
1627.1.1  = { discovered_by = POR }

1707.3.15 = {
	owner = BNG
	controller = BNG
	add_core = BNG
	remove_core = MUG
}
1760.1.1  = {
	owner = GBR
	controller = GBR
	remove_core = BNG
} # Given to GBR by Mir Qasim
