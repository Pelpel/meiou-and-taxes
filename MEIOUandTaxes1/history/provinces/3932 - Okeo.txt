# 3932 - Okeo

owner = KHM
controller = KHM
add_core = KHM
culture = khmer
religion = buddhism
capital = "Angkor Borei"
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
trade_goods = fish
discovered_by = chinese
discovered_by = indian
hre = no

1867.1.1 = { 			
	owner = FRA
	controller = FRA
}
