# 451 - Kabulistan

owner = CHG
controller = CHG
culture = pashtun
religion = sunni
capital = "Kabul"
trade_goods = carpet
hre = no
base_tax = 9
base_production = 9
base_manpower = 4
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1000.1.1 = {
	add_permanent_province_modifier = {
		name = inland_center_of_trade_modifier
		duration = -1
	}
	#add_permanent_province_modifier = {   # Hahahaha no it was not. City had very, very little population throughout history. Especially so in 1356.
	#	name = major_city
	#	duration = -1
	#}
	add_permanent_province_modifier = {
		name = pashtun_tribal_area
		duration = -1
	}
}
#1111.1.1 = { post_system = yes }
1115.1.1 = { bailiff = yes }
1120.1.1 = { textile = yes }
1199.1.1 = { road_network = yes }
1200.1.1 = { marketplace = yes }
1356.1.1   = {
	add_core = KAB
	add_core = CHG
	fort_14th = yes
}
1370.4.1   = {
	owner = TIM
	controller = TIM
	add_core = TIM
	remove_core = CHG
}
1444.1.1  = {
	owner = KTD
	controller = KTD
	remove_core = TIM
	remove_core = KAB
	add_core = DUR
} # Shaybanids break free from the Timurids
1461.1.1 = {
	owner = TIM
	controller = TIM
}
1469.8.27 = {
	owner = DUR
	controller = DUR
}
1504.6.1  = {
	controller = TIM
	owner = TIM
	add_core = TIM
} #Conquered by Babur
1515.1.1 = { training_fields = yes }
1526.4.21 = {
	owner = MUG
	controller = MUG
	add_core = MUG
	remove_core = TIM
} # Battle of Panipat
1530.1.1 = { temple = yes }
1566.6.1 = { revolt = { }
	owner = KAB
	controller = KAB
}	#Independent of Mughals for a long while
1585.1.1 = {
	controller = MUG
}	# Man Singh occupies Kabulistan after death of Mirza Hakim
1585.2.1 = {
	owner = MUG
} # Annexed into empire again
1672.1.1 = {
	controller = REB
	revolt = { type = nationalist_rebels }
} # Widespread tribal uprisings
1675.1.1 = {
	controller = MUG
	revolt = { }
} # End of uprisings
1707.5.12 = { discovered_by = GBR }
1737.1.1  = {
	controller = PER
}
1739.5.1  = {
	owner = PER
} # Captured by Persia, Nadir Shah
1747.10.1 = {
	owner = DUR
	controller = DUR
	add_core = DUR
	remove_core = MUG
} # Ahmad Shah established the Durrani empire
1750.1.1 = {   }
