# 1529 - Kazym

owner = KOD
controller = KOD
culture = khanty
religion = tengri_pagan_reformed
capital = "Kazym"
trade_goods = iron
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
is_city = yes
discovered_by = steppestech
discovered_by = muslim

1356.1.1 = {
	add_core = KOD
}
1530.1.1 = {
	owner = SIB
	controller = SIB
	add_core = SIB
}
1581.1.1 = {
	discovered_by = RUS 
	owner = RUS
	controller = RUS
	religion = orthodox
	culture = russian
} # Yermak Timofeevic
1606.1.1 = { add_core = RUS unrest = 3 } # Rebellions against Russian rule
1608.1.1 = { unrest = 5 }
1610.1.1 = { unrest = 2 }
1616.1.1 = { unrest = 6 }
1620.1.1 = { unrest = 0 }
