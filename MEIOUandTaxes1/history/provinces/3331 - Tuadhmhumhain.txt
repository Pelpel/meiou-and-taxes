# No previous file for North Munster

owner = MNS
controller = MNS
culture = irish
religion = catholic
hre = no
base_tax = 3
base_production = 3
trade_goods = livestock
base_manpower = 3
is_city = yes
capital = "Inis" # Ennis
discovered_by = western
discovered_by = muslim
discovered_by = eastern

700.1.1 = {
	add_permanent_province_modifier = { name = clan_land duration = -1 }
}
1250.1.1 = { temple = yes }
1356.1.1  = {
	add_core = MNS
	add_permanent_province_modifier = {
		name = medieval_castle
		duration = -1
	}
}
1543.1.1  = {
	owner = ENG
	controller = ENG
	add_core = ENG
} # Kingdom of Thomond disestablished
1642.1.1  = { controller = REB } # Estimated
1642.6.7  = { owner = IRE controller = IRE } # Confederation of Kilkenny
1650.4.10 = { controller = ENG } # Battle of Macroom
1652.4.1  = { owner = ENG } # End of the Irish Confederates
1655.1.1  = { fort_14th = yes }
 # Estimated
1689.3.12 = { controller = REB } # James II Lands in Ireland
1690.9.15 = { controller = ENG }
 # marketplace Estimated
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
