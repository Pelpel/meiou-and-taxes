# 614 Vieng Chan
# LS - Indochina set up
# TM-Smiles indochina-mod for meiou 

owner = LXA
controller = LXA
add_core = LXA
culture = laotian
religion = buddhism
capital = "Vientiane"
base_tax = 4
base_production = 4
base_manpower = 4
is_city = yes
trade_goods = lumber
discovered_by = chinese
discovered_by = indian
hre = no

1250.1.1 = { temple = yes }
1354.1.1 = { owner = LXA }
1356.1.1 = {
	fort_14th = yes
}
1707.1.1 = {
	owner = VIE
	controller = VIE
	add_core = VIE
	remove_core = LXA
} # Lan Xangs declared itself independent, partitioned into three kingdoms; Vientiane, Champasak & Luang Prabang
1829.1.1 = {
	owner = SIA
	controller = SIA
    	add_core = SIA
	unrest = 0
} # Annexed by Siam
1893.1.1 = {
	owner = FRA
	controller = FRA
    	unrest = 0
}
