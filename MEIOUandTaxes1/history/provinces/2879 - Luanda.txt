# 1158 - Luanda

culture = mbundu
religion = animism
capital = "Luanda"
trade_goods = unknown
hre = no
base_tax = 1
native_size = 50
native_ferocity = 1
native_hostileness = 3
discovered_by = central_african

1000.1.1   = {
	add_permanent_province_modifier = { 
		name = "natural_harbour" 
		duration = -1 
		}
}
1356.1.1 = {
    add_permanent_province_modifier = {
        name = trading_post_province
        duration = -1
    }
}
1481.1.1 = {
	discovered_by = POR
} # Bartolomeu Dias
1575.1.1 = {
	owner = POR
	controller = POR
	add_core = POR
	citysize = 740
	base_tax = 7
	base_production = 7
	trade_goods = slaves
} # Portuguese capture Luanda, aided by mercenaries from Ndongo and Kongo
1580.1.1 = { discovered_by = ENG } # Francis Drake
1627.1.1 = { capital = "S�o Paulo de Luanda" } # Became the administrative center
1634.1.1 = { fort_14th = yes } # Fortaleza de S�o Miguel
1641.1.1 = {
	owner = NED
	controller = NED
	add_core = NED
	capital = "Aardenburgh"
} # Dutch control
1648.1.1 = {
	owner = POR
	controller = POR
	capital = "S�o Paulo de Luanda"
} # A Brazilian-Portuguese expedition expelled the Dutch
1654.1.1 = { remove_core = NED } # The last Dutch outpost is conquered
