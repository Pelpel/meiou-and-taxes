# No previous file for Misaskwatomina

culture = cree
religion = totemism
capital = "Misaskwatomi"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 10
native_ferocity = 2
native_hostileness = 5

1692.1.1 = { discovered_by = ENG } #Henry Kelsey
1707.5.12 = { discovered_by = GBR }
1732.1.1  = { discovered_by = FRA } # Pierre Gaultier de Varennes
1753.1.1  = { 	owner = FRA 
		controller = FRA
		citysize = 300
		culture = francien
		religion = catholic
		trade_goods = fur } #Fort a la Corne
1763.2.10 = {	discovered_by = GBR
		owner = GBR
		controller = GBR
		culture = english
		religion = protestant
	    } # Treaty of Paris
1775.1.1  = { citysize = 800 } #Northwest company and other traders become more frequent.
1788.2.10 = { add_core = GBR }
1800.1.1  = { citysize = 1400 }

