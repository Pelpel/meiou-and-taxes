# 1025 - Kamikawa

culture = aynu
religion = animism
capital = "Kamikawa"
trade_goods = unknown
hre = no

native_size = 5
native_ferocity = 2
native_hostileness = 1

discovered_by = JAP
discovered_by = AKG
discovered_by = AKM
discovered_by = ANG
discovered_by = CSK
discovered_by = DTE
discovered_by = HJO
discovered_by = HKW
discovered_by = HKY
discovered_by = IGW
discovered_by = ISK
discovered_by = KTK
discovered_by = MIY
discovered_by = MRI
discovered_by = NNB
discovered_by = ODA
discovered_by = OTM
discovered_by = OUC
discovered_by = RZJ
discovered_by = SBA
discovered_by = SHN
discovered_by = SMZ
discovered_by = SOO
discovered_by = STO
discovered_by = TKD
discovered_by = TGW
discovered_by = TKI
discovered_by = UKI
discovered_by = USG
discovered_by = YMN

1672.12.1  = {
	owner = KKZ
	controller = KKZ
	add_core = KKZ
	add_core = JAP
	citysize = 250
	trade_goods = fish
	discovered_by = chinese
} # Shakushain's Revolt put down
1739.1.1   = { discovered_by = RUS } # Martin Spanberg
1787.1.1   = { discovered_by = FRA } # Jean-Francois de La P�rouse
