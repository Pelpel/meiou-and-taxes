# No previous file for Par�

culture = tupinamba
religion = pantheism
capital = "Par�"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 5
native_ferocity = 1
native_hostileness = 6

1500.1.1  = { discovered_by = CAS discovered_by = POR } # Pinz�n, Pedro �lvares Cabral 
1516.1.23 = { discovered_by = SPA }
1635.12.24 = {	owner = POR
		controller = POR
		citysize = 1060
		culture = portugese
		religion = catholic
				capital = "Cameta"
		trade_goods = sugar
	    }
1641.1.1  = { add_core = POR }
1750.1.1   = {
	add_core = BRZ
	culture = brazilian
}
1758.1.24 = { capital = "Portel" }
1822.9.7   = {
	owner = BRZ
	controller = BRZ
}
1825.1.1   = {
	remove_core = POR
}
