# 2392 - Zahumje

owner = HUM
controller = HUM
culture = bosnian
religion = gnostic
capital = "Mostar"
trade_goods = wool
hre = no
base_tax = 4
base_production = 4
base_manpower = 3
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1300.1.1 = { road_network = yes }
1356.1.1  = {
	add_core = HUM
}
1373.1.1   = {
	owner = BOS
	controller = BOS
	add_core = BOS
}
1441.1.1 = { 
	owner = HUM
	controller = HUM
	add_core = HUM
	remove_core = BOS
}
1444.1.1 = {
	add_core = TUR
}
1482.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = HUM
	add_core = BOS
	culture = bosnian
} # The Ottoman province of Hezergovina
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1550.1.1   = {
	
}
1650.1.1   = {
	culture = bosnian
	religion = sunni
}
1700.1.1   = {
	
	unrest = 7
}
