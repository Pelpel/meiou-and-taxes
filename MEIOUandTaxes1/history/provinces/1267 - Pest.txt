# 1267 - Pest

owner = HUN
controller = HUN
culture = hungarian
religion = catholic
capital = "Pest"
base_tax = 8
base_production = 8
base_manpower = 4
is_city = yes
fort_14th = yes
trade_goods = linen
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech
hre = no

1000.1.1 = {
	add_permanent_province_modifier = {
		name = minor_inland_center_of_trade_modifier
		duration = -1
	}
}
1100.1.1 = { marketplace = yes }
#1111.1.1 = { post_system = yes }
1119.1.1 = { bailiff = yes }
1200.1.1 = { road_network = yes }
1250.1.1 = { temple = yes }

1356.1.1  = {
	add_core = HUN
	add_permanent_province_modifier = {
		name = hungarian_estates
		duration = -1
	}
}
1526.8.30  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	add_permanent_claim = HAB
	fort_14th = yes
}
1541.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
}
1686.1.1 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = TUR
}
1703.8.1   = {
	controller = TRA
} # The town supports Francis II R�k�czi in his rebellion against the Habsburgs
1711.5.1   = {	controller = HAB } # Ceded to Austria The treaty of Szatmar, Austrian governors replaced the prince of Transylvania
1813.1.1  = {
	unrest = 3
} # Croatian national revival (Hrvatski narodni preporod)
