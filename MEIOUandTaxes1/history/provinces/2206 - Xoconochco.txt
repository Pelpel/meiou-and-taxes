# 2206 - Xoconochco
# GG - 28/07/2008
# Mesoamerican mod v1

owner = XOC
controller = XOC
add_core = XOC
culture = mamean
religion = mesoamerican_religion
capital = "Xoconochco" 

base_tax = 7
base_production = 7
#base_manpower = 2.0
base_manpower = 4.0
citysize = 20000
trade_goods = cacao




discovered_by = mesoamerican

hre = no

1356.1.1 = {
}
1487.1.1   = {
	owner = AZT
	controller = AZT
	citysize = 10000
	base_tax = 6
base_production = 6
#	base_manpower = 1.0
	base_manpower = 2.0
	trade_goods = maize
} # Conquest by Ahuitzotl, eighth tlatoani of Tenochtitlan
1506.1.1   = {
	add_core = AZT
}
1519.3.13  = {
	discovered_by = SPA
} # Hern�n Cort�s
1521.8.13  = {
	owner = SPA
	controller = SPA
	add_core = SPA
	citysize = 2000
	naval_arsenal = yes
	marketplace = yes
	bailiff = yes
} #Fall of Tenochtitlan
1546.1.1   = {
	add_core = SPA
}
1571.1.1   = {
	religion = catholic
}
1750.1.1   = {
	add_core = MEX
}
1810.9.16  = {
	owner = MEX
} # Declaration of Independence
1821.8.24  = {
	controller = MEX
	remove_core = SPA
} # Treaty of Cordba
1832.10.1  = {
	unrest = 3
} # Convention of 1832
1833.4.1   = {
	unrest = 6
} # Convention of 1833
