# 1063 - Angara

owner = YUA
controller = YUA
culture = buryat
religion = tengri_pagan_reformed
capital = "Angara"
trade_goods = fur
hre = no
is_city = yes
base_tax = 1
base_production = 1
base_manpower = 1
discovered_by = steppestech

1356.1.1 = {
	add_core = YUA
}
1392.1.1 = {
	owner = BRT
	controller = BRT
	add_core = BRT
	remove_core = YUA
}
1632.1.1  = { discovered_by = RUS }
1632.9.25 = {
	owner = RUS
	controller = RUS
   	religion = orthodox
   	culture = russian
	capital = "Bratsk"
}
1657.1.1 = {
	add_core = RUS
}
