# 1084 - Manjimun

culture = aboriginal
religion = polynesian_religion
capital = "Manjimun"
hre = no
trade_goods = unknown
native_size = 5
native_ferocity = 0.5
native_hostileness = 9

1625.1.1 = {
	discovered_by = NED
} # Dutch navigator Willem Janszoon
