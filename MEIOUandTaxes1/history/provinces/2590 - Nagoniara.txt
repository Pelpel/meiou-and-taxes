# 2590 - Nagoniara

culture = polynesian
religion = polynesian_religion
capital = "Nagoniara"
trade_goods = unknown # fish
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 20
native_ferocity = 2
native_hostileness = 9

1568.1.1 = { discovered_by = SPA } # Pedro Sarmiento de Gamboa
