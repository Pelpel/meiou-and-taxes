# V�stmanland
# MEIOU - Gigau

add_core = SWE
owner = SWE
controller = SWE
culture = swedish
religion = catholic
hre = no
base_tax = 2
base_production = 2
trade_goods = wax
base_manpower = 1
is_city = yes
capital = "V�ster�s"
discovered_by = western
discovered_by = eastern

1250.1.1 = { temple = yes }
1515.1.1 = { training_fields = yes }
1519.1.1 = { bailiff = yes }
1524.1.1   = { fort_14th = yes }
1527.6.1   = { religion = protestant}
1529.12.17 = { merchant_guild = yes }
1560.1.1   = {  }
 # Due to the support of manufactories
