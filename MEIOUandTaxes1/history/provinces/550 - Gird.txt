# 550 - Gird

owner = GWA
controller = GWA
culture = tomara
religion = hinduism
capital = "Gwalior"
trade_goods = cloth
hre = no
base_tax = 9
base_production = 9
base_manpower = 6
is_city = yes
fort_14th = yes
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech 
discovered_by = turkishtech

1100.1.1 = { marketplace = yes }
#1180.1.1 = { post_system = yes }
1199.1.1 = { road_network = yes }
1250.1.1 = { temple = yes }
1356.1.1  = {
	add_core = GWA
	fort_14th = yes
	add_permanent_province_modifier = {
		name = gwalior_fort
		duration = -1
	}
}
1443.1.1 = { 
	fort_14th = no
	fort_15th = yes }
1518.5.1 = {
	owner = DLH
	add_core = DLH
} # Conquered by Delhi
1526.4.1  = {
	controller = TIM
} #Conquered by Babur
1526.4.21 = {
	owner = GWA
	controller = GWA
} # Panipat
1527.1.1 = {
	owner = MUG
	controller = MUG
	add_core = MUG
} # Conquered by Mughals
1528.1.1 = {
	#owner = GWA
	#controller = GWA
	#add_core = GWA
	remove_core = DLH
}
1530.1.2 = { add_core = TRT }
1540.1.1 = {
	owner = GWA
	controller = GWA
} # Sher Shah Conquers Delhi
1543.1.1 = {
	owner = BNG
	controller = BNG
	fort_15th = no fort_16th = yes
} # Suri Expansion
1553.1.1 = {
	owner = DLH
	controller = DLH
	remove_core = BNG
 } #Death of Islam Shah Sur, Suri empire split
1558.1.1  = { controller = MUG }	#Gwalior surrenders after almost a two year seige
1558.2.1  = { owner = MUG  }
1690.1.1  = { discovered_by = ENG }
1707.5.12 = { discovered_by = GBR }
1737.1.1  = {
	owner = GWA
	controller = GWA
	add_core = GWA
	remove_core = MUG
}
