# 3353 - Shima

owner = KTK #During Muromachi, Ise owner also owned Shima
controller = KTK
culture = kansai
religion = mahayana
capital = "Toba"
trade_goods = fish
hre = no
is_city = yes # citysize = 1500
base_tax = 2
base_production = 2
base_manpower = 2
discovered_by = chinese

1356.1.1 = {
	add_core = NIK
	add_core = KTK
}
1360.1.1 = {
	add_core = TKI
	owner = TKI
	controller = TKI
}
1366.1.1 = {
	controller = NIK
	owner = NIK
}
1367.1.1 = {
	add_core = HKW
	owner = HKW
	controller = HKW
}
1377.1.1 = {
	owner = YMN
	controller = YMN
}
1379.1.1 = {
	owner = TKI
	controller = TKI
}
1389.1.1 = {
	owner = NIK
	controller = NIK
}
1391.1.1 = {
	owner = TKI
	controller = TKI
}
1393.1.1 = {
	owner = NIK
	controller = NIK
}
1400.1.1 = {
	owner = TKI
	controller = TKI
}
1424.1.1 = {
	owner = HKY
	controller = HKY
}
1428.1.1 = {
	owner = TKI
	controller = TKI
}
1440.1.1 = {
	add_core = ISK
	owner = ISK
	controller = ISK
}
1467.1.1 = {
	owner = TKI
	controller = TKI
}
1468.1.1 = {
	add_core = KTK
	owner = KTK
	controller = KTK
}
1477.1.1 = {
	owner = ISK
	controller = ISK
}
1508.1.1 = {
	owner = KTK
	controller = KTK
}
1525.1.1 = {
	owner = KTK
	controller = KTK
}
1542.1.1 = { discovered_by = POR }
1572.1.1 = {
	owner = ODA
	controller = ODA
}
1583.1.1 = {
	owner = TGW
	controller = TGW
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
