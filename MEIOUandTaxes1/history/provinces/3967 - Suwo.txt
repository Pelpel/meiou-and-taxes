# 3364 - Suwo

owner = OUC
controller = OUC
culture = chugoku
religion = mahayana
capital = "Yamaguchi"
trade_goods = rice
hre = no
base_tax = 10
base_production = 10
base_manpower = 6
is_city = yes
discovered_by = chinese

1133.1.1 = { mill = yes }
1356.1.1 = {
	add_core = OUC
}
1542.1.1 = { discovered_by = POR }
1557.1.1 = {
	add_core = MRI
	owner = MRI
	controller = MRI
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
