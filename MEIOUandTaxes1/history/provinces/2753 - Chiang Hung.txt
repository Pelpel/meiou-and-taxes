# 2753 - Chiang Hung

owner = DLI
controller = DLI
add_core = DLI
culture = lanna
religion = buddhism
capital = "Chiang Hung"

base_tax = 4
base_production = 4
#base_manpower = 1.0
base_manpower = 2.0
citysize = 10000
trade_goods = lumber



discovered_by = chinese
discovered_by = indian

hre = no

1000.1.1 = {
}
1253.1.1 = { owner = YUA controller = YUA }
1274.1.1 = { add_core = YUA } 			#creation of yunan province
1350.1.1 = { owner = DLI controller = DLI }
1383.1.1 = { 
	owner = MNG 
	controller = MNG
	add_core = MNG
	remove_core = YUA
}
1529.3.17 = { 
	marketplace = yes
	bailiff = yes
	courthouse = yes
}
1655.1.1 = {
	owner = ZOU
	controller = ZOU
	add_core = ZOU
	remove_core = MNG
}# Wu Sangui appointed as viceroy
1673.11.1 = {
	add_core = QNG
} # Wu Sangui revolt, core given to Qing for reconquering
1681.10.1 = {
	owner = QNG
	controller = QNG
	remove_core = ZOU
}
