# 787 - Bio Bio

culture = mapuche
religion = pantheism
capital = "Anacongua"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 10
native_ferocity = 1
native_hostileness = 5
discovered_by = south_american

1500.1.1  = {
	owner = INC
	controller = INC
	revolt = { type = nationalist_rebels size = 0 }
	unrest = 5
}
1520.1.1   = {
	discovered_by = SPA
} # Discovered by Ferdinand Magellan
1529.1.1 = {
	owner = CZC
	controller = CZC
	add_core = QUI
	add_core = CZC
	remove_core = INC
	bailiff = yes
	constable = yes
	marketplace = yes
}
1535.1.1  = {
	owner = SPA
	controller = SPA
	change_province_name = "Concepción"
	rename_capital = "Concepción"
	citysize = 2000
	culture = castillian
	religion = catholic
	trade_goods = fish
	revolt = { type = nationalist_rebels size = 0 }
	unrest = 9
}
1555.1.1   = {
	revolt = { }
	unrest = 0
}	
1599.1.1   = {
	revolt = { type = nationalist_rebels size = 0 }
	unrest = 9
}
1604.1.1   = {
	revolt = { }
	unrest = 0
}	
1650.1.1   = {
	citysize = 3500
}
1655.1.1   = {
	revolt = { type = nationalist_rebels size = 0 }
	unrest = 9
}
1660.1.1   = {
	revolt = { }
	unrest = 0
}	
1700.1.1   = {
	citysize = 5000
}
1723.1.1   = {
	revolt = { type = nationalist_rebels size = 0 }
	unrest = 9
}
1724.1.1   = {
	revolt = { }
	unrest = 0
}	
1750.1.1  = {
	citysize = 7500
	add_core = CHL
	culture = platean
}
1759.1.1   = {
	revolt = { type = nationalist_rebels size = 0 }
	unrest = 9
}
1766.1.1   = {
	revolt = { }
	unrest = 0
}	
1800.1.1   = {
	citysize = 10000
}
1810.9.18  = {
	owner = CHL
	controller = CHL
}
1818.2.12  = {
	remove_core = SPA
}
