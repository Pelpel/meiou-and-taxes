# No previous file for Jeddah

owner = HEJ
controller = HEJ
culture = hejazi
religion = sunni
capital = "Jeddah"
trade_goods = fish
hre = no
base_tax = 6
base_production = 6
base_manpower = 2
is_city = yes
discovered_by = ADA
discovered_by = MKU
discovered_by = KIL
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = indian

1088.1.1 = { dock = yes }
1100.1.1 = { marketplace = yes }
1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1356.1.1 = {
	owner = MAM
	controller = MAM
	add_core = HEJ
	add_core = MAM
}
1516.1.1   = { add_core = TUR }
1517.1.1   = { controller = TUR }
1517.4.13  = { owner = TUR remove_core = MAM } # Conquered by Ottoman troops
#1530.1.1   = {
#	owner = HEJ
#	controller = HEJ
#	add_core = HEJ
#	remove_core = TUR
#}
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1530.1.5 = {
	owner = HEJ
	controller = HEJ
}
1531.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
}
1802.1.1 = {
	owner = NAJ
	controller = NAJ
	add_core = NAJ
} # Incorporated into the First Saudi State
1818.9.9 = {
	owner = TUR
	controller = TUR 
} # The end of the Saudi State
