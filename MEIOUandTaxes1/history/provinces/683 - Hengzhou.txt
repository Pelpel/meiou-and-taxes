# 683 - Huguang Beihu

owner = YUA
controller = YUA
culture = miao
religion = animism
capital = "Xiangtan"
trade_goods = lumber
hre = no
base_tax = 9
base_production = 9
base_manpower = 3
is_city = yes


discovered_by = chinese
discovered_by = steppestech

0985.1.1 = {
	owner = SNG
	controller = SNG
	add_core = SNG
}
1200.1.1 = { paved_road_network = yes }
1276.1.1 = {
	owner = YUA
	controller = YUA
	add_core = YUA
}
1320.1.1 = {
	remove_core = SNG
}
#1356.1.1 = {	remove_core = YUA } # Red Turbans
1368.1.1 = {
	owner = MNG
	controller = MNG
	add_core = MNG
}
1513.1.1 = { unrest = 5 } # Peasant rebellion, Jiangxi
1514.1.1 = { unrest = 0 }
1529.3.17 = { 
	marketplace = yes
	courthouse = yes
	bailiff = yes
}

1662.1.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = MNG
} # The Qing Dynasty