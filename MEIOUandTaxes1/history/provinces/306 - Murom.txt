# 306 - Murom
# MEIOU-GG - Turko-Mongol mod

owner = MUR
controller = MUR
culture = russian
religion = orthodox
capital = "Murom"
base_tax = 5
base_production = 5
base_manpower = 3
is_city = yes
trade_goods = wool
discovered_by = eastern
discovered_by = western
discovered_by = steppestech
discovered_by = turkishtech
discovered_by = muslim
hre = no

1250.1.1 = { temple = yes }
1356.1.1  = {
	add_core = MUR
}
1382.1.1  = {
	remove_core = WHI
}
1392.1.1  = {
	owner = MOS
	controller = MOS
	add_core = MOS
	remove_core = MUR
}
1444.1.1 = {
	remove_core = GOL
	remove_core = BLU
	remove_core = WHI
}
1480.1.1 = {
	remove_core = GOL
} # Final destruction of the Golden Horde
1511.1.1   = { fort_14th = yes  }
1530.1.4  = {
	bailiff = yes	
}
1547.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia
1598.1.1   = { unrest = 5 } # "Time of troubles"
1610.9.27  = { add_core = POL } # Polish-Lituanian occupation
1612.1.1   = { remove_core = POL } # Knyaz Dmitry Pozharsky expelles the Polish troops.
1613.1.1   = { unrest = 0 } # Order returned, Romanov dynasty
 # Archangel cathedral
1670.1.1   = { unrest = 8 } # Stepan Razin
1671.1.1   = { unrest = 0 } # Razin is captured
1721.10.22 = {  } # Governmental reforms and the absolutism
1773.1.1   = { unrest = 5 } # Emelian Pugachev, Cossack insurrection
1774.9.14  = { unrest = 0 } # Pugachev is captured
