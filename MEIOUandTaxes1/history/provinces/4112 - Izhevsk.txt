# 4112 - Izhevsk

owner = WHI
controller = WHI
culture = tartar
religion = sunni 
capital = "Izhevsk"
base_tax = 3
base_production = 3 
base_manpower = 2
is_city = yes
trade_goods = wool
discovered_by = steppestech
discovered_by = turkishtech
discovered_by = muslim
hre = no

1356.1.1 = {
	unrest = 3
	add_core = WHI
	add_core = KAZ
	add_core = BLU
	add_core = NOG
}
1382.1.1   = {
	owner = GOL
	controller = GOL
	add_core = GOL
	remove_core = WHI
	unrest = 0
}
1438.1.1  = {
	owner = KAZ
	controller = KAZ   
	culture = kazani
}
1444.1.1 = {
	remove_core = GOL
	remove_core = BLU
	remove_core = WHI
	remove_core = NOG
}
1502.1.1 = {
	remove_core = GOL
} # Final destruction of the Golden Horde
1530.1.4  = {
	bailiff = yes	
}
1557.1.1   = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = NOG
} # Conquest of the Khanante by Ivan Grozny
1600.1.1   = {
	culture = russian
	religion = orthodox
}
