# 3993 - Rzhev

owner = SMO
controller = SMO   
culture = russian
religion = orthodox
hre = no
base_tax = 4
base_production = 4
trade_goods = wheat
base_manpower = 2
is_city = yes
capital = "Rzhev"
discovered_by = western
discovered_by = eastern
discovered_by = steppestech
discovered_by = muslim

#1133.1.1 = { mill = yes }
1356.1.1  = {
	add_core = SMO
	add_permanent_claim = LIT
}
1404.1.1  = {
	owner = LIT
	controller = LIT
	add_core = LIT
}

1514.1.1  = {
	owner = MOS
	controller = MOS
	add_core = MOS
	bailiff = yes
	farm_estate = yes
}
1523.8.16 = { mill = yes }
1547.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia
1550.1.1  = { remove_core = SMO }
1569.7.4  = { add_core = PLC } # Polish-Lithuanian Commonwealth
1610.9.27 = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Polish-Lithuanian occupation
1654.10.3 = { controller = RUS } # Reqonquered
1667.1.30 = {
	owner = RUS
	controller = RUS
	remove_core = PLC
} # Truce of Andrusovo
1708.1.1  = {  } # Valued by the tsars as a key fortress
1812.6.28  = { controller = FRA } # Occupied by French troops
1812.12.10 = { controller = RUS }
