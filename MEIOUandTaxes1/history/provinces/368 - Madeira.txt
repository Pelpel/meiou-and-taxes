# 368 - Madeira

culture = portugese
religion = catholic
capital = "Madeira"
trade_goods = sugar
hre = no
base_tax = 1
native_size = 0
native_ferocity = 0
native_hostileness = 0

1419.1.1  = {
	discovered_by = POR
	owner = POR
	controller = POR
	citysize = 500
	is_city = yes
	capital = "Porto Santo"
}
1444.1.1  = {
	add_core = POR
}
1493.1.1 = { temple = yes }
1497.1.1  = {
	capital = "Funchal"
	naval_arsenal = yes
	marketplace = yes 
}
1500.1.1  = {
	citysize = 1500
}
1600.1.1  = {
	citysize = 10000
}
1640.12.1 = {
	citysize = 20000
	trade_goods = wine
} # Sugar production shifted to Brazil
