# 526 - Malwa

owner = MLW
controller = MLW
culture = malvi
religion = hinduism
capital = "Ujjain"
trade_goods = cloth
hre = no
base_tax = 5
base_production = 5
base_manpower = 4
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

1250.1.1 = { temple = yes }
1356.1.1 = {
	#add_core = DLH
	add_core = MLW
	unrest = 3
}
1401.1.1 = {
	owner = MLW
	controller = MLW
	remove_core = DLH
	unrest = 0
}
1498.1.1 = { discovered_by = POR }
1530.1.1 = {
#	owner = MEW
#	controller = MEW
#	add_core = MEW
}
1530.1.1 = { add_claim = GUJ }
1530.2.3 = {
	add_permanent_claim = MUG
}
1531.1.1 = {
	controller = GUJ
} # Conquered by Ahmad Shahis
1532.1.1 = {
	owner = GUJ
} # Conquered by Ahmad Shahis
1535.1.1 = {
	controller = MUG
} # Mughal campaign
1537.1.1 = {
	owner = MLW
	controller = MLW
} # Reverts to Gujarati control and granted independence
1543.1.1 = {
	owner = BNG
	controller = BNG
} # Conquered by Sher Shah Sur
1545.5.22 = {
	owner = MLW
	controller = MLW
} # Death of Sher Shah, governor declares independence
1561.3.1 = { controller = MUG } #Pir Mohammad Khan
1561.8.1 = { controller = MLW } #Baz Bahadur regains controll as Pir Mohammad Khan is recalled
1562.5.1 = { controller = MUG } #Reconquered by Akbar
1562.6.1 = { owner = MUG } # Annexed by Mughal Empire
1564.1.1 = { controller = REB revolt = { type = noble_rebels } }	#Revolt of Uzbek commanders in Malwa
1565.1.1 = { controller = MUG revolt = { } }	#Revolt beaten down
1612.6.1 = { add_core = MUG }
1741.1.1 = { controller = MAR }	#Maratha expansion
1743.1.1 = {
	owner = BHO
	controller = BHO
	add_core = BHO
	remove_core = MUG
} # Marathas (Bhonsle)
1818.6.3 = {
	owner = GBR
	controller = GBR
	remove_core = MAR
}
