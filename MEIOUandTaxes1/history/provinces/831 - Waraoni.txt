# 831 - Guajiro

culture = guajiro
religion = pantheism
capital = "Warao"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 50
native_ferocity = 2
native_hostileness = 7

1498.1.1   = { discovered_by = CAS } # Christopher Columbus
1516.1.23  = { discovered_by = SPA }
1527.7.26  = {
	owner = SPA
	controller = SPA
	citysize = 500
	culture = castillian
	religion = catholic
	change_province_name = "Nueva Segovia"
	rename_capital = "Barquisimeto"
	base_tax = 3
base_production = 3
	trade_goods = coffee
	set_province_flag = trade_good_set
}
1592.1.1   = { add_core = SPA citysize = 1500 }
1750.1.1   = {
	add_core = VNZ
	culture = colombian
}
1811.7.5   = {
	owner = VNZ
	controller = VNZ
} # Venezuela declared its independence
1812.7.25  = {
	owner = SPA
	controller = SPA
} # The revolutionary army is defeated
1813.8.7   = {
	owner = VNZ
	controller = VNZ 
} # The Second Republic of Venezuela is established
1814.1.1   = {
	owner = SPA
	controller = SPA
} # The end of the second republic
1819.12.17 = {
	owner = COL
	controller = COL
	add_core = COL
	remove_core = SPA
} # The establishment of Gran Colombia
1830.1.13  = {
	owner = VNZ
	controller = VNZ
	add_core = VNZ
}
1831.11.19 = {
	remove_core = COL
} #Gran Colombia dismantled
