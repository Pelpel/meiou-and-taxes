# 4005 - Decapolis

owner = DCP
controller = DCP 
capital = "H�wenau"
culture = rhine_alemanisch
religion = catholic
hre = yes
base_tax = 7
base_production = 7
trade_goods = wine
base_manpower = 2
is_city = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1356.1.1   = {
	add_core = DCP
}
1500.1.1 = { road_network = yes }
1648.10.24 = {
	add_core = FRA
	owner = FRA
	controller = FRA
}
1679.1.26  = {
	hre = no
}
