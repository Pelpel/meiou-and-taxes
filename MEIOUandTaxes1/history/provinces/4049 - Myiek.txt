# 4049 - Myiek

owner = AYU
controller = AYU
culture = monic
religion = buddhism
capital = "Marit"
base_tax = 3
base_production = 3
base_manpower = 1
is_city = yes
trade_goods = ebony
discovered_by = chinese
discovered_by = indian
discovered_by = muslim
hre = no

1356.1.1  = {
	add_core = AYU
}
1535.1.1  = { discovered_by = POR }
1767.4.8  = {
	owner = SIA
	controller = SIA
    add_core = SIA
	remove_core = AYU
	unrest = 0
}
1793.1.1  = {
	owner = BRM
	controller = BRM
	add_core = BRM
	rename_capital = "Myeik" 
	change_province_name = "Myeik"
}
1826.2.24 = {
	owner = GBR
	controller = GBR
	add_core = GBR
}
