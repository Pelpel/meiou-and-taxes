# 3931 - Takeo

owner = KHM
controller = KHM
add_core = KHM
culture = khmer
religion = buddhism
capital = "Takeo"
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
trade_goods = rice
discovered_by = chinese
discovered_by = indian
discovered_by = muslim
hre = no

1535.1.1 = { discovered_by = POR }

1698.1.1 = {
	owner = ANN
	controller = ANN
	add_core = ANN
} # Vietnamese control
1862.6.5 = { 
	owner = FRA
	controller = FRA
}
