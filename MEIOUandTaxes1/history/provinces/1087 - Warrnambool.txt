# 1087 - Warnambool

culture = aboriginal
religion = polynesian_religion
capital = "Warnambool"
hre = no
trade_goods = unknown #gold
native_size = 10
native_ferocity = 0.5
native_hostileness = 9

#1770.1.1 = { discovered_by = GBR } # Captain James Cook
#FB - my sources indicate this is farther west than Cook came
