# No previous file for Thaungdut

owner = AVA
controller = AVA
culture = shan
religion = buddhism
capital = "Thaungdut"
trade_goods = tea
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes
discovered_by = chinese
discovered_by = indian
discovered_by = steppestech

1356.1.1 = {
	#add_core = ARK
	add_core = AVA
	add_core = MYA
	add_core = PEG
}
1527.1.1 = {
	owner = MYA
	controller = MYA
	#add_core = SST
	remove_core = AVA
}
1530.1.1 = {
	add_core = TAU
	remove_core = AVA
	remove_core = MYA
	remove_core = PEG
	unrest = 8
}
1554.1.1 = { add_core = TAU }
1555.1.22 = { controller = TAU }
1563.4.1 = {	
	owner = TAU
	remove_core = SST
}
1730.1.1 = { add_core = MLB } #Manipur kingdom
1752.2.28 = {
	owner = BRM
	controller = BRM
	add_core = BRM
	remove_core = TAU
	remove_core = AVA
	remove_core = PEG
	remove_core = SST
} # Annexed by Burma
1885.1.1 = {		
	owner = GBR
	controller = GBR
	unrest = 8
}
1896.1.1 = { unrest = 0 }
