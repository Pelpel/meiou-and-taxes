# 835 - Cueva

culture = muisca
religion = pantheism
capital = "Cueva"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 20
native_ferocity = 1
native_hostileness = 8
discovered_by = south_american

1000.1.1 = {
	add_permanent_province_modifier = {
		name = center_of_trade_modifier
		duration = -1
	}
}
1498.1.1   = {
	discovered_by = CAS
} # Christopher Columbus's last voyage
1509.1.1   = {
	owner = CAS
	controller = CAS
	religion = catholic
	culture = castillian
	citysize = 560
	change_province_name = "Panama"
	rename_capital = "Panama"
} 
1516.1.23  = {
	discovered_by = SPA
	owner = SPA
	controller = SPA
}
1530.1.1   = {
	
} # Panama became a marketplace for goods, contraband & slaves 
1534.1.1   = {
	add_core = SPA
}
1550.1.1   = {
	citysize = 4540
}  
1600.1.1   = {
	citysize = 8705
	} # Fortified to prevent raids 
1650.1.1   = {
	citysize = 9870
} 
1671.1.1   = {
	unrest = 7
} # Henry Morgan, believing England to still be at war with Spain took the city of Panama 
1671.6.6   = {
	unrest = 0
} # Morgan is arrested and Panama returned to Spain 
1750.1.1   = {
	add_core = COL
	culture = colombian
}
1810.7.20  = {
	owner = COL
	controller = COL
} # Colombia declares independence
1819.8.7   = {
	remove_core = SPA
} # Colombia's independence is recongnized

# 1831.11.19 - Grand Colombia is dissolved

# 1903.11.3 - Independence of Panama
