# 1516 - Saoura

owner = TLE
controller = TLE 
culture = berber
religion = sunni
capital = "A�n Sefra"
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
trade_goods = salt
discovered_by = muslim
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1337.1.1 = {
	owner = FEZ
	controller = FEZ
	add_core = TLE
}
1348.1.1 = {
	owner = TLE
	controller = TLE
}

1530.1.1 = {
	add_permanent_claim = ALG
}
1554.1.1 = {
	owner = ALG
	controller = ALG
	add_core = ALG
} # Estimated
1659.1.1 = { controller = REB } # Janissary revolt
1660.1.1 = { controller = TLE } # Estimated
1672.1.1 = {
	owner = ALG
	controller = ALG
	add_core = ALG
} # Estimated
1845.1.1 = {
	owner = FRA
	controller = FRA
}
