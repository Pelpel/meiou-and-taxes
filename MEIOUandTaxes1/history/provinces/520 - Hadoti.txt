# 520 - Hadoti

owner = HDT
controller = HDT
culture = harauti
religion = hinduism
capital = "Kota"
trade_goods = cotton
hre = no
base_tax = 5
base_production = 5
base_manpower = 3
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1100.1.1 = { plantations = yes }
1356.1.1  = {
	add_core = HDT
	#add_core = MEW
}
1455.1.1  = {
	controller = MLW
} # Conquest by Malwa
1456.1.1  = {
	owner = MLW
} # Conquest by Malwa
1498.1.1  = { discovered_by = POR }
1515.12.17 = { training_fields = yes }
1530.1.1 = {
	#controller = HDT
	#owner = HDT
	unrest = 10
}	
1530.1.2 = { 
	add_permanent_claim = MUG
}
1530.3.17 = {
	bailiff = yes
	marketplace = yes
	road_network = yes
}
1531.1.1  = {
	controller = REB
	revolt = { type = nationalist_rebels size = 3 }
}
1532.1.1  = {
	controller = MEW
	owner = MEW
	revolt = { }
}
1543.1.1  = {
	owner = BNG
	controller = BNG
} # Sur expansion
1553.1.1  = {
	owner = HDT
	controller = HDT
} # Independent
1569.3.1  = { controller = MUG } # Falls to Akbar
1569.5.1  = { owner = MUG } # Annexed by Mughals
1619.5.1  = { add_core = MUG }
1690.1.1  = { discovered_by = ENG }
1707.5.12 = { discovered_by = GBR }
1741.1.1  = { controller = GWA }	#Maratha expansion
1743.1.1  = {
	owner = GWA
	add_core = GWA
	remove_core = MUG
} # Marathas
1817.1.1  = {
	owner = HDT
	controller = HDT
	remove_core = GWA
} # Independent (british dependency)
