# 2931 - Tenere

culture = tuareg		
religion = sunni		 
capital = "Fachi"

native_size = 60
native_ferocity = 4.5
native_hostileness = 9
trade_goods = wool
hre = no
discovered_by = muslim
discovered_by = soudantech
discovered_by = sub_saharan
1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
