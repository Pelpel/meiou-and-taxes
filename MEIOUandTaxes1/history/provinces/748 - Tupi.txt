#748 - Tupi

culture = tupinamba
religion = pantheism
capital = "Tupi"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 25
native_ferocity = 1
native_hostileness = 6

1000.1.1   = {
	add_permanent_province_modifier = {
		name = amazonas_estuary_modifier
		duration = -1
	}
}
1500.1.1  = {
	discovered_by = CAS
	discovered_by = POR
	add_permanent_claim = POR
} # Pinz�n, Pedro �lvares Cabral 
1516.1.23 = {
	discovered_by = SPA
}
1616.1.1 = {
	owner = POR
	controller = POR
	citysize = 1560
	culture = portugese
	religion = catholic
	trade_goods = sugar
	change_province_name = "Gr�o Par�"
	rename_capital = "Bel�m"
}
1750.1.1   = {
	add_core = BRZ
	culture = brazilian
}
1758.1.1   = {
	}
1822.9.7   = {
	owner = BRZ
	controller = BRZ
}
1825.1.1   = {
	remove_core = POR
}
