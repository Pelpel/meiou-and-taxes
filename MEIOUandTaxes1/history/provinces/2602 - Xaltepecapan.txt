# 2602 - Texistepec

# owner = ZAP
# controller = ZAP
# add_core = ZAP
culture = zoque
religion = nahuatl
capital = "Texistepec" 

base_tax = 3
base_production = 3
#base_manpower = 1.0
base_manpower = 2.0
# citysize = 4000
trade_goods = gold

# 

# 
discovered_by = mesoamerican

hre = no

1521.8.13  = {
	discovered_by = SPA
}
1530.1.1   = {
	owner = SPA
	controller = SPA
	add_core = SPA
	capital = "Chiapas de Corzo"
	base_tax = 4
base_production = 4
#	base_manpower = 1.5
	base_manpower = 3.0
	citysize = 2000
	trade_goods = maize
	}
1553.1.1   = {
	add_core = SPA
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
	citysize = 2000
}
1810.9.16  = {
	owner = MEX
} # Declaration of Independence
1821.8.24  = {
	controller = MEX
	remove_core = SPA
} # Treaty of Cordba
1832.10.1  = {
	unrest = 3
} # Convention of 1832
1833.4.1   = {
	unrest = 6
} # Convention of 1833
