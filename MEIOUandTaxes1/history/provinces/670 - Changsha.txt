# 670 - Huguang Tianxin

owner = YUA
controller = YUA
culture = wuhan
religion = confucianism
capital = "Changsha"
trade_goods = tea
hre = no
base_tax = 11
base_production = 11
base_manpower = 5
is_city = yes
fort_14th = yes
discovered_by = chinese
discovered_by = indian
discovered_by = steppestech

0987.1.1 = {
	owner = SNG
	controller = SNG
	add_core = SNG
}
1200.1.1 = { paved_road_network = yes }
1265.1.1 = {
	owner = YUA
	controller = YUA
}
1271.12.18  = {#Proclamation of Yuan dynasty
	add_core = YUA
}
1320.1.1 = {
	remove_core = SNG
}
1356.1.1 = {
#	remove_core = YUA # Red Turbans
}
1365.1.1 = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = DAA
	medieval_university = yes
}
1529.3.17 = { 
	marketplace = yes
	courthouse = yes
	bailiff = yes
	plantations = yes
}
1662.1.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = MNG
} # The Qing Dynasty