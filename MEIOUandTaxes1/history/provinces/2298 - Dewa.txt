# 2298 - Dewa
# GG/LS - Japanese Civil War

owner = DTE
controller = DTE
culture = tohoku
religion = mahayana #shinbutsu
capital = "Yamagata"
trade_goods = fish
hre = no
base_tax = 10
base_production = 10
base_manpower = 6
is_city = yes
discovered_by = chinese

1356.1.1 = {
	add_core = DTE
}
1542.1.1   = { discovered_by = POR }
1630.1.1   = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
