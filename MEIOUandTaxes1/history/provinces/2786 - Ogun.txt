# 2786 - Ogun

owner = BEN
controller = BEN
culture = yorumba
religion = west_african_pagan_reformed
capital = "Eko"
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
trade_goods = slaves

discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1356.1.1 = {
	add_core = BEN
}
1472.1.1 = {
	discovered_by = POR
	# capital = "Lagos"
}
