# 3534 - Hueyacoctlan

owner = MET
controller = MET
add_core = MET
culture = metztitlani
religion = nahuatl
capital = "Hueyacoctlan"

base_tax = 3
base_production = 3
#base_manpower = 1.0
base_manpower = 2.0
citysize = 2500
trade_goods = maize


discovered_by = mesoamerican

hre = no

1530.1.1   = {
	discovered_by = SPA
	owner = SPA
	controller = SPA
	add_core = SPA
	marketplace = yes
	bailiff = yes
	courthouse = yes
	culture = castillian
	religion = catholic
	base_tax = 2
base_production = 2
	trade_goods = gold
} # Francisco V�zquez de Coronado y Luj�n
1583.1.1  = {
	owner = SPA
	controller = SPA
	# capital = "San Luis Potosi"
	citysize = 200
	}
1600.1.1   = {
	citysize = 3000
}
1608.1.1   = {
	add_core = SPA
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
}
1810.9.16  = {
	owner = MEX
	controller = MEX
} # Declaration of Independence
1821.8.24  = {
	remove_core = SPA
} # Treaty of Cord�ba
