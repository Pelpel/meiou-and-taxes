# 3440 - Quillaca

owner = AYG
controller = AYG 
add_core = AYG
culture = aimara
religion = inti
capital = "Quillaca"
trade_goods = wool
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
is_city = yes
discovered_by = south_american

1356.1.1 = {
	add_permanent_province_modifier = {
		name = "coalition_member"
		duration = -1
	}
}
1493.1.1   = {
	owner = INC
	controller = INC
	add_core = INC
	paved_road_network = yes
	bailiff = yes
	constable = yes
	marketplace = yes
}
1529.1.1 = {
	owner = CZC
	controller = CZC
	add_core = QUI
	add_core = CZC
	remove_core = INC
}
1535.1.1  = {
	discovered_by = SPA
	add_core = SPA
	owner = SPA
	controller = SPA
	religion = catholic
	culture = castillian
}
1750.1.1  = {
	add_core = BOL
	culture = peruvian
} # Decline as the mining began to wane
1809.7.16 = {
	owner = BOL
	controller = BOL
} # Bolivian War of Independence
1825.8.6  = {
	remove_core = SPA
}
