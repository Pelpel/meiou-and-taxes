# No previous file for Ka'igwu

culture = arapaho
religion = totemism
capital = "Ka'igwu"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 5
native_ferocity = 4
native_hostileness = 6

1760.1.1  = {	owner = CHY
		controller = CHY
		add_core = CHY
		trade_goods = fur
		culture = cheyenne
		is_city = yes } #Horses allow plain tribes to expand
