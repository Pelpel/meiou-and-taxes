# No previous file for Bursa al Sham

owner = MAM
controller = MAM
culture = shami
religion = sunni
capital = "Bursa"
trade_goods = wheat
hre = no
base_tax = 3
base_production = 3
base_manpower = 1
is_city = yes
fort_14th = yes
discovered_by = CIR
discovered_by = muslim
discovered_by = eastern
discovered_by = western
discovered_by = turkishtech

1356.1.1 = {
	add_core = MAM
	add_core = SYR
}
1516.1.1   = { add_core = TUR }
1516.11.8  = { controller = TUR }
1517.4.13  = { owner = TUR remove_core = MAM } # Conquered by Ottoman troops
1520.1.1 = { unrest = 4 } # Revolt against Ottoman rule
1521.1.1 = { unrest = 0 }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1530.1.5 = {
	owner = SYR
	controller = SYR
	remove_core = TUR
}
1531.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
}
1771.1.1 = { unrest = 4 } # Ali Bey gained control of Syria, reconstituting the Mamluk state
1772.1.1 = { unrest = 0 }
1831.1.1 = {
	controller = EGY
}
1833.6.1 = {
	owner = EGY
}
1841.2.1  = {
	owner = TUR
	controller = TUR
} # Part of the Ottoman Empire
