# No previous file for Yman

owner = MUD
controller = MUD
culture = jurchen
religion = tengri_pagan_reformed
capital = "Yman"
trade_goods = naval_supplies
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = chinese
discovered_by = steppestech


1356.1.1 = {
	add_core = MUD
}
1515.1.1 = { training_fields = yes }
1530.1.1 = { 
	marketplace = yes
	bailiff = yes
}
1616.2.17  = {
	owner = JIN
	controller = JIN
	add_core = JIN
	remove_core = MUD
}
1635.1.1 = {
	culture = manchu
}
1636.5.15 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = JIN
} # The Qing Dynasty
1858.1.1 = {
   	owner = RUS
   	controller = RUS
   	religion = orthodox
   	culture = russian
} # Treaty of Aigun
