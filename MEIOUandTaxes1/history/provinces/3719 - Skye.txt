# 3719 - Eileanan a-Staigh

owner = NOR
controller = NOR
culture = norse_gaelic
religion = catholic
hre = no
base_tax = 1
base_production = 1
trade_goods = wheat
base_manpower = 1
is_city = yes
capital = "Port an Eilein"
discovered_by = western
discovered_by = muslim
discovered_by = eastern

1250.1.1 = { temple = yes }
1266.1.1   = {
	owner = TLI
	controller = TLI
	add_core = TLI
}
1356.1.1 = {
	add_permanent_province_modifier = {
		name = medieval_castle
		duration = -1
	}
}
1493.1.1   = {
	owner = SCO
	controller = SCO
	add_core = SCO
}
1493.5.8   = {
	capital = "Bogh M�r"
}
1560.8.1   = { religion = reformed }
1707.5.12  = {
	owner = GBR
	controller = GBR
	add_core = GBR
}
1750.1.1   = { fort_14th = yes }
