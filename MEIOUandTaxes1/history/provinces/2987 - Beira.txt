# 2987 - Beira

owner = SOF
controller = SOF
culture = kimwani
religion = sunni
capital = "Beira"
base_tax = 5
base_production = 5
base_manpower = 3
is_city = yes
trade_goods = slaves
discovered_by = central_african
discovered_by = east_african
hre = no

1356.1.1  = { add_core = SOF }
1498.3.16 = { discovered_by = POR } #Vasco Da Gama
1505.1.1  = { owner = POR controller = POR add_core = POR
	naval_arsenal = yes
	customs_house = yes 
	marketplace = yes
}
1600.1.1  = { discovered_by = TUR }
