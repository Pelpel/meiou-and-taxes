# 765 - Tupinikim

culture = tupinamba
religion = pantheism
capital = "Tupiniki"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 35
native_ferocity = 1
native_hostileness = 7

1530.1.1   = {
	discovered_by = POR
	add_permanent_claim = POR
} # Pedro �lvares Cabral 
1658.1.1   = {
	owner = POR
	controller = POR
	change_province_name = "S�o Francisco do Sul"
	rename_capital = "S�o Francisco do Sul"
	citysize = 260
	culture = portugese
	religion = catholic
	trade_goods = tobacco
	set_province_flag = trade_good_set
}
1700.1.1   = {
	citysize = 1530
}
1750.1.1   = {
	add_core = BRZ
	culture = brazilian
}
1822.9.7   = {
	owner = BRZ
	controller = BRZ
}
1825.1.1   = {
	remove_core = POR
}
