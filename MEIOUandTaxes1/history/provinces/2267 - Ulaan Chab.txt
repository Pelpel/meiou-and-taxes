owner = YUA
controller = YUA
add_core = YUA 
culture = tumed
religion = tengri_pagan_reformed
capital = "Bayanuur"
trade_goods = lead
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.0
base_manpower = 0.0
citysize = 5235
discovered_by = chinese
discovered_by = steppestech

1392.1.1  = {
	remove_core = YUA
	owner = MNG
	controller = MNG
	religion = tengri_pagan_reformed
}
1435.1.1 = {
	owner = TMD
	controller = TMD
	add_core = TMD
	culture = tumed
} # Ordos Mongols move into Ordos
1515.1.1 = { training_fields = yes }
1586.1.1 = { religion = vajrayana } # State religion
1634.1.1 = {
	owner = MCH
	controller = MCH
} # Part of the Manchu empire
1644.6.6 = {
	owner = QNG
	controller = QNG
	add_core = QNG
}