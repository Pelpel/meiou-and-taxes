# No previous file for Nalgonda

owner = TLG
controller = TLG
culture = telegu
religion = hinduism
capital = "Golconda"
trade_goods = gems
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = indian
discovered_by = muslim

#1111.1.1 = { post_system = yes }
1356.1.1  = {
	add_core = GOC
	add_core = TLG
	fort_14th = yes
}
1365.1.1  = {
	owner = BAH
	controller = BAH
	add_core = GOC
	add_core = TLG
}
1405.1.1  = {
	add_core = BAH
}
1515.12.17 = { training_fields = yes }
1518.1.1  = {
	owner = GOC
	controller = GOC
	add_core = GOC
	remove_core = BAH
	road_network = yes
} # The Breakup of the Bahmani sultanate
1520.1.1 = { marketplace = yes }
1521.1.1 = { bailiff = yes }
1570.1.1 = {
	fort_14th = no
	fort_16th = yes
}
1600.1.1  = { discovered_by = turkishtech }
1687.1.1  = {
	owner = MUG
	controller = MUG
	add_core = MUG
} # Conquered by the mughal emperor Aurangzeb
1707.5.12 = { discovered_by = GBR }
1724.1.1 = {
	owner = HYD
	controller = HYD
	add_core = HYD
	remove_core = MUG
}
