#  - Alashan

owner = YUA
controller = YUA
add_core = YUA
culture = tumed
religion = tengri_pagan_reformed
capital = "Khara Khoto"
trade_goods = subsistence
hre = no
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
discovered_by = chinese
discovered_by = steppestech

1375.1.1 = {
	capital = "Ejina"
}
1392.1.1  = {
	remove_core = YUA
	owner = MNG
	controller = MNG
}
1425.1.1  = {
	owner = TMD
	controller = TMD
	add_core = TMD
}
1515.1.1 = { training_fields = yes }
1586.1.1 = { religion = vajrayana } # State religion
1688.1.1 = {
	owner = ZUN
	controller = ZUN
	culture = oirats
}
1696.1.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
} # Kangxi leads Qing army pushing Zunghars back
