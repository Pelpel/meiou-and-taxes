# No previous file for Empuries

owner = ARA		#Alfons V of Aragon
controller = ARA
culture = catalan
religion = catholic
capital = "Empuries" 
base_manpower = 2
base_tax = 2
base_production = 2
is_city = yes	
trade_goods = wine

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
hre = no

1300.1.1 = { road_network = yes }
1356.1.1   = {
	add_core = ARA
	add_permanent_province_modifier = {
		name = medieval_castle
		duration = -1
	}
}
1462.4.11  = { unrest = 5 } # Catalan nobility revolts against King Joan, later the Remen�a peasant revolt erupts.
1469.1.1   = { controller = ARA } # King Joan takes Girona
1473.1.1   = { unrest = 0 } # census of 1473 (population lost due to the war)
1484.1.1   = { unrest = 3 } # Second Remen�a revolt
1486.4.21  = { unrest = 0 } # King Ferr�n arbit rates a resolution between the revolted peasants and the catalan nobles. 
 # census of 1497 
1516.1.23  = { controller = SPA owner = SPA add_core = SPA
	bailiff = yes
	road_network = no paved_road_network = yes
	add_core = CAT  } #Ferdinand the Catholic dies, Charles inherits Aragon and becomes regent of Castille

 # Opening of the "Taula de Canvi"
1640.6.7   = {
	controller = REB
	unrest = 5
	add_core = CAT
} # Guerra dels Segadors 
1641.1.26  = {
	unrest = 0
	owner = CAT
	controller = CAT
} # Proclamation of the Republic of Catalonia
1641.2.27  = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # Death of Pau Claris i Casademunt, proclamation of Louis XIII de France as count of Barcelona.
1652.10.11 = {
	owner = SPA
	controller = SPA
	remove_core = FRA
} # Barcelona surrenders
1684.3.1   = { controller = FRA } # France defeats the Spanish army in Girona
1684.6.1   = { controller = SPA } # Spain retakes the city
1693.6.1   = { controller = FRA } # France occupies northern Catalonia
1697.9.20  = { controller = SPA } # Peace of Ryswick
1705.10.9  = { controller = HAB } # Catalonia recognises Archduke Carlos as its sovereign, joining the Anglo-Austrian cause in the War of Spanish Succession. The loyalist troops in Barcelona are defeated.
1711.1.25  = { controller = SPA } # French troops take Girona
1713.7.13  = { remove_core = ARA }
1813.8.31  = { controller = REB }
1813.12.11 = { controller = SPA }
