# 1395 - Zachoniaje Palessie

owner = LIT
controller = LIT
culture = ruthenian
religion = orthodox
capital = "Pinsk"
trade_goods = livestock
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech

1356.1.1   = {
	add_core = LIT
	add_permanent_province_modifier = {
		name = lithuanian_estates
		duration = -1
	}
}

1530.1.4  = {
	bailiff = yes	
}
1569.1.1   = { fort_14th = yes }
1569.7.1  = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1795.10.24  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	culture = byelorussian
} #Third Partition
