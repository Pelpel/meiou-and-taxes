# 93 - Pricipaute episcopale de liege , Limbourg
# Liege, Dinant, Maastricht

owner = LIE
controller = LIE
add_core = LIE
culture = wallonian
religion = catholic
capital = "L�dje" #Li�ge
base_tax = 9
base_production = 9
trade_goods = iron
base_manpower = 3
is_city = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim
hre = yes

915.1.1    = {
	add_permanent_province_modifier = {
		name = "abbey_principality_of_stavelot_malmedy"
		duration = -1
	}
}
1250.1.1   = { temple = yes }
1453.1.1   = { add_core = BUR }
1465.1.1   = { unrest = 4 } # Revolt imminent
1465.4.22  = { controller = REB } # Citizens revolt
1465.10.19 = { controller = LIE unrest = 0 } # Peace is restored
1467.1.1   = { owner = BUR controller = BUR unrest = 5 } # Charles the Bold installs Louis de Bourbon
1468.9.1   = { controller = REB } # Citizens rise up against the disliked Louis de Bourbon
1468.9.4   = { controller = BUR } # Charles the Bold sacks Li�ge
1477.1.5   = { owner = LIE controller = LIE remove_core = BUR add_core = HAB unrest = 0 } # Charles the Bold dies and Li�ge is re-established
1492.8.12  = { remove_core = HAB } # Li�ge signs a perpetual treaty of neutrality with Austria (and France)

1500.1.1 = { road_network = yes }
1518.1.1   = { fort_14th = yes }
 # Saint Paul's Cathedral finished
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1579.1.6   = {add_core = EBU}
 # Li�ge was always a centre of arms production

1650.1.1   = { fort_14th = no fort_15th = yes }

1715.1.1   = { fort_15th = no fort_16th = yes }
1750.1.1   = {  }
1789.12.3 = {
	controller = REB
	add_core = EBU
}
1790.1.11  = {
	owner = EBU
	controller = EBU
	remove_core = LIE
}
1791.1.1   = {
	owner = HAB
	controller = HAB
}
1797.12.26 = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # Treaty of Campo Formio
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
1815.3.16  = {
	owner = NED
	controller = NED
	add_core = NED
	remove_core = FRA
} # The United Kingdom of the Netherlands
1830.1.1    = {
	owner = EBU
	controller = EBU
}
