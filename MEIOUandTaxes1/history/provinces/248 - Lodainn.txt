#248 - Lothian

owner = SCO
controller = SCO
culture = lowland_scottish
religion = catholic
hre = no
base_tax = 8
base_production = 8
trade_goods = salt
base_manpower = 3
is_city = yes
capital = "Edinburgh"
add_core = SCO

 #St Giles Cathedral, Edinburgh
discovered_by = western
discovered_by = muslim
discovered_by = eastern
1100.1.1 = { 
	marketplace = yes 
	add_permanent_province_modifier = {
		name = medieval_castle
		duration = -1
	}
}
#1111.1.1 = { post_system = yes }
1119.1.1 = { bailiff = yes }
1250.1.1 = { temple = yes }

1410.6.1   = { medieval_university = yes } #University of St. Andrews
1482.8.1   = { controller = ENG }
1483.3.1   = { controller = SCO }
 #Estimated
 #College of Justice
1530.1.1 = { bailiff = yes }
1547.10.1  = { controller = ENG } #Rough Wooing
1550.1.1   = { controller = SCO } #Scots Evict English with French Aid
1560.1.1   = { fort_14th = yes }
1560.8.1   = {
	religion = reformed
	#reformation_center = reformed
}
1650.12.24 = { controller = ENG } #Cromwell Captures Edinburgh Castle
1652.4.21  = { controller = SCO } #Union of Scotland and the Commonwealth
 #constable, Customs House Estimated
1707.5.12  = {
	owner = GBR
	controller = GBR
	add_core = GBR
}
1750.1.1   = {   } #Regimental Camp, Tax Assessor Estimated
