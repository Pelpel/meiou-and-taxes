# 2716 - Thatta

owner = SND
controller = SND
culture = baluchi
religion = sunni
capital = "Thatta"
trade_goods = cotton
hre = no
base_tax = 9
base_production = 9
base_manpower = 3
is_city = yes

discovered_by = indian
discovered_by = muslim
discovered_by = steppestech 
discovered_by = turkishtech

1000.1.1 = {
	add_permanent_province_modifier = {
		name = center_of_trade_modifier
		duration = -1
	}
	add_permanent_province_modifier = { 
		name = "natural_harbour" 
		duration = -1 
	}
	add_permanent_province_modifier = {
		name = major_city
		duration = -1
	}
}
1115.1.1 = { bailiff = yes }
1120.1.1 = { plantations = yes }
#1180.1.1 = { post_system = yes }
1200.1.1 = { marketplace = yes }
1201.1.1 = { road_network = yes }
1356.1.1  = {
	add_core = SND
	fort_14th = yes
}
1498.1.1 = { discovered_by = POR }
1520.7.1 = {
	controller = KAB
	add_permanent_claim = MUG
} # Arghuns
1521.1.1 = {
	controller = SND
	culture = sindhi
} # Arghuns + tag change
1544.1.1 = {
	owner = BNG
	controller = BNG
} # Sur Expansionism
1545.1.1 = {
	owner = SND
	controller = SND
} # Sher Shah dies
1591.1.1 = {
	owner = MUG
	controller = MUG
	add_core = MUG
} # Integrated into Mughal Empire
1600.1.1 = {
	discovered_by = TUR
	
}
 #Shah Jahan Mosque
1700.1.1 = { 	}
1739.1.1 = {
	owner = SND 
	controller = SND
	remove_core = MUG
}
1839.1.1 = {
	owner = GBR
	controller = GBR
	add_core = GBR
}
