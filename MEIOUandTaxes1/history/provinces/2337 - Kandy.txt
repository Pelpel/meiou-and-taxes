# 2237 - Kandy

owner = KTH
controller = KTH
culture = sinhala
religion = buddhism
capital = "Matakkalappu"
trade_goods = cinnamon
hre = no

base_tax = 5
base_production = 5
base_manpower = 2
is_city = yes
discovered_by = muslim
discovered_by = indian
add_local_autonomy = 25

1356.1.1   = {
	add_core = KTH
}
1505.1.1   = { discovered_by = POR } # Francisco de Almeida
#1517.1.1   = {
#	owner = POR
#	controller = POR
#	capital = "Batticaloa "
#}
#1542.1.1   = { add_core = POR }
1515.1.1 = { training_fields = yes }
1530.3.17 = {
	bailiff = yes
	marketplace = yes
	dock = yes
}
1638.1.1 = {
	fort_17th = yes
}
1660.1.1   = { owner = NED controller = NED remove_core = POR } # Dutch control
1685.1.1   = { add_core = NED }
1799.8.1   = { controller = GBR } # Occupied by England
1802.3.25  = {
	owner = GBR
	add_core = GBR
	remove_core = NED
} # Treaty of Amiens
