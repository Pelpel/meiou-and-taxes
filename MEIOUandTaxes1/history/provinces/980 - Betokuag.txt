# No previous file for Betokuag

culture = beothuk
religion = totemism
capital = "Betokuag"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 5
native_ferocity = 2
native_hostileness = 5

1497.6.24 = { discovered_by = ENG } # John Cabot
1502.1.1  = { discovered_by = POR } # Gaspar, Miguel Corte-Real
1534.1.1  = { discovered_by = FRA } # Jacques Cartier	
1620.1.1  = {
	owner = FRA
	controller = FRA
	citysize = 500
	culture = francien
	religion = catholic
	trade_goods = fish
} # French fishermen dominate the southern coast and northern peninsula
1645.1.1  = { add_core = FRA is_city = yes }
1655.1.1  = {
 	fort_14th = yes 
	capital = "Plaisance"
} #Actually in Beothuk, but was the French capital, so in the French province
1713.4.11 = {
	owner = GBR
	controller = GBR
	capital = "Placentia"
	culture = english
	religion = protestant
	add_core = GBR
	remove_core = FRA
}  #Treaty of Utrecht, France abandon its half of Newfoundland
