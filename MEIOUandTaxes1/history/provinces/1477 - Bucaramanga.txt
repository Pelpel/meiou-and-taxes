# 1477 - Bucaramanga

culture = cariban
religion = pantheism
capital = "Bucaramanga"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 20
native_ferocity = 1
native_hostileness = 2

1502.1.1  = {
	discovered_by = CAS
	owner = CAS
	controller = CAS
	capital = "Santa Cruz de Mompos"
	citysize = 444
	culture = castillian
	religion = catholic
	trade_goods = coffee
	set_province_flag = trade_good_set
}
1516.1.23  = {
	discovered_by = SPA
	owner = SPA
	controller = SPA
}
1527.1.1  = {
	add_core = SPA
}
1600.1.1  = {
	citysize = 1091
}
1650.1.1  = {
	citysize = 1500
}
1700.1.1  = {
	citysize = 2200
}
1750.1.1  = {
	citysize = 3420
	add_core = COL
	culture = colombian
}
1800.1.1  = {
	citysize = 4100
}
1810.7.20  = {
	owner = COL
	controller = COL
} # Colombia declares independence
1819.8.7   = {
	remove_core = SPA
} # Colombia's independence is recongnized
# 1831.11.19 - Grand Colombia is dissolved
