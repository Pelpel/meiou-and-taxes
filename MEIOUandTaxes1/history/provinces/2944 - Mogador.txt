# 1585 - Ifni

owner = SOS
controller = SOS
culture = fassi
religion = sunni
capital = "Mogador"
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
trade_goods = sugar
discovered_by = muslim
discovered_by = western
discovered_by = turkishtech
hre = no

#1088.1.1 = { dock = yes shipyard = yes }
1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1356.1.1 = {
	add_core = SOS
	owner = FEZ
	controller = FEZ
	add_core = FEZ
}
1472.1.1 = {
	owner = SOS
	controller = SOS
	remove_core = FEZ
}
1476.1.1  = {
	owner = CAS
	controller = CAS
	add_core = CAS
	trade_goods = slaves
}
1515.1.1 = { training_fields = yes }
1516.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	remove_core = CAS
}
1524.1.1 = {
	owner = MOR
	controller = MOR
	add_core = MOR
	capital = "Sidi Ifni"
	trade_goods = millet
}
1525.1.1   = {
	owner = FEZ
	controller = FEZ
	add_core = FEZ
}
1530.1.1 = {
	owner = MOR
	controller = MOR
	add_core = MOR
	remove_core = SPA
	remove_core = FEZ
}
1554.1.1   = {
	owner = MOR
	controller = MOR
	add_core = MOR
	remove_core = FEZ
}
1603.8.1 = { unrest = 5 } #Death of al-Mansur, long period of instability follows
1613.1.1 = {
	owner = SOS
	controller = SOS
	unrest = 0
}
1659.1.1 = { unrest = 7 } # The last ruler of Saadi is overthrown
1660.1.1 = { unrest = 3 }
1668.8.2 = {
	owner = MOR
	controller = MOR
}
1672.1.1 = { unrest = 4 } # Oppositions against Ismail, & the idea of a unified state
1727.1.1 = { unrest = 0 }
