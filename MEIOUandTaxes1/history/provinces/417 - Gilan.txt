# 417 - Gilan

owner = MZA
controller = MZA
culture = tabari
religion = sunni #Dei Gratia
capital = "Racht"
trade_goods = silk
hre = no
base_tax = 6
base_production = 6
base_manpower = 2
is_city = yes
discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = steppestech
discovered_by = turkishtech

1356.1.1 = {
	add_core = MZA
}
1357.1.1   = {
	owner = WHI
	controller = WHI
}
1360.1.1   = {
	owner = JAI
	controller = JAI
}
1375.1.1   = {
	owner = MZA
	controller = MZA
}
1444.1.1 = {
	remove_core = JAI
}	
1493.1.1  = {
	owner = SAM
	controller = SAM
	add_core = SAM
	religion = shiite
} #The Safawid Order
1512.1.1  = {
	owner = PER
	controller = PER
	add_core = PER
	remove_core = SAM
	bailiff = yes
	marketplace = yes
} # The Safavids took over
1515.1.1 = { training_fields = yes }

1669.1.1  = { unrest = 3 } # Plundered by Stenka Razin
1720.1.1  = {
	controller = REB
	revolt = { type = nationalist_rebels size = 1 }
}
1730.1.1  = {
	controller = PER
	revolt = { }
	
}
1747.1.1  = {
	controller = REB
	revolt = { type = pretender_rebels size = 1 }
}
1760.1.1  = {
	controller = PER
	revolt = { }
} #Karim Khan
