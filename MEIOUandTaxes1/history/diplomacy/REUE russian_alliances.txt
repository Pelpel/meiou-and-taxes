#Russian vassal - Qasim khanate
#vassal = {
#	first = MOS
#	second = QAS
#	start_date = 1352.1.1
#	end_date = 1503.3.21
#}
#vassal = {
#	first = RUS
#	second = QAS
#	start_date = 1503.3.22
#	end_date = 1681.1.1
#}

# Bellozero vassal of Muscowy
vassal = {
	first = MOS
	second = BOZ
	start_date = 1325.1.1
	end_date = 1486.1.1
}

# Alliance between Pskov and Novgorod
alliance = {
	first = NOV
	second = PSK
	start_date = 1356.1.1
	end_date = 1510.1.24
}

# Muscowy subjugates Great Perm
vassal = {
	first = MOS
	second = PRM
	start_date = 1472.1.1
	end_date = 1505.4.1
}

# Russian vassal
vassal = {
	first = RUS
	second = ZAZ
	start_date = 1654.1.1
	end_date = 1735.1.1
}

# Russian vassal
vassal = {
	first = RUS
	second = KUR
	start_date = 1710.11.11
	end_date = 1795.10.23
}

# Muscovy and Denmark
alliance = {
	first = MOS
	second = DEN
	start_date = 1493.11.4
	end_date = 1503.1.1
}

# Russia and Sweden
alliance = {
	first = RUS
	second = SWE
	start_date = 1567.2.16
	end_date = 1568.9.29 
}

# Russia and Sweden
alliance = {
	first = RUS 
	second = SWE
	start_date = 1609.2.28
	end_date = 1610.7.19
}

# Russia and Poland (the Eternal Peace)
alliance = {
	first = RUS
	second = PLC
	start_date = 1686.5.6
	end_date = 1699.1.26
}

# Anti-Swedish alliance
alliance = {
	first = RUS
	second = PLC
	start_date = 1699.11.22
	end_date = 1706.9.24
}

# Anti-Swedish alliance
alliance = {
	first = RUS
	second = PLC
	start_date = 1709.8.8
	end_date = 1721.1.1
}

# Anti-Swedish alliance
alliance = {
	first = RUS
	second = SAX
	start_date = 1699.11.22
	end_date = 1706.9.24
}

# Anti-Swedish alliance
alliance = {
	first = RUS
	second = SAX
	start_date = 1709.8.8
	end_date = 1721.1.1
}

# Anti-Swedish alliance
alliance = {
	first = RUS
	second = DEN
	start_date = 1699.11.22
	end_date = 1700.8.18
}

# Anti-Swedish alliance
alliance = {
	first = RUS
	second = DEN
	start_date = 1709.10.28
	end_date = 1721.1.1
}

# Anti-Swedish alliance
alliance = {
	first = RUS
	second = PRU
	start_date = 1715.1.1
	end_date = 1721.1.1
}

# Anti-Swedish alliance
alliance = {
	first = RUS
	second = HAN
	start_date = 1715.1.1
	end_date = 1721.1.1
}

# Treaty of Hamburg, Russia & Prussia
alliance = {
	first = RUS
	second = PRU
	start_date = 1762.5.22
	end_date = 1762.7.17
}

# Russia and Prussia
alliance = {
	first = RUS
	second = PRU
	start_date = 1764.4.11
	end_date = 1781.5.1
}

# Treaty of Georgievsk
alliance = {
	first = RUS
	second = GEO
	start_date = 1783.7.24
	end_date = 1801.1.18
}

# The Second Coalition, led by Paul I
alliance = {
	first = RUS
	second = GBR
	start_date = 1798.12.24
	end_date = 1801.10.8
}

# The Second Coalition, Treaty of LunÚville
alliance = {
	first = RUS
	second = HAB
	start_date = 1798.12.24
	end_date = 1801.2.9
}

# The Second Coalition, Treaty of Florence
alliance = {
	first = RUS
	second = SIC
	start_date = 1798.12.24
	end_date = 1801.3.18
}

# The Second Coalition, led by Paul I
alliance = {
	first = RUS
	second = POR
	start_date = 1798.12.24
	end_date = 1801.9.1
}

# The Second Coalition, led by Paul I
alliance = {
	first = RUS
	second = TUR
	start_date = 1798.12.24
	end_date = 1801.2.9
}

# Bavaria joins the Second Coalition, led by Paul I
alliance = {
	first = RUS
	second = BAV
	start_date = 1799.1.1
	end_date = 1801.2.9
}

# Ivan III and Maria of Tver
royal_marriage = {
	first = MOS
	second = TVE
	start_date = 1452.1.1
	end_date = 1467.1.1
}

# Autonomous Grand duchy of Finland
union = {
	first = RUS
	second = FIN
	start_date = 1809.9.17
	end_date = 1917.12.6
}

# Kingdom of Poland after Congress of Vienna (so called Congress Poland)
union = {
	first = RUS
	second = POL
	start_date = 1815.6.9
	end_date = 1916.1.1
}
