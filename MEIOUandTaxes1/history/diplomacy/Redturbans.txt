# Red Turbans

vassal = {
	first = YUA
	second = LNG
	start_date = 1274.1.1
	end_date = 1381.1.1
}

vassal = {
	first = YUA
	second = QIN
	start_date = 1274.1.1
	end_date = 1381.1.1
}

vassal = {
	first = YUA
	second = CYU
	start_date = 1274.1.1
	end_date = 1381.1.1
}

vassal = {
	first = YUA
	second = CEN
	start_date = 1274.1.1
	end_date = 1381.1.1
}

vassal = {
	first = YUA
	second = CSE
	start_date = 1274.1.1
	end_date = 1381.1.1
}

vassal = {
	first = YUA
	second = YEN
	start_date = 1274.1.1
	end_date = 1392.1.1
}

vassal = {
	first = LNG
	second = DLI
	start_date = 1274.1.1
	end_date = 1380.1.1
}

