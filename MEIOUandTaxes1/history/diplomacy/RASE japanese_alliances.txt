# 1356-1392
# Nanbok-cho Period

# Northern Court (Ashikaga familial alliances)
alliance = {
	first = AKG
	second = HKW
	start_date = 1356.1.1
	end_date = 1392.1.1	
}

alliance = {
	first = AKG
	second = NIK
	start_date = 1356.1.1
	end_date = 1392.1.1
}

alliance = {
	first = AKG
	second = HKY
	start_date = 1356.1.1
	end_date = 1392.1.1
}

alliance = {
	first = AKG
	second = ISK
	start_date = 1356.1.1
	end_date = 1392.1.1
}

alliance= {
	first = AKG
	second = IGW
	start_date = 1356.1.1
	end_date = 1392.1.1
}

alliance = {
	first = AKG
	second = SBA
	start_date = 1356.1.1
	end_date = 1392.1.1
}

alliance = {
	first = AKG
	second = AKW
	start_date = 1356.1.1
	end_date = 1392.1.1
}

alliance = {
	first = AKG
	second = SKW
	start_date = 1356.1.1
	end_date = 1392.1.1
}

royal_marriage = {
	first = AKG
	second = HKW
	start_date = 1356.1.1
	end_date = 1392.1.1	
}

royal_marriage = {
	first = AKG
	second = NIK
	start_date = 1356.1.1
	end_date = 1392.1.1
}

royal_marriage = {
	first = AKG
	second = HKY
	start_date = 1356.1.1
	end_date = 1392.1.1
}

royal_marriage = {
	first = AKG
	second = ISK
	start_date = 1356.1.1
	end_date = 1392.1.1
}

royal_marriage = {
	first = AKG
	second = IGW
	start_date = 1356.1.1
	end_date = 1392.1.1
}

royal_marriage = {
	first = AKG
	second = SBA
	start_date = 1356.1.1
	end_date = 1392.1.1
}

royal_marriage = {
	first = AKG
	second = AKW
	start_date = 1356.1.1
	end_date = 1392.1.1
}

royal_marriage = {
	first = AKG
	second = SKW
	start_date = 1356.1.1
	end_date = 1392.1.1
}

# Southern Court


alliance = { 
	first = JAP
	second = KTK
	start_date = 1356.1.1
	end_date = 1392.1.1
}
alliance = { 
	first = JAP
	second = KKC
	start_date = 1356.1.1
	end_date = 1392.1.1
}


# 1392-1582
# Ashikaga shogunate

vassal = {
	first = OUC
	second = MRI
	start_date = 1392.1.1
	end_date = 1523.1.1
}

vassal = {
	first = RZJ
	second = OTM
	start_date = 1570.1.1
	end_date = 1578.1.1
}

vassal = {
	first = MRI
	second = UKI
	start_date = 1523.1.1
	end_date = 1550.1.1
}

vassal = {
	first = ODA
	second = KTK
	start_date = 1569.1.1
	end_date = 1579.1.1
}

vassal = {
	first = TKI
	second = ODA
	start_date = 1356.1.1
	end_date = 1477.1.1
}

vassal = { #additional tags start here
	first = DTE
	second = MGM
	start_date = 1514.1.1
	end_date = 1542.1.1
}

# 1582-1598
# Under Oda Hideyoshi's rule

vassal = {
	first = ODA
	second = MRI
	start_date = 1582.1.1
	end_date = 1598.1.1
}

vassal = {
	first = ODA
	second = SOO
	start_date = 1591.1.1
	end_date = 1598.1.1
}

vassal = {
	first = ODA
	second = SMZ
	start_date = 1591.1.1 
	end_date = 1598.1.1
}

vassal = {
	first = ODA
	second = CSK
	start_date = 1591.1.1
	end_date = 1598.1.1
}

vassal = {
	first = ODA
	second = SBA
	start_date = 1591.1.1
	end_date = 1598.1.1
}

vassal = {
	first = ODA
	second = TGW
	start_date = 1591.1.1
	end_date = 1598.1.1
}

vassal = {
	first = ODA
	second = USG
	start_date = 1591.1.1
	end_date = 1598.1.1
}

vassal = {
	first = ODA
	second = HJO
	start_date = 1591.1.1
	end_date = 1598.1.1
}

vassal = {
	first = ODA
	second = AKG
	start_date = 1591.1.1
	end_date = 1598.1.1
}

vassal = {
	first = ODA
	second = DTE
	start_date = 1591.1.1
	end_date = 1598.1.1
}

# 1598-1600
# Oda Hideyoshi's succession

vassal = {
	first = TGW
	second = DTE
	start_date = 1598.1.1
	end_date = 1600.9.15
}
# validator fix - no alliance and vassal at same time
# alliance = {
#	first = TGW
#	second = DTE
#	start_date = 1598.1.1
#	end_date = 1600.9.15
# }

vassal = {
	first = TGW
	second = HJO
	start_date = 1598.1.1
	end_date = 1600.9.15
}

# validator fix - no vassal and alliance at same time
# alliance = {
#	first = TGW
#	second = HJO
#	start_date = 1598.1.1
#	end_date = 1600.9.15
# }

vassal = {
	first = TGW
	second = AKG
	start_date = 1598.1.1
	end_date = 1600.9.15
}

# validator fix no vassal and alliance at same time
# alliance = {
#	first = TGW
#	second = AKG
#	start_date = 1598.1.1
#	end_date = 1600.9.15
# }

vassal = {
	first = TGW
	second = HKY
	start_date = 1598.1.1
	end_date = 1600.9.15
}

# validator fix no vassal and alliance at same time
# alliance = {
#	first = TGW
#	second = HKY
#	start_date = 1598.1.1
#	end_date = 1600.9.15
# }

alliance = {
	first = ODA
	second = SMZ
	start_date = 1598.1.1
	end_date = 1600.9.15
}

alliance = {
	first = ODA
	second = MRI
	start_date = 1598.1.1
	end_date = 1600.9.15
}

alliance = {
	first = ODA
	second = CSK
	start_date = 1598.1.1
	end_date = 1600.9.15
}

alliance = {
	first = ODA
	second = USG
	start_date = 1598.1.1
	end_date = 1600.9.15
}

# 1600-1615
# From the battle of Sekigahara to the fall of Hosaka

vassal = {
	first = TGW
	second = DTE
	start_date = 1600.9.15
	end_date = 1615.6.4
}

vassal = {
	first = TGW
	second = AKG
	start_date = 1600.9.15
	end_date = 1615.6.4
}

vassal = {
	first = TGW
	second = HJO
	start_date = 1600.9.15
	end_date = 1615.6.4
}

vassal = {
	first = TGW
	second = HKY
	start_date = 1600.9.15
	end_date = 1615.6.4
}

vassal = {
	first = TGW
	second = MRI
	start_date = 1600.9.15
	end_date = 1615.6.4
}

vassal = {
	first = TGW
	second = CSK
	start_date = 1600.9.15
	end_date = 1615.6.4
}

vassal = {
	first = TGW
	second = SMZ
	start_date = 1600.9.15
	end_date = 1615.6.4
}

vassal = {
	first = TGW
	second = SOO
	start_date = 1600.9.15
	end_date = 1615.6.4
}

# After 1615
# Tokugawa shogunate

vassal = {
	first = JAP
	second = DTE
	start_date = 1615.6.4
	end_date = 1630.1.1
}

vassal = {
	first = JAP
	second = SMZ
	start_date = 1615.6.4
	end_date = 1650.6.1
}
