#SKP - Selkups

government = tribal_republic government_rank = 3
mercantilism = 0.0
primary_culture = selkup
religion = tengri_pagan_reformed
technology_group = steppestech
capital = 1078 #Narym

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1550.1.1 = {
	monarch = {
		name = "Vonia"
		dynasty = "Selkup"
		ADM = 3
		DIP = 2
		MIL = 4
	}
}

