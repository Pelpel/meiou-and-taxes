# BUT - Butua

government = tribal_monarchy government_rank = 2
mercantilism = 0.0
primary_culture = shona
religion = animism
technology_group = central_african
capital = 1586	# Zimbabwe

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 5 }
}
