# F�rstentum Calenberg

government = feudal_monarchy government_rank = 3
mercantilism = 0.0
primary_culture = old_saxon
religion = catholic
technology_group = western
capital = 2618 # Calenberg (was Hannover)

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 4 }
	set_country_flag = welf_succession
}

1432.1.1 = {
	monarch = {
		name = "Wilhelm"
		dynasty = "von Welf"
		DIP = 3
		ADM = 3
		MIL = 3
	}
	heir = {
		name = "Wilhelm"
		monarch_name = "Wilhelm II"
		dynasty = "von Welf"
		birth_date = 1425.1.1
		death_date = 1503.7.7
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1473.1.1 = {
	monarch = {
		name = "Wilhelm II"
		dynasty = "von Welf"
		DIP = 3
		ADM = 3
		MIL = 3
	}
} #Transfered rule from Wilhelm to his son, Wilhelm goes to Braunschweig

#1482 PU with Braunschweig

1494.1.1 = {
	monarch = {
		name = "Erich I"
		dynasty = "von Welf"
		DIP = 3
		ADM = 3
		MIL = 3
	}
} #Erbteilung, Calenberg-G�ttingen founded
1530.1.2 = {
	government = administrative_monarchy
	heir = {
		name = "Erich"
		monarch_name = "Erich II"
		dynasty = "von Welf"
		birth_date = 1528.8.10
		death_date = 1584.11.17
		claim = 95
		ADM = 2
		DIP = 2
		MIL = 4
	}
	capital = 61 # Calenberg (was Hannover)
}

1545.1.1 = {
	monarch = {
		name = "Erich II"
		dynasty = "von Welf"
		DIP = 2
		ADM = 2
		MIL = 4
	}
}

1584.6.11 = {
	monarch = {
		name = "Julius"
		dynasty = "von Welf"
		DIP = 3
		ADM = 3
		MIL = 3
	}
	heir = {
		name = "Heinrich Julius"
		monarch_name = "Heinrich Julius"
		dynasty = "von Welf"
		birth_date = 1564.10.15
		death_date = 1613.7.20
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
	religion = protestant
} #PU with Braunschweig

1589.5.13 = {
	monarch = {
		name = "Heinrich Julius"
		dynasty = "von Welf"
		DIP = 3
		ADM = 3
		MIL = 3
	}
} #Still PU with Braunschweig

1591.4.15 = {
	heir = {
		name = "Friedrich Ulrich"
		monarch_name = "Friedrich Ulrich"
		dynasty = "von Welf"
		birth_date = 1591.4.15
		death_date = 1634.8.21
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1613.7.20 = {
	monarch = {
		name = "Friedrich Ulrich"
		dynasty = "von Welf"
		DIP = 3
		ADM = 3
		MIL = 3
	}
} #Still PU with Braunschweig

1634.8.11 = {
	monarch = {
		name = "Georg"
		dynasty = "von Welf"
		DIP = 2
		ADM = 3
		MIL = 4
	}
} #PU broken

1641.4.2 = {
	monarch = {
		name = "Christian Ludwig"
		dynasty = "von Welf"
		DIP = 3
		ADM = 2
		MIL = 3
	}
}

1648.1.1 = {
	monarch = {
		name = "Georg Wilhelm"
		dynasty = "von Welf"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1665.1.1 = {
	monarch = {
		name = "Johann Friedrich"
		dynasty = "von Welf"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1679.12.28 = {
	monarch = {
		name = "Ernst August"
		dynasty = "von Welf"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

#1692 became Hannover