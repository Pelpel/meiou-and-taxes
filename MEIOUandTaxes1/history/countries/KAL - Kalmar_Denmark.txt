# KAL - Scandinavia unified by Denmark
# Unification nation
# 2010-jan-16 - FB - HT3 changes

government = feudal_monarchy government_rank = 6
mercantilism = 0.0
technology_group = western
primary_culture = danish
religion = catholic
add_accepted_culture = norwegian
add_accepted_culture = norse
add_accepted_culture = swedish
add_accepted_culture = finnish
capital = 12	# Sjaelland

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 3 }
}

1300.1.1 = {
	set_country_flag = hanseatic_league
}

1536.1.1 = { 
	religion = protestant 
	government = constitutional_monarchy government_rank = 6}

1616.1.1 = {
	} #formaiton of the Danish east inda company
