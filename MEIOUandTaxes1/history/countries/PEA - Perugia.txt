#PEA - Perugia

government = oligarchic_republic
mercantilism = 0.0
primary_culture = umbrian
religion = catholic
technology_group = western
capital = 3699
historical_friend = FIR

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 4 }
}

1355.1.1 = {
	monarch = {
		name = "Republican Council"
		ADM = 4
		DIP = 4
		MIL = 2
	}
}
