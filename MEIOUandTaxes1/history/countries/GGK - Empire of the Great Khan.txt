# GGK - Empire of Gengis Khan
# Unification nation

government = altaic_monarchy government_rank = 6
mercantilism = 0.0
technology_group = chinese
religion = vajrayana #DEI GRATIA
primary_culture = mongol
capital = 702	# Hohhot

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 0 }
}
