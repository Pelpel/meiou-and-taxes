# UIG - Uighurustan

government = steppe_horde government_rank = 5
mercantilism = 0.0
primary_culture = uyghur
religion = sunni
technology_group = steppestech
capital = 2745

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 0 }
}
