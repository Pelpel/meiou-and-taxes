#PRO - Provence

government = feudal_monarchy government_rank = 3
mercantilism = 0.0
primary_culture = provencal
add_accepted_culture = angevin
religion = catholic
technology_group = western
capital = 201

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 2 }
}

1343.1.20 = {
	monarch = {
		name = "Jeanne"
		dynasty = "d'Anjou"
		ADM = 3
		DIP = 3
		MIL = 3
		female = yes
	}
	heir = {
		name = "Louis"
		monarch_name = "Louis"
		dynasty = "de Valois"
		birth_date = 1320.1.1
		death_date = 1384.9.20
		claim = 95
		ADM = 1
		DIP = 3
		MIL = 3
	}
}

1381.7.16 = {
	monarch = {
		name = "Louis"
		dynasty = "de Valois"
		ADM = 1
		DIP = 3
		MIL = 3
	}
	heir = {
		name = "Louis"
		monarch_name = "Louis II"
		dynasty = "de Valois"
		birth_date = 1350.1.1
		death_date = 1403.9.25
		claim = 95
		ADM = 1
		DIP = 3
		MIL = 3
	}
}

1384.9.20  = {
	monarch = {
		name = "Louis II"
		dynasty = "de Valois"
		ADM = 1
		DIP = 3
		MIL = 3
	}
}

1403.9.25 = {
	heir = {
		name = "Louis"
		monarch_name = "Louis III"
		dynasty = "de Valois"
		birth_date = 1403.9.25
		death_date = 1434.11.12
		claim = 95
		ADM = 2
		DIP = 3
		MIL = 2
	}
}

1417.1.1   = {
	monarch = {
		name = "Louis III"
		dynasty = "de Valois"
		ADM = 2
		DIP = 3
		MIL = 2
	}
	heir = {
		name = "Ren�"
		monarch_name = "Ren�"
		dynasty = "de Valois"
		birth_date = 1409.1.16
		death_date = 1480.7.10
		claim = 95
		ADM = 4
		DIP = 6
		MIL = 3
	}
}

1434.11.16 = {
	monarch = {
		name = "Ren�"
		dynasty = "de Valois"
		ADM = 4
		DIP = 6
		MIL = 3
	}
}

1480.1.1 = {
	monarch = {
		name = "Charles III"
		dynasty = "de Valois"
		ADM = 1
		DIP = 3
		MIL = 2
	}
}

1515.1.1 = { government = despotic_monarchy government_rank = 3 }

1560.1.1 = {
	religion = reformed
	government = constitutional_monarchy government_rank = 6}

1589.8.3 = { government = administrative_monarchy government_rank = 3 }

1661.3.9 = { government = absolute_monarchy government_rank = 3 }
