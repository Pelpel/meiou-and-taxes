#TAU - Taungu
#10.01.27 FB-HT3 - make HT3 changes

government = eastern_monarchy government_rank = 5
mercantilism = 10
primary_culture = burmese
religion = buddhism
technology_group = chinese
capital = 352	# Toungoo

1000.1.1 = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 1 }
}

1347.1.1 = {
	monarch = {
		name = "Thinkaba"
		dynasty = "Taungoo"
		ADM = 1
		DIP = 2
		MIL = 3
	}
	heir = {
		name = "Pyanchi"
		monarch_name = "Pyanchi"
		dynasty = "Taungoo"
		birth_Date = 1345.1.1
		death_Date = 1421.1.1
		claim = 95
		ADM = 2
		DIP = 3
		MIL = 1
	}
}

1358.1.1 = {
	monarch = {
		name = "Pyanchi"
		dynasty = "Taungoo"
		ADM = 2
		DIP = 3
		MIL = 1
	}
}

1421.1.1 = {
	monarch = {
		name = "Sawluthinhkaya"
		dynasty = "Taungoo"
		ADM = 4
		DIP = 2
		MIL = 5
	}
}

1451.1.1 = {
	monarch = {
		name = "Zalahtinyan"
		dynasty = "Taungoo"
		ADM = 3
		DIP = 3
		MIL = 3
	}
	heir = {
		name = "Sithukyawhtin"
		monarch_name = "Sithukyawhtin"
		dynasty = "Taungoo"
		birth_Date = 1400.1.1
		death_Date = 1481.1.1
		claim = 95
		ADM = 4
		DIP = 4
		MIL = 3
	}
}

1470.1.1 = {
	heir = {
		name = "Sithunge"
		monarch_name = "Sithunge"
		dynasty = "Taungoo"
		birth_Date = 1450.1.1
		death_Date = 1486.1.1
		claim = 95
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1481.1.1 = {
	monarch = {
		name = "Sithunge"
		dynasty = "Taungoo"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1481.1.1 = {
	heir = {
		name = "Minkyinyo"
		monarch_name = "Minkyinyo"
		dynasty = "Taungoo"
		birth_Date = 1460.1.1
		death_Date = 1530.12.1
		claim = 95
		ADM = 5
		DIP = 4
		MIL = 4
	}
}

1486.1.1 = {
	monarch = {
		name = "Minkyinyo"
		dynasty = "Taungoo"
		ADM = 5
		DIP = 4
		MIL = 4
	}
}

1516.1.1 = {
	heir = {
		name = "Tabinshwehti"
		monarch_name = "Tabinshwehti"
		dynasty = "Taungoo"
		birth_Date = 1516.1.1
		death_Date = 1550.1.1
		claim = 95
		ADM = 6
		DIP = 6
		MIL = 6
		leader = { name = "Tabinshwehti" type = general rank = 0 fire = 5 shock = 5 manuever = 5 siege = 4 }
	}
	set_variable = { which = "centralization_decentralization" value = -2 }
	add_country_modifier = { name = blessing_of_god duration = -1 }
	add_accepted_culture = arakanese
	add_accepted_culture = monic
	add_accepted_culture = shan
	add_accepted_culture = chin
	add_accepted_culture = karen
	add_accepted_culture = kachin
	historical_rival = LNA
	historical_rival = LXA
	historical_rival = AYU
	historical_rival = MYA
	historical_rival = AVA
	historical_rival = SST
	historical_rival = PEG
}

1530.12.1 = {				#conquered of all Burma
	monarch = {
		name = "Tabinshwehti"
		dynasty = "Taungoo"
		ADM = 6
		DIP = 6
		MIL = 6
		leader = { name = "Tabinshwehti" type = general rank = 0 fire = 5 shock = 5 manuever = 5 siege = 3 }
	}
	#add_country_modifier = { name = blessing_of_god duration = -1 }
}

1530.12.1 = {
	heir = {
		name = "Bayinnaung"
		monarch_name = "Bayinnaung"
		dynasty = "Taungoo"
		birth_Date = 1520.1.1
		death_Date = 1581.12.1
		claim = 95
		ADM = 5
		DIP = 4
		MIL = 6
	}
}

1535.1.1 = { add_country_modifier = { name = blessing_of_god duration = -1 } }  ############

1550.1.1 = {
	monarch = {
		name = "Bayinnaung"
		dynasty = "Taungoo"
		ADM = 5
		DIP = 4
		MIL = 6
	}
}

1550.1.1 = {
	heir = {
		name = "Nanda Bayin"
		monarch_name = "Nanda Bayin"
		dynasty = "Taungoo"
		birth_Date = 1550.1.1
		death_Date = 1597.1.1
		claim = 95
		ADM = 1
		DIP = 1
		MIL = 2
	}
}

1581.12.1 = {
	monarch = {
		name = "Nanda Bayin"
		dynasty = "Taungoo"
		ADM = 1
		DIP = 1
		MIL = 2
	}
}

1597.1.1 = {
	monarch = {
		name = "Htin"
		dynasty = "Nyaungyan"
		ADM = 6
		DIP = 6
		MIL = 4
	}
}

1597.1.1 = {
	heir = {
		name = "Anaukpetlun"
		monarch_name = "Anaukpetlun"
		dynasty = "Nyaungyan"
		birth_Date = 1580.1.1
		death_date = 1628.1.1
		claim = 95
		ADM = 6
		DIP = 3
		MIL = 6
	}
}

1605.1.1 = {
	monarch = {
		name = "Anaukpetlun"
		dynasty = "Nyaungyan"
		ADM = 6
		DIP = 3
		MIL = 6
	}
}

1605.1.1 = {
	heir = {
		name = "Minredeippa"
		monarch_name = "Minredeippa"
		dynasty = "Nyaungyan"
		birth_Date = 1600.1.1
		death_Date = 1629.1.1
		claim = 95
		ADM = 1
		DIP = 2
		MIL = 2
	}
}

1628.1.1 = {
	monarch = {
		name = "Minredeippa"
		dynasty = "Nyaungyan"
		ADM = 1
		DIP = 2
		MIL = 2
	}
}

1628.1.1 = {
	heir = {
		name = "Thalun"
		monarch_name = "Thaulun"
		dynasty = "Nyaungyan"
		birth_Date = 1605.1.1
		death_Date = 1647.8.1
		claim = 95
		ADM = 1
		DIP = 1
		MIL = 3
	}
}

1629.1.1 = {
	monarch = {
		name = "Thalun"
		dynasty = "Nyaungyan"
		ADM = 1
		DIP = 1
		MIL = 3
	}
}

1629.1.1 = {
	heir = {
		name = "Pindale"
		monarch_name = "Pindale"
		dynasty = "Nyaungyan"
		birth_Date = 1600.1.1
		death_Date = 1661.1.1
		claim = 95
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1647.8.1 = {
	monarch = {
		name = "Pindale"
		dynasty = "Nyaungyan"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1647.8.1 = {
	heir = {
		name = "Pye"
		monarch_name = "Pye"
		dynasty = "Nyaungyan"
		birth_Date = 1620.1.1
		death_Date = 1672.4.1
		claim = 95
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1661.1.1 = {
	monarch = {
		name = "Pye"
		dynasty = "Nyaungyan"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1672.4.1 = {
	monarch = {
		name = "Narawara"
		dynasty = "Nyaungyan"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1673.3.1 = {
	monarch = {
		name = "Minrekyawdin"
		dynasty = "Nyaungyan"
		ADM = 1
		DIP = 2
		MIL = 2
	}
}

1673.3.1 = {
	heir = {
		name = "Sane"
		monarch_name = "Sane"
		dynasty = "Nyaungyan"
		birth_Date = 1650.1.1
		death_Date = 1714.1.1
		claim = 95
		ADM = 2
		DIP = 1
		MIL = 1
	}
}

1698.1.1 = {
	monarch = {
		name = "Sane"
		dynasty = "Nyaungyan"
		ADM = 2
		DIP = 1
		MIL = 1
	}
}

1698.1.1 = {
	heir = {
		name = "Taninganwe"
		monarch_name = "Taninganwe"
		dynasty = "Nyaungyan"
		birth_Date = 1690.1.1
		death_Date = 1733.12.1
		claim = 95
		ADM = 2
		DIP = 1
		MIL = 1
	}
}

1714.1.1 = {
	monarch = {
		name = "Taninganwe"
		dynasty = "Nyaungyan"
		ADM = 2
		DIP = 1
		MIL = 1
	}
}

1714.1.1 = {
	heir = {
		name = "Mahadammayaza-Dipati"
		monarch_name = "Mahadammayaza-Dipati"
		dynasty = "Nyaungyan"
		birth_date = 1700.1.1
		death_Date = 1752.1.1
		claim = 95
		ADM = 1
		DIP = 2
		MIL = 1
	}
}

1733.12.1 = {
	monarch = {
		name = "Mahadammayaza-Dipati"
		dynasty = "Nyaungyan"
		ADM = 1
		DIP = 2
		MIL = 1
	}
}

1752.1.1 = {
	monarch = {
		name = "Alaungpaya"
		dynasty = "Konbaung"
		ADM = 6
		DIP = 6
		MIL = 6
	}
}

1752.1.1 = {			# 1752.2.28
	heir = {
		name = "Naugdawgyi"
		monarch_name = "Naugdawgyi"
		dynasty = "Konbaung"
		birth_Date = 1740.1.1
		death_date = 1763.11.1
		claim = 95
		ADM = 1
		DIP = 2
		MIL = 1
	}
}

1760.5.12 = {
	monarch = {
		name = "Naugdawgyi"
		dynasty = "Konbaung"
		ADM = 1
		DIP = 2
		MIL = 1
	}
}

1760.5.12 = {
	heir = {
		name = "Hsinbyushin"
		monarch_name = "Hsinbyushin"
		dynasty = "Konbaung"
		birth_Date = 1750.1.1
		death_Date = 1776.6.1
		claim = 95
		ADM = 4
		DIP = 4
		MIL = 6
	}
}

1763.11.1 = {
	monarch = {
		name = "Hsinbyushin"
		dynasty = "Konbaung"
		ADM = 4
		DIP = 4
		MIL = 6
	}
}

1763.11.1 = {
	heir = {
		name = "Singu Min"
		monarch_name = "Singu Min"
		dynasty = "Konbaung"
		birth_date = 1750.1.1
		death_Date = 1782.2.1
		claim = 95
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1776.6.1 = {
	monarch = {
		name = "Singu Min"
		dynasty = "Konbaung"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1776.6.1 = {
	heir = {
		name = "Maung Maung"
		monarch_name = "Maung Maung"
		dynasty = "Konbaung"
		birth_Date = 1770.1.1
		death_Date = 1782.3.1
		claim = 95
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1782.2.1 = {
	monarch = {
		name = "Maung Maung"
		dynasty = "Konbaung"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1782.2.1 = {
	heir = {
		name = "Bodawpaya"
		monarch_name = "Bodawpaya"
		dynasty = "Konbaung"
		birth_Date = 1760.1.1
		death_Date = 1819.6.6
		claim = 95
		ADM = 5
		DIP = 4
		MIL = 4
	}
}

1782.3.1 = {
	monarch = {
		name = "Bodawpaya"
		dynasty = "Konbaung"
		ADM = 5
		DIP = 4
		MIL = 4
	}
}

1782.3.1 = {
	heir = {
		name = "Bagyidaw"
		monarch_name = "Bagyidaw"
		dynasty = "Konbaung"
		birth_Date = 1770.1.1
		death_Date = 1830.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 5
	}
}

1819.6.6 = {
	monarch = {
		name = "Bagyidaw"
		dynasty = "Konbaung"
		ADM = 3
		DIP = 3
		MIL = 5
	}
}
