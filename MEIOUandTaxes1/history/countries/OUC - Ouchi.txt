# OUC - Ouchi clan

government = early_medieval_japanese_gov
government_rank = 3
mercantilism = 0.0
primary_culture = chugoku
religion = mahayana
technology_group = chinese
capital = 1027	# Nagato

historical_friend = KOR
historical_friend = JOS

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 2 }
}

1356.1.1 = {
	monarch = {
		name = "Yoshihiro" 
		dynasty = "Ouchi"
		ADM = 3
		DIP = 4
		MIL = 4
	}
}

1394.3.23 = {
	heir = {
		name = "Mochiyo"
		monarch_name = "Mochiyo"
		dynasty = "Ouchi"
		birth_date = 1394.3.23
		death_date = 1441.8.14
		claim = 70
		ADM = 3
		DIP = 2
		MIL = 3
	}
}
	
1400.1.7 = { 
	monarch = {
		name = "Hiroshige"
		dynasty = "Ouchi"
		ADM = 2
		DIP = 1
		MIL = 2
	}
	}
	
1401.1.1 = { 
	monarch = {
		name = "Morimi"
		dynasty = "Ouchi"
		ADM = 3
		DIP = 2
		MIL = 4
	}
}
	
1431.1.1 = { 
	monarch = {
		name = "Mochiyo"
		dynasty = "Ouchi"
		ADM = 3
		DIP = 2
		MIL = 3
	}
	heir = {
		name = "Norihiro"
		monarch_name = "Norihiro"
		dynasty = "Ouchi"
		birth_date = 1420.3.20
		death_date = 1465.9.3
		claim = 90
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1441.1.1 = { 
	monarch = {
		name = "Norihiro"
		dynasty = "Ouchi"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
	
1446.9.18 = {
	heir = {
		name = "Masahiro"
		monarch_name = "Masahiro"
		dynasty = "Ouchi"
		birth_date = 1446.9.18
		death_date = 1495.10.6
		claim = 90
		ADM = 4
		DIP = 4
		MIL = 5
	}
}
	
1465.9.3 = { 
	monarch = {
		name = "Masahiro"
		dynasty = "Ouchi"
		ADM = 4
		DIP = 4
		MIL = 5
	}
}
	
1477.4.7 = {
	heir = {
		name = "Yoshioki"
		monarch_name = "Yoshioki"
		dynasty = "Ouchi"
		birth_date = 1477.4.7
		death_date = 1529.1.29
		claim = 90
		ADM = 4
		DIP = 5
		MIL = 5
	}
}

1495.10.6 = { 
	monarch = {
		name = "Yoshioki"
		dynasty = "Ouchi"
		ADM = 4
		DIP = 5
		MIL = 5
	}
}

1507.12.18 = {
	heir = {
		name = "Yoshitaka"
		monarch_name = "Yoshitaka"
		dynasty = "Ouchi"
		birth_date = 1507.12.18
		death_date = 1551.9.30
		claim = 90
		ADM = 3
		DIP = 3
		MIL = 2
	}
}

1526.1.1 = {
	set_global_flag = iwami_mine
}

1529.1.29 = { 
	monarch = {
		name = "Yoshitaka"
		dynasty = "Ouchi"
		ADM = 3
		DIP = 3
		MIL = 2
	}
}

1544.1.1 = {
	heir = {
		name = "Yoshinaga"
		monarch_name = "Yoshinaga"
		dynasty = "Ouchi"
		birth_date = 1532.1.1
		death_date = 1557.5.1
		claim = 70
		ADM = 1
		DIP = 1
		MIL = 1
	}
}
1545.1.1 = {
	heir = {
		name = "Yoshitaka"
		monarch_name = "Yoshitaka"
		dynasty = "Ouchi"
		birth_date = 1545.1.1
		death_date = 1551.10.1
		claim = 90
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1551.10.1 = { 
	monarch = {
		name = "Yoshinaga"
		dynasty = "Ouchi"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}
