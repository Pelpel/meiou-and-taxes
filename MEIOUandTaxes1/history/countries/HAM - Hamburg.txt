# HAM - Hamburg

government = merchant_imperial_city # merchant_republic
mercantilism = 20
technology_group = western
religion = catholic
primary_culture = old_saxon
capital = 44
fixed_capital = 44	# Hamburg
historical_rival = DEN
historical_friend = FRL #Lübeck
historical_friend = FRB #Bremen


1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 0 }
}

1356.1.1 = {
	monarch = {
		name = "Stadtrat"
		ADM = 3
		DIP = 3
		MIL = 5
	}
}

1399.1.1 = {
	set_country_flag = hanseatic_league
}

1529.1.1 = {
	religion = protestant
	remove_historical_rival = DEN
	historical_neutral = DEN
}
