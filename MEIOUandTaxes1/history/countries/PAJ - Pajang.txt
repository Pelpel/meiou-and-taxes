# PAJ - Pajang
# GG - Adapted from Divide et Impera

government = eastern_monarchy government_rank = 5
mercantilism = 0.0
primary_culture = sulawesi
religion = sunni
technology_group = austranesian
capital = 629	# Pajang

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 4 }
}

1568.1.1 = {
	monarch = {
		name = "Jaka Tingir Pangeran Adivijaya Surya Alam"
		dynasty = "Rajasanagara"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1586.1.1 = {
	monarch = {
		name = "Pangeran Benawa"
		dynasty = "Rajasanagara"
		ADM = 2
		DIP = 2
		MIL = 6
	}
}