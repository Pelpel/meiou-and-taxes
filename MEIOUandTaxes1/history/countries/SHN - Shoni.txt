# SHN - Shoniclan

government = early_medieval_japanese_gov
government_rank = 3
mercantilism = 0.0
primary_culture = kyushu
religion = mahayana
technology_group = chinese
capital = 1020		# Hizen

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 2 }
}

1336.4.11 = {
	monarch = {
		name = "Yorinao"
		dynasty = "Shoni"
		ADM = 3
		DIP = 3
		MIL = 3
		}
}

1372.1.30  = {
	monarch = {
		name = "Fuyusuke"
		dynasty = "Shoni"
		ADM = 3
		DIP = 2
		MIL = 3
		}
}

1375.9.22  = {
	monarch = {
		name = "Fuyusuke"
		dynasty = "Shoni"
		ADM = 3
		DIP = 2
		MIL = 3
		}
}

1375.9.22 = {
	heir = {
		name = "Mitsusada"
		monarch_name = "Mitsusada"
		dynasty = "Shoni"
		birth_date = 1375.9.22
		death_date = 1404.1.1
		claim = 90
		ADM = 3
		DIP = 4
		MIL = 2
	}
}

1387.1.1 = {
	monarch = {
		name = "Sadayori"
		dynasty = "Shoni"
		ADM = 3
		DIP = 4
		MIL = 2
		}
}

1387.1.1 = {
	heir = {
		name = "Mitsusada"
		monarch_name = "Mitsusada"
		dynasty = "Shoni"
		birth_date = 1380.1.1
		death_date = 1433.9.29
		claim = 90
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1404.1.1 = {
	monarch = {
		name = "Mistusada"
		dynasty = "Shoni"
		ADM = 2
		DIP = 2
		MIL = 2
		}
}

1404.1.1 = {
	heir = {
		name = "Suketsugu"
		monarch_name = "Suketsugu"
		dynasty = "Shoni"
		birth_date = 1400.1.1
		death_date = 1441.1.1
		claim = 90
		ADM = 2
		DIP = 2
		MIL = 1
	}
}

1433.9.29 = {
	monarch = {
		name = "Suketsugu"
		dynasty = "Shoni"
		ADM = 2
		DIP = 2
		MIL = 1
		}
}

1441.1.1 = {
	monarch = {
		name = "Noriyori" 
		dynasty = "Shoni"
		ADM = 2
		DIP = 2
		MIL = 1
		}
}

1450.1.1 = {
	heir = {
		name = "Masasuke"
		monarch_name = "Masasuke"
		dynasty = "Shoni"
		birth_date = 1450.1.1
		death_date = 1497.1.1
		claim = 90
		ADM = 3
		DIP = 2
		MIL = 2
	}
}

1468.1.1 = {
	monarch = {
		name = "Masasuke" 
		dynasty = "Shoni"
		ADM = 3
		DIP = 2
		MIL = 2
		}
}

1491.1.1 = {
	heir = {
		name = "Sukemoto"
		monarch_name = "Sukemoto"
		dynasty = "Shoni"
		birth_date = 1491.1.1
		death_date = 1536.1.22
		claim = 90
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1497.1.1 = {
	monarch = {
		name = "Sukemoto" 
		dynasty = "Shoni"
		ADM = 3
		DIP = 2
		MIL = 2
		}
}

1529.1.1 = {
	heir = {
		name = "Fuyuhisa"
		monarch_name = "Fuyuhisa"
		dynasty = "Shoni"
		birth_date = 1529.1.1
		death_date = 1559.2.18	
		claim = 90
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1536.1.22 = {
	monarch = {
		name = "Fuyuhisa" 
		dynasty = "Shoni"
		ADM = 1
		DIP = 1
		MIL = 1
		}
}