# ITD - Cispadane Republic
# 2010-jan-16 - FB - HT3 changes

government = revolutionary_republic
mercantilism = 0.0
technology_group = western
religion = catholic
primary_culture = emilian
capital = 113 #Bologna

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = -3 }
}

1796.10.15 = {
	monarch = {
		name = "Directory"
		DIP = 1
		ADM = 3
		MIL = 1
	}
}
