# HUE - Huetamo

government = tribal_monarchy government_rank = 2
mercantilism = 0.0
primary_culture = purepechan
religion = nahuatl
technology_group = mesoamerican
capital = 3526 # Huetamo

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1356.1.1   = {
	monarch = {
		name = "Monarch"
		dynasty = "Xalisco"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}
