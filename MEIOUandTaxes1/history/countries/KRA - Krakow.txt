# KRA - Republic of Krakow
# 2010-jan-21 - FB - HT3 changes

government = constitutional_republic
mercantilism = 0.0
primary_culture = polish
religion = catholic
technology_group = eastern
capital = 262	# Krakow

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 4 }
}

1815.5.3 = {
	monarch = {
		name = "National Assembly"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
