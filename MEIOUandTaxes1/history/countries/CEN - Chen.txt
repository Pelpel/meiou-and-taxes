
government = chinese_monarchy_5 government_rank = 4
mercantilism = 0.0
technology_group = chinese
religion = confucianism
primary_culture = minyu
capital = 680

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 2 }
}

1350.1.1 = {
	monarch = {
		name = "Youding"
		dynasty = "Chen"
		ADM = 3
		DIP = 2
		MIL = 5
	}
}
1351.1.1  = {
#	set_country_flag = lost_mandate_of_heaven
#	add_country_modifier = { name = wrath_of_god duration = -1 }
}
