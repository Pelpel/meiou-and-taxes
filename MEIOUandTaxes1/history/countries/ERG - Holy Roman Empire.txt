# ERG - Holy Roman Empire

government = feudal_monarchy government_rank = 6
mercantilism = 0.0
technology_group = western
primary_culture = frankish
religion = catholic
capital = 81

1000.1.1 = {
	set_variable = { which = "centralization_decentralization" value = 2 }
}
1750.1.1 = { government = constitutional_republic }
