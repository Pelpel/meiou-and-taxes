
government = administrative_republic
mercantilism = 0.0
# serfdom_freesubjects = 5
# secularism_theocracy = -4
technology_group = western
religion = catholic
primary_culture = high_alemanisch
capital = 165	# Bern

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1356.1.1 = {
	monarch = {
		name = "The Tagsatzung"
		ADM = 4
		DIP = 4
		MIL = 4
	}
}

1528.1.1 = {
	religion = reformed
	add_accepted_culture = lombard
	add_accepted_culture = arpitan
	# secularism_theocracy = -5
} #center of reformed faith but also lutheran and catholics

1798.1.1 = {
	government = constitutional_republic
}
