
government = tribal_monarchy government_rank = 2
primary_culture = yaka
religion = animism
technology_group = central_african
unit_type = sub_saharan
capital = 2967 # matamba

1000.1.1 = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 5 }
}
