# ZZZ - Zazzau

government = tribal_monarchy government_rank = 2
mercantilism = 0.0
primary_culture = haussa
religion = sunni
technology_group = sub_saharan

capital = 1553	# Zazzau

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1350.1.1 = {
	monarch = {
		name = "Abdallah"
		dynasty = "Zaria"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1536.1.1 = {
	monarch = {
		name = "Bakwa Turunku"
		dynasty = "Zaria"
		DIP = 6
		ADM = 6
		MIL = 6
		female = yes
	}
} # death in 1566

1576.1.1 = {
	monarch = {
		name = "Amina"
		dynasty = "Zaria"
		DIP = 6
		ADM = 6
		MIL = 6
		female = yes
	}
} # death in 1610

1610.1.1 = {
	monarch = {
		name = "Muhammad"
		dynasty = "Zaria"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}
