# SID - Sidama

government = feudal_monarchy government_rank = 3
mercantilism = 10
primary_culture = amhara
religion = coptic
technology_group = east_african unit_type = soudantech
capital = 1567

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 2 }
}

1344.1.1 = {
	monarch = {
		name = "Yeshaq I"
		dynasty = "Solomonid"
		DIP = 0
		ADM = 2
		MIL = 0
	}
}

1530.1.1 = {
	monarch = {
		name = "Yeshaq II"
		dynasty = "Solomonid"
		DIP = 0
		ADM = 2
		MIL = 0
	}
}
