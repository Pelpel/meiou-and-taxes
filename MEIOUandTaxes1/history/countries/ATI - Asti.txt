# ATI - Asti

government = merchant_republic
mercantilism = 0.0
primary_culture = piedmontese
religion = catholic
technology_group = western
capital = 3703

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 4 }
}

1355.1.1 = {
	monarch = {
		name = "Republican Council"
		ADM = 4
		DIP = 4
		MIL = 2
	}
}
