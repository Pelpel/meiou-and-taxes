country_decisions = {

	#naval_recruiting_act = {
	#	potential = {
	#		NOT = { has_country_modifier = the_recruiting_act }
	#		NOT = { has_country_modifier = the_land_recruiting_act }
	#		has_idea_group = naval_ideas
	#		num_of_ports = 1
	#		dip_tech = 20
	#	}
	#	allow = {
	#		full_idea_group =  naval_ideas
	#		OR = {
	#			has_idea_group = naval_leadership_ideas
	#			has_idea_group = naval_quality_ideas
	#			has_idea_group = grand_fleet_ideas
	#			has_idea_group = merchant_marine_ideas
	#			dip_tech = 28
	#		}
	#		OR = {
	#			advisor = grand_admiral
	#			advisor = naval_reformer
	#			mil = 5
	#		}
	#	}
	#	effect = {
	#		add_country_modifier = {
	#			name = "the_recruiting_act"
	#			duration = -1
	#		}
	#	}
	#	ai_will_do = {
	#		factor = 1
	#	}
	#}
	
	militia_act = {
		potential = {
			NOT = { has_country_modifier = the_military_act }
			government = monarchy
			OR = {
				technology_group = western
				technology_group = eastern
			}
		}
		allow = {
			full_idea_group =  administrative_ideas
			or = { mil = 4 advisor = commandant }
			is_monarch_leader = yes
			NOT = { has_country_modifier = the_military_act }			
		}
		effect = {
			add_country_modifier = {
				name = "the_military_act"
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	#navigation_act = {
	#	potential = {
	#		NOT = { has_country_modifier = the_navigation_act }
	#		num_of_colonial_subjects = 1
	#		num_of_protectorates = 1
	#	}
	#	allow = {
	#		full_idea_group = trade_ideas
	#		or = { advisor = rear_admiral advisor = collector advisor = trader }
	#		num_of_protectorates = 2
	#		num_of_colonial_subjects = 3
	#	}
	#	effect = {
	#		add_country_modifier = {
	#			name = "the_navigation_act"
	#			duration = -1
	#		}			
	#	}
	#	ai_will_do = {
	#		factor = 1
	#	}
	#}
	
	enlist_privateers_western = {
		potential = {
			NOT = { has_country_modifier = hire_privateers }
			OR = {
				technology_group = western
				technology_group = eastern
			}
			num_of_ports = 1
			dip_tech = 20
		}
		allow = {
			navy_size_percentage = 0.3
			full_idea_group = naval_ideas
			OR = {
				advisor = navigator
				advisor = naval_reformer
				mil = 4
			}
		}
		effect = {
			add_country_modifier = {
				name = "hire_privateers"
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	enlist_privateers_muslim = {
		potential = {
			NOT = { has_country_modifier = hire_privateers }
			NOT = { has_country_modifier = the_anti_piracy_act }
			OR = {
				technology_group = turkishtech
				technology_group = high_turkishtech
				technology_group = muslim
			}
			num_of_ports = 1
			dip_tech = 12
		}
		allow = {
			navy_size_percentage = 0.3
			OR = { 
				full_idea_group = naval_ideas
				tag = FEZ
				tag = TLE
				tag = MOR
				tag = TUN
				tag = HAF
			}
			OR = {
				advisor = navigator
				advisor = naval_reformer
				mil = 3
			}
		}
		effect = {
			add_country_modifier = {
				name = "hire_privateers"
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
			# Barbary Coast
		}
	}
	
	#naval_convoy_system = {
	#	potential = {
	#		NOT = { has_country_modifier = convoy_system }
	#		num_of_colonial_subjects = 1
	#		is_year = 1500
	#	}
	#	allow =  {
	#		OR = {
	#			advisor = naval_reformer
	#			advisor = grand_admiral
	#		}
	#		mil = 4
	#		any_subject_country = {
	#			is_colonial_nation = yes
	#			gold = 1
	#		}
	#	}
	#	effect = {
	#		add_country_modifier = {
	#			name = "convoy_system"
	#			duration = -1
	#		}			
	#	}
	#	ai_will_do = {
	#		factor = 1
	#		# England, France, Barbary Coast
	#	}
	#}
	
	anti_piracy_act = {
		potential = {
			NOT = { has_country_modifier = the_anti_piracy_act }
			NOT = { has_country_modifier = hire_privateers }
			OR = {
				technology_group = western
				technology_group = eastern
				technology_group = high_turkishtech
				technology_group = turkishtech
				technology_group = muslim
			}
			NOT = { government = irish_monarchy }			
			NOT = { culture_group = maghreb }
			NOT = { culture_group = berber_group }
			num_of_ports = 1
			dip_tech = 20
		}
		allow = {
			NOT = { overextension_percentage = 0.025 }
			is_at_war = no
		}
		effect = {
			add_country_modifier = {
				name = "the_anti_piracy_act"
				duration = -1
			}
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	#land_recruiting_act = {
	#	potential = {
	#		NOT = { has_country_modifier = the_land_recruiting_act }
	#		NOT = { has_country_modifier = the_recruiting_act }
	#		OR = { 
	#			leadership_ideas = 4
	#			logistic_ideas = 4
	#		}
	#	}
	#	allow = {
	#		full_idea_group = leadership_ideas
	#		full_idea_group = logistic_ideas 
	#		is_at_war = yes
	#	}
	#	effect = {
	#		add_country_modifier = {
	#			name = "the_land_recruiting_act"
	#			duration = -1
	#		}
	#	}
	#	ai_will_do = {
	#		factor = 1
	#	}
	#}

	#impressment_of_sailors = {
	#	major = no
	#	potential = {
	#		num_of_ports = 1
	#		NOT = { has_country_modifier = impressed_sailors }
	#		has_idea_group = naval_ideas
	#		dip_tech = 16
	#		OR = {
	#			NOT = { has_country_flag = impressment_of_sailors }
	#			had_country_flag = { flag = impressment_of_sailors days = 3650 }
	#		}
	#	}
	#	allow = {
	#		NOT = { sailor_percentage = 0.30 }
	#		NOT = { has_country_modifier = impressed_sailors }
	#		is_at_war = yes
	#		full_idea_group = naval_ideas
	#	}
	#	effect = {
	#		add_yearly_manpower = 2
	#		set_country_flag = impressment_of_sailors
	#		add_country_modifier = {
    #				name = "impressed_sailors"
   	#			duration = 1825
	#		}
	#	}
	#	ai_will_do = {
	#		factor = 1
	#	}
	#}
}
