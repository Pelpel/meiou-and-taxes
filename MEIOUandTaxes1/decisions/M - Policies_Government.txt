country_decisions = {
	
#	check_internal_policies = {
#		potential = {
#			check_variable = { which = Centralisation_Effort_Years value = 1 }
#		}
#		allow = {
#			check_variable = { which = Centralisation_Effort_Years value = 1 }
#		}
#		effect = {
#			country_event = { id = internal_policies.001 days = 0 }
#		}
#		ai_will_do = {
#			factor = 0
#		}
#	}
	
	centralise_the_state_1 = {
		potential = {
			NOT = { has_country_modifier = "centralisation_efforts" }
			NOT = { has_country_modifier = "decentralisation_efforts" }
			check_variable = { which = "centralization_decentralization" value = 5 }
		}
		allow =  {
			has_regency = no
			OR = {
				ADM = 3
				advisor = statesman
			}
			stability = 0
			is_at_war = no
			OR = {
				AND = {
					government = medieval_monarchy
					check_variable = { which = "centralization_decentralization" value = 2 }
				}
				AND = {
					government = feudal_monarchy
					check_variable = { which = "centralization_decentralization" value = 0 }
				}
				AND = {
					government = despotic_monarchy
					check_variable = { which = "centralization_decentralization" value = -1 }
				}
				AND = {
					government = administrative_monarchy
					check_variable = { which = "centralization_decentralization" value = -2 }
				}
				AND = {
					government = constitutional_monarchy
					check_variable = { which = "centralization_decentralization" value = -2 }
				}
				AND = {
					NOT = { government = medieval_monarchy }
					NOT = { government = feudal_monarchy }
					NOT = { government = despotic_monarchy }
					NOT = { government = administrative_monarchy }
					NOT = { government = constitutional_monarchy }
				}
			}
		}
		effect = {
			custom_tooltip = "centralise_the_state_tooltip"
			add_country_modifier = { name = "centralisation_efforts" duration = -1 }
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.50
				government = republic
			}
			modifier = {
				factor = 0.25
				government = theocratic_government
			}
			modifier = {
				factor = 0.10
				government = monastic_order_government
			}
		}
		ai_importance = 1000
	}

	centralise_the_state_2 = {
		potential = {
			NOT = { has_country_modifier = "centralisation_efforts" }
			NOT = { has_country_modifier = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = 5 } }
			check_variable = { which = "centralization_decentralization" value = 4 }
		}
		allow =  {
			has_regency = no
			OR = {
				ADM = 3
				advisor = statesman
			}
			stability = 0
			is_at_war = no
			OR = {
				AND = {
					government = medieval_monarchy
					check_variable = { which = "centralization_decentralization" value = 2 }
				}
				AND = {
					government = feudal_monarchy
					check_variable = { which = "centralization_decentralization" value = 0 }
				}
				AND = {
					government = despotic_monarchy
					check_variable = { which = "centralization_decentralization" value = -1 }
				}
				AND = {
					government = administrative_monarchy
					check_variable = { which = "centralization_decentralization" value = -2 }
				}
				AND = {
					government = constitutional_monarchy
					check_variable = { which = "centralization_decentralization" value = -2 }
				}
				AND = {
					NOT = { government = medieval_monarchy }
					NOT = { government = feudal_monarchy }
					NOT = { government = despotic_monarchy }
					NOT = { government = administrative_monarchy }
					NOT = { government = constitutional_monarchy }
				}
			}
		}
		effect = {
			custom_tooltip = "centralise_the_state_tooltip"
			add_country_modifier = { name = "centralisation_efforts" duration = -1 }
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.50
				government = republic
			}
			modifier = {
				factor = 0.25
				government = theocratic_government
			}
			modifier = {
				factor = 0.10
				government = monastic_order_government
			}
		}
		ai_importance = 1000
	}

	centralise_the_state_3 = {
		potential = {
			NOT = { has_country_modifier = "centralisation_efforts" }
			NOT = { has_country_modifier = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = 4 } }
			check_variable = { which = "centralization_decentralization" value = 3 }
		}
		allow =  {
			has_regency = no
			OR = {
				ADM = 3
				advisor = statesman
			}
			stability = 0
			is_at_war = no
			OR = {
				AND = {
					government = medieval_monarchy
					check_variable = { which = "centralization_decentralization" value = 2 }
				}
				AND = {
					government = feudal_monarchy
					check_variable = { which = "centralization_decentralization" value = 0 }
				}
				AND = {
					government = despotic_monarchy
					check_variable = { which = "centralization_decentralization" value = -1 }
				}
				AND = {
					government = administrative_monarchy
					check_variable = { which = "centralization_decentralization" value = -2 }
				}
				AND = {
					government = constitutional_monarchy
					check_variable = { which = "centralization_decentralization" value = -2 }
				}
				AND = {
					NOT = { government = medieval_monarchy }
					NOT = { government = feudal_monarchy }
					NOT = { government = despotic_monarchy }
					NOT = { government = administrative_monarchy }
					NOT = { government = constitutional_monarchy }
				}
			}
		}
		effect = {
			custom_tooltip = "centralise_the_state_tooltip"
			add_country_modifier = { name = "centralisation_efforts" duration = -1 }
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.50
				government = republic
			}
			modifier = {
				factor = 0.25
				government = theocratic_government
			}
			modifier = {
				factor = 0.10
				government = monastic_order_government
			}
		}
		ai_importance = 1000
	}

	centralise_the_state_4 = {
		potential = {
			NOT = { has_country_modifier = "centralisation_efforts" }
			NOT = { has_country_modifier = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = 3 } }
			check_variable = { which = "centralization_decentralization" value = 2 }
		}
		allow =  {
			has_regency = no
			OR = {
				ADM = 3
				advisor = statesman
			}
			stability = 0
			is_at_war = no
			OR = {
				AND = {
					government = medieval_monarchy
					check_variable = { which = "centralization_decentralization" value = 2 }
				}
				AND = {
					government = feudal_monarchy
					check_variable = { which = "centralization_decentralization" value = 0 }
				}
				AND = {
					government = despotic_monarchy
					check_variable = { which = "centralization_decentralization" value = -1 }
				}
				AND = {
					government = administrative_monarchy
					check_variable = { which = "centralization_decentralization" value = -2 }
				}
				AND = {
					government = constitutional_monarchy
					check_variable = { which = "centralization_decentralization" value = -2 }
				}
				AND = {
					NOT = { government = medieval_monarchy }
					NOT = { government = feudal_monarchy }
					NOT = { government = despotic_monarchy }
					NOT = { government = administrative_monarchy }
					NOT = { government = constitutional_monarchy }
				}
			}
		}
		effect = {
			custom_tooltip = "centralise_the_state_tooltip"
			add_country_modifier = { name = "centralisation_efforts" duration = -1 }
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.50
				government = republic
			}
			modifier = {
				factor = 0.25
				government = theocratic_government
			}
			modifier = {
				factor = 0.10
				government = monastic_order_government
			}
		}
		ai_importance = 1000
	}

	centralise_the_state_5 = {
		potential = {
			NOT = { has_country_modifier = "centralisation_efforts" }
			NOT = { has_country_modifier = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = 2 } }
			check_variable = { which = "centralization_decentralization" value = 1 }
		}
		allow =  {
			has_regency = no
			OR = {
				ADM = 3
				advisor = statesman
			}
			stability = 0
			is_at_war = no
			OR = {
				AND = {
					government = medieval_monarchy
					check_variable = { which = "centralization_decentralization" value = 2 }
				}
				AND = {
					government = feudal_monarchy
					check_variable = { which = "centralization_decentralization" value = 0 }
				}
				AND = {
					government = despotic_monarchy
					check_variable = { which = "centralization_decentralization" value = -1 }
				}
				AND = {
					government = administrative_monarchy
					check_variable = { which = "centralization_decentralization" value = -2 }
				}
				AND = {
					government = constitutional_monarchy
					check_variable = { which = "centralization_decentralization" value = -2 }
				}
				AND = {
					NOT = { government = medieval_monarchy }
					NOT = { government = feudal_monarchy }
					NOT = { government = despotic_monarchy }
					NOT = { government = administrative_monarchy }
					NOT = { government = constitutional_monarchy }
				}
			}
		}
		effect = {
			custom_tooltip = "centralise_the_state_tooltip"
			add_country_modifier = { name = "centralisation_efforts" duration = -1 }
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.50
				government = republic
			}
			modifier = {
				factor = 0.25
				government = theocratic_government
			}
			modifier = {
				factor = 0.10
				government = monastic_order_government
			}
		}
		ai_importance = 1000
	}

	centralise_the_state_6 = {
		potential = {
			NOT = { has_country_modifier = "centralisation_efforts" }
			NOT = { has_country_modifier = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = 1 } }
			check_variable = { which = "centralization_decentralization" value = 0 }
		}
		allow =  {
			has_regency = no
			OR = {
				ADM = 3
				advisor = statesman
			}
			stability = 0
			is_at_war = no
			OR = {
				AND = {
					government = medieval_monarchy
					check_variable = { which = "centralization_decentralization" value = 2 }
				}
				AND = {
					government = feudal_monarchy
					check_variable = { which = "centralization_decentralization" value = 0 }
				}
				AND = {
					government = despotic_monarchy
					check_variable = { which = "centralization_decentralization" value = -1 }
				}
				AND = {
					government = administrative_monarchy
					check_variable = { which = "centralization_decentralization" value = -2 }
				}
				AND = {
					government = constitutional_monarchy
					check_variable = { which = "centralization_decentralization" value = -2 }
				}
				AND = {
					NOT = { government = medieval_monarchy }
					NOT = { government = feudal_monarchy }
					NOT = { government = despotic_monarchy }
					NOT = { government = administrative_monarchy }
					NOT = { government = constitutional_monarchy }
				}
			}
		}
		effect = {
			custom_tooltip = "centralise_the_state_tooltip"
			add_country_modifier = { name = "centralisation_efforts" duration = -1 }
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.50
				government = republic
			}
			modifier = {
				factor = 0.25
				government = theocratic_government
			}
			modifier = {
				factor = 0.10
				government = monastic_order_government
			}
		}
		ai_importance = 1000
	}

	centralise_the_state_7 = {
		potential = {
			NOT = { has_country_modifier = "centralisation_efforts" }
			NOT = { has_country_modifier = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = 0 } }
			check_variable = { which = "centralization_decentralization" value = -1 }
		}
		allow =  {
			has_regency = no
			OR = {
				ADM = 3
				advisor = statesman
			}
			stability = 0
			is_at_war = no
			OR = {
				AND = {
					government = medieval_monarchy
					check_variable = { which = "centralization_decentralization" value = 2 }
				}
				AND = {
					government = feudal_monarchy
					check_variable = { which = "centralization_decentralization" value = 0 }
				}
				AND = {
					government = despotic_monarchy
					check_variable = { which = "centralization_decentralization" value = -1 }
				}
				AND = {
					government = administrative_monarchy
					check_variable = { which = "centralization_decentralization" value = -2 }
				}
				AND = {
					government = constitutional_monarchy
					check_variable = { which = "centralization_decentralization" value = -2 }
				}
				AND = {
					NOT = { government = medieval_monarchy }
					NOT = { government = feudal_monarchy }
					NOT = { government = despotic_monarchy }
					NOT = { government = administrative_monarchy }
					NOT = { government = constitutional_monarchy }
				}
			}
		}
		effect = {
			custom_tooltip = "centralise_the_state_tooltip"
			add_country_modifier = { name = "centralisation_efforts" duration = -1 }
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.50
				government = republic
			}
			modifier = {
				factor = 0.25
				government = theocratic_government
			}
			modifier = {
				factor = 0.10
				government = monastic_order_government
			}
		}
		ai_importance = 1000
	}

	centralise_the_state_8 = {
		potential = {
			NOT = { has_country_modifier = "centralisation_efforts" }
			NOT = { has_country_modifier = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = -1 } }
			check_variable = { which = "centralization_decentralization" value = -2 }
		}
		allow =  {
			has_regency = no
			OR = {
				ADM = 3
				advisor = statesman
			}
			stability = 0
			is_at_war = no
			OR = {
				AND = {
					government = medieval_monarchy
					check_variable = { which = "centralization_decentralization" value = 2 }
				}
				AND = {
					government = feudal_monarchy
					check_variable = { which = "centralization_decentralization" value = 0 }
				}
				AND = {
					government = despotic_monarchy
					check_variable = { which = "centralization_decentralization" value = -1 }
				}
				AND = {
					government = administrative_monarchy
					check_variable = { which = "centralization_decentralization" value = -2 }
				}
				AND = {
					government = constitutional_monarchy
					check_variable = { which = "centralization_decentralization" value = -2 }
				}
				AND = {
					NOT = { government = medieval_monarchy }
					NOT = { government = feudal_monarchy }
					NOT = { government = despotic_monarchy }
					NOT = { government = administrative_monarchy }
					NOT = { government = constitutional_monarchy }
				}
			}
		}
		effect = {
			custom_tooltip = "centralise_the_state_tooltip"
			add_country_modifier = { name = "centralisation_efforts" duration = -1 }
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.50
				government = republic
			}
			modifier = {
				factor = 0.25
				government = theocratic_government
			}
			modifier = {
				factor = 0.10
				government = monastic_order_government
			}
		}
		ai_importance = 1000
	}

	centralise_the_state_9 = {
		potential = {
			NOT = { has_country_modifier = "centralisation_efforts" }
			NOT = { has_country_modifier = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = -2 } }
			check_variable = { which = "centralization_decentralization" value = -3 }
		}
		allow =  {
			has_regency = no
			OR = {
				ADM = 3
				advisor = statesman
			}
			stability = 0
			is_at_war = no
			OR = {
				AND = {
					government = medieval_monarchy
					check_variable = { which = "centralization_decentralization" value = 2 }
				}
				AND = {
					government = feudal_monarchy
					check_variable = { which = "centralization_decentralization" value = 0 }
				}
				AND = {
					government = despotic_monarchy
					check_variable = { which = "centralization_decentralization" value = -1 }
				}
				AND = {
					government = administrative_monarchy
					check_variable = { which = "centralization_decentralization" value = -2 }
				}
				AND = {
					government = constitutional_monarchy
					check_variable = { which = "centralization_decentralization" value = -2 }
				}
				AND = {
					NOT = { government = medieval_monarchy }
					NOT = { government = feudal_monarchy }
					NOT = { government = despotic_monarchy }
					NOT = { government = administrative_monarchy }
					NOT = { government = constitutional_monarchy }
				}
			}
		}
		effect = {
			custom_tooltip = "centralise_the_state_tooltip"
			add_country_modifier = { name = "centralisation_efforts" duration = -1 }
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.50
				government = republic
			}
			modifier = {
				factor = 0.25
				government = theocratic_government
			}
			modifier = {
				factor = 0.10
				government = monastic_order_government
			}
		}
		ai_importance = 1000
	}

	centralise_the_state_10 = {
		potential = {
			NOT = { has_country_modifier = "centralisation_efforts" }
			NOT = { has_country_modifier = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = -3 } }
			check_variable = { which = "centralization_decentralization" value = -4 }
		}
		allow =  {
			has_regency = no
			OR = {
				ADM = 3
				advisor = statesman
			}
			stability = 0
			is_at_war = no
			OR = {
				AND = {
					government = medieval_monarchy
					check_variable = { which = "centralization_decentralization" value = 2 }
				}
				AND = {
					government = feudal_monarchy
					check_variable = { which = "centralization_decentralization" value = 0 }
				}
				AND = {
					government = despotic_monarchy
					check_variable = { which = "centralization_decentralization" value = -1 }
				}
				AND = {
					government = administrative_monarchy
					check_variable = { which = "centralization_decentralization" value = -2 }
				}
				AND = {
					government = constitutional_monarchy
					check_variable = { which = "centralization_decentralization" value = -2 }
				}
				AND = {
					NOT = { government = medieval_monarchy }
					NOT = { government = feudal_monarchy }
					NOT = { government = despotic_monarchy }
					NOT = { government = administrative_monarchy }
					NOT = { government = constitutional_monarchy }
				}
			}
		}
		effect = {
			custom_tooltip = "centralise_the_state_tooltip"
			add_country_modifier = { name = "centralisation_efforts" duration = -1 }
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.50
				government = republic
			}
			modifier = {
				factor = 0.25
				government = theocratic_government
			}
			modifier = {
				factor = 0.10
				government = monastic_order_government
			}
		}
		ai_importance = 1000
	}
	
	cancel_centralisation = {
		potential = {
			has_country_modifier = "centralisation_efforts"
		}
		allow = {
			has_country_modifier = "centralisation_efforts"
		}
		effect = {
			remove_country_modifier = "centralisation_efforts"
		}
		ai_will_do = {
			factor = 0
		}
	}

	decentralise_the_state_1 = {
		potential = {
			NOT = { has_country_modifier = "centralisation_efforts" }
			NOT = { has_country_modifier = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = -4 } }
			check_variable = { which = "centralization_decentralization" value = -5 }
		}
		allow =  {
			has_regency = no
			OR = {
				ADM = 3
				advisor = statesman
			}
			stability = 0
			is_at_war = no
			OR = {
				AND = {
					government = enlightened_despotism
					NOT = { check_variable = { which = "centralization_decentralization" value = 3 } }
				}
				AND = {
					government = absolute_monarchy
					NOT = { check_variable = { which = "centralization_decentralization" value = 1 } }
				}
				AND = {
					NOT = { government = enlightened_despotism }
					NOT = { government = absolute_monarchy }
				}
			}
		}
		effect = {
			custom_tooltip = "decentralise_the_state_tooltip"
			country_event = { id = centralisation.101 days = 100 }
			add_country_modifier = { name = "decentralisation_efforts" duration = 100 }
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 0
		}
	}

	decentralise_the_state_2 = {
		potential = {
			NOT = { has_country_modifier = "centralisation_efforts" }
			NOT = { has_country_modifier = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = -3 } }
			check_variable = { which = "centralization_decentralization" value = -4 }
		}
		allow =  {
			has_regency = no
			OR = {
				ADM = 3
				advisor = statesman
			}
			stability = 0
			is_at_war = no
			OR = {
				AND = {
					government = enlightened_despotism
					NOT = { check_variable = { which = "centralization_decentralization" value = 3 } }
				}
				AND = {
					government = absolute_monarchy
					NOT = { check_variable = { which = "centralization_decentralization" value = 1 } }
				}
				AND = {
					NOT = { government = enlightened_despotism }
					NOT = { government = absolute_monarchy }
				}
			}
		}
		effect = {
			custom_tooltip = "decentralise_the_state_tooltip"
			country_event = { id = centralisation.101 days = 100 }
			add_country_modifier = { name = "decentralisation_efforts" duration = 100 }
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 0
		}
	}

	decentralise_the_state_3 = {
		potential = {
			NOT = { has_country_modifier = "centralisation_efforts" }
			NOT = { has_country_modifier = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = -2 } }
			check_variable = { which = "centralization_decentralization" value = -3 }
		}
		allow =  {
			has_regency = no
			OR = {
				ADM = 3
				advisor = statesman
			}
			stability = 0
			is_at_war = no
			OR = {
				AND = {
					government = enlightened_despotism
					NOT = { check_variable = { which = "centralization_decentralization" value = 3 } }
				}
				AND = {
					government = absolute_monarchy
					NOT = { check_variable = { which = "centralization_decentralization" value = 1 } }
				}
				AND = {
					NOT = { government = enlightened_despotism }
					NOT = { government = absolute_monarchy }
				}
			}
		}
		effect = {
			custom_tooltip = "decentralise_the_state_tooltip"
			country_event = { id = centralisation.101 days = 100 }
			add_country_modifier = { name = "decentralisation_efforts" duration = 100 }
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 0
		}
	}

	decentralise_the_state_4 = {
		potential = {
			NOT = { has_country_modifier = "centralisation_efforts" }
			NOT = { has_country_modifier = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = -1 } }
			check_variable = { which = "centralization_decentralization" value = -2 }
		}
		allow =  {
			has_regency = no
			OR = {
				ADM = 3
				advisor = statesman
			}
			stability = 0
			is_at_war = no
			OR = {
				AND = {
					government = enlightened_despotism
					NOT = { check_variable = { which = "centralization_decentralization" value = 3 } }
				}
				AND = {
					government = absolute_monarchy
					NOT = { check_variable = { which = "centralization_decentralization" value = 1 } }
				}
				AND = {
					NOT = { government = enlightened_despotism }
					NOT = { government = absolute_monarchy }
				}
			}
		}
		effect = {
			custom_tooltip = "decentralise_the_state_tooltip"
			country_event = { id = centralisation.101 days = 100 }
			add_country_modifier = { name = "decentralisation_efforts" duration = 100 }
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 0
		}
	}

	decentralise_the_state_5 = {
		potential = {
			NOT = { has_country_modifier = "centralisation_efforts" }
			NOT = { has_country_modifier = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = 0 } }
			check_variable = { which = "centralization_decentralization" value = -1 }
		}
		allow =  {
			has_regency = no
			OR = {
				ADM = 3
				advisor = statesman
			}
			stability = 0
			is_at_war = no
			OR = {
				AND = {
					government = enlightened_despotism
					NOT = { check_variable = { which = "centralization_decentralization" value = 3 } }
				}
				AND = {
					government = absolute_monarchy
					NOT = { check_variable = { which = "centralization_decentralization" value = 1 } }
				}
				AND = {
					NOT = { government = enlightened_despotism }
					NOT = { government = absolute_monarchy }
				}
			}
		}
		effect = {
			custom_tooltip = "decentralise_the_state_tooltip"
			country_event = { id = centralisation.101 days = 100 }
			add_country_modifier = { name = "decentralisation_efforts" duration = 100 }
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 0
		}
	}

	decentralise_the_state_6 = {
		potential = {
			NOT = { has_country_modifier = "centralisation_efforts" }
			NOT = { has_country_modifier = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = 1 } }
			check_variable = { which = "centralization_decentralization" value = 0 }
		}
		allow =  {
			has_regency = no
			OR = {
				ADM = 3
				advisor = statesman
			}
			stability = 0
			is_at_war = no
			OR = {
				AND = {
					government = enlightened_despotism
					NOT = { check_variable = { which = "centralization_decentralization" value = 3 } }
				}
				AND = {
					government = absolute_monarchy
					NOT = { check_variable = { which = "centralization_decentralization" value = 1 } }
				}
				AND = {
					NOT = { government = enlightened_despotism }
					NOT = { government = absolute_monarchy }
				}
			}
		}
		effect = {
			custom_tooltip = "decentralise_the_state_tooltip"
			country_event = { id = centralisation.101 days = 100 }
			add_country_modifier = { name = "decentralisation_efforts" duration = 100 }
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 0
		}
	}

	decentralise_the_state_7 = {
		potential = {
			NOT = { has_country_modifier = "centralisation_efforts" }
			NOT = { has_country_modifier = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = 2 } }
			check_variable = { which = "centralization_decentralization" value = 1 }
		}
		allow =  {
			has_regency = no
			OR = {
				ADM = 3
				advisor = statesman
			}
			stability = 0
			is_at_war = no
			OR = {
				AND = {
					government = enlightened_despotism
					NOT = { check_variable = { which = "centralization_decentralization" value = 3 } }
				}
				AND = {
					government = absolute_monarchy
					NOT = { check_variable = { which = "centralization_decentralization" value = 1 } }
				}
				AND = {
					NOT = { government = enlightened_despotism }
					NOT = { government = absolute_monarchy }
				}
			}
		}
		effect = {
			custom_tooltip = "decentralise_the_state_tooltip"
			country_event = { id = centralisation.101 days = 100 }
			add_country_modifier = { name = "decentralisation_efforts" duration = 100 }
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 0
		}
	}

	decentralise_the_state_8 = {
		potential = {
			NOT = { has_country_modifier = "centralisation_efforts" }
			NOT = { has_country_modifier = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = 3 } }
			check_variable = { which = "centralization_decentralization" value = 2 }
		}
		allow =  {
			has_regency = no
			OR = {
				ADM = 3
				advisor = statesman
			}
			stability = 0
			is_at_war = no
			OR = {
				AND = {
					government = enlightened_despotism
					NOT = { check_variable = { which = "centralization_decentralization" value = 3 } }
				}
				AND = {
					government = absolute_monarchy
					NOT = { check_variable = { which = "centralization_decentralization" value = 1 } }
				}
				AND = {
					NOT = { government = enlightened_despotism }
					NOT = { government = absolute_monarchy }
				}
			}
		}
		effect = {
			custom_tooltip = "decentralise_the_state_tooltip"
			country_event = { id = centralisation.101 days = 100 }
			add_country_modifier = { name = "decentralisation_efforts" duration = 100 }
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 0
		}
	}

	decentralise_the_state_9 = {
		potential = {
			NOT = { has_country_modifier = "centralisation_efforts" }
			NOT = { has_country_modifier = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = 4 } }
			check_variable = { which = "centralization_decentralization" value = 3 }
		}
		allow =  {
			has_regency = no
			OR = {
				ADM = 3
				advisor = statesman
			}
			stability = 0
			is_at_war = no
			OR = {
				AND = {
					government = enlightened_despotism
					NOT = { check_variable = { which = "centralization_decentralization" value = 3 } }
				}
				AND = {
					government = absolute_monarchy
					NOT = { check_variable = { which = "centralization_decentralization" value = 1 } }
				}
				AND = {
					NOT = { government = enlightened_despotism }
					NOT = { government = absolute_monarchy }
				}
			}
		}
		effect = {
			custom_tooltip = "decentralise_the_state_tooltip"
			country_event = { id = centralisation.101 days = 100 }
			add_country_modifier = { name = "decentralisation_efforts" duration = 100 }
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 0
		}
	}

	decentralise_the_state_10 = {
		potential = {
			NOT = { has_country_modifier = "centralisation_efforts" }
			NOT = { has_country_modifier = "decentralisation_efforts" }
			NOT = { check_variable = { which = "centralization_decentralization" value = 5 } }
			check_variable = { which = "centralization_decentralization" value = 4 }
		}
		allow =  {
			has_regency = no
			OR = {
				ADM = 3
				advisor = statesman
			}
			stability = 0
			is_at_war = no
			OR = {
				AND = {
					government = enlightened_despotism
					NOT = { check_variable = { which = "centralization_decentralization" value = 3 } }
				}
				AND = {
					government = absolute_monarchy
					NOT = { check_variable = { which = "centralization_decentralization" value = 1 } }
				}
				AND = {
					NOT = { government = enlightened_despotism }
					NOT = { government = absolute_monarchy }
				}
			}
		}
		effect = {
			custom_tooltip = "decentralise_the_state_tooltip"
			country_event = { id = centralisation.101 days = 100 }
			add_country_modifier = { name = "decentralisation_efforts" duration = 100 }
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 0
		}
	}
	
#	cancel_decentralisation = {
#		potential = {
#			has_country_modifier = "decentralisation_efforts"
#		}
#		allow = {
#			has_country_modifier = "decentralisation_efforts"
#		}
#		effect = {
#			remove_country_modifier = "decentralisation_efforts"
#		}
#		ai_will_do = {
#			factor = 0
#		}
#	}
	
	check_italian_policies = {
		potential = {
			check_variable = { which = italian_nations value = 5 }
			check_variable = { which = italian_power value = 1 }
		}
		allow = {
			check_variable = { which = italian_nations value = 5 }
			check_variable = { which = italian_power value = 1 }
		}
		effect = {
			country_event = { id = internal_policies.101 days = 0 }
		}
		ai_will_do = {
			factor = 0
		}
	}

	decentralise_the_state_stab = {
		potential = {
			NOT = { check_variable = { which = "centralization_decentralization" value = 5 } }
			NOT = { stability = 1 }
		}
		allow =  {
			OR = {
				ADM = 3
				advisor = statesman
			}
			is_at_war = no
			NOT = { has_real_disaster_trigger = yes }
			NOT = { has_country_modifier = stability_cooldown_decent }
			OR = {
				AND = {
					government = enlightened_despotism
					NOT = { check_variable = { which = "centralization_decentralization" value = 3 } }
				}
				AND = {
					government = absolute_monarchy
					NOT = { check_variable = { which = "centralization_decentralization" value = 1 } }
				}
				AND = {
					NOT = { government = enlightened_despotism }
					NOT = { government = absolute_monarchy }
				}
			}
		}
		effect = {
			country_event = { id = centralisation.101 days = 100 }
			add_stability_1 = yes
			add_ruler_modifier = { name = "decentralisation_efforts" }
			add_country_modifier = {
				name = "stability_cooldown_decent"
				duration = 1825
			}
			if = { limit = { ai = no } add_adm_power = -100 }
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				stability = 0
			}
		}
		ai_importance = 1000
	}

}
