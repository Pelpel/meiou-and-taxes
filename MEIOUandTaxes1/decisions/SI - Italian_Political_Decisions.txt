# Serenissima Italia Political Decisions
# by Marco Dandolo

country_decisions = {
    
    move_capital_to_torino = {
        major = yes    
        potential = {
            OR = {
                tag = SAV
                tag = SPI
            }
            capital = 205
            NOT = { has_country_flag = savoia_moved_to_torino }
        }
        allow = {
            adm_tech = 18
            years_of_income = 0.35
            is_at_war = no
            owns = 103
        }
        effect = {
            add_years_of_income = -0.25
            add_prestige = 5
            103 = { add_base_tax = 1 }
            103 = { add_base_manpower = 1 }
            set_capital = 103
			if = {
				limit = {
					NOT = { primary_culture = piedmontese }
				}
				change_primary_culture = piedmontese
			}
            set_country_flag = savoia_moved_to_torino
        }
        
        ai_will_do = {
            factor = 1
        }
    }
    
    move_capital_to_pesaro = {
        major = yes    
        potential = {
            tag = URB
            capital = 1414
            NOT = { has_country_flag = urbino_moved_to_pesaro }
        }
        allow = {
            adm_tech = 18
            years_of_income = 0.35
            is_at_war = no
            owns = 3700 #Meiou
        }
        effect = {
            add_years_of_income = -0.25
            add_prestige = 7
            3700 = { add_base_tax = 1 }
            3700 = { add_base_manpower = 1 }
            set_capital = 3700
            set_country_flag = urbino_moved_to_pesaro
        }
        
        ai_will_do = {
            factor = 1
        }
    }
    
    finish_duomo_di_siena = {
        major = yes    
        potential = {
            tag = SIE
            capital = 117
            NOT = { has_country_flag = siena_duomo_build }
        }
        allow = {
            piety = -0.20
            religion = catholic
            adm_tech = 15
            OR = { 
				adm = 4
				advisor = architect
			}
            treasury = 1200
            is_at_war = no
            owns = 117
        }
        effect = {
            add_treasury = -1000
            add_prestige = 20
            add_adm_power = 100
            117 = { add_base_tax = 1 }
            117 = { add_base_manpower = 1 }
            117 = { add_building = temple }
            set_country_flag = siena_duomo_build
        }
        
        ai_will_do = {
            factor = 1
        }
    }
    
    become_duke_of_mantua = {
        major = yes    
        potential = {
            tag = MAN
            government = monarchy
			NOT = { government_rank = 3 }
            NOT = { has_country_flag = duke_of_mantua }
        }
        allow = {
            adm_tech = 15
            is_at_war = no
            109 = { is_part_of_hre = yes }
            years_of_income = 1.5
        }
        effect = {
            add_years_of_income = -1
            add_prestige = 10
			change_government = feudal_monarchy
			set_government_rank = 3
			add_legitimacy = 50
            set_country_flag = duke_of_mantua
        }
        
        ai_will_do = {
            factor = 1
        }
    }
	
		become_duke_of_milan = {
        major = yes    
        potential = {
            tag = MLO
            government = monarchy
			NOT = { government_rank = 3 }
            NOT = { has_country_flag = duke_of_milan }
        }
        allow = {
            adm_tech = 9
            is_at_war = no
			prestige = 10
			legitimacy = 70
            104 = { is_part_of_hre = yes }
            years_of_income = 1.5
        }
        effect = {
            add_years_of_income = -1
            add_prestige = 10
			change_government = feudal_monarchy
			set_government_rank = 3
            add_legitimacy = 50
            set_country_flag = duke_of_milan
        }
        
        ai_will_do = {
            factor = 1
        }
    }
    
    create_duchy_for_family = {
        major = yes    
        potential = {
            NOT = { tag = MLO }
            owns = 1346
            NOT = { 1346 = { 
                        is_core = ROOT 
                    }
            }
            NOT = { 1346 = {
                        is_core = PAR
                    }
            }
            NOT = { exists = PAR }
            NOT = {
                owns = 101 #Liguria
				owns = 104 #Lombardia
                owns = 106 #Modena
				owns = 1347 #Pavia
                owns = 2564 #Massa 
                owns = 2372 #Cremona
            }
        }
        allow = {
            government = monarchy
            is_at_war = no
        }
        effect = {
            add_prestige = 15
            add_legitimacy = 30
            1346 = { add_core = PAR }
            release_vassal = PAR
            PAR = { 
				define_ruler = {
					name = "Alessandro"
					dynasty = ROOT
				}
			}
        }
        
        ai_will_do = {
            factor = 1
        }
    }
    
    establish_livorno_harbour = {
        major = yes    
        potential = {
            OR = {
				tag = TUS
				tag = PIS
				AND = {
					tag = ITA
					NOT = { owns = 101 }
					NOT = { owns = 112 }
				}
			}			
            NOT = { has_country_flag = livorno_is_harbour }
			1380 = { has_province_modifier = lack_of_harbour }
        }
        allow = {
			adm_tech = 24
            treasury = 600
            is_at_war = no
            owns = 1380
			owns = 3868
        }
        effect = {
            add_treasury = -500
			add_adm_power = -50
            1380 = { # Pisa
                add_base_tax = -2
				add_base_production = -2
			}
			3868 = { # Livorno
				add_base_tax = 3
				add_base_production = 3
				add_base_manpower = 1
				change_trade_goods = naval_supplies
				add_permanent_province_modifier = {
					name = minor_center_of_trade_modifier
					duration = -1
				}
			}				
            set_country_flag = livorno_is_harbour
        }
        
        ai_will_do = {
            factor = 1
        }
    }
    
    reestablish_pisa = {
        major = yes    
        potential = {
            tag = PIS
            capital = 1380
			NOT = { has_country_flag = pisa_is_harbour }
			1380 = { has_province_modifier = lack_of_harbour }
			NOT = { has_country_flag = livorno_is_harbour }
        }
        allow = {
            years_of_income = 0.35
            is_at_war = no
            owns = 1380
            has_country_modifier = order_of_santo_stefano
        }
        effect = {
            add_years_of_income = -0.25
            add_prestige = 10
            1380 = { 
                add_base_tax = 1 
                galley = ROOT
            }
            set_country_flag = pisa_is_harbour
        }
        
        ai_will_do = {
            factor = 1
        }
    }
    
    create_order_santo_stefano = {
        major = yes    
        potential = {
            tag = TUS
            NOT = { has_country_modifier = order_of_santo_stefano }
        }
        allow = {
            mil = 3
            years_of_income = 0.25
            num_of_ports = 2
            num_of_galley = 5
        }
        effect = {
            add_years_of_income = -0.15
            add_prestige = 5
            add_papal_influence = 15
            add_country_modifier = {
                name = "order_of_santo_stefano"
                duration = -1
            }
            set_country_flag = order_of_santo_stefano
        }
        

        ai_will_do = {
            factor = 1
        }
    }
	
	signoria_to_monarchy = {
		major = yes
		potential = {
			government = signoria_monarchy
			is_subject = no
		}
		allow =  {
			adm_tech = 16
			stability = 1
			OR = { 
				full_idea_group = administrative_ideas
				adm = 4
			}
			is_at_war = no 
			num_of_cities = 2
			has_regency = no
		}
		effect = {
			change_government = feudal_monarchy
			subtract_stability_1 = yes
			every_owned_province = { add_unrest = 5 }
		}
		ai_will_do = {
			factor = 1
		}
	}
}
