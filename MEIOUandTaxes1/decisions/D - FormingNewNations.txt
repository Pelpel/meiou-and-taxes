
#######################################
#                                     #
#          whatifnations.txt          #
#                                     #
#######################################
#
#2011-jan-16 FB add ai_importance = 200
#2011-aug-01 FB fixes to denmark_norway_nation
# 2013-aug-26 GG EUIV changes
#
#######################################
#
# List of decisions :
#
# restore_latine_empire
#
########################################

country_decisions = {

	indonesian_nation = {
		major = yes
		potential = {
			NOT = { exists = IND }
			OR = {
				culture_group = malay
				culture_group = javan_group
				primary_culture = bugis
				primary_culture = moluccan
				primary_culture = papuan
				primary_culture = sulawesi
			}
			OR = {
				religion_group = muslim
				religion_group = christian
				religion_group = taoic
			}
			NOT = { tag = MPH }
			NOT = { tag = SRV }
			NOT = { tag = MLC }
		}
		allow = {
			is_subject = no
			is_at_war = no
			OR = {
				check_variable = { which = "Demesne_in_Indonesia" value = 40 }
				check_variable = { which = "Cores_on_Indonesia" value = 30 }
			}
			owns = 2108   # Jakarta
			owns = 2105   # Surubaya
			owns = 2095   # Siak
			owns = 622   # Srivijaya
			owns = 641   # Makassar

		}
		effect = {
			sumatra_region = { limit = { owned_by = ROOT } remove_core = IND add_core = IND }
			sumatra_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = IND }
			java_region = { limit = { owned_by = ROOT } remove_core = IND add_core = IND }
			java_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = IND }
			borneo_region = { limit = { owned_by = ROOT } remove_core = IND add_core = IND }
			borneo_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = IND }
			indonesia_region = { limit = { owned_by = ROOT } remove_core = IND add_core = IND }
			indonesia_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = IND }
			philippines_region = { limit = { owned_by = ROOT } remove_core = IND add_core = IND }
			philippines_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = IND }
	        add_prestige = 15
			if = {
				limit = { government = monarchy NOT = { government_rank = 5 } }
				set_government_rank = 5
			}
			#filipino_group_union_effect = yes
			#add_accepted_culture = filipino
			#add_accepted_culture = maguindanao
			#add_accepted_culture = tagalog
			#add_accepted_culture = cebuano
			#add_accepted_culture = taiwanese
			#malay_union_effect = yes
			#add_accepted_culture = polynesian
			#add_accepted_culture = cham
			#add_accepted_culture = malayan
			#add_accepted_culture = indonesian
			#add_accepted_culture = acehenese
			#add_accepted_culture = batak
			#add_accepted_culture = minang
			#add_accepted_culture = banjar
			#add_accepted_culture = sumatran
			#add_accepted_culture = tausug
			#javan_group_union_effect = yes
			#add_accepted_culture = sundanese
			#add_accepted_culture = javan
			#add_accepted_culture = balinese
			
	        change_tag = IND
			increase_centralisation = yes
		}
		ai_will_do = {
		    factor = 1
		}
		ai_importance = 400
	}

	srivijaya_nation = {
		major = yes
		potential = {
			NOT = { exists = SRV }
			NOT = { tag = MPH }
			NOT = { tag = IND }
			NOT = { tag = MLC }
			culture_group = malay
			OR = {
				religion = buddhism
				religion = hinduism
			}
		}
		allow = {
			is_subject = no
			is_at_war = no
			OR = {
				check_variable = { which = "Demesne_in_Indonesia" value = 40 }
				check_variable = { which = "Cores_on_Indonesia" value = 30 }
			}
			owns = 2108   # Jakarta
			owns = 2105   # Surubaya
			owns = 2095   # Siak
			owns = 622   # Srivijaya
			owns = 641   # Makassar

		}
		effect = {
			sumatra_region = { limit = { owned_by = ROOT } remove_core = SRV add_core = SRV }
			sumatra_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = SRV }
			java_region = { limit = { owned_by = ROOT } remove_core = SRV add_core = SRV }
			java_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = SRV }
			borneo_region = { limit = { owned_by = ROOT } remove_core = SRV add_core = SRV }
			borneo_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = SRV }
			indonesia_region = { limit = { owned_by = ROOT } remove_core = SRV add_core = SRV }
			indonesia_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = SRV }
			philippines_region = { limit = { owned_by = ROOT } remove_core = SRV add_core = SRV }
			philippines_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = SRV }
	        add_prestige = 15
			if = {
				limit = { government = monarchy NOT = { government_rank = 5 } }
				set_government_rank = 5
			}
	        change_tag = SRV
			increase_centralisation = yes
		}
		ai_will_do = {
		    factor = 1
		}
		ai_importance = 400
	}

	mongol_nation = {
		major = yes
		potential = {
			OR = {
				culture_group = altaic
				culture_group = tartar_group
			}
			NOT = { tag = BLU }
			NOT = { tag = WHI }
			is_nomad = yes
			NOT = { exists = GGK }
		}
		allow = {
			is_subject = no
			is_at_war = no
			OR = {
				check_variable = { which = "Demesne_in_Altaic_Throne" value = 45 }
				check_variable = { which = "Cores_on_Altaic_Throne" value = 30 }
			}
		}
		effect = {
			kazakh_region = { limit = { owned_by = ROOT } remove_core = GGK add_core = GGK }
			kazakh_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = GGK }
			steppes_region = { limit = { owned_by = ROOT } remove_core = GGK add_core = GGK }
			steppes_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = GGK }
			east_siberia_region = { limit = { owned_by = ROOT } remove_core = GGK add_core = GGK }
			east_siberia_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = GGK }
			west_siberia_region = { limit = { owned_by = ROOT } remove_core = GGK add_core = GGK }
			west_siberia_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = GGK }
			add_prestige = 15
			if = {
				limit = { government = monarchy NOT = { government_rank = 6 } }
				set_government_rank = 6
			}
			change_tag = GGK
			increase_centralisation = yes
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}

	become_kingdom_of_jerusalem = {
		major = yes
		potential = {
			OR = {
				tag = CYP
				tag = KNI
				tag = KAM
				dynasty = "de Lusignan"
				AND = {
					OR = {
						tag = KNP
						tag = NAP
					}
					dynasty = "d'Anjou"
				}
			}
			NOT = { exists = KOJ }
			religion = catholic
			palestine_area = { owned_by = ROOT }
		}
		allow = {
			owns = 379
			is_at_war = no
			OR = {
				check_variable = { which = "Demesne_in_Holy_Land" value = 8 }
				check_variable = { which = "Cores_on_Holy_Land" value = 5 }
			}
		}
		effect = {
			palestine_area = { limit = { owned_by = ROOT } remove_core = KOJ add_core = KOJ }
			palestine_area = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = KOJ }
			every_country = {
				limit = { 
					any_owned_province = { area = palestine_area is_core = PREV }
					NOT = { tag = ROOT }
				}
				add_historical_rival = KOJ
			}
			add_prestige = 15
			if = {
				limit = { 
					NOT = { government_rank = 5 }
				}
				set_government_rank = 5
			}
			if = {
				limit = { OR = { government = theocratic_government	government = monastic_order_government } }
				if = {
					limit = { NOT = { adm_tech = 35 } }
					change_government = feudal_monarchy
				}
				if = {
					limit = { adm_tech = 35 }
					change_government = absolute_monarchy
				}
			}
			change_tag = KOJ
			increase_centralisation = yes
			379 = {
				change_province_name = "Jerusalem"
				rename_capital = "Jerusalem"
			}
			set_capital = 379
			increase_centralisation = yes
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = KOJ_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 0
		}
	}

	byzantium_empire = {
		major = yes
		potential = {
			culture_group = turko_byzantine
			OR = {
				religion = orthodox
				religion = coptic
				religion = hellenic_pagan
			}
			forming_BYZ_trigger = yes
			NOT = { exists = BYZ }
			NOT = { tag = ERG }
			NOT = { tag = PAP }
			NOT = { tag = USA }
			NOT = { tag = BRZ }
			NOT = { tag = MEX }
			NOT = { tag = GRE }
			NOT = { tag = TUR }
			NOT = { tag = OTT }
		}
		allow = {
			owns = 1402	#Kostantiniyye
			is_subject = no
			is_at_war = no
			is_core = 1402
			OR = {
				check_variable = { which = "Demesne_in_Constantinople" value = 25 }
				check_variable = { which = "Cores_on_Constantinople" value = 15 }
			}
		}
		effect = {
			greece_region = { limit = { owned_by = ROOT } remove_core = BYZ add_core = BYZ }
			greece_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = BYZ }
			coastal_anatolia_region = { limit = { owned_by = ROOT } remove_core = BYZ add_core = BYZ }
			coastal_anatolia_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = BYZ }
			inland_anatolia_region = { limit = { owned_by = ROOT } remove_core = BYZ add_core = BYZ }
			inland_anatolia_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = BYZ }
			add_prestige = 20
			change_tag = BYZ
			BYZ = { set_capital = 1402 }
			if = {
				limit = { government = monarchy NOT = { government_rank = 6 } }
				set_government_rank = 6
			}
			increase_centralisation = yes
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = BYZ_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1.0
		}
		ai_importance = 400
	}

#	gods_kingdom = {
#		major = yes
#		potential = {
#			has_country_flag = you_know_this_isnt_supposed_to_be_in_the_mod_yet
#			NOT = {
#				exists = ITA
#				exists = ITE
#			}
#			tag = PAP
#			NOT = { has_country_modifier = kingdom_of_god }
#		}
#		allow = {
#			owns = 2530	#Roma
#			owns = 118	#Lazio
#			OR = {
#				check_variable = { which = "Demesne_in_Italy" value = 30 }
#				AND = {
#					check_variable = { which = "Cores_on_Emilia_Romagna" value = 4 }
#					check_variable = { which = "Cores_on_Tuscany" value = 8 }
#					check_variable = { which = "Cores_on_Lombardy" value = 6 }
#					check_variable = { which = "Cores_on_Sardinia" value = 3 }
#					check_variable = { which = "Cores_on_Liguria_Piedmont" value = 4 }
#					check_variable = { which = "Cores_on_Veneto" value = 6 }
#					check_variable = { which = "Cores_on_Umbria_Marcha" value = 4 }
#				}
#			}
#			num_of_cities = 30
#			is_subject = no
#			is_at_war = no
#		}
#		effect = {
#			italy_region = { limit = { owned_by = ROOT } add_core = PAP }
#			italy_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = PAP }
#			latin_union_effect = yes
#			set_country_flag = kingdom_of_god
#			# add_country_modifier = { name = "kingdom_of_god" duration = -1 }
#			add_prestige = 15
#			increase_centralisation = yes
#		}
#		ai_will_do = {
#			factor = 1
#		}
#		ai_importance = 400
#	}

	lothar_nation = {
		major = yes
		potential = {
			NOT = { exists = LOT }
			OR = {
				culture_group = low_frankish
				primary_culture = lorrain
			}
			NOT = { tag = PAP }
			NOT = { tag = NED }
			NOT = { tag = ERG }
		}
		allow = {
			is_subject = no
			is_at_war = no
			OR = {
				check_variable = { which = "Demesne_in_Lotharingia" value = 20 }
				check_variable = { which = "Cores_on_Lotharingia" value = 15 }
			}
			owns = 92	# Brussel
			owns = 97	# Amsterdam
			owns = 189	# Lorraine
		}
		effect = {
			high_countries_region = { limit = { owned_by = ROOT } remove_core = LOT add_core = LOT }
			high_countries_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = LOT }
			low_countries_region = { limit = { owned_by = ROOT } remove_core = LOT add_core = LOT }
			low_countries_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = LOT }
			belgii_region = { limit = { owned_by = ROOT } remove_core = LOT add_core = LOT }
			belgii_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = LOT }
			add_prestige = 15
			if = {
				limit = { government = monarchy NOT = { government_rank = 5 } }
				set_government_rank = 5
			}
			change_tag = LOT
			#low_frankish_union_effect = yes
			#add_accepted_culture = dutch
			#add_accepted_culture = flemish
			#add_accepted_culture = brabantian
			#add_accepted_culture = wallonian
			#add_accepted_culture = burgundian
			#add_accepted_culture = frisian
			increase_centralisation = yes
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = LOT_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}

	unite_scandinavia = {
		major = yes
		potential = {
			OR = {
				culture_group = nord_germanic
				culture_group = swedish_group
			}
			NOT = { exists = KAL }
			forming_KAL_trigger = yes
		}
		allow = {
			is_subject = no
			OR = {
				AND = {
					tag = DAN
					NOT = { exists = DEN }
					NOT = { exists = NOR }
					NOT = { exists = SWE }
				}
				AND = {
					tag = DEN
					NOT = { exists = DAN }
					NOT = { exists = NOR }
					NOT = { exists = SWE }
				}
				AND = {
					tag = NOR
					NOT = { exists = DAN }
					NOT = { exists = DEN }
					NOT = { exists = SWE }
				}
				AND = {
					tag = SWE
					NOT = { exists = DAN }
					NOT = { exists = DEN }
					NOT = { exists = NOR }
				}
			}
			OR = {
				check_variable = { which = "Demesne_in_Scandinavia" value = 30 }
				check_variable = { which = "Cores_on_Scandinavia" value = 25 }
			}
			owns = 1		# Uppland
			owns = 12		# Sjaelland
			owns = 16		# Akershus
			is_at_war = no
		}
		effect = {
			swedish_region = { limit = { owned_by = ROOT } remove_core = KAL add_core = KAL }
			swedish_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = KAL }
			danish_region = { limit = { owned_by = ROOT } remove_core = KAL add_core = KAL }
			danish_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = KAL }
			norwegian_region = { limit = { owned_by = ROOT } remove_core = KAL add_core = KAL }
			norwegian_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = KAL }
			finland_region = { limit = { owned_by = ROOT } remove_core = KAL add_core = KAL }
			finland_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = KAL }
			add_prestige = 15
			if = {
				limit = { government = monarchy NOT = { government_rank = 5 } }
				set_government_rank = 5
			}
			#nord_germanic_union_effect = yes
			#add_accepted_culture = danish
			#add_accepted_culture = scanian
			#add_accepted_culture = jutish
			#add_accepted_culture = norwegian
			#add_accepted_culture = vestlandsk
			#add_accepted_culture = trondersk
			#add_accepted_culture = norse
			#swedish_group_union_effect = yes
			#add_accepted_culture = swedish
			#add_accepted_culture = gotar
			#add_accepted_culture = gutnish
			#add_accepted_culture = finnish
			#add_accepted_culture = estonian
			#add_accepted_culture = ingrian
			change_tag = KAL
			increase_centralisation = yes
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = SCA_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}

	form_suvarnabhumi = {
		major = yes
		potential = {
			NOT = { exists = SUV }
			num_of_cities = 30
			capital_scope = { province_group = suvarnabhumi_group }
		}
		allow = {
			OR = {
				AND = {
					check_variable = { which = "Demesne_in_Thai_Plain" value = 7 }
					check_variable = { which = "Demesne_in_Burma" value = 7 }
					check_variable = { which = "Demesne_in_Malaya_Peninsula" value = 4 }
				}
				AND = {
					check_variable = { which = "Cores_on_Thai_Plain" value = 7 }
					check_variable = { which = "Cores_on_Burma" value = 7 }
					check_variable = { which = "Cores_on_Malaya_Peninsula" value = 4 }
				}
			}
			is_subject = no
			owns = 609  #Angkor
			owns = 600  #Ayutthaya
			owns = 586
			owns = 590
			is_at_war = no
		}
		effect = {
			burma_region = { limit = { owned_by = ROOT } remove_core = SUV add_core = SUV }
			burma_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = SUV }
			lan_xang_region = { limit = { owned_by = ROOT } remove_core = SUV add_core = SUV }
			lan_xang_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = SUV }
			angkor_region = { limit = { owned_by = ROOT } remove_core = SUV add_core = SUV }
			angkor_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = SUV }
			siam_region = { limit = { owned_by = ROOT } remove_core = SUV add_core = SUV }
			siam_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = SUV }
			malaya_region = { limit = { owned_by = ROOT } remove_core = SUV add_core = SUV }
			malaya_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = SUV }
			sumatra_region = { limit = { owned_by = ROOT } remove_core = SUV add_core = SUV }
			sumatra_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = SUV }
			add_prestige = 15
			if = {
				limit = { government = monarchy NOT = { government_rank = 5 } }
				set_government_rank = 5
			}
			change_tag = SUV
			increase_centralisation = yes
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = SUV_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	form_burma = {
		major = yes
		potential = {
			NOT = { tag = SUV }
			NOT = { exists = BRM }
			primary_culture = burmese
			is_year = 1680
		}
		allow = {    
			OR = {
				check_variable = { which = "Demesne_in_Burma" value = 7 }
				check_variable = { which = "Cores_on_Burma" value = 5 }
			}
			owns = 586
			is_subject = no
			is_at_war = no
		}
		effect = {
			burma_region = { limit = { owned_by = ROOT } remove_core = BRM add_core = BRM }
			burma_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = BRM }
			add_prestige = 15
			if = {
				limit = { government = monarchy NOT = { government_rank = 5 } }
				set_government_rank = 5
			}
			change_tag = BRM
			#burman_union_effect = yes
			#add_accepted_culture = burmese
			#add_accepted_culture = arakanese
			#add_accepted_culture = monic
			#add_accepted_culture = shan
			#add_accepted_culture = chin
			#add_accepted_culture = karen
			#add_accepted_culture = kachin
			increase_centralisation = yes
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = TAU_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	form_dai_viet = {   
		major = yes
		potential = {
			NOT = { exists = DAI }
			primary_culture = vietnamese
		}
		allow = {
			OR = { 	
				and = {
					tag = ANN
					NOT = { exists = TOK }
					NOT = { exists = VUU }
					NOT = { exists = MAC }
				}
				and = {
					tag = TOK
					NOT = { exists = ANN }
					NOT = { exists = VUU }
					NOT = { exists = MAC }
				}
				and = {
					tag = VUU
					NOT = { exists = TOK }
					NOT = { exists = ANN }
					NOT = { exists = MAC }
				}
				and = {
					tag = MAC
					NOT = { exists = TOK }
					NOT = { exists = VUU }
					NOT = { exists = ANN }
				}
			}
			is_subject = no
			OR = {
				check_variable = { which = "Demesne_in_Viet_Region" value = 9 }
				check_variable = { which = "Cores_on_Viet_Region" value = 5 }
			}
			is_at_war = no
		}
		effect = {
			vietnam_region = { limit = { owned_by = ROOT } remove_core = DAI add_core = DAI }
			vietnam_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = DAI }
			nam_tien_region = { limit = { owned_by = ROOT } remove_core = DAI add_core = DAI }
			nam_tien_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = DAI }
			add_prestige = 15
			if = {
				limit = { government = monarchy NOT = { government_rank = 5 } }
				set_government_rank = 5
			}
			if = {
				limit = { has_country_modifier = puppet_le_emperor }
				remove_country_modifier = puppet_le_emperor
			}
			change_tag = DAI
			increase_centralisation = yes
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = DAI_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	form_lan_xang = {
		major = yes
		potential = {
			NOT = { exists = LXA }
			OR = { 	
				tag = LUA
				tag = VIE
			}
		}
		allow = {
			is_subject = no
			OR = { 	
				and = {
					tag = LUA
					NOT = { exists = VIE }
				}
				and = {
					tag = VIE
					NOT = { exists = LUA }
				}
			}
			OR = {
				check_variable = { which = "Demesne_in_Lan_Xang" value = 9 }
				check_variable = { which = "Cores_on_Lan_Xang" value = 5 }
			}
			is_at_war = no
		}
		effect = {
			lan_xang_region = { limit = { owned_by = ROOT } remove_core = LXA add_core = LXA }
			lan_xang_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = LXA }
			add_prestige = 15
			if = {
				limit = { government = monarchy NOT = { government_rank = 5 } }
				set_government_rank = 5
			}
			change_tag = LXA

			increase_centralisation = yes
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = LXA_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	silesia_nation = {
		major = yes
		potential = {
			NOT = { exists = SIL }
			primary_culture = silesian
		}
		allow = {
			is_at_war = no
			OR = {
				AND = {
					is_subject = no
					custom_trigger_tooltip = {
						num_of_owned_provinces_with = {
							value = 7
							region = silesia_region
						}
						tooltip = silesia_nation_claim_big
					}
				}
				AND = {
					is_subject = yes
					custom_trigger_tooltip = {
						num_of_owned_provinces_with = {
							value = 4
							region = silesia_region
						}
						tooltip = silesia_nation_claim_small
					}
				}
			}
		}
	 	effect = {
			add_prestige = 20
			silesia_region = { limit = { owned_by = ROOT } remove_core = SIL add_core = SIL }
			silesia_region = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = SIL }
			change_tag = SIL
			increase_centralisation = yes
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = SIL_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	hansa_nation = {
		major = yes
		potential = {
			NOT = { exists = HSA }
			culture_group = low_germanic
			OR = {
				government = merchant_imperial_city
				government = imperial_city
			}
		}
		allow = {
			is_at_war = no
			owns_or_vassal_of = 45
			owns_or_vassal_of = 44
			owns_or_vassal_of = 1357
			OR = {
				tag = FRL
				NOT = {
					exists = FRL
				}
				FRL = {
					vassal_of = PREV
				}
			}
			OR = {
				tag = HAM
				NOT = {
					exists = HAM
				}
				HAM = {
					vassal_of = PREV
				}
			}
			OR = {
				tag = FRB
				NOT = {
					exists = FRB
				}
				FRB = {
					vassal_of = PREV
				}
			}
		}
	 	effect = {
			add_prestige = 20
			change_tag = HSA
			change_government = merchant_oligarchic_republic
			if = {
				limit = {
					exists = FRL
					NOT = {
						tag = FRL
					}
				}
				inherit = FRL
			}
			if = {
				limit = {
					exists = HAM
					NOT = {
						tag = HAM
					}
				}
				inherit = HAM
			}
			if = {
				limit = {
					exists = FRB
					NOT = {
						tag = FRB
					}
				}
				inherit = FRB
			}
			increase_centralisation = yes
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = HSA_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}

	mossi_nation = {
		major = yes
		potential = {
			NOT = { exists = MSI }
			primary_culture = mossi
		}
		allow = {
			is_at_war = no
			OR = {
				tag = GUR
				NOT = {
					exists = GUR
				}
				GUR = {
					vassal_of = ROOT
				}
			}
			OR = {
				tag = LIP
				NOT = {
					exists = LIP
				}
				LIP = {
					vassal_of = ROOT
				}
			}
			OR = {
				tag = UAG
				NOT = {
					exists = UAG
				}
				UAG = {
					vassal_of = ROOT
				}
			}
			OR = {
				tag = YAT
				NOT = {
					exists = YAT
				}
				YAT = {
					vassal_of = ROOT
				}
			}
			custom_trigger_tooltip = {
				num_of_owned_provinces_with = {
					value = 9
					culture = mossi
				}
				tooltip = mossi_nation_claim
			}
		}
	 	effect = {
			add_prestige = 20
			change_tag = MSI
			every_province = {
				limit = { culture = mossi }
				remove_core = MSI add_core = MSI
			}
			if = {
				limit = {
					exists = GUR
					NOT = {
						tag = GUR
					}
				}
				inherit = GUR
			}
			if = {
				limit = {
					exists = LIP
					NOT = {
						tag = LIP
					}
				}
				inherit = LIP
			}
			if = {
				limit = {
					exists = UAG
					NOT = {
						tag = UAG
					}
				}
				inherit = UAG
			}
			if = {
				limit = {
					exists = YAT
					NOT = {
						tag = YAT
					}
				}
				inherit = YAT
			}
			increase_centralisation = yes
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = mossi_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}

	haussa_nation = {
		major = yes
		potential = {
			NOT = { exists = HAU }
			primary_culture = haussa
		}
		allow = {
			is_at_war = no
			OR = {
				tag = GOB
				NOT = {
					exists = GOB
				}
				GOB = {
					vassal_of = ROOT
				}
			}
			OR = {
				tag = KNO
				NOT = {
					exists = KNO
				}
				KNO = {
					vassal_of = ROOT
				}
			}
			OR = {
				tag = KTS
				NOT = {
					exists = KTS
				}
				KTS = {
					vassal_of = ROOT
				}
			}
			OR = {
				tag = ZZZ
				NOT = {
					exists = ZZZ
				}
				ZZZ = {
					vassal_of = ROOT
				}
			}
			custom_trigger_tooltip = {
				num_of_owned_provinces_with = {
					value = 10
					culture = haussa
				}
				tooltip = haussa_nation_claim
			}

		}
	 	effect = {
			add_prestige = 20
			change_tag = HAU
			every_province = {
				limit = { culture = haussa }
				remove_core = HAU add_core = HAU
			}
			if = {
				limit = {
					exists = GOB
					NOT = {
						tag = GOB
					}
				}
				inherit = GOB
			}
			if = {
				limit = {
					exists = KNO
					NOT = {
						tag = KNO
					}
				}
				inherit = KNO
			}
			if = {
				limit = {
					exists = KTS
					NOT = {
						tag = KTS
					}
				}
				inherit = KTS
			}
			if = {
				limit = {
					exists = ZZZ
					NOT = {
						tag = ZZZ
					}
				}
				inherit = ZZZ
			}
			increase_centralisation = yes
			if = {
				limit = {
					has_custom_ideas = no
					NOT = { has_idea_group = hausa_ideas }
				}
				swap_national_ideas_effect = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}

	imperial_city_to_republic = {
		major = yes
		potential = {
			OR = { 
				AND = { tag = AUG NOT = { exists = AUH } }
				AND = { tag = BSL NOT = { exists = SWI } }
				AND = { tag = GNV NOT = { exists = SWI } }
				AND = { tag = FRB NOT = { exists = BRE } }
				AND = { tag = FRL NOT = { exists = HSA } }
				AND = { tag = NUR NOT = { exists = NUS } }
				}
			num_of_cities = 3
		}
		allow = {
			is_at_war = no
			is_subject = no
			OR = { 
				ADM = 4
				advisor = statesman
				num_of_cities = 6
			}
		}
		effect = {
			if = {
				limit = { tag = AUG }
				change_tag = AUH
			}
			if = {
				limit = { tag = BSL }
				change_tag = SWI
			}
			if = {
				limit = { tag = GNV }
				change_tag = SWI
			}
			if = {
				limit = { tag = FRB }
				change_tag = BRE
			}
			if = {
				limit = { tag = FRL }
				change_tag = HSA
			}
			if = {
				limit = { tag = NUR }
				change_tag = NUS
			}
			if = {
				limit = { government = imperial_city }
				change_government = administrative_republic
			}
		}
		ai_will_do = { factor = 1 }
	}

	dalmatia_nation = {
		major = no
		potential = {
			NOT = { exists = DLM }
			NOT = { tag = CRO }
			NOT = { tag = BOS }
			OR = {
				primary_culture = croatian
				primary_culture = dalmatian
			}
			is_colonial_nation = no
		}
		allow = {
			is_subject = no
			owns = 136 # Split
			owns = 137 # Dubrovnik
			owns = 2571 # Zadar
			owns = 2388 # Sibenik
			is_at_war = no
		}
		effect = {
			east_adriatic_coast_area = { limit = { owned_by = ROOT } remove_core = DLM add_core = DLM }
			east_adriatic_coast_area = { limit = { NOT = { owned_by = ROOT } } add_permanent_claim = DLM }
			add_prestige = 20
			change_tag = DLM
			increase_centralisation = yes
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}

}
