
province_event = {
	id =  colonial.1
	title = "EVTNAME6138"
	desc = "EVTDESC6138"
	picture = COLONIZATION_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		owner = { has_idea_group = exploration_ideas }
		has_empty_adjacent_province = yes
		owner = { NOT = { num_of_colonies = 2 } }
		OR = {
			owner = { has_country_flag = NF_ruthless }
			owner = { has_country_flag = NF_peacenik }
			AND = {
				owner = { has_country_flag = NF_trader }
				has_port = yes
			}
		}
	}

	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = "EVTOPTA6138"
		random_empty_neighbor_province = {
			create_colony = 200
		}
	}
	option = {
		name = "EVTOPTB6138"
		trigger = {
			owner = { num_of_colonies = 1 }
		}
		random_owned_province = {
			limit = {
				is_colony = yes
			}
			add_colonysize = 50
		}
	}
}


### Events to give more fun for overseas in the late game ###

province_event = {
	id =  colonial.2
	title = "EVTNAME1420"
	desc = "EVTDESC1420"
	picture = ELECTION_REPUBLICAN_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		is_year = 1750
		is_overseas = yes
		is_colony = no
		religion_group = christian
		owner = { religion_group = christian }
		NOT = { has_province_modifier = founding_father }
		NOT = { has_province_modifier = loyalist_stronghold }
		OR = {
			owner = { has_country_flag = NF_ruthless }
			owner = { has_country_flag = NF_peacenik }
			AND = {
				owner = { has_country_flag = NF_trader }
				has_port = yes
			}
		}
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = "EVTOPTA1420"
		ai_chance = {
			factor = 100
		}
		add_province_modifier = {
			name = "founding_father"
			duration = -1
		}
	}
	
	option	= {
		name = "EVTOPTB1420"
		owner = {
			add_country_modifier = {
				name = narrowminded_modifier
				duration = 3650
			}
			add_country_modifier = {
				name = serfdom_modifier
				duration = 3650
			}
		}
		owner = { set_country_flag = treat_colonials_harshly }
	}
}

province_event = {
	id =  colonial.3
	title = "EVTNAME1421"
	desc = "EVTDESC1421"
	picture = RELIGION_eventPicture
	
	is_triggered_only = yes

	trigger = {
		is_year = 1750
		is_colony = no
		is_overseas = yes
		religion_group = christian
		owner = { religion_group = christian }
		owner = { has_country_flag = treat_colonials_harshly }
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = "EVTOPTA1421"
		add_unrest = 3
	}
}

province_event = {
	id =  colonial.4
	title = "EVTNAME1422"
	desc = "EVTDESC1422"
	picture = DIPLOMACY_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		is_year = 1750
		is_overseas = yes
		is_colony = no
		religion_group = christian
		owner = { religion_group = christian }
		NOT = { has_province_modifier = founding_father }
		NOT = { has_province_modifier = loyalist_stronghold }
	}

	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = "EVTOPTA1422"
		ai_chance = {
			factor = 100
			modifier = {
				factor = 0
				NOT = { owner = { treasury = 100 } }
			}
		}
		add_unrest = 1
		owner = { add_treasury = -100 }
		add_province_modifier = {
			name = "loyalist_stronghold"
			duration = -1
		}
	}
	
	option = {
		name = "EVTOPTB1422"
		nationalist_rebels = 1
	}
}


### Events to give persecuted minorities into colonies ###

province_event = {
	id =  colonial.5
	title = "EVTNAME1423"
	desc = "EVTDESC1423"
	picture = STREET_SPEECH_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		is_overseas = yes
		is_colony = yes
		colonysize = 600
		OR= {
			religion = catholic
			religion = protestant
		}
		owner = { 
			OR= {
				religion = catholic
				religion = protestant
			}
			any_owned_province = {
				is_overseas = no
				religion = reformed
			}
		}
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = "EVTOPTA1423"
		change_variable = { which = province_religion value = -14 }
		change_variable = { which = reformed value = 14 }
		change_religion = reformed
		change_culture = owner
		add_colonysize = 400
		add_province_modifier = {
			name = "religious_migration"
			duration = 8760
		}
	}
}

province_event = {
	id =  colonial.6
	title = "EVTNAME1423"
	desc = "EVTDESC1424"
	picture = REFORM_eventPicture
	
	is_triggered_only = yes

	trigger = {
		is_overseas = yes
		is_colony = yes
		colonysize = 600
		OR= {
			religion = catholic
			religion = reformed
		}
		owner = { 
			OR= {
				religion = catholic
				religion = reformed
			}
			any_owned_province = {
				is_overseas = no
				religion = protestant
			}
		}
	}

	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = "EVTOPTA1423"
		change_variable = { which = province_religion value = -14 }
		change_variable = { which = protestant value = 14 }
		change_religion = protestant
		change_culture = owner
		add_colonysize = 400
		add_province_modifier = {
			name = "religious_migration"
			duration = 8760
		}
	}
}

province_event = {
	id =  colonial.7
	title = "EVTNAME1423"
	desc = "EVTDESC1425"
	picture = REFORM_eventPicture
	
	is_triggered_only = yes

	trigger = {
		is_overseas = yes
		is_colony = yes
		colonysize = 600
		OR= {
			religion = protestant
			religion = reformed
		}
		owner = { 
			OR= {
				religion = protestant
				religion = reformed
			}
			any_owned_province = {
				is_overseas = no
				religion = catholic
			}
		}
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = "EVTOPTA1423"
		change_variable = { which = province_religion value = -14 }
		change_variable = { which = catholic value = 14 }
		change_religion = catholic
		change_culture = owner
		add_colonysize = 400
		add_province_modifier = {
			name = "religious_migration"
			duration = 8760
		}
	}
}

### Scripted by Sara Wendel-�rtqvist ###

# A Witch!
province_event = {
	id = colonial.8
	title = "colonial.EVTNAME8"
	desc = "colonial.EVTDESC8"
	picture = ANGRY_MOB_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		OR = {
			province_id = 964 # Ramopough
			province_id = 965 # Metoac
			province_id = 966 # Matabesic
			province_id = 967 # Shawmut
		}
		is_year = 1600
		owner = {
			technology_group = western
		}
		religion_group = christian
	}
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = "colonial.EVTOPTA8"
		owner = {
			add_stability_1 = yes
		}
	}
	option = {
		name = "colonial.EVTOPTB8"
		owner = {
			add_dip_power = 50
		}
	}
}

# Rival Settlements
province_event = {
	id = colonial.9
	title = "colonial.EVTNAME9"
	desc = "colonial.EVTDESC9"
	picture = COLONIZATION_eventPicture
	
	is_triggered_only = yes

	trigger = {
		NOT = { has_province_flag = rival_settlements }
		OR = {
			owner = { has_country_flag = NF_ruthless }
			owner = { has_country_flag = NF_peacenik }
			AND = {
				owner = { has_country_flag = NF_trader }
				has_port = yes
			}
		}
	}
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = "colonial.EVTOPTA9"
		owner = { add_prestige = 10 }
	}
	option = {
		name = "colonial.EVTOPTB9"
		add_base_tax = 1	
		set_province_flag = rival_settlements
	}
}

# Rival Settlements
province_event = {
	id = colonial.10
	title = "colonial.EVTNAME10"
	desc = "colonial.EVTDESC10"
	picture = COLONIZATION_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		has_province_flag = rival_settlements
	}
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = "colonial.EVTOPTA10"
		owner = {  add_dip_power = 10 }
	}
	option = {
		name = "colonial.EVTOPTB10"
		owner = {  add_adm_power = 10 }
	}	
}

# Rival Settlements
province_event = {
	id = colonial.11
	title = "colonial.EVTNAME11"
	desc = "colonial.EVTDESC11"
	picture = COLONIZATION_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		has_province_flag = rival_settlements
	}
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = "colonial.EVTOPTA11"
		owner = { add_mil_power = -25 }

	}
	option = {
		name = "colonial.EVTOPTB11"
		owner = { add_years_of_income = -0.10 }

	}
}

# The Call of the Hunt
province_event = {
	id = colonial.12
	title = "colonial.EVTNAME12"
	desc = "colonial.EVTDESC12"
	picture = NATIVES_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		any_empty_neighbor_province = {
			NOT = { trade_goods = fur }
			native_size = 1000
			OR = {
				culture_group = west_algonquian
				culture_group = east_algonquian
				culture_group = athabascan_group
				culture_group = caddoan
				culture_group = plain_indians
			}
		}
		OR = {
			owner = { has_country_flag = NF_ruthless }
			owner = { has_country_flag = NF_peacenik }
			AND = {
				owner = { has_country_flag = NF_trader }
				has_port = yes
			}
		}
	}
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = "colonial.EVTOPTA12"
		add_base_manpower = 2
	}
	option = {
		name = "colonial.EVTOPTB12"
		create_advisor = colonial_governor
	}
	option = {
		name = "colonial.EVTOPTC12"
		change_trade_goods = fur
	}
}

# Fur Trade Dwindles
province_event = {
	id = colonial.13
	title = "colonial.EVTNAME13"
	desc = "colonial.EVTDESC13"
	picture = MERCHANTS_TALKING_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		trade_goods = fur
		NOT = {
			any_neighbor_province = {
				OR = {
				culture_group = west_algonquian
				culture_group = east_algonquian
				culture_group = athabascan_group
				culture_group = caddoan
				culture_group = plain_indians
				}
			}
		}
		NOT = {
			any_neighbor_province = {
				is_empty = yes
			}
		}
		NOT = {
			any_neighbor_province = {
				owner = {
					government = steppe_horde
				}
				controller = {
					government = steppe_horde
				}
			}
		}
	}
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = "colonial.EVTOPTA13"
		change_trade_goods = naval_supplies
	}
	option = {
		name = "colonial.EVTOPTC13"
		if = {
			limit = {
				owner = {
					capital_scope = {
						continent = europe
					}
				}
			}
			change_trade_goods = wheat
		}
		if = {
			limit = {
				owner = {
					capital_scope = {
						continent = africa
					}
				}
			}
			change_trade_goods = millet
		}
		if = {
			limit = {
				owner = {
					capital_scope = {
						continent = asia
					}
				}
			}
			change_trade_goods = rice
		}
		if = {
			limit = {
				owner = {
					capital_scope = {
						OR = {
							continent = north_america
							continent = south_america
						}
					}
				}
			}
			change_trade_goods = maize
		}
	}
}

# Jesuit mission founded
province_event = {
	id = colonial.14
	title = "colonial.EVTNAME14"
	desc = "colonial.EVTDESC14"
	picture = RELIGION_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		is_colony = yes
		OR = {
			continent = north_america
			continent = south_america
		}
		owner = {
			has_country_modifier = the_societas_jesu
			religion = catholic
		}
		is_religion_enabled = protestant
		NOT = {
			is_year = 1650
		}
		NOT = {
			has_province_modifier = jesuit_mission
		}
	}
	
	mean_time_to_happen = {
		days = 1
		
		modifier = {
			factor = 1.5
			owner = {
				has_country_modifier = counter_reformation
			}
		}
		modifier = {
			factor = 1.75
			owner = {
				culture_group = iberian
			}
		}
	}
	
	option = {
		name = "colonial.EVTOPTA14"
		add_province_modifier = {
			name = jesuit_mission
			duration = -1
		}
	}
}

# Our colony has been struck by Roman Fever!
province_event = {
	id = colonial.15
	title = "colonial.EVTNAME15"
	desc = "colonial.EVTDESC15"
	picture = PLAGUE_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		is_colony = yes
		OR = {
			continent = asia
			continent = africa
			continent = south_america
			continent = oceania
		}
		NOT = {
			owner = {
				has_country_modifier = roman_fever_timer
			}
		}
		NOT = {
			continent = asia
		}
		NOT = {
			continent = africa
		}
		NOT = {
			continent = south_america
		}
		NOT = {
			continent = oceania
		}
	}
	
	mean_time_to_happen = {
		days = 1
	}
	
	immediate = {
		hidden_effect = {
			owner = {
				add_country_modifier = {
					name = roman_fever_timer
					duration = 5475
					hidden = yes
				}
			}
		}
	}
	
	option = {
		name = "colonial.EVTOPTA15"
		owner = { add_adm_power = -25 }
		add_province_modifier = {
			name = organised_colony
			duration = 1825
		}
	}
	option = {
		name = "colonial.EVTOPTB15"
		add_province_modifier = {
			name = quarantine_colony
			duration = 1825
		}
	}
	option = {
		name = "colonial.EVTOPTC15"
		add_province_modifier = {
			name = roman_fever_colony
			duration = 1825
		}
		random_neighbor_province = {
			limit = {
				is_colony = yes
				owner = {
					tag = ROOT
				}
			}
			add_province_modifier = {
				name = roman_fever_colony
				duration = 1825
			}
		}
	}
}

# Colonial Expansion
province_event = {
	id = colonial.16
	title = "EVTNAME6400"
	desc = "EVTDESC6400"
	picture = COLONIZATION_eventPicture

	is_triggered_only = yes

	trigger = {
		has_empty_adjacent_province = yes
		any_neighbor_province = {
			is_empty = yes
			has_discovered = ROOT
		}
		owner = { NOT = { num_of_colonies = 3 } }

	}
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = "EVTOPTA6400"
		random_empty_neighbor_province = {
			limit = {
				has_discovered = ROOT
			}
			create_colony = 200
		}
	}
	option = {
		name = "EVTOPTB6400"
		trigger = {
			owner = {
				num_of_colonies = 1
			}
		}
		owner = {
			random_owned_province = {
				limit = {
					is_colony = yes
				}
				add_colonysize = 50
			}
		}
	}
}

# Colonial Migration
province_event = {
	id = colonial.17
	title = "EVTNAME6401"
	desc = "EVTDESC6401"
	picture = COLONIZATION_eventPicture

	is_triggered_only = yes

	trigger = {
		colonysize = 300
		NOT = { colonysize = 500 }
		owner = {
			any_owned_province = {
				is_colony = yes
				colonysize = 500
			}
		}
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = "EVTOPTA6401"
		add_colonysize = -200
		owner = {
			random_owned_province = {
				limit = { is_colony = yes colonysize = 500 }
				add_colonysize = 200
			}
		}	
	}
}

# New World Coffee
province_event = {
	id = colonial.18
	title = "colonial.EVTNAME18"
	desc = "colonial.EVTDESC18"
	picture = TRADEGOODS_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		is_year = 1650
		OR = {
			region = la_plata_region
			region = peru_region
			province_group = amazonia_group
			region = carribeans_region
			central_america_region_trigger = yes
			province_group = the_spanish_main_group
		}
		OR = {
			farming_province_trigger = yes
			trade_goods = fish
			trade_goods = wool
			trade_goods = naval_supplies
			trade_goods = salt
			trade_goods = fur
		}
		NOT = {
			any_neighbor_province = {
				is_empty = yes
			}
		}		
	}
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = "colonial.EVTOPTA18"
		ai_chance = { factor = 90 }
		change_trade_goods = coffee
	}
	option = {
		name = "colonial.EVTOPTC18"
		ai_chance = { factor = 10 }
	}
}

# Development of colonies
province_event = {
	id = colonial.19
	title = "colonial.19.t"
	desc = "colonial.19.n"
	picture = COMPANY_BASE_eventPicture

	trigger = {
		has_province_flag = colonial_province
		NOT = { trade_goods = unknown }
		OR = {
			owner = { has_country_flag = NF_ruthless }
			owner = { has_country_flag = NF_peacenik }
			AND = {
				owner = { has_country_flag = NF_trader }
				has_port = yes
			}
		}
	}

	mean_time_to_happen = {
		months = 1200

		modifier = {
			base_tax = 2
			factor = 1.1
		}
		modifier = {
			base_tax = 3
			factor = 1.1
		}
		modifier = {
			base_tax = 4
			factor = 1.1
		}
		modifier = {
			base_tax = 5
			factor = 1.1
		}
		modifier = {
			base_tax = 6
			factor = 1.2
		}
		modifier = {
			base_tax = 7
			factor = 1.2
		}
		modifier = {
			base_tax = 8
			factor = 1.2
		}
		modifier = {
			base_tax = 9
			factor = 1.2
		}
		modifier = {
			base_tax = 10
			factor = 1.2
		}
		modifier = {
			base_tax = 11
			factor = 1.3
		}
		modifier = {
			base_tax = 12
			factor = 1.3
		}
		modifier = {
			base_tax = 13
			factor = 1.3
		}
		modifier = {
			base_tax = 14
			factor = 1.3
		}
		modifier = {
			base_tax = 15
			factor = 1.3
		}
		modifier = {
			base_tax = 16
			factor = 1.5
		}
		modifier = {
			base_tax = 17
			factor = 1.5
		}
		modifier = {
			base_tax = 18
			factor = 1.5
		}
		modifier = {
			base_tax = 19
			factor = 1.5
		}
		modifier = {
			base_tax = 20
			factor = 1.5
		}
		modifier = {
			OR = {
				trade_goods = cotton
				trade_goods = sugar
				trade_goods = coffee
			}
			factor = 0.6
		}
		modifier = {
			OR = {
				trade_goods = fur
				trade_goods = lumber
			}
			factor = 0.8
		}
		modifier = {
			OR = {
				trade_goods = pepper
				trade_goods = clove
				trade_goods = nutmeg
				trade_goods = cinnamon
			}
			factor = 0.7
		}
		modifier = {
			owner = {
				is_colonial_nation = yes
			}
			factor = 0.9
		}
		modifier = {
			owner = {
				is_former_colonial_nation = yes
			}
			factor = 0.8
		}
		modifier = {
			province_group = small_island_group
			factor = 1.3
		}
		modifier = {
			has_climate = tropical
			factor = 2.5
		}
		modifier = {
			has_climate = arid
			factor = 3.0
		}
		modifier = {
			has_winter = mild_winter
			factor = 1.3
		}
		modifier = {
			has_winter = normal_winter
			factor = 1.7
		}
		modifier = {
			has_winter = severe_winter
			factor = 2.0
		}
		modifier = {
			has_climate = arctic
			factor = 5.0
		}
		modifier = {
			owner = {
				dip_tech = 20
			}
			factor = 0.9
		}
		modifier = {
			owner = {
				dip_tech = 30
			}
			factor = 0.9
		}
		modifier = {
			owner = {
				dip_tech = 40
			}
			factor = 0.9
		}
		modifier = {
			owner = {
				dip_tech = 50
			}
			factor = 0.9
		}

	}

	option = {
		name = "colonial.19.a"
		add_base_tax = 1
	}
}

# Africa open to colonization
country_event = {
	id = colonial.20
	title = "colonial.20.t"
	desc = "colonial.20.n"
	picture = COMPANY_BASE_eventPicture

	major = yes
	fire_only_once = yes
	is_triggered_only = yes
	
	option = {
		name = "OPT.EXCELLENT"
		hidden_effect = {
			set_global_flag = african_coast_open
			every_province = {
				limit = {
					continent = africa
					is_empty = yes
					has_port = yes
					NOT = { base_tax = 1 }
				}
				add_base_tax = 1
			}
		}
	}
}

country_event = {
	id = colonial.21
	title = "colonial.21.t"
	desc = "colonial.21.n"
	picture = COMPANY_BASE_eventPicture

	major = yes
	fire_only_once = yes
	is_triggered_only = yes
	
	option = {
		name = "OPT.EXCELLENT"
		hidden_effect = {
			set_global_flag = african_inland_open
			every_province = {
				limit = {
					continent = africa
					is_empty = yes
					NOT = { base_tax = 1 }
				}
				add_base_tax = 1
			}
		}
	}
}

# Australia open to colonization
country_event = {
	id = colonial.22
	title = "colonial.22.t"
	desc = "colonial.22.n"
	picture = COMPANY_BASE_eventPicture

	major = yes
	fire_only_once = yes
	is_triggered_only = yes
	
	option = {
		name = "OPT.EXCELLENT"
		hidden_effect = {
			set_global_flag = south_wales_open
			every_province = {
				limit = {
					area = southern_australia_area
					is_empty = yes
					has_port = yes
					NOT = { base_tax = 1 }
				}
				add_base_tax = 1
			}
		}
	}
}

country_event = {
	id = colonial.23
	title = "colonial.23.t"
	desc = "colonial.23.n"
	picture = COMPANY_BASE_eventPicture

	major = yes
	fire_only_once = yes
	is_triggered_only = yes
	
	option = {
		name = "OPT.EXCELLENT"
		hidden_effect = {
			set_global_flag = australia_open
			every_province = {
				limit = {
					OR = {
						region = australia_region
						area = papua_area
					}
					is_empty = yes
					NOT = { base_tax = 1 }
				}
				add_base_tax = 1
			}
		}
	}
}

# Culture in the Asia colonies - commented out as it is replaced by a Vanilla feature in trading company zones.
#country_event = {
#	id = colonial.24
#	title = "colonial.24.t"
#	desc = "colonial.24.n"
#	picture = COMPANY_BASE_eventPicture
#
#	is_triggered_only = yes
#	
#	option = {
#		name = "OPT.EXCELLENT"
#		hidden_effect = {
#			every_province = {
#				limit = {
#					has_province_flag = taiwanese_natives
#					NOT = { culture = taiwanese }
#					is_city = yes
#				}
#				set_province_flag = colonised_province
#				change_culture = taiwanese
#				change_religion = polynesian_religion #Either convert the province or change variables, try not to do both
#				change_variable = { which = "polynesian_religion" value = 15 }
#				change_variable = { which = province_religion value = -15 }
#				add_base_tax = 2
#				add_base_production = 2
#			}
#			every_province = {
#				limit = {
#					has_province_flag = tagalog_natives
#					NOT = { culture = tagalog }
#					is_city = yes
#				}
#				set_province_flag = colonised_province
#				change_culture = tagalog
#				change_religion = hinduism
#				change_variable = { which = "hinduism" value = 15 }
#				change_variable = { which = province_religion value = -15 }
#				add_base_tax = 2
#				add_base_production = 2
#			}
#			every_province = {
#				limit = {
#					has_province_flag = cebuano_natives
#					NOT = { culture = cebuano }
#					is_city = yes
#				}
#				set_province_flag = colonised_province
#				change_culture = cebuano
#				change_religion = hinduism
#				change_variable = { which = "hinduism" value = 15 }
#				change_variable = { which = province_religion value = -15 }
#				add_base_tax = 2
#				add_base_production = 2
#			}
#			every_province = {
#				limit = {
#					has_province_flag = sulawesi_natives
#					NOT = { culture = sulawesi }
#					is_city = yes
#				}
#				set_province_flag = colonised_province
#				change_culture = sulawesi
#				change_religion = polynesian_religion
#				change_variable = { which = polynesian_religion value = 15 }
#				change_variable = { which = province_religion value = -15 }
#				add_base_tax = 2
#				add_base_production = 2
#			}
#			every_province = {
#				limit = {
#					has_province_flag = bugis_natives
#					NOT = { culture = bugis }
#					is_city = yes
#				}
#				set_province_flag = colonised_province
#				change_culture = bugis
#				change_religion = polynesian_religion
#				change_variable = { which = polynesian_religion value = 15 }
#				change_variable = { which = province_religion value = -15 }
#				add_base_tax = 2
#				add_base_production = 2
#			}
#			every_province = {
#				limit = {
#					has_province_flag = melanesian_natives
#					NOT = { culture = melanesian }
#					is_city = yes
#				}
#				set_province_flag = colonised_province
#				change_culture = melanesian
#				change_religion = polynesian_religion
#				change_variable = { which = polynesian_religion value = 15 }
#				change_variable = { which = province_religion value = -15 }
#				add_base_tax = 2
#				add_base_production = 2
#			}
#			every_province = {
#				limit = {
#					has_province_flag = moluccan_natives
#					NOT = { culture = moluccan }
#					is_city = yes
#				}
#				set_province_flag = colonised_province
#				change_culture = moluccan
#				change_religion = polynesian_religion
#				change_variable = { which = polynesian_religion value = 15 }
#				change_variable = { which = province_religion value = -15 }
#				add_base_tax = 2
#				add_base_production = 2
#			}
#			every_province = {
#				limit = {
#					has_province_flag = papuan_natives
#					NOT = { culture = papuan }
#					is_city = yes
#				}
#				set_province_flag = colonised_province
#				change_culture = papuan
#				change_religion = polynesian_religion
#				change_variable = { which = polynesian_religion value = 15 }
#				change_variable = { which = province_religion value = -15 }
#				add_base_tax = 2
#				add_base_production = 2
#			}
#			every_province = {
#				limit = {
#					region = oceanea_region
#					NOT = { culture = polynesian }
#					NOT = { culture = papuan }
#					is_city = yes
#				}
#				set_province_flag = colonised_province
#				change_culture = polynesian
#				change_religion = polynesian_religion
#				change_variable = { which = polynesian_religion value = 15 }
#				change_variable = { which = province_religion value = -15 }
#				add_base_tax = 1
#				add_base_production = 1
#			}
#		}
#	}
#}

# Development of colonies
province_event = {
	id = colonial.25
	title = "colonial.19.t"
	desc = "colonial.19.n"
	picture = COMPANY_BASE_eventPicture

	trigger = {
		has_province_flag = colonial_province
		NOT = { trade_goods = unknown }
		OR = {
			owner = { has_country_flag = NF_ruthless }
			owner = { has_country_flag = NF_peacenik }
			AND = {
				owner = { has_country_flag = NF_trader }
				has_port = yes
			}
		}
	}

	mean_time_to_happen = {
		months = 1200

		modifier = {
			base_production = 2
			factor = 1.1
		}
		modifier = {
			base_production = 3
			factor = 1.1
		}
		modifier = {
			base_production = 4
			factor = 1.1
		}
		modifier = {
			base_production = 5
			factor = 1.1
		}
		modifier = {
			base_production = 6
			factor = 1.2
		}
		modifier = {
			base_production = 7
			factor = 1.2
		}
		modifier = {
			base_production = 8
			factor = 1.2
		}
		modifier = {
			base_production = 9
			factor = 1.2
		}
		modifier = {
			base_production = 10
			factor = 1.2
		}
		modifier = {
			base_production = 11
			factor = 1.3
		}
		modifier = {
			base_production = 12
			factor = 1.3
		}
		modifier = {
			base_production = 13
			factor = 1.3
		}
		modifier = {
			base_production = 14
			factor = 1.3
		}
		modifier = {
			base_production = 15
			factor = 1.3
		}
		modifier = {
			base_production = 16
			factor = 1.5
		}
		modifier = {
			base_production = 17
			factor = 1.5
		}
		modifier = {
			base_production = 18
			factor = 1.5
		}
		modifier = {
			base_production = 19
			factor = 1.5
		}
		modifier = {
			base_production = 20
			factor = 1.5
		}
		modifier = {
			OR = {
				trade_goods = cotton
				trade_goods = sugar
				trade_goods = coffee
			}
			factor = 0.6
		}
		modifier = {
			OR = {
				trade_goods = fur
				trade_goods = lumber
			}
			factor = 0.8
		}
		modifier = {
			OR = {
				trade_goods = pepper
				trade_goods = clove
				trade_goods = nutmeg
				trade_goods = cinnamon
			}
			factor = 0.7
		}
		modifier = {
			owner = {
				is_colonial_nation = yes
			}
			factor = 0.9
		}
		modifier = {
			owner = {
				is_former_colonial_nation = yes
			}
			factor = 0.8
		}
		modifier = {
			province_group = small_island_group
			factor = 1.3
		}
		modifier = {
			has_climate = tropical
			factor = 2.5
		}
		modifier = {
			has_climate = arid
			factor = 3.0
		}
		modifier = {
			has_winter = mild_winter
			factor = 1.3
		}
		modifier = {
			has_winter = normal_winter
			factor = 1.7
		}
		modifier = {
			has_winter = severe_winter
			factor = 2.0
		}
		modifier = {
			has_climate = arctic
			factor = 5.0
		}
		modifier = {
			owner = {
				dip_tech = 20
			}
			factor = 0.9
		}
		modifier = {
			owner = {
				dip_tech = 30
			}
			factor = 0.9
		}
		modifier = {
			owner = {
				dip_tech = 40
			}
			factor = 0.9
		}
		modifier = {
			owner = {
				dip_tech = 50
			}
			factor = 0.9
		}

	}

	option = {
		name = "colonial.19.a"
		add_base_production = 1
	}
}

# Development of colonies
province_event = {
	id = colonial.26
	title = "colonial.19.t"
	desc = "colonial.19.n"
	picture = COMPANY_BASE_eventPicture

	trigger = {
		has_province_flag = colonial_province
		NOT = { trade_goods = unknown }
		OR = {
			owner = { has_country_flag = NF_ruthless }
			owner = { has_country_flag = NF_peacenik }
			AND = {
				owner = { has_country_flag = NF_trader }
				has_port = yes
			}
		}
	}

	mean_time_to_happen = {
		months = 3000

		modifier = {
			base_manpower = 2
			factor = 1.1
		}
		modifier = {
			base_manpower = 3
			factor = 1.1
		}
		modifier = {
			base_manpower = 4
			factor = 1.1
		}
		modifier = {
			base_manpower = 5
			factor = 1.1
		}
		modifier = {
			base_manpower = 6
			factor = 1.2
		}
		modifier = {
			base_manpower = 7
			factor = 1.2
		}
		modifier = {
			base_manpower = 8
			factor = 1.2
		}
		modifier = {
			base_manpower = 9
			factor = 1.2
		}
		modifier = {
			base_manpower = 10
			factor = 1.2
		}
		modifier = {
			base_manpower = 11
			factor = 1.3
		}
		modifier = {
			base_manpower = 12
			factor = 1.3
		}
		modifier = {
			base_manpower = 13
			factor = 1.3
		}
		modifier = {
			base_manpower = 14
			factor = 1.3
		}
		modifier = {
			base_manpower = 15
			factor = 1.3
		}
		modifier = {
			base_manpower = 16
			factor = 1.5
		}
		modifier = {
			base_manpower = 17
			factor = 1.5
		}
		modifier = {
			base_manpower = 18
			factor = 1.5
		}
		modifier = {
			base_manpower = 19
			factor = 1.5
		}
		modifier = {
			base_manpower = 20
			factor = 1.5
		}
		modifier = {
			OR = {
				trade_goods = cotton
				trade_goods = sugar
				trade_goods = coffee
			}
			factor = 0.6
		}
		modifier = {
			OR = {
				trade_goods = fur
				trade_goods = lumber
			}
			factor = 0.8
		}
		modifier = {
			OR = {
				trade_goods = pepper
				trade_goods = clove
				trade_goods = nutmeg
				trade_goods = cinnamon
			}
			factor = 0.7
		}
		modifier = {
			owner = {
				is_colonial_nation = yes
			}
			factor = 0.9
		}
		modifier = {
			owner = {
				is_former_colonial_nation = yes
			}
			factor = 0.8
		}
		modifier = {
			province_group = small_island_group
			factor = 1.3
		}
		modifier = {
			has_climate = tropical
			factor = 2.5
		}
		modifier = {
			has_climate = arid
			factor = 3.0
		}
		modifier = {
			has_winter = mild_winter
			factor = 1.3
		}
		modifier = {
			has_winter = normal_winter
			factor = 1.7
		}
		modifier = {
			has_winter = severe_winter
			factor = 2.0
		}
		modifier = {
			has_climate = arctic
			factor = 5.0
		}
		modifier = {
			owner = {
				dip_tech = 20
			}
			factor = 0.9
		}
		modifier = {
			owner = {
				dip_tech = 30
			}
			factor = 0.9
		}
		modifier = {
			owner = {
				dip_tech = 40
			}
			factor = 0.9
		}
		modifier = {
			owner = {
				dip_tech = 50
			}
			factor = 0.9
		}

	}

	option = {
		name = "colonial.19.a"
		add_base_manpower = 1
	}
}

# Early explorers
country_event = {
	id = colonial.27
	title = "colonial.27.t"
	desc = "colonial.27.n"
	picture = SHIP_SAILING_eventPicture
	
	trigger = {
		has_country_modifier = early_exploration
		has_country_flag = early_exploration
		NOT = { has_idea_group = exploration_ideas }
		NOT = { num_of_explorers = 1 }
	}

	mean_time_to_happen = {
		months = 12
	}
	
	option = {		# Hire Explorer
		name = "colonial.27.a"
		ai_chance = { factor = 100 }
		add_treasury = -20
		create_explorer = 10
	}
}

# End early exploration
country_event = {
	id = colonial.28
	title = "colonial.28.t"
	desc = "colonial.28.n"
	picture = SHIP_SAILING_eventPicture
	
	trigger = {
		has_country_modifier = early_exploration
		has_country_flag = early_exploration
		has_idea_group = exploration_ideas
		NOT = { has_country_flag = end_early_exploration }
	}

	mean_time_to_happen = {
		months = 2
	}
	
	immediate = {
		set_country_flag = end_early_exploration
	}

	option = {		# Age of colonization
		name = "colonial.28.a"
		ai_chance = { factor = 100 }
		add_prestige = 3
		add_navy_tradition = 3
		remove_country_modifier = early_exploration
	}
}

# DISCOVER OF AZORES
country_event = {
	id = colonial.29
	title = "colonial.29.t"
	desc = "colonial.29.n"
	picture = SHIP_SAILING_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		has_country_modifier = early_exploration
		has_country_flag = early_exploration
		NOT = { has_idea_group = exploration_ideas }
		owns = 368
		is_core = 368
	}

	mean_time_to_happen = {
		months = 24
	}
	
	option = {		# Discover of Azores
		name = "colonial.29.a"
		ai_chance = { factor = 100 }
		discover_province = 1668
		discover_province = 1669
		discover_province = 367
	}
}

country_event = {
	id = colonial.30
	title = "colonial.30.t"
	desc = "colonial.30.n"
	picture = COMPANY_BASE_eventPicture

	major = yes
	fire_only_once = yes
	is_triggered_only = yes
	
	option = {
		name = "OPT.EXCELLENT"
		hidden_effect = {
			set_global_flag = amazonia_open
			every_province = {
				limit = {
					continent = south_america
					is_empty = yes
					NOT = { base_tax = 1 }
				}
				add_base_tax = 1
			}
		}
	}
}

country_event = {
	id = colonial.31
	title = "colonial.31.t"
	desc = "colonial.31.n"
	picture = COMPANY_BASE_eventPicture

	major = yes
	fire_only_once = yes
	is_triggered_only = yes
	
	option = {
		name = "OPT.EXCELLENT"
		hidden_effect = {
			set_global_flag = alaska_open
			every_province = {
				limit = {
					OR = {
						area = alaska_area
						area = unalaska_area
					}
					is_empty = yes
					NOT = { base_tax = 1 }
				}
				add_base_tax = 1
			}
		}
	}
}
