###################################################
#                                                 #
#     Mercantilism Bi-Yearly On Action Events     #
#                                                 #
###################################################


# Our Mercantilist Policies Strengthen trade
country_event = {
	id = trade.6326
	title = "EVTNAME6326"
	desc = "EVTDESC6326"
	picture = MERCHANTS_TALKING_eventPicture
	
	is_triggered_only = yes
	trigger = {
		mercantilism = 50
		num_of_merchants = 3
		NOT = {
			has_country_modifier = mercantilism_modifier
		}
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = "EVTOPTA6326"
		add_country_modifier = {
			name = mercantilism_modifier
			duration = 3650
		}
	}
}

# Merchant defects
country_event = {
	id = trade.6327
	title = "EVTNAME6327"
	desc = "EVTDESC6327" 
	picture = MERCHANTS_TALKING_eventPicture

	is_triggered_only = yes
	
	trigger = {
		NOT = { mercantilism = 50 }
		num_of_merchants = 1
		any_active_trade_node = {
			is_sea = yes
		}
		NOT = {
			has_country_modifier = freetrade_modifier
		}
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = "EVTOPTA6327"
		random_active_trade_node = {
			recall_merchant = root
		}
		add_mercantilism = 1
	}	

	option = {
		name = "EVTOPTB6327"
		add_country_modifier = {
			name = freetrade_modifier
			duration = 3650
		}
		add_mercantilism = -0.02
	}
} 

# Trade Expansion
country_event = {
	id = trade.6328
	title = "EVTNAME6328"
	desc = "EVTDESC6328"
	picture = MERCHANTS_TALKING_eventPicture

	is_triggered_only = yes
	
	trigger = {
		NOT = { mercantilism = 20 }
		any_active_trade_node = {
			is_sea = yes
		}	
		NOT = {
			has_country_modifier = freetrade_modifier
		}			
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = "EVTOPTA6328"
		add_country_modifier = {
			name = freetrade_modifier
			duration = 3650
		}		
	}
}	

# Merchants worried about lack of protection
country_event = {
	id = trade.6329
	title = "EVTNAME6329"
	desc = "EVTDESC6329"
	picture = MERCHANTS_TALKING_eventPicture

	is_triggered_only = yes
	
	trigger = {
		NOT = { mercantilism = 40 }
	}

	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = "EVTOPTA6329"
		add_country_modifier = {
			name = "lack_of_protection"
			duration = 120
		}
	}
	option = {
		name = "EVTOPTB6329"
		add_mercantilism = 1
		add_dip_power = -25
	}
}
