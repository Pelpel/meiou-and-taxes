#### Post-peace core gain ####
country_event = {
	id = hindustani_unification.1
	
	title = "hindustani_unification.1.t"
	desc = "hindustani_unification.1.d"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	hidden = yes
	
	trigger = {
		government_rank = 6
		OR = {
			capital_scope = {
				OR = {
					region = bengal_region
					region = hindusthan_region
					region = rajputana_region
					region = west_india_region
					region = deccan_region
					region = coromandel_region
					region = western_ghats_region
					region = afghanistan_region
				}
			}
		}
		adm_tech = 20
		any_owned_province = {
			OR = {
				region = bengal_region
				region = hindusthan_region
				region = rajputana_region
				region = west_india_region
				region = deccan_region
				region = coromandel_region
				region = western_ghats_region
			}
			NOT = { is_core = ROOT }
		}
	}
		
	option = {
		name = "hindustani_unification.1a"
		every_owned_province = {
			limit = {
				OR = {
					region = bengal_region
					region = hindusthan_region
					region = rajputana_region
					region = west_india_region
					region = deccan_region
					region = coromandel_region
					region = western_ghats_region
				}
				NOT = { is_core = ROOT }
			}
			add_claim = ROOT			
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
			}	
		}
	}
}

#Dakani Viceroyality
country_event = {
	id = hindustani_unification.10
	
	title = "hindustani_unification.1.t"
	desc = "hindustani_unification.1.d"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	hidden = yes
	
	trigger = {
		has_country_flag = viceroyality_of_the_deccan_created
		HYD = { is_subject_of = ROOT }
		any_owned_province = {
			OR = {
				area = konkan_area
				region = deccan_region
			}
		}
	}
		
	option = {
		name = "hindustani_unification.1a"
		every_owned_province = {
			limit = {
				OR = {
					area = konkan_area
					region = deccan_region
				}
			}
			remove_core = ROOT
			remove_claim = ROOT
			add_claim = HYD	
			cede_province = HYD		
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
			}		
		}
	}
}

#Dakani Viceroyality - Carnatic
country_event = {
	id = hindustani_unification.11
	
	title = "hindustani_unification.1.t"
	desc = "hindustani_unification.1.d"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	hidden = yes
	
	trigger = {
		has_country_flag = nawab_of_the_carnatic_created
		OR = {
			AND = {
				HYD = { is_subject_of = ROOT }
				KRK = { is_subject_of = HYD }
			}
			KRK = { is_subject_of = ROOT }
		}
		any_owned_province = {
			area = carnatic_area
		}
	}
		
	option = {
		name = "hindustani_unification.1a"
		every_owned_province = {
			limit = {
				area = carnatic_area
			}
			remove_core = ROOT
			remove_claim = ROOT
			remove_core = HYD
			remove_claim = HYD
			cede_province = KRK		
		}
	}
}
country_event = {
	id = hindustani_unification.12
	
	title = "hindustani_unification.1.t"
	desc = "hindustani_unification.1.d"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	hidden = yes
	
	trigger = {
		has_country_flag = nawab_of_kurpa_created
		OR = {
			AND = {
				HYD = { is_subject_of = ROOT }
				KRP = { is_subject_of = HYD }
			}
			KRP = { is_subject_of = ROOT }
		}
		any_owned_province = {
			area = rayalaseema_area
		}
	}
		
	option = {
		name = "hindustani_unification.1a"
		every_owned_province = {
			limit = {
				area = rayalaseema_area
			}
			remove_core = ROOT
			remove_claim = ROOT
			remove_core = HYD
			remove_claim = HYD
			add_claim = KRP	
			cede_province = KRP		
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
			}	
		}
	}
}
country_event = {
	id = hindustani_unification.13
	
	title = "hindustani_unification.1.t"
	desc = "hindustani_unification.1.d"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	hidden = yes
	
	trigger = {
		has_country_flag = nawab_of_savanur_created
		OR = {
			AND = {
				HYD = { is_subject_of = ROOT }
				SVN = { is_subject_of = HYD }
			}
			SVN = { is_subject_of = ROOT }
		}
		any_owned_province = {
			area = kanara_area
		}
	}
		
	option = {
		name = "hindustani_unification.1a"
		every_owned_province = {
			limit = {
				area = kanara_area
			}
			remove_core = ROOT
			remove_claim = ROOT
			remove_core = HYD
			remove_claim = HYD
			add_claim = SVN	
			cede_province = SVN		
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
			}	
		}
	}
}