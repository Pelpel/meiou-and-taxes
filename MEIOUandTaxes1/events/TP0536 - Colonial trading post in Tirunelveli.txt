#############################################
#                                           #
#  Trading post event series                #
#  province number: 536                     #
#  by FB based on an idea by gigau          #
#  with help and encouragement from dharper #
#  v 11-jan-2014                            #
#                                           #
#############################################
# SUMMARY
# Purpose 1 - to simulate European countries trading with eastern provinces
# Purpose 2 - to simulate European countries taking control of eastern
# provinces using mainly political and economic methods
#
# IDs tp_tirunelveli.1 - tp_tirunelveli.xx
# tp_tirunelveli.1 Open a trading post in the province
# tp_tirunelveli.2 Buy more influence in the province
# tp_tirunelveli.3 Build a workshop in the province
# tp_tirunelveli.4 Support rebels in the province
# tp_tirunelveli.5 Claim core on the province
# tp_tirunelveli.6 Seize control of the province
# tp_tirunelveli.7 Buy goodwill from the owner
# tp_tirunelveli.8 Buy more goodwill from the owner
# tp_tirunelveli.9 Buy the province - make the offer
# tp_tirunelveli.10 Buy the province - owner response
# tp_tirunelveli.11 Buy the province - sale refused
# tp_tirunelveli.12 Buy the province - ally
# tp_tirunelveli.13 Buy the province - ally & money
# tp_tirunelveli.14 Buy the province - money
############################################
# To Do:
# - attacks on trading post by owner
# - attacks on trading post by trading rivals (at war)
# - attacks on trading post by trading rivals (not at war)
# - owner offering province in return for war support
# - benefits of neighbouring provinces being owned
############################################
# Notes:
# - this event series is province specific because of the need to specify the
#    province in country events in which the country is not the owner
# - when copying this event series for another province you should:
# - change all instances of the province number
# - add a new triggered modifier for the province
# - add localization text for the description
# - this event series requires the MEIOU isolationist_expansionist policy slider
############################################

namespace = tp_tirunelveli

# tp_tirunelveli.1 Open a trading post in the province
# the triggers only completely rule out isolationist countries but
# the MTTH is set to make it most likely the country will be Latin
# and heavily committed to trade and colonisation
country_event = {

	id = tp_tirunelveli.1

	trigger = {
		OR = {
			AND = {
				dip_tech = 19 # year = 1490
				has_idea_group = exploration_ideas
			}
			AND = {
				dip_tech = 26
				has_idea_group = expansion_ideas
			}
		}
		OR = {
			NOT = { is_colonial_nation = yes }
			AND = {
				is_colonial_nation = yes
				capital_scope = {
					OR = {
						region = bengal_region
						region = hindusthan_region
						region = rajputana_region
						region = west_india_region
						region = deccan_region
						region = coromandel_region
						region = western_ghats_region
					}
				}
			}
		}
		num_of_ports = 2
		has_discovered = 536
		treasury = 100
		536 = { owner = { NOT = { technology_group = western } } }
		NOT = { has_country_flag = TP_buy_tirunelveli }
		NOT = { owns = 536 }
		536 = { NOT = { has_province_flag = TP_trading_post } NOT = { has_province_flag = closed_to_foreigners } }
		NOT = { has_country_flag = TP_trading_post_tirunelveli_rejected }
	}

	mean_time_to_happen = {
		months = 1200
		modifier = {
			factor = 0.65
			technology_group = western
		}
		modifier = {
			factor = 0.65
			religion_group = christian
		}
		modifier = {
			factor = 0.75
			is_colonial_nation = yes
		}
		modifier = {
			factor = 0.75
			OR = { 
				has_country_modifier = india_trade_co
				has_country_modifier = dutch_india_trade_co
				has_country_modifier = portuguese_india_trade_co
			}
		}
		modifier = {
			factor = 0.8
			has_idea = colonial_ventures
		}
		modifier = {
			factor = 0.8
			has_idea = merchant_adventures
		}
		modifier = {
			factor = 0.9
			has_idea = quest_for_the_new_world
		}
		modifier = {
			factor = 0.9
			has_idea = national_trade_policy
		}
		modifier = {
			factor = 0.9
			has_idea = shrewd_commerce_practise
		}
		modifier = {
			factor = 0.9
			trader = 1
		}
		modifier = {
			factor = 0.9
			trader = 2
		}
		modifier = {
			factor = 0.9
			trader = 3
		}
		modifier = {
			factor = 0.9
			navigator = 1
		}
		modifier = {
			factor = 0.9
			navigator = 2
		}
		modifier = {
			factor = 0.9
			navigator = 3
		}
	}

	title = "tp_tirunelveli.1.n"
	desc = "tp_tirunelveli.1.t"
	picture = TRADING_POST_eventPicture

	option = {
		name = "tp_generic.1.a"
		ai_chance = { factor = 90 }
		add_treasury = -100
		add_dip_power = -50
		536 = {
			set_province_flag = TP_trading_post
			add_building = trading_post
		}
		every_country = {
			limit = {
				owns = 536
				OR = {
					ai = no
					ADM = 6
				}
			}
			set_country_flag = TP_rejection_option_tirunelveli
		}
		set_country_flag = TP_trading_post_tirunelveli
	}
	option = {
		name = "tp_generic.1.b"
		ai_chance = { factor = 10 }
		set_country_flag = TP_trading_post_tirunelveli_rejected
	}
}

# tp_tirunelveli.2 Buy more influence in the province
country_event = {

	id = tp_tirunelveli.2

	trigger = {
		has_country_flag = TP_trading_post_tirunelveli
		treasury = 100
		NOT = { has_country_flag = TP_buy_tirunelveli }
		NOT = { owns = 536 }
		536 = { owner = { NOT = { technology_group = western } } has_province_flag = TP_trading_post }
		NOT = { has_country_flag = TP_influence_tirunelveli_02 }
		NOT = { has_country_flag = TP_rejected_tirunelveli_02 }
	}

	mean_time_to_happen = {
		months = 120

		modifier = {
			factor = 0.9
			prestige = 10
		}
		modifier = {
			factor = 0.9
			prestige = 20
		}
		modifier = {
			factor = 0.9
			prestige = 40
		}
		modifier = {
			factor = 0.9
			dip_tech = 18
		}
		modifier = {
			factor = 0.9
			dip_tech = 31
		}
		modifier = {
			factor = 0.9
			dip_tech = 44
		}
		modifier = {
			factor = 0.8
			full_idea_group = trade_ideas
		}
		modifier = {
			factor = 0.75
			OR = { 
				has_country_modifier = india_trade_co
				has_country_modifier = dutch_india_trade_co
				has_country_modifier = portuguese_india_trade_co
			}
		}
		modifier = {
			factor = 0.95
			trader = 1
		}
		modifier = {
			factor = 0.9
			trader = 2
		}
		modifier = {
			factor = 0.8
			trader = 3
		}
	}

	title = "tp_tirunelveli.2.n"
	desc = "tp_tirunelveli.2.t"
	picture = TRADING_POST_eventPicture

	option = {
		name = "tp_generic.2.a"
		ai_chance = { factor = 90 }
		add_treasury = -50
		set_country_flag = TP_influence_tirunelveli_02
	}
	option = {
		name = "tp_generic.2.b"
		ai_chance = { factor = 10 }
		set_country_flag = TP_rejected_tirunelveli_02
	}
}

# tp_tirunelveli.3 Expand our presence in the province
country_event = {

	id = tp_tirunelveli.3

	trigger = {
		has_country_flag = TP_trading_post_tirunelveli
		536 = {
			has_building = trading_post
			NOT = { has_building = trading_post }
		}
		treasury = 100
		NOT = { has_country_flag = TP_buy_tirunelveli }
		NOT = { owns = 536 }
		536 = {
			owner = { NOT = { technology_group = western } }
			has_province_flag = TP_trading_post
		}
		NOT = { has_country_flag = TP_influence_tirunelveli_03 }
		NOT = { has_country_flag = TP_rejected_tirunelveli_03 }
	}

	mean_time_to_happen = {
		months = 240

		modifier = {
			factor = 0.9
			prestige = 10
		}
		modifier = {
			factor = 0.9
			prestige = 20
		}
		modifier = {
			factor = 0.9
			prestige = 40
		}
		modifier = {
			factor = 0.9
			dip_tech = 18
		}
		modifier = {
			factor = 0.9
			dip_tech = 31
		}
		modifier = {
			factor = 0.9
			dip_tech = 44
		}
		modifier = {
			factor = 0.8
			full_idea_group = trade_ideas
		}
		modifier = {
			factor = 0.75
			OR = { 
				has_country_modifier = india_trade_co
				has_country_modifier = dutch_india_trade_co
				has_country_modifier = portuguese_india_trade_co
			}
		}
		modifier = {
			factor = 0.95
			trader = 1
		}
		modifier = {
			factor = 0.9
			trader = 2
		}
		modifier = {
			factor = 0.8
			trader = 3
		}
	}

	title = "tp_tirunelveli.3.n"
	desc = "tp_tirunelveli.3.t"
	picture = TRADING_POST_eventPicture

	option = {
		name = "tp_generic.3.a"
		ai_chance = { factor = 90 }
		add_treasury = -50
		536 = {
			add_building = trading_post
			set_province_flag = TP_trading_post_improved
		}
		set_country_flag = TP_influence_tirunelveli_03
	}
	option = {
		name = "tp_generic.3.b"
		ai_chance = { factor = 10 }
		set_country_flag = TP_rejected_tirunelveli_03
	}
}

# tp_tirunelveli.4 Support rebels in the province
country_event = {

	id = tp_tirunelveli.4

	trigger = {
		has_country_flag = TP_trading_post_tirunelveli
		treasury = 100
		NOT = { has_country_flag = TP_buy_tirunelveli }
		NOT = { owns = 536 }
		536 = { owner = { NOT = { technology_group = western } } has_province_flag = TP_trading_post }
		NOT = { has_country_flag = TP_influence_tirunelveli_04 }
		NOT = { has_country_flag = TP_rejected_tirunelveli_04 }
	}

	mean_time_to_happen = {
		months = 360

		modifier = {
			factor = 0.9
			prestige = 10
		}
		modifier = {
			factor = 0.9
			prestige = 20
		}
		modifier = {
			factor = 0.9
			prestige = 40
		}
		modifier = {
			factor = 0.9
			dip_tech = 18
		}
		modifier = {
			factor = 0.9
			dip_tech = 31
		}
		modifier = {
			factor = 0.9
			dip_tech = 44
		}
		modifier = {
			factor = 0.8
			full_idea_group = trade_ideas
		}
		modifier = {
			factor = 0.75
			OR = { 
				has_country_modifier = india_trade_co
				has_country_modifier = dutch_india_trade_co
				has_country_modifier = portuguese_india_trade_co
			}
		}
		modifier = {
			factor = 0.95
			trader = 1
		}
		modifier = {
			factor = 0.9
			trader = 2
		}
		modifier = {
			factor = 0.8
			trader = 3
		}
	}

	title = "tp_tirunelveli.4.n"
	desc = "tp_tirunelveli.4.t"
	picture = TRADING_POST_eventPicture

	option = {
		name = "tp_generic.4.a"
		ai_chance = { factor = 90 }
		add_treasury = -50
		536 = { add_unrest = 1 }
		set_country_flag = TP_influence_tirunelveli_04
	}
	option = {
		name = "tp_generic.4.b"
		ai_chance = { factor = 10 }
		set_country_flag = TP_rejected_tirunelveli_04
	}
}

# tp_tirunelveli.5 Claim core on the province
country_event = {

	id = tp_tirunelveli.5

	trigger = {
		has_country_flag = TP_trading_post_tirunelveli
		OR = {
			has_country_flag = TP_influence_tirunelveli_02
			has_country_flag = TP_influence_tirunelveli_03
			has_country_flag = TP_influence_tirunelveli_04
		}
		NOT = { is_core = 536 }
		NOT = { has_country_flag = TP_buy_tirunelveli }
		NOT = { owns = 536 }
		536 = { owner = { NOT = { technology_group = western } } has_province_flag = TP_trading_post }
		NOT = { has_country_flag = TP_rejected_core_tirunelveli }
	}

	mean_time_to_happen = {
		months = 2500

		modifier = {
			factor = 0.7
			has_country_flag = TP_influence_tirunelveli_02
		}
		modifier = {
			factor = 0.7
			has_country_flag = TP_influence_tirunelveli_03
		}
		modifier = {
			factor = 0.7
			has_country_flag = TP_influence_tirunelveli_04
		}
		modifier = {
			factor = 0.9
			dip_tech = 31
		}
		modifier = {
			factor = 0.9
			dip_tech = 44
		}
		modifier = {
			factor = 0.8
			full_idea_group = trade_ideas
		}
		modifier = {
			factor = 0.75
			OR = { 
				has_country_modifier = india_trade_co
				has_country_modifier = dutch_india_trade_co
				has_country_modifier = portuguese_india_trade_co
			}
		}
		modifier = {
			factor = 0.95
			trader = 1
		}
		modifier = {
			factor = 0.9
			trader = 2
		}
		modifier = {
			factor = 0.8
			trader = 3
		}
	}

	title = "tp_tirunelveli.5.n"
	desc = "tp_tirunelveli.5.t"
	picture = TRADING_POST_eventPicture

	option = {
		name = "tp_generic.5.a"
		ai_chance = { factor = 90 }
		536 = {
			add_core = ROOT
			owner = {
				add_casus_belli = { target = ROOT type = cb_insult months = 60 }
				add_opinion = {
					who = ROOT
					modifier = reaction_tp_claimed
				}
			}
		}
	}
	option = {
		name = "tp_generic.5.b"
		ai_chance = { factor = 10 }
		set_country_flag = TP_rejected_core_tirunelveli
	}
}

# tp_tirunelveli.6 Sieze control and ownership of the province
country_event = {

	id = tp_tirunelveli.6

	trigger = {
		has_country_flag = TP_trading_post_tirunelveli
		is_core = 536
		NOT = { has_country_flag = TP_buy_tirunelveli }
		NOT = { owns = 536 }
		536 = { owner = { NOT = { technology_group = western } } has_province_flag = TP_trading_post }
		536 = { owner = { NOT = { war_with = ROOT } } }
		NOT = { has_country_flag = TP_rejected_sieze_tirunelveli }
	}

	mean_time_to_happen = {
		months = 1800

		modifier = {
			factor = 0.7
			has_country_flag = TP_influence_tirunelveli_02
		}
		modifier = {
			factor = 0.7
			has_country_flag = TP_influence_tirunelveli_03
		}
		modifier = {
			factor = 0.7
			has_country_flag = TP_influence_tirunelveli_04
		}
		modifier = {
			factor = 0.9
			prestige = 10
		}
		modifier = {
			factor = 0.9
			prestige = 20
		}
		modifier = {
			factor = 0.9
			prestige = 40
		}
		modifier = {
			factor = 0.8
			full_idea_group = trade_ideas
		}
		modifier = {
			factor = 0.75
			OR = { 
				has_country_modifier = india_trade_co
				has_country_modifier = dutch_india_trade_co
				has_country_modifier = portuguese_india_trade_co
			}
		}
		modifier = {
			factor = 0.95
			trader = 1
		}
		modifier = {
			factor = 0.9
			trader = 2
		}
		modifier = {
			factor = 0.8
			trader = 3
		}
	}

	title = "tp_tirunelveli.6.n"
	desc = "tp_tirunelveli.6.t"
	picture = TRADING_POST_eventPicture

	option = {
		name = "tp_generic.6.a"
		ai_chance = { factor = 50 }
		536 = {
			change_controller = ROOT
			owner = {
				# only go to war if they'll have a province left after after the annexation
				random_owned_province = {
					limit = { owner = { total_development = 25 } }
					owner = {
						declare_war_with_cb = {
							who = ROOT
							casus_belli = cb_core
						}
					}
				}
				536 = { cede_province = ROOT }
				add_casus_belli = { target = ROOT type = cb_TP_war months = 60 }
				add_opinion = {
					who = ROOT
					modifier = reaction_tp_grabbed
				}
			}
		}
	}
	option = {
		name = "tp_generic.6.b"
		ai_chance = { factor = 50 }
		set_country_flag = TP_rejected_sieze_tirunelveli
	}
}

# tp_tirunelveli.7 Buy goodwill from the owner
country_event = {

	id = tp_tirunelveli.7

	trigger = {
		has_country_flag = TP_trading_post_tirunelveli
		treasury = 100
		NOT = { has_country_flag = TP_buy_tirunelveli }
		NOT = { owns = 536 }
		536 = { owner = { NOT = { technology_group = western } } has_province_flag = TP_trading_post }
		NOT = { has_country_flag = TP_influence_tirunelveli_07 }
		NOT = { has_country_flag = TP_rejected_tirunelveli_07 }
	}

	mean_time_to_happen = {
		months = 2500

		modifier = {
			factor = 0.5
			is_core = 536
		}
		modifier = {
			factor = 0.9
			prestige = 10
		}
		modifier = {
			factor = 0.9
			prestige = 20
		}
		modifier = {
			factor = 0.9
			prestige = 40
		}
		modifier = {
			factor = 0.9
			dip_tech = 18
		}
		modifier = {
			factor = 0.9
			dip_tech = 31
		}
		modifier = {
			factor = 0.9
			dip_tech = 44
		}
		modifier = {
			factor = 0.8
			full_idea_group = trade_ideas
		}
		modifier = {
			factor = 0.75
			OR = { 
				has_country_modifier = india_trade_co
				has_country_modifier = dutch_india_trade_co
				has_country_modifier = portuguese_india_trade_co
			}
		}
		modifier = {
			factor = 0.95
			trader = 1
		}
		modifier = {
			factor = 0.9
			trader = 2
		}
		modifier = {
			factor = 0.8
			trader = 3
		}
	}

	title = "tp_tirunelveli.7.n"
	desc = "tp_tirunelveli.7.t"
	picture = TRADING_POST_eventPicture

	option = {
		name = "tp_generic.7.a"
		ai_chance = { factor = 50 }
		add_treasury = -100
		random_country = {
			limit = { owns = 536 }
			add_treasury = 100
			add_opinion = {
				who = ROOT
				modifier = tp_goodwill_bought
			}
		}
		set_country_flag = TP_influence_tirunelveli_07
	}
	option = {
		name = "tp_generic.7.b"
		ai_chance = { factor = 50 }
		set_country_flag = TP_rejected_tirunelveli_07
	}
}

# tp_tirunelveli.8 Buy more goodwill from the owner
country_event = {

	id = tp_tirunelveli.8

	trigger = {
		has_country_flag = TP_trading_post_tirunelveli
		treasury = 100
		NOT = { has_country_flag = TP_buy_tirunelveli }
		NOT = { owns = 536 }
		536 = { owner = { NOT = { technology_group = western } } has_province_flag = TP_trading_post }
		NOT = { has_country_flag = TP_influence_tirunelveli_08 }
		NOT = { has_country_flag = TP_rejected_tirunelveli_08 }
	}

	mean_time_to_happen = {
		months = 3600

		modifier = {
			factor = 0.5
			is_core = 536
		}
		modifier = {
			factor = 0.9
			prestige = 10
		}
		modifier = {
			factor = 0.9
			prestige = 20
		}
		modifier = {
			factor = 0.9
			prestige = 40
		}
		modifier = {
			factor = 0.9
			dip_tech = 18
		}
		modifier = {
			factor = 0.9
			dip_tech = 31
		}
		modifier = {
			factor = 0.9
			dip_tech = 44
		}
		modifier = {
			factor = 0.8
			full_idea_group = trade_ideas
		}
		modifier = {
			factor = 0.75
			OR = { 
				has_country_modifier = india_trade_co
				has_country_modifier = dutch_india_trade_co
				has_country_modifier = portuguese_india_trade_co
			}
		}
		modifier = {
			factor = 0.95
			trader = 1
		}
		modifier = {
			factor = 0.9
			trader = 2
		}
		modifier = {
			factor = 0.8
			trader = 3
		}
	}

	title = "tp_tirunelveli.8.n"
	desc = "tp_tirunelveli.8.t"
	picture = TRADING_POST_eventPicture

	option = {
		name = "tp_generic.7.a"
		ai_chance = { factor = 50 }
		add_treasury = -100
		random_country = {
			limit = { owns = 536 }
			add_treasury = 100
			add_opinion = {
				who = ROOT
				modifier = tp_goodwill_bought
			}
		}
		set_country_flag = TP_influence_tirunelveli_08
	}
	option = {
		name = "tp_generic.7.b"
		ai_chance = { factor = 50 }
		set_country_flag = TP_rejected_tirunelveli_08
	}
}

# tp_tirunelveli.9 Buy the province - make the offer
country_event = {

	id = tp_tirunelveli.9

	trigger = {
		has_country_flag = TP_trading_post_tirunelveli
		536 = { NOT = { has_province_flag = TP_offer_to_buy } }
		treasury = 600
		NOT = { owns = 536 }
		536 = { owner = { NOT = { technology_group = western } } has_province_flag = TP_trading_post }
		NOT = { has_country_flag = TP_buy_tirunelveli }
		NOT = { has_country_flag = TP_rejected_buy_tirunelveli }
	}

	mean_time_to_happen = {
		months = 4800

		modifier = {
			factor = 0.5
			is_core = 536
		}
		modifier = {
			factor = 0.7
			has_country_flag = TP_influence_tirunelveli_07
		}
		modifier = {
			factor = 0.7
			has_country_flag = TP_influence_tirunelveli_08
		}
		modifier = {
			factor = 0.9
			has_country_flag = TP_influence_tirunelveli_02
		}
		modifier = {
			factor = 0.9
			has_country_flag = TP_influence_tirunelveli_03
		}
		modifier = {
			factor = 0.9
			has_country_flag = TP_influence_tirunelveli_04
		}
		modifier = {
			factor = 0.9
			prestige = 10
		}
		modifier = {
			factor = 0.9
			prestige = 20
		}
		modifier = {
			factor = 0.9
			prestige = 40
		}
		modifier = {
			factor = 0.8
			full_idea_group = trade_ideas
		}
		modifier = {
			factor = 0.75
			OR = { 
				has_country_modifier = india_trade_co
				has_country_modifier = dutch_india_trade_co
				has_country_modifier = portuguese_india_trade_co
			}
		}
		modifier = {
			factor = 0.95
			trader = 1
		}
		modifier = {
			factor = 0.9
			trader = 2
		}
		modifier = {
			factor = 0.8
			trader = 3
		}
	}

	title = "tp_tirunelveli.9.n"
	desc = "tp_tirunelveli.9.t"
	picture = TRADING_POST_eventPicture

	option = {
		name = "tp_generic.9.a"
		ai_chance = { factor = 50 }
		add_treasury = -100
		536 = { set_province_flag = TP_offer_to_buy }
		set_country_flag = TP_buy_tirunelveli
	}
	option = {
		name = "tp_generic.9.b"
		ai_chance = { factor = 50 }
		set_country_flag = TP_rejected_buy_tirunelveli
	}
}

# tp_tirunelveli.10 Buy the province - owner response
#NB this event happens to the province owner (NOT the trading post owner)
#option 1 - sale_refused - high probability if poor relations and low influence
#option 2 - sale_agreed_ally - high probability if good relations and high influence
#option 3 - sale_agreed_ally_money - medium probability if good relations
#option 4 - sale_agreed_money - medium probability if high influence
country_event = {

	id = tp_tirunelveli.10

	trigger = {
		owns = 536
		536 = {
			owner = { NOT = { technology_group = western } }
			has_province_flag = TP_offer_to_buy
		}
	}

	mean_time_to_happen = {
		days = 1
	}

	title = "tp_tirunelveli.10.n"
	desc = "tp_tirunelveli.10.t"
	picture = TRADING_POST_eventPicture

	option = {
		name = "tp_generic.10.a"
		ai_chance = {
			factor = 25
			days = 1
			modifier = {
				factor = 5
				any_known_country = {
					has_country_flag = TP_buy_tirunelveli
					NOT = { has_opinion = { who = ROOT value = -100 } }
					NOT = {
						OR = {
							has_country_flag = TP_influence_tirunelveli_07
							has_country_flag = TP_influence_tirunelveli_08
						}
					}
				}
			}
		}
		536 = { clr_province_flag = TP_offer_to_buy }
		536 = { set_province_flag = TP_sale_refused }
	}
	option = {
		name = "tp_generic.10.b"
		ai_chance = {
			factor = 25
			days = 1
			modifier = {
				factor = 5
				any_known_country = {
					has_country_flag = TP_buy_tirunelveli
					has_opinion = { who = ROOT value = -100 }
					OR = {
						has_country_flag = TP_influence_tirunelveli_07
						has_country_flag = TP_influence_tirunelveli_08
					}
				}
			}
		}
		536 = { clr_province_flag = TP_offer_to_buy }
		536 = { set_province_flag = TP_sale_agreed_ally }
	}
	option = {
		name = "tp_generic.10.c"
		ai_chance = {
			factor = 25
			days = 1
			modifier = {
				factor = 5
				any_known_country = {
					has_country_flag = TP_buy_tirunelveli
					has_opinion = { who = ROOT value = -100 }
					NOT = {
						OR = {
							has_country_flag = TP_influence_tirunelveli_07
							has_country_flag = TP_influence_tirunelveli_08
						}
					}
				}
			}
		}
		add_treasury = 200
		536 = { clr_province_flag = TP_offer_to_buy }
		536 = { set_province_flag = TP_sale_agreed_ally_money }
	}
	option = {
		name = "tp_generic.10.d"
		ai_chance = {
			factor = 25
			days = 1
			modifier = {
				factor = 5
				any_known_country = {
					has_country_flag = TP_buy_tirunelveli
					NOT = { has_opinion = { who = ROOT value = -100 } }
					OR = {
						has_country_flag = TP_influence_tirunelveli_07
						has_country_flag = TP_influence_tirunelveli_08
					}
				}
			}
		}
		add_treasury = 500
		536 = { clr_province_flag = TP_offer_to_buy }
		536 = { set_province_flag = TP_sale_agreed_money }
	}
}

# tp_tirunelveli.11 Buy the province - sale refused
country_event = {

	id = tp_tirunelveli.11

	trigger = {
		has_country_flag = TP_trading_post_tirunelveli
		NOT = { owns = 536 }
		536 = { owner = { NOT = { technology_group = western } } has_province_flag = TP_sale_refused }
	}

	mean_time_to_happen = {
		days = 1
	}

	title = "tp_tirunelveli.11.n"
	desc = "tp_tirunelveli.11.t"
	picture = TRADING_POST_eventPicture

	option = {
		name = "tp_generic.11.a"
		536 = {
			#owner = {
			#	has_opinion = { who = ROOT value = -25 }
			#}
			clr_province_flag = TP_sale_refused
		}
	}
}

# tp_tirunelveli.12 Buy the province - ally
country_event = {

	id = tp_tirunelveli.12

	trigger = {
		has_country_flag = TP_trading_post_tirunelveli
		NOT = { owns = 536 }
		536 = { owner = { NOT = { technology_group = western } } has_province_flag = TP_sale_agreed_ally }
	}

	mean_time_to_happen = {
		days = 1
	}

	title = "tp_tirunelveli.12.n"
	desc = "tp_tirunelveli.12.t"
	picture = TRADING_POST_eventPicture

	option = {
		name = "OPT.VERYGOOD"
		536 = {
			change_controller = ROOT
			owner = {
				536 = { cede_province = ROOT }
				add_opinion = {
					who = ROOT
					modifier = tp_province_bought
				}
				create_alliance = ROOT
			}
			clr_province_flag = TP_sale_agreed_ally
		}
	}
}

# tp_tirunelveli.13 Buy the province - ally & money
country_event = {

	id = tp_tirunelveli.13

	trigger = {
		has_country_flag = TP_trading_post_tirunelveli
		NOT = { owns = 536 }
		536 = { owner = { NOT = { technology_group = western } } has_province_flag = TP_sale_agreed_ally_money }
	}

	mean_time_to_happen = {
		days = 1
	}

	title = "tp_tirunelveli.13.n"
	desc = "tp_tirunelveli.13.t"
	picture = TRADING_POST_eventPicture

	option = {
		name = "OPT.VERYGOOD"
		add_treasury = -200
		536 = {
			change_controller = ROOT
			owner = {
				536 = { cede_province = ROOT }
				add_opinion = {
					who = ROOT
					modifier = tp_province_bought
				}
				create_alliance = ROOT
			}
			clr_province_flag = TP_sale_agreed_ally_money
		}
	}
}

# tp_tirunelveli.14 Buy the province - money
country_event = {

	id = tp_tirunelveli.14

	trigger = {
		has_country_flag = TP_trading_post_tirunelveli
		NOT = { owns = 536 }
		536 = { owner = { NOT = { technology_group = western } } has_province_flag = TP_sale_agreed_money }
	}

	mean_time_to_happen = {
		days = 1
	}

	title = "tp_tirunelveli.14.n"
	desc = "tp_tirunelveli.14.t"
	picture = TRADING_POST_eventPicture

	option = {
		name = "OPT.VERYGOOD"
		add_treasury = -500
		536 = {
			change_controller = ROOT
			owner = {
				536 = { cede_province = ROOT }
				add_opinion = {
					who = ROOT
					modifier = tp_province_bought
				}
			}
			clr_province_flag = TP_sale_agreed_money
		}
	}
}

# 8053615 Human Player Rejects TP
country_event = {

	id = tp_tirunelveli.15

	#is_triggered_only = yes
	
	trigger = {
		has_country_flag = TP_rejection_option_tirunelveli
	}
	
	mean_time_to_happen = {
		days = 1
	}

	title = "tp_tirunelveli.15.n"
	desc = "tp_tirunelveli.15.t"
	picture = TRADING_POST_eventPicture

	option = {
		name = "OPT.FIGHTEM"
		ai_chance = {
			factor = 50
			modifier = {
				factor = 1.2
				num_of_cities = ROOT
			}
			modifier = {
				factor = 1.2
				prestige = ROOT
			}
			modifier = {
				factor = 1.2
				ADM = 7
			}
			modifier = {
				factor = 1.2
				ADM = 8
			}
			modifier = {
				factor = 1.2
				ADM = 9
			}
			modifier = {
				factor = 1.2
				num_of_infantry = ROOT
			}
			modifier = {
				factor = 1.2
				num_of_heavy_ship = ROOT
			}
			modifier = {
				factor = 0.8
				NOT = { num_of_cities = ROOT }
			}
			modifier = {
				factor = 0.8
				NOT = { prestige = ROOT }
			}
			modifier = {
				factor = 0.8
				NOT = { ADM = 7 }
			}
			modifier = {
				factor = 0.8
				NOT = { ADM = 8 }
			}
			modifier = {
				factor = 0.8
				NOT = { ADM = 9 }
			}
			modifier = {
				factor = 0.8
				NOT = { num_of_infantry = ROOT }
			}
			modifier = {
				factor = 0.8
				NOT = { num_of_heavy_ship = ROOT }
			}
		}
		every_country = {
			limit = { has_country_flag = TP_trading_post_tirunelveli }
			clr_country_flag = TP_trading_post_tirunelveli
			set_country_flag = TP_trading_post_tirunelveli_rejected
			set_country_flag = TP_rejectionevent_tirunelveli
		}
		clr_country_flag = TP_rejection_option_tirunelveli
		536 = { clr_province_flag = TP_trading_post set_province_flag = closed_to_foreigners }
	}

	option = {
		name = "OPT.WELCOME"
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0.8
				num_of_cities = ROOT
			}
			modifier = {
				factor = 0.8
				prestige = ROOT
			}
			modifier = {
				factor = 0.8
				ADM = 7
			}
			modifier = {
				factor = 0.8
				ADM = 8
			}
			modifier = {
				factor = 0.8
				ADM = 9
			}
			modifier = {
				factor = 0.8
				num_of_infantry = ROOT
			}
			modifier = {
				factor = 0.8
				num_of_heavy_ship = ROOT
			}
			modifier = {
				factor = 1.2
				NOT = { num_of_cities = ROOT }
			}
			modifier = {
				factor = 1.2
				NOT = { prestige = ROOT }
			}
			modifier = {
				factor = 1.2
				NOT = { ADM = 7 }
			}
			modifier = {
				factor = 1.2
				NOT = { ADM = 8 }
			}
			modifier = {
				factor = 1.2
				NOT = { ADM = 9 }
			}
			modifier = {
				factor = 1.2
				NOT = { num_of_infantry = ROOT }
			}
			modifier = {
				factor = 1.2
				NOT = { num_of_heavy_ship = ROOT }
			}
		}
		clr_country_flag = TP_rejection_option_tirunelveli
	}
}

country_event = {

	id = tp_tirunelveli.16

	trigger = {
		has_country_flag = TP_rejectionevent_tirunelveli
	}
	
	mean_time_to_happen = {
		days = 1
	}

	title = "tp_tirunelveli.16.n"
	desc = "tp_tirunelveli.16.t"
	picture = TRADING_POST_eventPicture
	
	option = {
		name = "OPT.DAMN"
		clr_country_flag = TP_rejectionevent_tirunelveli
	}
}
