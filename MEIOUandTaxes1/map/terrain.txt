
##################################################################
### Terrain Categories
###
###

categories =  {
	pti = {
		type = pti
	}

	ocean = {
		movement_cost = 1.0
		is_water = yes
		sound_type = sea
		color = { 255 255 255 }
	}

	inland_ocean = {
		movement_cost = 1.0
		is_water = yes
		inland_sea = yes
		sound_type = sea
		color = { 0 0 200 }
		
		terrain_override = {
			1851 1853 1856 2782 2784 
		}
	}

	arctic = {
		movement_cost = 2.0
		attrition = 2
		supply_limit = 4
		type = arctic
		sound_type = desert
		color = { 235 235 235 }
		
		local_development_cost = 0.5
		nation_designer_cost_multiplier = 0.75
		terrain_override = {
			2194 2193 978 979 2192 2191 977 2190 2189 
		}
	}

	farmlands = {
		movement_cost = 1.00
		
		supply_limit = 8
		type = plains
		sound_type = plains
		color = { 137 104 165  }
		
		allowed_num_of_buildings = 1
		nation_designer_cost_multiplier = 1.1
		terrain_override = {
			684 2117 2605 4035 2249 707 704 705 2492 697 2468 2170 2254 708 698 688 724 2472 #North China
			672 710 675 2486 2082 682 2478 667 2797 2149 695 #South China
			1329 363 151 1402 3876 3881 330 3396 1326 1438 1446 2552 322 2862#MENA
			416 1317 445 422 412 3292 3824 405 457 442 3285#Eran+CA
			2796 2435 2280 2283 3959 3955 3943 1032 3967 1026 2291 3956 2288 3953 2801 733 737 2233 730#Japan+Korea
			225 2311 1508 2312 2310 215 3370 217 227#Iberia
			1346 1345 3381 103 114 3700 118 111 3869 2562 3697#Italy
			183 184 3375 167 89 4186 2354 203 2239 196 3372 1388 192 976 1387 2234 391 2307 88 1335 177 178 2356 63 #France
			3376 1426 147 146 2612 161 3785 1267 153 1270#Balkans+Carpathia
			256 1531 1279 3758 2355 262 1844 2815 259 280 299 2848 276 4018 272 1393 1296 294 2580 295 289 1292 1307 1308 1310 1293 301 #Eastern Europe
			237 236 2386 1411 3326 1283 373 3335 248 #Atlantic Isles
			6 2755 15 14 1 #Scandinavia
			87 90 2363 95 98 97 2372 2370 91 92 2360#low countries
			77 78 2187 3379 64 1360 3378 1365 3732 #Hochdeutschlands
			134 1510 83 265 268 270 1359 132#cisleithania
			2698 1252 3821 3818 1350 3851 1373 1353 973 85 2623 #niederdeustchland
			
			2628 3159 2630 532 3163 3161 545 3174 3166 2569 3145 542 2682 3173 549 3142#South India
			2425 2426 3215 3905 2429 2670 601 2422 3908 2819 3226 3227 2319 3231 609 3933 604 610 3938 3937 #Indochina
			595 3239 596 629 3924 3923 630 3927 2197 626 2104 2105 641 619 2216 658 #Austronesia
		}
	}
	
	floodplains = {
		movement_cost = 1.00
		supply_limit = 10
		type = plains
		sound_type = plains
		color = { 224 154 161  }
		
		allowed_num_of_buildings = 1
		nation_designer_cost_multiplier = 1.2
		terrain_override = {
			663 2150 2607 2125 2147 1068 694 1046 1089 2145 2272 
			2480 2477 712 1172 670 2494 1337 685 2510 1143 2121 711 2073 2078 680 2141 692 1053#South China
			699 1041 701 702 2491 2560 2252 696 #North China
			398 392 3826 358 2498 361 1338 2543 3003 1213 360 1214 4094 4093 1322 410 3087 3085 415 3086 #MENA
			426 417 458#Greater Persia+CA
			2287 2282 2277 3948 2279 2293 1020 3946 736 735 #Japan+Korea
			1471 2410 2200 3532#Mesoamerica
			104 2372 109 1378 1347 1377#Italy
			2698 3185 2113 3102 3103 511 3111 521 3115 2163 522 3126 524 3127 2555 3128 3131 555 3132 2557 3130 
			2210 2632 2688 558 3202 2689 3183 3184 2693 3187 3188 3203 1588 3189 561 2696 567 563 2716 2717 2584 3108#North India
			3135 3172 3178 3175 3204 3136 516 3125 2709 3143 3190 3134 3191 2699 2690 3129 2705 3123 3109 510 564 3199 2112 3538 #North India
			2687 3149 3142 1587 2212 2683 507 2627 3174 553 546 2673 3154 2674 539 3162#South India
			3216 352 2427 584 600 3223 2821 2417 3932 605 2827 613 2395 3938 #Indochina
			2830 2108 630 2109 2106 2110#Austronesia
		}
	}
	
	forest = {
		movement_cost = 1.50

		combat_width = -0.35
		supply_limit = 6
		type = forest
		sound_type = forest
		color = { 91 123 45 }
		
		local_development_cost = 0.2
		nation_designer_cost_multiplier = 0.9
		terrain_override = {
			972 2186  1857 3992 264 3274 1048 1050 727 3247 
			3650 3648 987 3647 3583 845 3582 920 1462 947 3579 3580 
			957 944 1489 1459 960 3637 2198 274 
			1030 1032 2286 3962 3966 3974
			2326#South China
			1318 3090 3078 #Greater Persia+CA			
			2298 2300 1019 1016 #Japan+Korea
			3801 326 2581 1444 #MENA
			3347 2567 3368 212 2368 208 1421#Iberia
			1858 119#Italy
			2812 189 4228 1381#France
			3755 1403#Balkans+Carpathia
			2399 2832 2814 4025 42 271 39 1432 3994 314#Eastern Europe
			427 3325 3324 328 #Atlantic Isles
			4 7 1259 29 31 32 1262 30 19 11 21 26 #Scandinavia
			1511 4004#low countries
		}
	}
	
	hills = {
		movement_cost = 1.5
		defence = 1
		local_defensiveness = 0.15
		color = { 135 70 0 }

		combat_width = -0.45
		supply_limit = 6
		type = hills
		sound_type = mountain
		
		local_development_cost = 0.25
		nation_designer_cost_multiplier = 0.85
		terrain_override = {
			3373 3862 2514 127 891 3446 376 791 3872 
			1405 3463 3465 763 3468 3471 3469 760 3774 758 490 487 2188 3672 786 368 
			764 3702 782 1065 3474 1518 1528 3899 3942 1527 1226 3860 1382  2385  
			2366 193 1323 194 370 3382 130 204   
			2471 2251 687 
			2793 1143 1043 2493 2142 692 2490 680 2606 2488 1053 679  2478 683 
			2731 473 428 128 3747 129 2383 2473 2313     1422 
			3263 3106 642 643 644 4045 3928 2432 2433 4044  1306 281
			2377 
			850 2202 2209 2203 2645 2404 3530 3544 856 3529 855 2403 3528 1468 3526 3524 3525
			1470 3522 2408 3523 3537 2405 2402 3534 3533 2597 2201 2409 2644 2204 2600 2601 2640
			2345 3439 3440 3442 2344
			681 668 671 678 2464 710 2462 1760 #South China
			3808 3807 2577 4234 3830 3836 3079#Greater Persia+CA
			1017 1023 1025 2296 2281 3965 2294 2805 2804 2803 2800 2802#Japan+Korea
			3863 2308 1409 1408 210 3354 198 3360 207#Iberia
			116 117 3699 1414 3701 2563 1390 2564 2854 1413 102 3703 121#Italy
			1384 2550 #France
			1767 3773 2389 3777 2124 1424 1401 2137 157 2376 3754 1276 #Balkans+Carpathia
			263 258 2535 3760 2357 1269 261 3757#Eastern Europe
			409 246 242 1412 1416 3337 249 3328 #Atlantic Isles
			10 3998#Scandinavia
			94 93#low countries
			65 69  3379 2615 2622 2359 1361 1370 1363 3812 3717 2757 3730#Hochdeutschlands
			71 3739 3748 3822 #niederdeustchland
			518 2709 526 3140 3116 527 2211 3141 3170 2554 3171 559 3181 3180 3104 2703 517 2723 2708#North India
			533 3200 3147 2679 2680 3161 530 3164 3146 2684 2681 2675 3156 3177 3153 2677 2244 3160 #South India
			3195 3906 587 2728 3224 3232 2829 4212 2396 3234 2394 2393 616 4218 611 588 #Indochina
			2214 657 653 3341 2099 3926 632 3315 648 647 #Austronesia
		}
	}

	woods = {
		movement_cost = 1.25

		color = { 165 205 108 }

		combat_width = -0.35
		supply_limit = 6
		type = forest
		sound_type = forest
		
		local_development_cost = 0.15
		nation_designer_cost_multiplier = 0.9
		
		terrain_override = {
			428 2536 191 2365 170 4008  
			128 3245 3987 3984 3787 161 2378
			4037 4031 731 723 726
			2492 #North China
			429 464 3081 1445 468 3828 3849 3294 450 2724#Greater Persia+CA
			1018 2278 1022 3968 2299 #Japan+Korea
			2540 3878 325 327 2238 3391 #MENA
			2368 2369 2473 3361 3865 3347 1423 3369 143 3351 2454 209 216 2314#Iberia
			3866 2534 122 2852 #Italy
			2240 171 180 1512 2362#France
			254 255 2834 2833 1391 283 3792 2398 2836 3398 1264 1265 1433 1430#Eastern Europe
			3323 3334 247#Atlantic Isles
			1256 8 2 1258 26 18 17 21 #Scandinavia
			#low countries
			3157 535 2570 534 543 2676 540 552 2243 3165 2707#South India
			3217 2321 2370 3235 2423 4217 3233 3934 #Indochina
			598 597 594 2093 2097 642 4045 3339 3921 2090 2089 620 3916 3915 2213 652 654#Austronesia
		}
	}
	
	mountain = {
		movement_cost = 2.0
		defence = 2
		attrition = 2
		local_defensiveness = 0.45
		color = { 117 108 119 }
		
		combat_width = -0.60
		supply_limit = 5
		type = mountain
		sound_type = mountain
		
		local_development_cost = 0.5
		nation_designer_cost_multiplier = 0.75
		terrain_override = {
			205  1082 2438 2548 1343 3444 2348 2344 3438 
			3434 3435 3437 3436 3427 3428 3426 3423 3424 2340 3418 2067 3420 3421 
			820  3612 936 3614 954 3621 3620 3624 889 1823 2199 2167 3570 3669  
			1464 1465 3675 3676 875 876 3681 1822 2469 3668 2470 2441 371  185 166 2625  
			3257 3261   665 686   2250 669 2259 3208 2748 
			3300 3302 3848 1036 1407 1255
			2484 2488 693 2085 2086 664#South China
			709 3242 3243 3241 #North China			
			3100 3831 3827 2559 3825 577#Greater Persia+CA
			2286 3974 2290 2806 734 2328#Japan+Korea
			2932 2941 346 2933#MENA
			1420 211#Iberia
			68 1376 110 2855 #Italy
			3371 179 1385 1375#France
			3871 1767 3779 2392 1392 1398 3786 158 3788#Balkans+Carpathia
			2536 2379 2236 3791 260#Eastern Europe
			23 10 1266 #Scandinavia
			3139 2686 3169 2685 3168 #North India
			3920 3919 2098 #Austronesia
		}
	}
	
	desert_mountain = {
		movement_cost = 3.0
		defence = 2
		attrition = 2
		local_defensiveness = 0.35
		
		color = { 147 148 119 }
		
		combat_width = -0.60
		supply_limit = 4
		type = mountain
		sound_type = desert
		
		local_development_cost = 0.5
		nation_designer_cost_multiplier = 0.75
		terrain_override = {
			3076 384 2665 3445 2347 3441 2740 #MENA
			2715 4030 3082 3099 3834 #Greater Persia+CA
		}
	}

	impassable_mountains = {
		movement_cost = 8.0
		defence = 6
		sound_type = desert
		
		color = { 128 128 128 }
		
		local_development_cost = 10
	}

	grasslands = {
		movement_cost = 1.0

		supply_limit = 7
		type = plains
		sound_type = plains
		color = { 211 201 184 }
		
		allowed_num_of_buildings = 1
		nation_designer_cost_multiplier = 1
		terrain_override = {
			3028 3888 2666 2544 2660 2951 
			779 778 3454 781 3452 789 790 1057 3265 3267 4042 
			713 2266 3258 3259 3262 2248 4038 2253 3268 720 622 623 641 
			2649 24
			461 477 454 3846 3311 3306 3096#Greater Persia+CA
			168 2329 169 #France
			4018 4021 2545 1287 3397 2537 2401 297 37 4110 41 1263 3756 305 290 308 #Eastern Europe
			234 3330 375 3331 3325 240 3324 251#Atlantic Isles
			1428 3720 13 16#Scandinavia
			1372 1371 2451#low countries
			79#Hochdeutschlands
			2835 2621 3727 53 46 264 3813 3380 2449 2845 #niederdeustchland
			505 504 503 3119 2713 3138 2704 2631 2706 #North India
		}
	}
	
	plains = {
		movement_cost = 1.0

		supply_limit = 8
		type = plains
		sound_type = plains
		color = { 241 221 184 }
		
		allowed_num_of_buildings = 1
		nation_designer_cost_multiplier = 1
		terrain_override = {
			703 700 728 3252 3244 2485 #North China
			1663 1419 893 897 884 3553  #North America
			3536 3535 2599#Mesoamerica
			3809 3810 3811 2576 3289 447 #Greater Persia+CA
			2634 385 2501#MENA
			3353 2448 3363#Iberia
			1387 170 187 188 2331 191 186 3374 1386 2332 2330 175 173 3861 #France
			3775 1848 3780 3781 1450 2436 3769 3770 3787 3783 2378#Balkans+Carpathia
			274 1286 304 2545 313 3991 1194 298 3980 4016 2546 1217 3964 3988 2437 1429#Eastern Europe
			3870 1285 2853 108 1355 #Italy
			374 3329 3327 2529 372 243 245 244 3005#Atlantic Isles
			2616 1351 75 4005 1367 80 82 81 974#Hochdeutschlands
			50 4015 3745 59 3744 1352 56 3815 2813 3741 3723 55 1368 51 1362 2364 3724 44 1357 4009 2367 97 #niederdeustchland
			4003 2561 1278 3730#cisleithania
			2702 550 3118 3120 528 3117 520 2712 2711 2710 2714 3192 #North India
			2678 #South India
			3903 3904 2822 2322 602 2733 608 2397 3936 2825 2818 2319 3228#Indochina
			3342 3925 2107 628 2111 625 624	#Austronesia
		}
	}

	jungle = {
		movement_cost = 2.0
		defence = 1
		attrition = 2

		combat_width = -0.35
		supply_limit = 5
		type = jungle
		sound_type = jungle
		color = { 40 180 149 }
		
		local_development_cost = 0.35
		nation_designer_cost_multiplier = 0.8
		terrain_override = {
			2890 2885 2787 1128 1129 2768  2419 2980 3456 3457 1126 1142 1146 1154 1156
			3455 3477 3482 758 3473 3461 2075 2071 801 775 774 3488 3491 
			2753
			674 3234 2424 3915 3922 622 623 607#Austronesia
			
		}
	}	
	
	marsh = {
		movement_cost = 2.0
		defence = 1
		attrition = 1

		combat_width = -0.4
		supply_limit = 5
		type = marsh
		sound_type = forest
		color = { 76 112 105 }
		
		local_development_cost = 0.25
		nation_designer_cost_multiplier = 0.85
		terrain_override = {
			315 362 2891 3597  3721 3722 3084 4092 3867 200 399 40  
			2840 2839 1394 2837 1395 2838 #Pinsk
			586 2823 568 503 3124 3176 3182 605 2306 2421 3223 #sea
			921 926 927 928 1455 3593 3594 3696 112 3401 
			
		}
	}
	
	desert = {
		movement_cost = 2
		attrition = 2
		local_defensiveness = -0.1
		
		supply_limit = 4
		type = desert
		sound_type = desert
		
		color = { 220 210 0 }
		
		local_development_cost = 0.35
		nation_designer_cost_multiplier = 0.8
		terrain_override = {
			2743 2119 2268 3274 2747 2746 
			3306 461 477 3309 3311 2223 471 3299 3295 3298 3283 455 438 443 3286 
			462 439 441 386 446 390 3839 432 3095 3096 3094 3097 
			 3284 3846 3297 3296 2221 2979 1157 1159 1184 2254 
			3210 3211 739 2257 3275 
			2721 2718 3101 2719 2720 519 512#North India
		}
	}
	
	taiga = {
		movement_cost = 2.0
		attrition = 1
		defence = 1
	
		supply_limit = 5
		type = forest
		sound_type = forest
		
		color = { 128 200 250 }
		
		local_development_cost = 0.25
		nation_designer_cost_multiplier = 0.9
		terrain_override = {
			1260 3997 891 2327 1261 35 279 278 4023 1072 1075 
			1529 1074 2445 2154 2155 2157 1040 1039 1038 1037 2744 2301 3248 1049 
			3249 1047 1044 1045 1052 3255 1051 1054 1059 2156 1063 1060 1064 1065 
			2153 1066 1078 981 980 3634 890 985 991 1009 2197 997 4202 4203 4204 
			2481 1000 1003 994 3638 3639 3641 3651 3652 992 2196 3657 1001 4205 
			3656 1002 3655 3654 3653 1007 3661 1006 1005 3662 1010 1008 3578 910 
			908 1011 3663 2168 909 2169 2189 2190 3660 1004 3640 3644 3250 3251 
			3270 2161 2159 999 1517 995 996 4206 4208 4209 4200 4194 1055 4195 1062 
			1056 4197 
		}
	}
	
	
	tundra = {
		movement_cost = 1.5
		attrition = 2
		defence = 1
	
		supply_limit = 4
		type = arctic
		sound_type = plains
		
		color = { 200 225 250 }
		
		local_development_cost = 0.25
		nation_designer_cost_multiplier = 0.9
		terrain_override = {
			33 277 2441 2152 2158  1036 1035 1034 1530 979 978 2194 2193 3670 2475 
			2160 1104 998 
		}
	}
	
	coastline = {
		movement_cost = 1.0

		supply_limit = 5
		local_defensiveness = 0.10
		
		sound_type = sea
		
		color = { 200 128 0 }

		local_development_cost = 0.1
		nation_designer_cost_multiplier = 0.95
		terrain_override = {
			3500 741 3499 1481 743 744 1490 745 746 747 1483 3492 742 2752 27 
			3491 748 749 3489 3486 753 3485 754 3480 1485 3479 755 3476 756 3470 
			757 3467 3466 761 762 764 3464 765 767 768 770 772 776 751 1484 1482 
			2185 3677 2184 3679 872 870 869 1824 3690 871 3689 878 1735 3894 3893 
			1228 1521 1519 1227 3898 3897 3896 3895 172 2191 2192 4008 126 1533 228 
			334 2896 2869 2870 2962 676 677 1332 1582 1163 2881 
			1164 1581 1165 1166 2982 2986 2983 1177 2985 1180 1151 2994 1181 1564 
			1190 1191 1186 1187 1188 1565 2352 2995 1195 3000 2996 2998 1198 1566 
			3032 3033 1199 3034 3035 3036 3037 3054 3039 1200 3050 1201 3047 3044 
			1574 3006 1202 1506 2981 2879 2975 1140 1505 1504 2775 1141 3816 
			1138 1139 2779 1549 2776 2790 2786 2885 2890 2877 1126 2878 2875 2572 
			2758 2874 2892 1125 1123 2894 1119 2768 1120 2767 2872 2773 2883 2218 
			4040 4041 1110 1114 2871 1499 2882 2434 834 
			859 2642 864 3527 1711 854 849 851 847 2205 2207 3519 2206 844 841 2208
			826 1475 823 795 3402 3403 3404 797 3408 804 3411 806 809 812 3414 816
			1491 2066 3430 3431 3432 3433 2342 3445 2346 2349 787
			2574 2381 4060 4059 320 1435 3875 2635 1534 351 2868 353 4073 2663 2865 2863 340#MENA
			3366 3367 2358 2565 1284#Iberia
			2276 1021 3947 2302 3957 3972 1014 1013 1012#Japan+Korea
			1415 3868 2453 2241#Italy
			2452 2571 136 137 138 2237 3873 142 1436 3383 164 #Balkans+Carpathia
			1354 3768 36 3400#Eastern Europe
			1514 3719 1417 369 96#Atlantic Isles
			9 25 1257 20 67 2515 1250 1251 #Scandinavia
			3817 2849 47#niederdeustchland
			3238 4056 537 1248 421 1100 1102 1103 1101 #South India
			574 420 2734 4050 592 4216#Indochina
			407 4047 2094 659 631 907 645 2430 651 655 2215 2588 2102 2101 2525 4048 1425 2003 #Austronesia
		}
	}
	
	coastal_desert = {
		color = { 255 211 110 }

		type = desert
		sound_type = desert

		movement_cost = 1.0
		local_development_cost = 0.25
		nation_designer_cost_multiplier = 0.9		
		supply_limit = 4

		terrain_override = {
			380 3077 474 388 4095 1211 2650 1571 3043 393 397 396 395 394 467 2539 466 400 3068 465 402 3074 
			401 3887 364 356 1339 354 2528 2861 338 4070 4071 2971 4069 #MENA
			2228 431 2235 2229 434 575 576#Greater Persia+CA
		}	
	}

	#drylands
	drylands = {
		movement_cost = 1.00
		
		supply_limit = 9
		type = plains
		sound_type = plains
		color = { 208 158 108  }

		local_development_cost = 0.05
		nation_designer_cost_multiplier = 0.95		
		terrain_override = {
			866 865 1733 1466 877 879 3543 862 3542 880 881 2638 2639 882 3540 
			1467 3541 1734 860 885 887 3549 886 780 3453 868 867 2166 848 
			853 1463 3539 858 3547 863 3548 1461 888 3551 1418 3552  2267 718
			722 2251 2257 2476 2256 2246#North China
			1313 1314 2219 416 3279 3847 3843 3293 405 3811 436 2231 463 3278 2222 456 3305 2740 2745 2741 2743 476 2220 2742 3213 #Greater Persia+CA
			419 423 3900 1320 3089 2636 377 1327 1333 4062 325 328 323 382 414 331 3883 1755 4057 378 3886 2655 1334 2499 2315 2860 341 2864 4075 343 339 4068 3095 3097 3842 2224 3093 3091 1319 1532 2940 2942#MENA
			181 220 1845 222 226 3357 214 209 217 3348 3349 231 1422 2310 182 333 3364 3365 224 213 219 3355 3362 221#Iberia
			2531 2532 123 2856 125 124 101 1513 105 120 1380 115 2858 2242 1247 #Italy
			2361 201 2335 3377 174 176 202 1383#France
			130 2388 131 4034 1425 3384 1434 154 163 3385 2387 1591 1427 129 149 805 2373 3778 2390 1274 1275#Balkans+Carpathia
			3793 2626 1400 3794 3877 1344 1298 1549 1300 3799 2549 1297 302#Eastern Europe
			4007 4006 2756 2614 3734#Hochdeutschlands
		}
	}

	#highlands
	highlands = {
		movement_cost = 1.50
		defence = 1
		local_defensiveness = 0.2
		color = { 135 70 0 }

		combat_width = -0.45
		supply_limit = 6
		type = hills
		sound_type = mountain
		
		local_development_cost = 0.2
		nation_designer_cost_multiplier = 0.9
		terrain_override = {
			 2162 2260 2258 3207 2263 2262 2261 
			3521 2407 2406 852 3531 857 1469
			798 3410 807 811 3412 813 3413 819 3415 3416 820 3419 3421 3420
			2067 3422 3423 3424 2340 2069 2068 3428 2339 3429 2343 3436 3437 3435
			3434 3438 3426 3427 1206 1568 3067 3066 1576 1569 3011 3065 1207
			2467 2083 660 #China
			3884 3802 418 2664 1515 347 4072 336 2935 #MENA
			2225 3083 2227 3092 451 3291 453#Greater Persia+CA
			2289  #Japan+Korea
			3359 199 223 1268 139 1405 1404#Europe
			252 253 3338 2512#Atlantic Isles
			165 1374 73 #Hochdeutschlands
			569 583 571#Indochina
			
		} 
	}

	savannah = {
		movement_cost = 1.00
			
		supply_limit = 7
		type = plains
		sound_type = plains
		color = { 201 214 148  }

		local_development_cost = 0.15
		nation_designer_cost_multiplier = 0.95			
		terrain_override = {
			1487 3451 781 1170 2982 2986 2983 1177 2989 2988 2991 1586 2970 1179 1507 
			2992 2968 1178 1183 1174 2993 2990 2969 1150 1579 1578 1193 1192 1492 1189 
			1153 
		}
	}
	
	#steppe
	steppe = {
		movement_cost = 1.00
		
		supply_limit = 7
		type = plains
		sound_type = plains
		color = { 185 106 210  }
		
		local_development_cost = 0.10
		nation_designer_cost_multiplier = 0.95
		terrain_override = {
			      4020 1296   
			 1312  1081 1073 475 1080 2442 472 1076 3304 479 3308 1071 
			480 3276 1069 2270 1067 2271 3272 1061 3271 3270 3269 719 3266 3264 
			721 3260 3256 1488 3448 3449 3450 3277 3312 2444 4199 4198 
			  
			   
			785 3447 1077 1070   3273 1066 2446 1079 4201 
			160 284 4021 282 1431 285 4019 300 3796 1302 286 1303 2656 1309 303 287 1304 2439 2443 1311 1083 3287 3310#Eastern Europe
		}
	}	
}


##################################################################
### Graphical terrain
###		type	=	refers to the terrain defined above, "terrain category"'s 
### 	color 	= 	index in bitmap color table (see terrain.bmp)
###

terrain = {
	plains					= { type = plains			color = { 	0	 } }
	hills					= { type = hills			color = { 	1	 } }
	desert_mountain			= { type = mountain			color = { 	2	 } }
	desert					= { type = desert			color = { 	3	 } }
	grasslands				= { type = grasslands		color = { 	4	 } }

	terrain_5				= { type = mountain			color = { 	5	 } }
	mountain				= { type = mountain			color = { 	6	 } }
	desert_mountain_low		= { type = desert			color = { 	7	 } }
	forest					= { type = forest			color = { 	8	 } }
	marsh					= { type = marsh			color = { 	9	 } }

	terrain_10				= { type = marsh			color = { 	10	 } }
	terrain_11				= { type = desert			color = { 	11	 } }
	savannah				= { type = savannah			color = { 	12	 } }
	terrain_13				= { type = marsh			color = { 	13	 } }
	terrain_14				= { type = farmlands		color = { 	14	 } }

	ocean					= { type = ocean			color = { 	15	 } }
	snow					= { type = mountain 		color = { 	16	 } } # (SPECIAL CASE) Used to identify permanent snow
	inland_ocean_17 		= { type = inland_ocean		color = {	17	 } }
	high_hills				= { type = hills			color = { 	18	 } }
	coastal_desert_18		= { type = taiga			color = { 	19	 } }

	coastline				= { type = coastline		color = { 	36	 } }
	woods					= { type = woods			color = { 	255	 } }
	jungle					= { type = jungle			color = { 	254	 } }
	terrain_20				= { type = hills			color = { 	20	 } }
	terrain_21				= { type = hills			color = { 	21	 } }

	terrain_22				= { type = hills			color = { 	22	 } }
	terrain_23				= { type = hills			color = { 	23	 } }
	terrain_24				= { type = farmlands		color = { 	24	 } }
	terrain_25				= { type = farmlands		color = { 	25	 } }
	terrain_26				= { type = hills			color = { 	26	 } }

	terrain_27				= { type = hills			color = { 	27	 } }
	terrain_28				= { type = hills			color = { 	28	 } }
	terrain_29				= { type = desert_mountain	color = { 	29	 } } # Isthmus
	terrain_30				= { type = desert			color = { 	30	 } }
	terrain_31				= { type = marsh			color = { 	31	 } }

	terrain_32				= { type = plains			color = { 	32	 } }
	terrain_33				= { type = plains			color = { 	33	 } }
	terrain_34				= { type = plains			color = { 	34	 } }
	terrain_35				= { type = tundra			color = { 	35	 } }
}

##################################################################
### Tree terrain
###		terrain	=	refers to the terrain tag defined above
### 	color 	= 	index in bitmap color table (see tree.bmp)
###
	
tree =
{
	forest				= { terrain = forest 			color = { 	3 4 6 7 19 20	} }
	woods				= { terrain = woods 			color = { 	2 5 8 18	} }
	jungle				= { terrain = jungle 			color = { 	13 14 15	} }
	palms				= { terrain = desert 			color = { 	12	} }
	savana				= { terrain = grasslands 		color = { 	27 28 29 30	} }
}

