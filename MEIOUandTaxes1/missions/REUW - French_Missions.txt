# French Missions

annex_orleans = {
	
	type = country

	category = DIP
	
	target_provinces = {
		owned_by = ORL
	}
	allow = {
		tag = FRA
		is_subject = no
		ORL = {
			vassal_of = ROOT
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			religion_group = ROOT
			OR = {
				NOT = { has_country_flag = french_apanage }
				ROOT = { has_country_flag = edit_de_moulins }
			}
		}
	}
	success = {
		NOT = { exists = ORL }
		all_target_province = {
			owned_by = ROOT
		}
	}
	abort = {
		OR = {
			is_subject = yes
			NOT = { exists = ORL }
			ORL = { NOT = { vassal_of = ROOT } }
			ORL = { NOT = { religion_group = ROOT } }
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			dip = 4
		}
	}
	effect = {
		add_prestige = 5
		add_dip_power = 30
	}
}


annex_provence = {
	
	type = country

	category = DIP
	
	target_provinces = {
		owned_by = PRO
	}
	allow = {
		tag = FRA
		is_subject = no
		NOT = { exists = ORL }
		PRO = {
			vassal_of = ROOT
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			religion_group = ROOT
		}
	}
	success = {
		NOT = { exists = PRO }
		all_target_province = {
			owned_by = ROOT
		}
	}
	abort = {
		OR = {
			is_subject = yes	
			NOT = { exists = PRO }
			PRO = { NOT = { vassal_of = ROOT } }
			PRO = { NOT = { religion_group = ROOT } }
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			dip = 4
		}
	}
	effect = {
		add_prestige = 5
		add_dip_power = 30
	}
}


annex_auvergne = {
	
	type = country

	category = DIP
	
	target_provinces = {
		owned_by = AUV
	}
	allow = {
		tag = FRA
		is_subject = no	
		NOT = { exists = ORL }
		AUV = {
			vassal_of = ROOT
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			religion_group = ROOT
		}
	}
	abort = {
		OR = {
			is_subject = yes
			NOT = { exists = AUV }
			AUV = { 
				OR = {
					NOT = { vassal_of = ROOT }
					NOT = { religion_group = ROOT }
				}
			}
		}
	}
	success = {
		NOT = { exists = AUV }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			dip = 4
		}
	}
	effect = {
		add_prestige = 5
		add_dip_power = 30
	}
}


annex_bourbonnais = {
	
	type = country
	
	category = DIP
	
	target_provinces = {
		owned_by = BOU
	}
	allow = {
		tag = FRA
		is_subject = no
		NOT = { exists = ORL }
		BOU = {
			vassal_of = ROOT
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			religion_group = ROOT
		}
	}
	abort = {
		OR = {
			is_subject = yes
			NOT = { exists = BOU }
			BOU = { NOT = { vassal_of = ROOT } }
			BOU = { NOT = { religion_group = ROOT } }
		}
	}
	success = {
		NOT = { exists = BOU }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			dip = 4
		}
	}
	effect = {
		add_prestige = 5
		add_dip_power = 30
	}
}


subjugate_brittany = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	allow = {
		tag = FRA
		is_subject = no
		NOT = { has_country_modifier = military_vassalization }
		mil = 3
		NOT = { exists = ORL }
		NOT = { exists = AUV }
		NOT = { exists = BOU }
		NOT = { exists = PRO }
		BRI = {
			is_subject = no
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject = yes
			NOT = { exists = BRI }
			BRI = { is_subject = yes }
		}
	}
	success = {
		BRI = { vassal_of = ROOT }
	}
	chance = {
		factor = 2000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = BRI value = 0 } }
		}
	}
	immediate = {
		add_casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = BRI
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = BRI
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = military_vassalization
			duration = 3650
		}
		hidden_effect = {
			remove_casus_belli = {
				type = cb_vassalize_mission
				target = BRI
			}
		}
	}
}


annex_savoy = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	target_provinces = {
		owned_by = SAV
	}
	allow = {
		tag = FRA
		NOT = { has_global_flag = hundred_year_war }
		NOT = { has_country_modifier = military_victory }
		is_subject = no	
		NOT = { exists = ORL }
		NOT = { exists = AUV }
		NOT = { exists = BOU }
		NOT = { exists = PRO }
		SAV = {
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			religion_group = ROOT
		}
	}
	abort = {
		OR = {
			is_subject = yes
			NOT = { exists = SAV }
			SAV = { NOT = { religion_group = ROOT } }
		}
	}
	success = {
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1500
		modifier = {
			factor = 2
			dip = 4
		}
		modifier = {
			factor = 2
			has_opinion = { who = SAV value = 100 }
		}
		modifier = {
			factor = 2
			has_opinion = { who = SAV value = 200 }
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_country_modifier = {
			name = "military_victory"
			duration = 3650
		}
		every_target_province = {
			limit = {
				NOT = { is_core = ROOT }
			}
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
			}
		}
	}
}


louvre_expansion = {
	
	type = country

	category = ADM
	
	allow = {
		tag = FRA
		owns = 183	# Paris
		183 = {
			can_build = fine_arts_academy
			NOT = { has_building = fine_arts_academy }
			NOT = { has_building = refinery }
			NOT = { has_building = wharf }
			NOT = { has_building = weapons }
			NOT = { has_building = textile }
			}
	}
	abort = {
		NOT = { owns = 183 }
	}
	success = {
		183 = { has_building = fine_arts_academy }
	}
	chance = {
		factor = 2000
		modifier = {
			factor = 2
			advisor = statesman
		}	
	}
	effect = {
		add_prestige = 5
		define_advisor = { type = artist skill = 2 discount = yes}
	}
}


annex_alsace = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	target_provinces = {
		owned_by = ALS
	}
	allow = {
		tag = FRA
		NOT = { has_global_flag = hundred_year_war }
		NOT = { has_country_modifier = military_victory }
		is_subject = no	
		mil = 4
		ALS = {
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			religion_group = ROOT
		}
	}
	abort = {
		OR = {
			is_subject = yes
			NOT = { exists = ALS }
			ALS = { NOT = { religion_group = ROOT } }
		}
	}
	success = {
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			dip = 4
		}
		modifier = {
			factor = 2
			has_opinion = { who = ALS value = 100 }
		}
		modifier = {
			factor = 2
			has_opinion = { who = ALS value = 200 }
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
			name = "military_victory"
			duration = 3650
		}
		every_target_province = {
			limit = {
				NOT = { is_core = ROOT }
			}
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
			}
		}
	}
}


france_discovers_north_america = {
	
	type = country

	category = DIP
	
	allow = {
		tag = FRA
		NOT = { has_global_flag = hundred_year_war }
		NOT = { has_country_modifier = colonial_enthusiasm }
		has_idea = quest_for_the_new_world
		NOT = { canada_region = { has_discovered = ROOT } }
		canada_region = { range = ROOT }
		num_of_ports = 1
	}
	abort = {
		OR = {
			NOT = { has_idea = quest_for_the_new_world }
			NOT = { num_of_ports = 1 }
		}
	}
	success = {
		canada_region = {
			has_discovered = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea = land_of_opportunity
		}
		modifier = {
			factor = 2
			has_idea = colonial_ventures
		}	
	}
	effect = {
		add_dip_power = 25
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 1875
		}
	}
}


french_colony_in_north_america = {
	
	type = country

	category = DIP
	
	allow = {
		tag = FRA
		NOT = { has_global_flag = hundred_year_war }
		NOT = { has_country_modifier = colonial_enthusiasm }
		canada_region = {
			has_discovered = ROOT
			is_empty = yes
			range = ROOT
		}
		NOT = { canada_region = { country_or_vassal_holds = ROOT } }
		num_of_colonists = 1
		num_of_ports = 1
	}
	abort = {
		OR = {
			NOT = { num_of_ports = 1 }
			AND = {
				NOT = { canada_region = { country_or_vassal_holds = ROOT } }
				NOT = { canada_region = { is_empty = yes } }
			}
		}
	}
	success = {
		canada_region = { country_or_vassal_holds = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea = land_of_opportunity
		}
		modifier = {
			factor = 2
			has_idea = colonial_ventures
		}
		modifier = {
			factor = 2
			num_of_colonists = 2
		}
		modifier = {
			factor = 2
			num_of_colonists = 3
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 3650
		}
	}
}


france_discovers_the_carribean = {
	
	type = country

	category = DIP
	
	allow = {
		tag = FRA
		NOT = { has_global_flag = hundred_year_war }
		has_idea = quest_for_the_new_world
		NOT = { has_country_modifier = colonial_enthusiasm }
		NOT = { carribeans_region = { has_discovered = ROOT } }
		carribeans_region = { range = ROOT }
		num_of_ports = 1
	}
	abort = {
		OR = {
			NOT = { has_idea = quest_for_the_new_world }
			NOT = { num_of_ports = 1 }
		}
	}
	success = {
		carribeans_region = {
			has_discovered = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea = land_of_opportunity
		}
		modifier = {
			factor = 2
			has_idea = colonial_ventures
		}	
	}
	effect = {
		add_dip_power = 25
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 730
		}
	}
}


french_colony_in_the_carribean = {
	
	type = country

	category = DIP

	allow = {
		tag = FRA
		NOT = { has_global_flag = hundred_year_war }
		NOT = { has_country_modifier = colonial_enthusiasm }
		carribeans_region = {
			has_discovered = ROOT
			is_empty = yes
			range = ROOT
		}
		NOT = { carribeans_region = { country_or_vassal_holds = ROOT } }
		num_of_colonists = 1
		num_of_ports = 1
	}
	abort = {
		OR = {
			NOT = { num_of_ports = 1 }
			AND = {
				NOT = { carribeans_region = { country_or_vassal_holds = ROOT } }
				NOT = { carribeans_region = { is_empty = yes } }
			}	
		}
	}
	success = {
		carribeans_region = { country_or_vassal_holds = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea = land_of_opportunity
		}
		modifier = {
			factor = 2
			has_idea = colonial_ventures
		}
		modifier = {
			factor = 2
			num_of_colonists = 2
		}
		modifier = {
			factor = 2
			num_of_colonists = 3
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 730
		}
	}
}


defend_france_against_england = {
	
	type = country

	category = MIL
	
	allow = {
		tag = FRA
		exists = ENG
		has_global_flag = hundred_year_war
		is_subject = no
		france_region = { country_or_vassal_holds = ENG }
		NOT = { has_country_flag = england_out_of_france }
	}
	abort = {
		is_subject = yes
	}
	success = {
		NOT = { war_with = ENG }
		NOT = { france_region = { country_or_vassal_holds = ENG } }
	}
	chance = {
		factor = 3000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			ENG = { NOT = { mil = 2 } }
		}
	}
	effect = {
		add_adm_power = 50
		add_army_tradition = 50
		set_country_flag = england_out_of_france
		add_country_modifier = {
			name = "english_out_of_france"
			duration = 3650
		}
	}
}


recover_calais = {

	type = country

	category = MIL
	ai_mission = yes
	
	allow = {
		tag = FRA
		exists = ENG
		is_subject = no
		NOT = { has_country_modifier = military_victory }
		mil = 4
		ENG = { owns_or_vassal_of = 63 }		# Calais
		NOT = { war_with = ENG }
		63 = {
			any_neighbor_province = {
				owned_by = ROOT
			}
		}
	}
	abort = {
		OR = {
			is_subject = yes
			AND = {
				ENG = { NOT = { owns_or_vassal_of = 63 } }
				NOT = { owns_or_vassal_of = 63 }
			}
		}
	}
	success = {
		NOT = { war_with = ENG }
		owns_or_vassal_of = 63
	}
	chance = {
		factor = 2000
	}
	immediate = {
		add_claim = 63
	}
	abort_effect = {
		remove_claim = 63
	}
	effect = {
		add_country_modifier = {
			name = "military_victory"
			duration = 3650
		}
		if = {
			limit = {
				63 = { NOT = { is_core = ROOT } }
			}
			63 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
				}
			}
		}
	}
}


recover_aquitaine = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	allow = {
		tag = FRA
		has_global_flag = hundred_year_war 
		is_subject = no	
		mil = 4
		NOT = { is_year = 1475 }
		NOT = { war_with = ENG }
		ENG = {
			owns_or_vassal_of = 2239	# Saintonge 
			owns_or_vassal_of = 176	# Bordeaux
		}
	}
	abort = {
		OR = {
			is_subject = yes
			AND = {
				ENG = {
					NOT = { owns_or_vassal_of = 2239 }	# Saintonge
					NOT = { owns_or_vassal_of = 176 }	# Gascogne
				}
				NOT = { owns_or_vassal_of = 2239 }
				NOT = { owns_or_vassal_of = 176 }
			}
		}
	}
	success = {
		NOT = { war_with = ENG }
		owns_or_vassal_of = 2239
		owns_or_vassal_of = 176
	}
	chance = {
		factor = 3000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = ENG value = -100 } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = ENG value = -200 } }
		}
	}
	immediate = {
		add_claim = 2239
		add_claim = 176
	}
	abort_effect = {
		remove_claim = 2239
		remove_claim = 176
	}
	effect = {
		add_prestige = 10
		if = {
			limit = {
				2239 = { NOT = { is_core = ROOT } }
			}
			2239 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
				}
			}
		}
		if = {
			limit = {
				176 = { NOT = { is_core = ROOT } }
			}
			176 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
				}
			}
		}
	}
}


conquer_milan = {
	
	type = country

	category = MIL
	ai_mission = yes
	target_provinces = {
		owned_by = MLO
	}
	allow = {
		tag = FRA
		NOT = { has_global_flag = hundred_year_war }
		is_subject = no
		mil = 4
		NOT = { has_country_modifier = italian_ambition_modifier }
		MLO = {
			is_neighbor_of = ROOT
			religion_group = ROOT
			NOT = { war_with = ROOT }
			NOT = { alliance_with = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject = yes	
			MLO = { NOT = { is_neighbor_of = ROOT } }
		}
	}
	success = {
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 2000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = MLO value = -100 } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = MLO value = -200 } }
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = italian_ambition_modifier
			duration = 3650
		}
		every_target_province = {
			limit = {
				NOT = { is_core = ROOT }
			}
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
			}
		}
	}
}


annex_genoa = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	target_provinces = {
		owned_by = GEN
	}
	allow = {
		tag = FRA
		NOT = { has_global_flag = hundred_year_war }
		is_subject = no	
		mil = 4
		NOT = { has_country_modifier = italian_ambition_modifier }
		GEN = {
			is_neighbor_of = ROOT
			religion_group = ROOT
			NOT = { war_with = ROOT }
			NOT = { alliance_with = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject = yes
			NOT = { exists = GEN }
			AND = {
				GEN = { NOT = { owns = 101 } }
				NOT = { owns = 101 }
			}
			GEN = {
				OR = {
					NOT = { is_neighbor_of = ROOT }
					NOT = { religion_group = ROOT }
				}
			}
		}
	}
	success = {
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1500
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = GEN value = -100 } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = GEN value = -200 } }
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = italian_ambition_modifier
			duration = 3650
		}
		every_target_province = {
			limit = {
				NOT = { is_core = ROOT }
			}
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
			}
		}
	}
}


establish_national_guard = {
	
	type = country

	category = ADM
	
	allow = {
		tag = FRA
		owns = 183	# Paris
		183 = {
			can_build = regimental_camp
			NOT = { has_building = regimental_camp }
		}
	}
	abort = {
		NOT = { owns = 183 }
	}
	success = {
		advisor = army_reformer
		183 = { has_building = regimental_camp }
	}
	chance = {
		factor = 2000
		modifier = {
			factor = 2
			mil = 5
		}	
	}
	effect = {
		add_army_tradition = 25
	}
}


france_the_united_states_relations = {
	
	type = country

	category = DIP
	
	allow = {
		tag = FRA
		exists = USA
		NOT = { has_country_modifier = foreign_contacts }
		NOT = { war_with = USA }
		NOT = { has_opinion = { who = USA value = 100 } }
		NOT = { FROM = { is_rival = ROOT } }
		NOT = { ROOT = { is_rival = FROM } }
	}
	abort = {
		OR = {
			NOT = { exists = USA }
			war_with = USA
		}
	}
	success = {
		USA = { has_opinion = { who = ROOT value = 150 } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = USA value = 0 } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = USA value = -100 } }
		}	
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
				name = "foreign_contacts"
				duration = 3650
			}
	}
}


subjugate_lorraine = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	allow = {
		tag = FRA
		NOT = { has_global_flag = hundred_year_war }
		is_subject = no	
		mil = 4
		NOT = { exists = ORL }
		NOT = { exists = AUV }
		NOT = { exists = BOU }
		NOT = { exists = PRO }
		NOT = { has_country_modifier = military_vassalization }
		LOR = {
			is_subject = no
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject = yes
			NOT = { exists = LOR }
			LOR = { is_subject = yes }
		}
	}
	success = {
		LOR = { vassal_of = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = LOR value = 0 } }
		}
	}
	immediate = {
		add_casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = LOR
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = LOR
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = military_vassalization
			duration = 1825
		}
		hidden_effect = {
			remove_casus_belli = {
				type = cb_vassalize_mission
				target = LOR
			}
		}
	}
}


annex_lorraine = {
	
	type = country

	category = DIP
	
	target_provinces = {
		owned_by = LOR
	}
	allow = {
		tag = FRA
		NOT = { has_global_flag = hundred_year_war }
		is_subject = no	
		NOT = { exists = ORL }
		NOT = { exists = AUV }
		NOT = { exists = BOU }
		NOT = { exists = PRO }		
		LOR = {
			vassal_of = ROOT
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			religion_group = ROOT
		}
	}
	abort = {
		OR = {
			is_subject = yes
			NOT = { exists = LOR }
			LOR = { NOT = { religion_group = ROOT } }
		}
	}
	success = {
		NOT = { exists = LOR }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			dip = 4
		}
		modifier = {
			factor = 2
			has_opinion = { who = LOR value = 100 }
		}
		modifier = {
			factor = 2
			has_opinion = { who = LOR value = 200 }
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 10
		every_target_province = {
			limit = {
				NOT = { is_core = ROOT }
			}
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
			}
		}
	}
}


defend_the_netherlands = {
	
	type = country

	category = MIL
	
	allow = {
		tag = FRA
		exists = SPA
		is_subject = no
		SPA = { is_neighbor_of = ROOT }
		low_countries_region = {
			owned_by = SPA
			any_neighbor_province = {
				owned_by = FRA
			}
		}
	}
	abort = {
		is_subject = yes
	}
	success = {
		NOT = { war_with = SPA }
		NOT = { low_countries_region = { owned_by = SPA } }
		low_countries_region = { owned_by = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			SPA = { NOT = { num_of_cities = ROOT } }
		}
		modifier = {
			factor = 2
			SPA = {
				NOT = { num_of_infantry = ROOT }
				NOT = { num_of_cavalry = ROOT }
			}
		}
	}
	effect = {
		add_army_tradition = 50
		every_owned_province = {
			limit = { OR = { region = belgii_region region = low_countries_region } }
			add_province_modifier = {
				name = "faster_integration"
				duration = 3650
			}
		}
	}
}
