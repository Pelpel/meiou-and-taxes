control_scania = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	allow = {
		tag = SWE
		is_subject = no
		mil = 4
		DEN = { owns = 6 }
		NOT = { alliance_with = DEN }
		NOT = { has_country_modifier = baltic_ambition }
	}
	abort = {
		OR = {
			AND = {
				DEN = { NOT = { owns = 6 } }
				NOT = { owns = 6 }
			}
			is_subject = yes
		}
	}
	success = {
		owns = 6
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 5
			is_year = 1658
		}
	}
	effect = {
		add_prestige = 5
		6 = {
			add_province_modifier = {
				name = "faster_integration"
				duration = 3650
			}
		}
		add_country_modifier = {
			name = "baltic_ambition"
			duration = 3650
		}
	}
}


control_halland = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	allow = {
		tag = SWE
		is_subject = no
		mil = 4
		DEN = { owns = 26 }
		NOT = { alliance_with = DEN }
	}
	abort = {
		OR = {
			AND = {
				DEN = { NOT = { owns = 26 } }
				NOT = { owns = 26 }
			}
			is_subject = yes
		}
	}
	success = {
		owns = 26
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 5
			is_year = 1645
		}
	}
	effect = {
		add_prestige = 5
		26 = {
			add_province_modifier = {
				name = "faster_integration"
				duration = 3650
			}
		}
	}
}


control_estonia = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	target_provinces = {
		area = estonia_ingria_area
		NOT = { owned_by = ROOT }
	}
	allow = {
		tag = SWE
		is_subject = no
		mil = 4
		NOT = { has_country_modifier = baltic_ambition }
	}
	abort = {
		is_subject = yes
	}
	success = {
		all_target_province = {
		    owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 5
			is_year = 1561
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
			name = "baltic_ambition"
			duration = 3650
		}
		every_target_province = {
			limit = {
				NOT = { is_core = ROOT }
			}
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
			}
		}
	}
}


control_latvia = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	target_provinces = {
		area = livonia_area
		NOT = { province_id = 273 }
		NOT = { province_id = 3765 }
		NOT = { owned_by = ROOT }
	}
	allow = {
		tag = SWE
		is_subject = no
		mil = 4
		owns = 36
		NOT = { has_country_modifier = baltic_ambition }
	}
	abort = {
		is_subject = yes
	}
	success = {
		all_target_province = {
		    owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
			name = "baltic_ambition"
			duration = 3650
		}
		every_target_province = {
			limit = {
				NOT = { is_core = ROOT }
			}
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
			}
		}
	}
}


swedish_pommerania = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	target_provinces = {
		OR = {
			province_id = 43
			province_id = 47
			province_id = 48
			province_id = 2845
			province_id = 2850
			province_id = 2843
			province_id = 2847
			province_id = 2844
			province_id = 2849
		}
		NOT = { owner = { alliance_with = ROOT } }
	}	
	allow = {
		tag = SWE
		is_subject = no
		mil = 4
		owns = 6
		OR = {
			43 = {
				owner = { NOT = { alliance_with = ROOT } }
				not = { owned_by = ROOT }
			}
			47 = {
				owner = { NOT = { alliance_with = ROOT } }
				not = { owned_by = ROOT }
			}
			2845 = {
				owner = { NOT = { alliance_with = ROOT } }
				not = { owned_by = ROOT }
			}
			2850 = {
				owner = { NOT = { alliance_with = ROOT } }
				not = { owned_by = ROOT }
			}
			2843 = {
				owner = { NOT = { alliance_with = ROOT } }
				not = { owned_by = ROOT }
			}
			2847 = {
				owner = { NOT = { alliance_with = ROOT } }
				not = { owned_by = ROOT }
			}
			2844 = {
				owner = { NOT = { alliance_with = ROOT } }
				not = { owned_by = ROOT }
			}
			2849 = {
				owner = { NOT = { alliance_with = ROOT } }
				not = { owned_by = ROOT }
			}
			48 = {
				owner = { NOT = { alliance_with = ROOT } }
				not = { owned_by = ROOT }
			}
		}
		NOT = { has_country_modifier = baltic_ambition }
	}
	abort = {
		is_subject = yes
	}
	success = {
	    all_target_province = {
		    owned_by = SWE
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 5
			is_year = 1649
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "baltic_ambition"
			duration = 3650
		}
		every_target_province = {
			limit = {
				NOT = { is_core = ROOT }
			}
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
			}
		}
	}
}


control_gotland = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	allow = {
		tag = SWE
		is_subject = no
		NOT = { owns = 25 }
		25 = {
			owner = { NOT = { alliance_with = ROOT } }
		}
		NOT = { has_country_modifier = baltic_ambition }
	}
	abort = {
		is_subject = yes
	}
	success = {
		owns = 25
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 5
			is_year = 1645
		}
		modifier = {
			factor = 2
			GOT = { owns = 25 }
		}
	}
	immediate = {
		add_claim = 25
	}
	abort_effect = {
		remove_claim = 25
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
			name = "baltic_ambition"
			duration = 3650
		}
		if = {
			limit = {
				25 = { NOT = { is_core = ROOT } }
			}
			25 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
				}
			}
		}
	}
}


vassalize_norway_swe = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	allow = {
		tag = SWE
		is_subject = no
		mil = 6
		exists = NOR
		NOR = {
			is_neighbor_of = ROOT
			NOT = { vassal_of = ROOT }
			NOT = { num_of_cities = ROOT }
			is_subject = no
			owns = 16		# Oslo, Akershus
		}
	}
	abort = {
		OR = {
			NOT = { exists = NOR }
			NOR = { is_subject = yes }
			is_subject = yes			
		}
	}
	success = {
		NOR = { vassal_of = SWE }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = NOR value = 0 } }
		}
	}
	immediate = {
		add_casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = NOR
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = NOR
		}
	}
	effect = {
		add_army_tradition = 20
		hidden_effect = {
			remove_casus_belli = {
				type = cb_vassalize_mission
				target = NOR
			}
		}
	}
}


annex_norway_swe = {
	
	type = country

	category = DIP
	
	allow = {
		tag = SWE
		is_subject = no
		exists = NOR
		NOR = {
			religion_group = ROOT
			vassal_of = ROOT
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			owns = 16		# Oslo, Akershus
		}
	}
	abort = {
		OR = {
			NOT = { exists = NOR }
			NOR = { NOT = { religion_group = ROOT } }
			is_subject = yes
		}
	}
	success = {
		NOT = { exists = NOR }
		owns = 16
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_opinion = { who = NOR value = 100 }
		}
		modifier = {
			factor = 2
			has_opinion = { who = NOR value = 200 }
		}
	}
	immediate = {
	}
	abort_effect = {
	}
	effect = {
		add_prestige = 20
		add_stability_1 = yes
	}
}


no_territory_to_denmark = {

	type = country

	category = MIL
	
	target_provinces = {
		region = swedish_region
	}
	allow = {
		tag = SWE
		is_subject = no
		mil = 4
		exists = DEN
		swedish_region = { owned_by = DEN }
		is_at_war = no
		stability = 3
		army_size = DEN
		DEN = { is_at_war = no }
	}
	abort = {
		OR = {
			is_subject = yes
			NOT = { exists = DEN }
			NOT = { swedish_region = { owned_by = DEN } }
		}
	}
	success = {
		NOT = { swedish_region = { owned_by = DEN } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}	
	}
	effect = {
		add_prestige = 10
		add_war_exhaustion = -5
	}
}


reconquer_finland = {

	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces = {
		region = finland_region
	}
	allow = {
		tag = SWE
		is_subject = no
		mil = 4
		OR = {
			exists = MOS
			exists = RUS
		}
		OR = {
			finland_region = { owned_by = MOS }
			finland_region = { owned_by = RUS }
		}
	}
	abort = {
		OR = {
			AND = {
				NOT = { finland_region = { owned_by = MOS } }
				NOT = { finland_region = { owned_by = RUS } }
			}
			is_subject = yes
		}
	}
	success = {
		NOT = { war_with = MOS }
		NOT = { war_with = RUS }
		NOT = { finland_region = { owned_by = MOS } }
		NOT = { finland_region = { owned_by = RUS } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}	
	}
	effect = {
		add_mil_power = 100
		add_stability_1 = yes
	}
}


swedish_colony_in_north_america = {
	
	type = country

	category = DIP
	
	allow = {
		tag = SWE
		is_year = 1600
		east_america_superregion = {
			has_discovered = ROOT
			is_empty = yes
		}
		NOT = {
			east_america_superregion = {
				country_or_vassal_holds = ROOT
			}
		}
		num_of_colonists = 1
	}
	abort = {
		NOT = { east_america_superregion = { is_empty = yes } }
		NOT = { east_america_superregion = { country_or_vassal_holds = ROOT } }
	}
	success = {
		east_america_superregion = { country_or_vassal_holds = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea = land_of_opportunity
		}
		modifier = {
			factor = 2
			has_idea = colonial_ventures
		}
	}
	effect = {
		add_prestige = 10
	}
}


fortify_the_eastern_border = {
	
	type = our_provinces

	category = ADM
	
	allow = {
		owner = { tag = SWE }
		can_build = fort_15th
		region = finland_region
		has_building = fort_14th
		NOT = { has_building = fort_15th }
		any_neighbor_province = {
			region = russia_region
			OR = {
				owned_by = MOS
				owned_by = RUS
			}
		}
	}
	abort = {
		NOT = {
			any_neighbor_province = {
				region = russia_region
				OR = {
					owned_by = MOS
					owned_by = RUS
				}
			}
		}
	}
	success = {
		has_building = fort_15th
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			OR = {
				AND = {
					owner = { NOT = { has_opinion = { who = MOS value = 0 } } }
					any_neighbor_province = {
						region = russia_region
						owned_by = MOS
					}
				}
				AND = {
					owner = { NOT = { has_opinion = { who = RUS value = 0 } } }
					any_neighbor_province = {
						region = russia_region
						owned_by = RUS
					}
				}
			}
		}
	}
	effect = {
		add_army_tradition = 10
	}
}


#colonize_lapland = {
#	
#	type = country
#
#	category = ADM
#	
#	allow = {
#		tag = SWE
#		num_of_colonists = 1
#		891 = { is_empty = yes base_tax = 1 }
#	}
#	abort = {
#		891 = { is_empty = no }
#		NOT = { owns = 891 }
#	}
#	success = {
#		owns = 891
#	}
#	chance = {
#		factor = 1000
#		modifier = {
#			factor = 2
#			num_of_colonists = 2
#		}
#		modifier = {
#			factor = 5
#			is_year = 1640
#		}
#	}
#	effect = {
#		add_prestige = 5
#	}
#}


# T�get �ver B�lt
control_jylland = {

	type = country

	category = MIL
	
	allow = {
		tag = SWE
		war_with = DEN
		15 = { owned_by = DEN }
		NOT = { controls = 15 }
		NOT = { danish_region = { owned_by = ROOT } }
		NOT = { has_country_modifier = military_victory  }
	}
	abort = {
		NOT = { war_with = DEN }
	}
	success = {
		controls = 15
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}	
	}
	effect = {
		add_army_tradition = 10
		add_country_modifier = {
			name = "military_victory"
			duration = 3650
		}
	}
}
