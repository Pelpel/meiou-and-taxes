dg_add_legitimacy_large = {
	if = { 
		limit = { piety = 0.60 }
		add_legitimacy = 10
		add_devotion = 20
		add_republican_tradition = 5
		}
	if = { 
		limit = { 
			piety = 0.20 
			NOT = { piety = 0.60 }
			}
		add_legitimacy = 8
		add_devotion = 16
		add_republican_tradition = 4
		}
	if = { 
		limit = { 
			piety = -0.20 
			NOT = { piety = 0.20 }
			}
		add_legitimacy = 6
		add_devotion = 12
		add_republican_tradition = 3
		}
	if = { 
		limit = { 
			piety = -0.60 
			NOT = { piety = -0.20 }
			}
		add_legitimacy = 4
		add_devotion = 8
		add_republican_tradition = 2
		}
	if = { 
		limit = { NOT = { piety = -0.60 } }
		add_legitimacy = 2
		add_devotion = 4
		add_republican_tradition = 1
		}
	}
dg_add_legitimacy = {
	if = { 
		limit = { piety = 0.60 }
		add_legitimacy = 5
		add_devotion = 10
		add_republican_tradition = 2.5
		}
	if = { 
		limit = { 
			piety = 0.20 
			NOT = { piety = 0.60 }
			}
		add_legitimacy = 4
		add_devotion = 8
		add_republican_tradition = 2
		}
	if = { 
		limit = { 
			piety = -0.20 
			NOT = { piety = 0.20 }
			}
		add_legitimacy = 3
		add_devotion = 6
		add_republican_tradition = 1.5
		}
	if = { 
		limit = { 
			piety = -0.60 
			NOT = { piety = -0.20 }
			}
		add_legitimacy = 2
		add_devotion = 4
		add_republican_tradition = 1
		}
	if = { 
		limit = { NOT = { piety = -0.60 } }
		add_legitimacy = 1
		add_devotion = 2
		add_republican_tradition = 0.5
		}
	}
dg_add_legitimacy_small = {
	if = { 
		limit = { piety = 0.60 }
		add_legitimacy = 2.5
		add_devotion = 5
		add_republican_tradition = 1.25
		}
	if = { 
		limit = { 
			piety = 0.20 
			NOT = { piety = 0.60 }
			}
		add_legitimacy = 2
		add_devotion = 4
		add_republican_tradition = 1
		}
	if = { 
		limit = { 
			piety = -0.20 
			NOT = { piety = 0.20 }
			}
		add_legitimacy = 1.5
		add_devotion = 3
		add_republican_tradition = 0.75
		}
	if = { 
		limit = { 
			piety = -0.60 
			NOT = { piety = -0.20 }
			}
		add_legitimacy = 1
		add_devotion = 2
		add_republican_tradition = 0.5
		}
	if = { 
		limit = { NOT = { piety = -0.60 } }
		add_legitimacy = 0.5
		add_devotion = 1
		add_republican_tradition = 0.25
		}
	}
dg_reduce_legitimacy_large = {
	if = { 
		limit = { piety = 0.60 }
		add_legitimacy = -10
		add_devotion = -20
		add_republican_tradition = -5
		}
	if = { 
		limit = { 
			piety = 0.20 
			NOT = { piety = 0.60 }
			}
		add_legitimacy = -8
		add_devotion = -16
		add_republican_tradition = -4
		}
	if = { 
		limit = { 
			piety = -0.20 
			NOT = { piety = 0.20 }
			}
		add_legitimacy = -6
		add_devotion = -12
		add_republican_tradition = -3
		}
	if = { 
		limit = { 
			piety = -0.60 
			NOT = { piety = -0.20 }
			}
		add_legitimacy = -4
		add_devotion = -8
		add_republican_tradition = -2
		}
	if = { 
		limit = { NOT = { piety = -0.60 } }
		add_legitimacy = -2
		add_devotion = -4
		add_republican_tradition = -1
		}
	}
dg_reduce_legitimacy = {
	if = { 
		limit = { piety = 0.60 }
		add_legitimacy = -5
		add_devotion = -10
		add_republican_tradition = -2.5
		}
	if = { 
		limit = { 
			piety = 0.20 
			NOT = { piety = 0.60 }
			}
		add_legitimacy = -4
		add_devotion = -8
		add_republican_tradition = -2
		}
	if = { 
		limit = { 
			piety = -0.20 
			NOT = { piety = 0.20 }
			}
		add_legitimacy = -3
		add_devotion = -6
		add_republican_tradition = -1.5
		}
	if = { 
		limit = { 
			piety = -0.60 
			NOT = { piety = -0.20 }
			}
		add_legitimacy = -2
		add_devotion = -4
		add_republican_tradition = -1
		}
	if = { 
		limit = { NOT = { piety = -0.60 } }
		add_legitimacy = -1
		add_devotion = -2
		add_republican_tradition = -0.5
		}
	}
dg_reduce_legitimacy_small = {
	if = { 
		limit = { piety = 0.60 }
		add_legitimacy = -2.5
		add_devotion = -5
		add_republican_tradition = -1.25
		}
	if = { 
		limit = { 
			piety = 0.20 
			NOT = { piety = 0.60 }
			}
		add_legitimacy = -2
		add_devotion = -4
		add_republican_tradition = -1
		}
	if = { 
		limit = { 
			piety = -0.20 
			NOT = { piety = 0.20 }
			}
		add_legitimacy = -1.5
		add_devotion = -3
		add_republican_tradition = -0.75
		}
	if = { 
		limit = { 
			piety = -0.60 
			NOT = { piety = -0.20 }
			}
		add_legitimacy = -1
		add_devotion = -2
		add_republican_tradition = -0.5
		}
	if = { 
		limit = { NOT = { piety = -0.60 } }
		add_legitimacy = -0.5
		add_devotion = -1
		add_republican_tradition = -0.25
		}
	}

dg_reduce_exhaustion = {
	if = { 
		limit = { piety = 0.60 }
		add_war_exhaustion = -5
		}
	if = { 
		limit = { 
			piety = 0.20 
			NOT = { piety = 0.60 }
			}
		add_war_exhaustion = -4
		}
	if = { 
		limit = { 
			piety = -0.20 
			NOT = { piety = 0.20 }
			}
		add_war_exhaustion = -3
		}
	if = { 
		limit = { 
			piety = -0.60 
			NOT = { piety = -0.20 }
			}
		add_war_exhaustion = -2
		}
	if = { 
		limit = { NOT = { piety = -0.60 } }
		add_war_exhaustion = -1
		}
	}
dg_add_prestige = {
	if = { 
		limit = { piety = 0.60 }
		add_prestige = 20
		}
	if = { 
		limit = { 
			piety = 0.20 
			NOT = { piety = 0.60 }
			}
		add_prestige = 16
		}
	if = { 
		limit = { 
			piety = -0.20 
			NOT = { piety = 0.20 }
			}
		add_prestige = 12
		}
	if = { 
		limit = { 
			piety = -0.60 
			NOT = { piety = -0.20 }
			}
		add_prestige = 8
		}
	if = { 
		limit = { NOT = { piety = -0.60 } }
		add_prestige = 4
		}
	}
dg_add_prestige_small = {
	if = { 
		limit = { piety = 0.60 }
		add_prestige = 5
		}
	if = { 
		limit = { 
			piety = 0.20 
			NOT = { piety = 0.60 }
			}
		add_prestige = 4
		}
	if = { 
		limit = { 
			piety = -0.20 
			NOT = { piety = 0.20 }
			}
		add_prestige = 3
		}
	if = { 
		limit = { 
			piety = -0.60 
			NOT = { piety = -0.20 }
			}
		add_prestige = 2
		}
	if = { 
		limit = { NOT = { piety = -0.60 } }
		add_prestige = 1
		}
	}
dg_add_prestige_large = {
	if = {
		limit = { piety = 0.60 }
		add_prestige = 50
		}
	if = { 
		limit = { 
			piety = 0.20 
			NOT = { piety = 0.60 }
			}
		add_prestige = 40
		}
	if = { 
		limit = { 
			piety = -0.20 
			NOT = { piety = 0.20 }
			}
		add_prestige = 30
		}
	if = { 
		limit = { 
			piety = -0.60 
			NOT = { piety = -0.20 }
			}
		add_prestige = 20
		}
	if = { 
		limit = { NOT = { piety = -0.60 } }
		add_prestige = 10
		}
	}	
dg_reduce_prestige = {
	if = { 
		limit = { piety = 0.60 }
		add_prestige = -20
		}
	if = { 
		limit = { 
			piety = 0.20 
			NOT = { piety = 0.60 }
			}
		add_prestige = -16
		}
	if = { 
		limit = { 
			piety = -0.20 
			NOT = { piety = 0.20 }
			}
		add_prestige = -12
		}
	if = { 
		limit = { 
			piety = -0.60 
			NOT = { piety = -0.20 }
			}
		add_prestige = -8
		}
	if = { 
		limit = { NOT = { piety = -0.60 } }
		add_prestige = -4
		}
	}
dg_reduce_prestige_small = {
	if = { 
		limit = { piety = 0.60 }
		add_prestige = -5
		}
	if = { 
		limit = { 
			piety = 0.20 
			NOT = { piety = 0.60 }
			}
		add_prestige = -4
		}
	if = { 
		limit = { 
			piety = -0.20 
			NOT = { piety = 0.20 }
			}
		add_prestige = -3
		}
	if = { 
		limit = { 
			piety = -0.60 
			NOT = { piety = -0.20 }
			}
		add_prestige = -2
		}
	if = { 
		limit = { NOT = { piety = -0.60 } }
		add_prestige = -1
		}
	}
dg_reduce_prestige_large = {
	if = {
		limit = { piety = 0.60 }
		add_prestige = -50
		}
	if = { 
		limit = { 
			piety = 0.20 
			NOT = { piety = 0.60 }
			}
		add_prestige = -40
		}
	if = { 
		limit = { 
			piety = -0.20 
			NOT = { piety = 0.20 }
			}
		add_prestige = -30
		}
	if = { 
		limit = { 
			piety = -0.60 
			NOT = { piety = -0.20 }
			}
		add_prestige = -20
		}
	if = { 
		limit = { NOT = { piety = -0.60 } }
		add_prestige = -10
		}
	}	
dg_add_intolerance_long = {
	if = {
		limit = { 
			NOT = { has_country_modifier = religious_tolerance } 
			NOT = { has_country_modifier = religious_intolerance } 
			}
		add_piety = 0.20
		}
	if = {
		limit = { has_country_modifier = religious_tolerance }
		add_piety = 0.40
		remove_country_modifier = religious_tolerance
		}
	if = { 
		limit = { piety = 0.60 }
		add_country_modifier = { name = religious_intolerance duration = 6000 }
		}
	if = { 
		limit = { 
			piety = 0.20 
			NOT = { piety = 0.60 }
			}
		add_country_modifier = { name = religious_intolerance duration = 4800 }
		}
	if = { 
		limit = { 
			piety = -0.20 
			NOT = { piety = 0.20 }
			}
		add_country_modifier = { name = religious_intolerance duration = 3600 }
		}
	if = { 
		limit = { 
			piety = -0.60 
			NOT = { piety = -0.20 }
			}
		add_country_modifier = { name = religious_intolerance duration = 2400 }
		}
	if = { 
		limit = { NOT = { piety = -0.60 } }
		add_war_exhaustion = -1
		add_country_modifier = { name = religious_intolerance duration = 1200 }
		}
	}

dg_add_intolerance = {
	if = {
		limit = { 
			NOT = { has_country_modifier = religious_tolerance } 
			NOT = { has_country_modifier = religious_intolerance } 
			}
		add_piety = 0.20
		}
	if = {
		limit = { has_country_modifier = religious_tolerance }
		add_piety = 0.40
		remove_country_modifier = religious_tolerance
		}
	if = { 
		limit = { piety = 0.60 }
		add_country_modifier = { name = religious_intolerance duration = 4000 }
		}
	if = { 
		limit = { 
			piety = 0.20 
			NOT = { piety = 0.60 }
			}
		add_country_modifier = { name = religious_intolerance duration = 3200 }
		}
	if = { 
		limit = { 
			piety = -0.20 
			NOT = { piety = 0.20 }
			}
		add_country_modifier = { name = religious_intolerance duration = 2400 }
		}
	if = { 
		limit = { 
			piety = -0.60 
			NOT = { piety = -0.20 }
			}
		add_country_modifier = { name = religious_intolerance duration = 1600 }
		}
	if = { 
		limit = { NOT = { piety = -0.60 } }
		add_country_modifier = { name = religious_intolerance duration = 800 }
		}
	}

dg_add_intolerance_short = {
	if = {
		limit = { 
			NOT = { has_country_modifier = religious_tolerance } 
			NOT = { has_country_modifier = religious_intolerance } 
			}
		add_piety = 0.20
		}
	if = {
		limit = { has_country_modifier = religious_tolerance }
		add_piety = 0.40
		remove_country_modifier = religious_tolerance
		}
	if = { 
		limit = { piety = 0.60 }
		add_country_modifier = { name = religious_intolerance duration = 2000 }
		}
	if = { 
		limit = { 
			piety = 0.20 
			NOT = { piety = 0.60 }
			}
		add_country_modifier = { name = religious_intolerance duration = 1600 }
		}
	if = { 
		limit = { 
			piety = -0.20 
			NOT = { piety = 0.20 }
			}
		add_country_modifier = { name = religious_intolerance duration = 1200 }
		}
	if = { 
		limit = { 
			piety = -0.60 
			NOT = { piety = -0.20 }
			}
		add_country_modifier = { name = religious_intolerance duration = 800 }
		}
	if = { 
		limit = { NOT = { piety = -0.60 } }
		add_country_modifier = { name = religious_intolerance duration = 400 }
		}
	}

dg_add_tolerance = {
	if = {
		limit = { 
			NOT = { has_country_modifier = religious_tolerance } 
			NOT = { has_country_modifier = religious_intolerance } 
			}
		add_piety = -0.20
		}
	if = {
		limit = { has_country_modifier = religious_intolerance }
		add_piety = -0.40
		remove_country_modifier = religious_intolerance
		}
	if = { 
		limit = { piety = 0.60 }
		add_country_modifier = { name = religious_tolerance duration = 4000 }
		}
	if = { 
		limit = { 
			piety = 0.20 
			NOT = { piety = 0.60 }
			}
		add_country_modifier = { name = religious_tolerance duration = 3200 }
		}
	if = { 
		limit = { 
			piety = -0.20 
			NOT = { piety = 0.20 }
			}
		add_country_modifier = { name = religious_tolerance duration = 2400 }
		}
	if = { 
		limit = { 
			piety = -0.60 
			NOT = { piety = -0.20 }
			}
		add_country_modifier = { name = religious_tolerance duration = 1600 }
		}
	if = { 
		limit = { NOT = { piety = -0.60 } }
		add_country_modifier = { name = religious_tolerance duration = 800 }
		}
	}
dg_add_controversy = {
	if = { 
		limit = { piety = 0.60 }
		add_country_modifier = { name = religious_controversy duration = 2000 }
		}
	if = { 
		limit = { 
			piety = 0.20 
			NOT = { piety = 0.60 }
			}
		add_country_modifier = { name = religious_controversy duration = 1600 }
		}
	if = { 
		limit = { 
			piety = -0.20 
			NOT = { piety = 0.20 }
			}
		add_country_modifier = { name = religious_controversy duration = 1200 }
		}
	if = { 
		limit = { 
			piety = -0.60 
			NOT = { piety = -0.20 }
			}
		add_country_modifier = { name = religious_controversy duration = 800 }
		}
	if = { 
		limit = { NOT = { piety = -0.60 } }
		add_country_modifier = { name = religious_controversy duration = 400 }
		}
	}
dg_religious_scandal_mild = {
	if = { 
		limit = { piety = 0.20 }
		add_country_modifier = { name = religious_scandal_country_strong duration = 400 }
		}
	if = { 
		limit = { piety = -0.20 NOT = { piety = 0.20 } }
		add_country_modifier = { name = religious_scandal_country duration = 400 }
		}
	if = { 
		limit = { NOT = { piety = -0.20 } }
		add_country_modifier = { name = religious_scandal_country_weak duration = 400 }
		}
	}
	