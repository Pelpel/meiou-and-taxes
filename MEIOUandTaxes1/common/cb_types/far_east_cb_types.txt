# Chinese civil war
cb_chinese_civil_war = {
	ai_peace_desire = -95

	prerequisites = {
		OR = {
			AND = {
				culture_group = chinese_group
				NOT = { primary_culture = chinese_colonial }
			}
			has_country_flag = barbarian_claimant_china
		}
		
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			capital_scope = { chinese_region_trigger = yes }
			any_owned_province = {
				chinese_region_trigger = yes
			}
		}
		is_neighbor_of = FROM
	}
	
	war_goal = unify_china
}

cb_unite_jurchens = {
	ai_peace_desire = -50

	prerequisites = {
		adm_tech = 25
		primary_culture = jurchen
		
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			capital_scope = { jurchen_region_trigger = yes }
			any_owned_province = {
				jurchen_region_trigger = yes
			}
		}
		is_neighbor_of = FROM
	}
	
	war_goal = unify_jurchens
}