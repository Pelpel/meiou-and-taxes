#################
# Religious CBs #
#################

cb_holy_war_cleric = { 

	is_triggered_only = yes

	war_goal = take_border	

}

cb_holy_war_defensive = {

	valid_for_subject = no

	prerequisites = {
		OR = { 
			cb_on_religious_enemies = yes
			patriarch_authority = 0.75
			}
		OR = { 
			piety = -0.60
			AND = { 
				cb_on_religious_enemies = yes
				patriarch_authority = 0.75
				}
			}
		NOT = { religion_group = FROM }
		FROM = { any_owned_province = { religion = ROOT } }
		OR = { 
			NOT = { border_distance = { who = FROM distance = 80 } }
			AND = { 
				NOT = { border_distance = { who = FROM distance = 160 } }
				piety = -0.20
				}
			AND = { 
				NOT = { border_distance = { who = FROM distance = 240 } }
				piety = 0.20
				}
			AND = { 
				NOT = { border_distance = { who = FROM distance = 320 } }
				piety = 0.60
				}
			AND = { 
				NOT = { border_distance = { who = FROM distance = 400 } }
				piety = 1.00
				}
			}
		}
	war_goal = take_province_religion
}

cb_holy_war_offensive = {

	valid_for_subject = no

	prerequisites = {
		OR = { 
			cb_on_religious_enemies = yes
			patriarch_authority = 0.75
			AND = {
				owns_rome_trigger = yes
				has_global_flag = orthodox_pentarchy_restored
				OR = { religion = orthodox religion = coptic }
				FROM = { religion = catholic }
				}
			}
		OR = { 
			piety = -0.20
			AND = { 
				cb_on_religious_enemies = yes
				patriarch_authority = 0.75
				piety = -0.60
				}
			}
		OR = { 
			AND = {
				NOT = { religion = catholic } #Covered by crusades
				NOT = { religion = sunni } #Covered by jihads
				NOT = { religion = shiite } #Covered by jihads
				}
			cb_on_religious_enemies = yes
			}
		NOT = { religion_group = FROM }
		OR = { 
			is_neighbor_of = FROM
			NOT = { border_distance = { who = FROM distance = 80 } }
			}
		}
	war_goal = superiority_crusade
}

cb_holy_war_heretic_defensive = {

	valid_for_subject = no

	prerequisites = {
		OR = { 
			cb_on_religious_enemies = yes
			patriarch_authority = 0.75
			}
		religion_group = FROM
		NOT = { religion = FROM }
		OR = { 
			piety = 0.20
			AND = { 
				cb_on_religious_enemies = yes
				patriarch_authority = 0.75
				piety = -0.20
				}
			}
		FROM = { any_owned_province = { religion = ROOT } }
		OR = { 
			is_neighbor_of = FROM
			NOT = { border_distance = { who = FROM distance = 80 } }
			AND = { 
				NOT = { border_distance = { who = FROM distance = 160 } }
				piety = 0.60
				}
			AND = { 
				NOT = { border_distance = { who = FROM distance = 240 } }
				piety = 1.00
				}
			}
		}
	war_goal = take_province_religion
}

cb_holy_war_heretic_offensive = {

	valid_for_subject = no

	prerequisites = {
		OR = { 
			cb_on_religious_enemies = yes
			patriarch_authority = 0.75
			}
		OR = { 
			piety = 0.60
			AND = { 
				cb_on_religious_enemies = yes
				patriarch_authority = 0.75
				piety = 0.20
				}
			}
		religion_group = FROM
		NOT = { religion = FROM }
		OR = { 
			is_neighbor_of = FROM
			NOT = { border_distance = { who = FROM distance = 80 } }
			}
		}
	war_goal = superiority_heretic
}

cb_holy_war_vengeance = { 
	
	valid_for_subject = no

	is_triggered_only = yes

	war_goal = superiority_crusade
	
}

cb_fidei_defensor = {
	
	valid_for_subject = no

	prerequisites = {
		is_defender_of_faith = yes
		piety = -0.20
		NOT = {	religion = FROM }
		any_known_country = { 
			religion = ROOT
			war_with = FROM
			}
		OR = { 
			is_neighbor_of = FROM
			NOT = { border_distance = { who = FROM distance = 200 } }
			AND = { 
				piety = 0.20
				NOT = { border_distance = { who = FROM distance = 400 } }
				}
			AND = { 
				piety = 0.60
				NOT = { border_distance = { who = FROM distance = 600 } }
				}
			AND = { 
				OR = { 
					piety = 1.00
					cb_on_religious_enemies = yes
					}
				NOT = { border_distance = { who = FROM distance = 800 } }
				}
			}
		}
		
	attacker_disabled_po = {
		po_demand_provinces
		po_annex
		po_form_personal_union
		po_change_government
		po_change_religion
		po_become_vassal
		po_become_protectorate
	}
	
	war_goal = defend_country_faith
}

cb_holy_war_heresy = {

	valid_for_subject = no

	prerequisites = {
		is_neighbor_of = FROM
		cb_on_religious_enemies = yes
		religion_group = from
		NOT = { religion = from }
		FROM = { any_owned_province = { religion = ROOT } }
		}
	
	allowed_provinces = {
		always = yes
		}

	war_goal = superiority_heretic
}

cb_schism = {

	valid_for_subject = no

	prerequisites = {
		religion = catholic
		exists = AVI
		exists = PAP
		PAP = { government = papal_government }
		AVI = { government = papal_government }
		OR = { 
			government = theocracy
			is_defender_of_faith = yes
			is_papal_controller = yes
			piety = 0.20
			has_country_flag = sponsored_antipope
			}
		OR = { 
			AND = { 
				has_country_flag = support_antipope
				FROM = { tag = PAP }
				}
			AND = { 
				has_country_flag = support_roman_pope
				FROM = { tag = AVI }
				}
			}
		}
	attacker_disabled_po = {
		po_demand_provinces
		po_annex
		po_form_personal_union
		po_change_government
		po_change_religion
		po_become_protectorate
		po_release_vassals
		po_release_annexed
		po_gold
		po_annul_treaties
	}
	war_goal = force_pope_to_resign
}

#################
# Christian CBs #
#################

cb_catholic_crusade = { 
	
	valid_for_subject = no

	prerequisites = {
		religion = catholic
		NOT = { religion = FROM } 
		OR = {
			FROM = { is_crusade_target = yes }
			FROM = { has_country_flag = crusade_target } #DEI GRATIA
			}
		NOT = { has_country_flag = excommunicated }
		PAP = { check_variable = { which = papal_authority value = 200 } }
		piety = -0.20
		OR = { 
			is_papal_controller = yes
			is_neighbor_of = FROM
			AND = { 
				NOT = { border_distance = { who = FROM distance = 100 } }
				PAP = { check_variable = { which = papal_authority value = 200 } }
				OR = { 
					piety = 0.20
					PAP = { check_variable = { which = papal_authority value = 300 } }
					}
				}
			AND = { 
				NOT = { border_distance = { who = FROM distance = 200 } }
				PAP = { check_variable = { which = papal_authority value = 300 } }
				OR = { 
					piety = 0.60
					government = theocracy
					PAP = { check_variable = { which = papal_authority value = 400 } }
					}
				}
			AND = { 
				NOT = { border_distance = { who = FROM distance = 300 } }
				PAP = { check_variable = { which = papal_authority value = 400 } }
				OR = { 
					piety = 1.00
					PAP = { check_variable = { which = papal_authority value = 500 } }
					}
				}
			}
		}
	war_goal = superiority_crusade
}

cb_excommunication = {
	
	valid_for_subject = no

	prerequisites = {
		FROM = {
			has_country_flag = excommunicated
			religion = catholic
			}
		religion = catholic
		piety = -0.20
		PAP = { check_variable = { which = papal_authority value = 300 } }
		NOT = { has_country_flag = excommunicated }
		FROM = {
			any_owned_province = {
				OR = {
					is_core = ROOT
					is_claim = ROOT
					any_neighbor_province = { owned_by = ROOT }
				}			
			}
		}
	}

	war_goal = take_province_excommunication
}

cb_religious_league_minor = { 
	league = yes
	ai_peace_desire = -30
	
	valid_for_subject = no

	prerequisites = {
		has_dlc = "Art of War"
		is_in_league_war = no
		is_league_leader = yes
		FROM = { 
			is_emperor = yes 
			is_in_league_war = no
			}
		is_league_enemy = FROM
		is_revolution_target = no
	}
	attacker_disabled_po = {
		po_demand_provinces
		po_annex
		po_change_government
		po_release_annexed
	}
	
	war_goal = superiority_religious_league_minor
}

##############
# Muslim CBs #
##############

cb_jihad_cleric = { 

	valid_for_subject = no

	is_triggered_only = yes

	war_goal = take_border	

}

cb_jihad_caliph = {
	
	valid_for_subject = no

	prerequisites = {
		has_country_flag = caliph
		check_variable = { which = caliph_authority value = 26 }
		FROM = { NOT = { religion = ROOT } }
		OR = { 
			is_neighbor_of = FROM
			NOT = { border_distance = { who = FROM distance = 100 } }
			}
		is_at_war = no
		religion_group = muslim
		OR = { 
			religion_group = muslim
			FROM = { any_owned_province = { religion = ROOT } }
			check_variable = { which = caliph_authority value = 51 }
			}
		FROM = { NOT = { has_country_flag = caliph } } #Handled with cb_rival_caliph
		}
	war_goal = superiority_crusade
}

cb_rival_caliph = {
	
	valid_for_subject = no

	prerequisites = {
		has_country_flag = caliph
		religion_group = muslim
		FROM = { has_country_flag = caliph religion_group = muslim }
		OR = { 
			is_rival = FROM
			FROM = { NOT = { religion = ROOT } }
			FROM = { culture_group = ROOT }
			is_neighbor_of = FROM
			NOT = { capital_distance = { who = FROM distance = 500 } }
			check_variable = { which = caliph_authority value = 26 }
			}
		}
	attacker_disabled_po = {
		po_change_government
		po_change_religion
		po_become_protectorate
		po_release_vassals
		po_release_annexed
		po_annul_treaties
	}
	war_goal = humiliate
}

cb_claim_caliph = {
	
	valid_for_subject = no

	prerequisites = {
		religion_group = muslim
		is_neighbor_of = FROM
		FROM = { 
			has_country_flag = caliph 
			OR = { 
				NOT = { army_size = ROOT }
				NOT = { num_of_cities = ROOT }
				}
			religion_group = muslim 
			}
		NOT = { has_country_flag = caliph }
		piety = -0.20
		}
	war_goal = take_caliph
}

#############
# Pagan CBs #
#############

cb_flower_war = {

	valid_for_subject = no

	prerequisites = {
		religion = nahuatl
		is_neighbor_of = FROM
		FROM = {
			OR = {
				num_of_cities = ROOT #More powerful than you
				army_size = ROOT #More powerful than you
				is_rival = ROOT #Your rival
				is_enemy = ROOT #Their rival
				NOT = { any_neighbor_country = { NOT = { tag = ROOT } } } #Surrounded by you
				}
			NOT = { truce_with = ROOT }
			}
		}
	war_goal = take_prisoners #special - should give high prestige from battles, high cost to annex provinces/countries.

}

cb_lost_icon = { 

	valid_for_subject = no

	is_triggered_only = yes

	war_goal = take_prisoners

}

#####################
# Succession Crises #
#####################

cb_foreign_pretender = { 

	is_triggered_only = yes
	
	attacker_disabled_po = {
		po_demand_provinces
		po_annex
		po_form_personal_union
		po_change_government
		po_change_religion
	}

	war_goal = take_capital_vassalize

}

cb_reunion = {

	is_triggered_only = yes
	
	attacker_disabled_po = {
		po_demand_provinces
		po_change_government
		po_change_religion
	}

	war_goal = reunify_country
}

#############
# Other CBs #
#############

cb_annulment = {

	is_triggered_only = yes

	attacker_disabled_po = {
        po_form_personal_union
    }
	
	war_goal = take_capital_throne_annulment

}

cb_religious_independence = {

	is_triggered_only = yes

	independence = yes
	
	war_goal = defend_capital_religious_independence

}