# Annexation Actions
annexationaction = {

	# French Apanage System
	condition = {
		tooltip = DA_APANAGE_VASSAL
		potential = {
			FROM = {
				vassal_of = ROOT
				has_country_flag = french_apanage
			}
		}
		allow = {
			FROM = {
				vassal_of = ROOT
				overlord = { has_country_flag = edit_de_moulins }
			}
		}
	}
	# Burgundy has its autonomy garanteed
	condition = {
		tooltip = DA_AUTONOMY_GUARANTEED
		potential = {
			FROM = {
				vassal_of = ROOT
				has_country_flag = autonomy_garanteed
			}
		}
		allow = {
			FROM = {
				vassal_of = ROOT
				NOT = { has_country_flag = autonomy_garanteed }
			}
		}
	}
	# Croatia has its autonomy guaranteed
	condition = {
		tooltip = DA_PACTA_CONVENTA_V
		potential = {
			FROM = {
				vassal_of = ROOT
				has_country_flag = pacta_conventa
			}
		}
		allow = {
			FROM = {
				vassal_of = ROOT
				NOT = { has_country_flag = pacta_conventa }
			}
		}
	}
	# Turkish Vassals have their autonomy guaranteed
	condition = {
		tooltip = DA_TURKISH_VASSALS_V
		potential = {
			FROM = {
				vassal_of = ROOT
				has_country_flag = turkish_vassal
			}
		}
		allow = {
			FROM = {
				vassal_of = ROOT
				NOT = { has_country_flag = turkish_vassal }
			}
		}
	}
	# Japanese Shogunate
	condition = { 
		tooltip = DA_IMPERIAL_JAPAN
		potential = {
			culture_group = japanese
			government_rank = 6
			FROM = {
				vassal_of = ROOT
			}
		}
		allow = {
			has_country_flag = shogunate_ended
			NOT = {
				OR = {
				# Shogunate
					AND = {
						government = early_medieval_japanese_gov
						government_rank = 5 
						NOT = { government_rank = 6}
						num_of_owned_provinces_with = {
							value = 15
							region = japan_region
						}
					}
					AND = {
						government = late_medieval_japanese_gov
						government_rank = 5 
						NOT = { government_rank = 6}
						num_of_owned_provinces_with = {
							value = 29
							region = japan_region
						}
				
					}
					AND = {
						government = early_modern_japanese_gov
						government_rank = 5 
						NOT = { government_rank = 6}
						num_of_owned_provinces_with = {
							value = 43
							region = japan_region
						}
					}
				# Daimyos
					AND = {
						government = early_medieval_japanese_gov
						NOT = { government_rank = 5}
						num_of_owned_provinces_with = {
							value = 11
							region = japan_region
						}
					}
					AND = { 
						government = late_medieval_japanese_gov
						NOT = { government_rank = 5}
						num_of_owned_provinces_with = {
							value = 21
							region = japan_region
						}
					}
					AND = {
						government = early_modern_japanese_gov
						NOT = { government_rank = 5}
						num_of_owned_provinces_with = {
							value = 31
							region = japan_region
						}
					}
				}
			}
			FROM = {
				vassal_of = ROOT
				check_variable = { which = Integration_Factor value = 100 }
			}
		}
	}
	condition = { 
		tooltip = DA_TAIKUN_JAPAN
		potential = {
			culture_group = japanese
			government_rank = 5
			NOT = { government_rank = 6 }
			OR = {
				government = early_medieval_japanese_gov 
				government = late_medieval_japanese_gov 
				government = early_modern_japanese_gov 
			}
			FROM = {
				vassal_of = ROOT
			}
		}
		allow = {
			#government_rank = 6
			has_country_flag = japan_united
			NOT = { has_country_flag = shogunate_ended }
			NOT = {
				OR = {
				# Shogunate
					AND = {
						government = early_medieval_japanese_gov
						government_rank = 5 
						NOT = { government_rank = 6}
						num_of_owned_provinces_with = {
							value = 15
							region = japan_region
						}
					}
					AND = {
						government = late_medieval_japanese_gov
						government_rank = 5 
						NOT = { government_rank = 6}
						num_of_owned_provinces_with = {
							value = 29
							region = japan_region
						}
				
					}
					AND = {
						government = early_modern_japanese_gov
						government_rank = 5 
						NOT = { government_rank = 6}
						num_of_owned_provinces_with = {
							value = 43
							region = japan_region
						}
					}
				# Daimyos
					AND = {
						government = early_medieval_japanese_gov
						NOT = { government_rank = 5}
						num_of_owned_provinces_with = {
							value = 11
							region = japan_region
						}
					}
					AND = { 
						government = late_medieval_japanese_gov
						NOT = { government_rank = 5}
						num_of_owned_provinces_with = {
							value = 21
							region = japan_region
						}
					}
					AND = {
						government = early_modern_japanese_gov
						NOT = { government_rank = 5}
						num_of_owned_provinces_with = {
							value = 31
							region = japan_region
						}
					}
				}
			}
			FROM = {
				vassal_of = ROOT
				check_variable = { which = Integration_Factor value = 100 }
			}
		}
	}
	condition = { 
		tooltip = DA_DAIMYO_ANNEXATION
		potential = {
			culture_group = japanese
			NOT = { government_rank = 5 }
			OR = {
				government = early_medieval_japanese_gov 
				government = late_medieval_japanese_gov 
				government = early_modern_japanese_gov 
			}
			FROM = {
				vassal_of = ROOT
			}
		}
		allow = {
			always = no
		}
	}
	# Generic Annexation
	condition = { 
		tooltip = DA_NOT_ASSIMILATED_V
		potential = {
			FROM = {
				vassal_of = ROOT
				NOT = { check_variable = { which = Integration_Factor value = 100 } }
			}
		}
		allow = {
			FROM = {
				vassal_of = ROOT
				check_variable = { which = Integration_Factor value = 100 }
			}
		}
	}
	# Minimum relation with vassal
	condition = { 
		tooltip = DA_ASSIMILATED_V
		potential = {
			FROM = {
				vassal_of = ROOT
			}
		}
		allow = {
			FROM = {
				vassal_of = ROOT
				NOT = { has_country_flag = french_apanage }
				NOT = { has_country_flag = autonomy_garanteed }
				NOT = { has_country_flag = pacta_conventa }
				check_variable = { which = Integration_Factor value = 100 }
			}
		}
	}
}
