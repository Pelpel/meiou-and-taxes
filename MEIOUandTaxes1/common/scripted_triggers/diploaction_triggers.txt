indian_state_trigger = {
	capital_scope = {
		province_group = india_charter
	}
	OR = {
		government = indian_monarchy
		government = rajput_monarchy
		government = tribal_monarchy
		government = maratha_confederacy
	}
}

turkish_emperor_trigger = {
	technology_group = turkishtech
	government_rank = 5
}

inferior_government_trigger = {
	NOT = { government = altaic_monarchy }
	NOT = { government = altaic_republic }
	NOT = { government = steppe_horde }
	NOT = { government = tribal_republic }
	NOT = { government = tribal_monarchy }
}

# culture triggers crash the game
#persian_emperor_trigger = {
#	culture_group = persian_group
#	government_rank = 6
#}

african_state_trigger = {
	NOT = { technology_group = western }
	NOT = { technology_group = muslim }
	NOT = { technology_group = turkishtech }
	capital_scope = {
		OR = {
			region = west_africa_region
			region = kongo_region
			region = middle_africa_region
			region = horn_of_africa_region
			region = east_africa_region
			region = monomotapa_region
			region = madagascar_region
			region = sudan_region
			region = ethiopia_region
			region = central_africa_region
			region = mali_region
		}
	}
}

# culture triggers crash the game
#russian_principality_trigger = {
#	culture_group = east_slavic
#	technology_group = eastern
#	NOT = { has_country_flag = relocated_capital_st_petersburg }
#}

mandala_system_state_trigger = {
	NOT = { technology_group = western }
	NOT = { technology_group = turkishtech }
	capital_scope = {
		OR = {
			region = sumatra_region
			region = java_region
			region = borneo_region
			region = indonesia_region
			region = philippines_region
			region = burma_region
			region = vietnam_region
			region = nam_tien_region
			region = lan_xang_region
			region = angkor_region
			region = siam_region
			region = malaya_region
		}		
	}
}

daimyo_trigger = {
	NOT = { technology_group = western }
	capital_scope = {
		region = japan_region		
	}
}

amerind_trigger = {
	OR = {
		technology_group = south_american
		technology_group = mesoamerican
	}
}

# culture triggers crash the game
#same_culture_or_neighbor_or_subject_neigbor_trigger = {
#	OR = {
#		culture_group = ROOT
#		is_neighbor_of = ROOT
#		any_country = {
#			is_neighbor_of = FROM
#			is_subject_of = ROOT
#		}
#		any_country = {
#			is_neighbor_of = ROOT
#			is_subject_of = FROM
#		}
#	}
#}

neighbor_or_subject_neigbor_trigger = {
	OR = {
		is_neighbor_of = ROOT
		any_country = {
			is_neighbor_of = FROM
			is_subject_of = ROOT
		}
		any_country = {
			is_neighbor_of = ROOT
			is_subject_of = FROM
		}
	}
}