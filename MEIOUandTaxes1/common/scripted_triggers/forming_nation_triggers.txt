forming_DAN_trigger = {
	NOT = { tag = NSE }
}

forming_ENG_trigger = {
	NOT = { tag = UEF }
	NOT = { tag = NSE }
}

forming_FRA_trigger = {
	NOT = { tag = UEF }
	NOT = { tag = CEL }
}

forming_IRE_trigger = {
	NOT = { tag = CEL }
}

forming_TUR_trigger = {
	NOT = { tag = TUR }
	NOT = { tag = TUY }
	OR = {
		government = monarchy
		government = republic
	}
}

forming_SAX_trigger = {
	NOT = { exists = ASE }
}

forming_BRI_trigger = {
	NOT = { tag = CEL }
}

forming_GBR_trigger = {
	NOT = { tag = UEF }
	NOT = { tag = NSE }
	NOT = { tag = CEL }
}

forming_BYZ_trigger = {
	NOT = { tag = TUY }
	NOT = { tag = LAT }
}

forming_KAL_trigger = {
	NOT = { tag = UEF }
	NOT = { tag = NSE }
}
