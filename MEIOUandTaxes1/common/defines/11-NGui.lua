
NDefines.NGui.DELAY_ACCEPT_EVENT = 0.1				-- vanilla = 0.5	Seconds. Disables event option buttons for the duration if > 0
NDefines.NGui.PROVINCE_FOCUS_ZOOM_HEIGHT = 0.1		-- vanilla = 0.25
NDefines.NGui.MACRO_BUILD_FOCUS_ZOOM_HEIGHT = 0.1	-- vanilla = 0.149
NDefines.NGui.MACRO_BUILD_BIG_SMALL_HEIGHT = 0.1	-- vanilla = 0.15
