#Lancers (50)

type = cavalry
unit_type = indian
maneuver = 2

offensive_morale = 8
defensive_morale = 6
offensive_fire = 1
defensive_fire = 3
offensive_shock = 8
defensive_shock = 5
