#54 - Clipper (Kosmopoliet, 1854)
type = transport

hull_size = 26 #800 tons burthen /20
base_cannons = 9 #Chasseur had 16; later clippers appear to have had none
sail_speed = 14 #top speed 14-17 knots

sailors = 58

sprite_level = 5

trigger = { 
	is_colonial_nation = no
	}
