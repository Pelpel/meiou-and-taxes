#7 - Fuchuan Warship
type = light_ship

hull_size = 19 #About 60% of a cog
base_cannons = 21 #Two heavy and one light per side
sail_speed = 6 #4-7 knots
trade_power = 2

sailors = 50

sprite_level = 1

trigger = { 
	OR = { 
		technology_group = chinese
		technology_group = austranesian
		}
	NOT = { culture_group = korean_group }
	}
