#35 - Early Frigate - Red Seal Ship
type = light_ship

hull_size = 35 #359 tons burthen (/15)
base_cannons = 42
sail_speed = 8 #3 knots rowing, 14 knots full sail
trade_power = 6

sailors = 106

sprite_level = 4

trigger = { 
	OR = {
		technology_group = chinese
		technology_group = austranesian
		}
	NOT = { tag = FRA }
	}
