#36 - Xebec
type = galley

hull_size = 18
base_cannons = 15 
sail_speed = 8

sailors = 240

sprite_level = 3

trigger = {
	NOT = { technology_group = western }
	NOT = { technology_group = eastern }
	NOT = { technology_group = indian }
	NOT = { technology_group = hawaii_tech }
	}
	