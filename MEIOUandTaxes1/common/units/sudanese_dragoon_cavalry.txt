#34 - Dragoons

type = cavalry
unit_type = soudantech
maneuver = 2

offensive_morale = 4
defensive_morale = 6
offensive_fire = 5
defensive_fire = 1
offensive_shock = 1
defensive_shock = 6

trigger = {
	any_owned_province = { #Tsetse flies and jungle
		NOT = { region = central_africa_region }
		NOT = { region = kongo_region }
		NOT = { region = south_africa_region }
		NOT = { region = east_africa_region }
		}
	}
