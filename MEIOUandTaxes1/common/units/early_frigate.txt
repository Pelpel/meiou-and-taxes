#Early Frigate
type = light_ship

hull_size = 30
base_cannons = 37
sail_speed = 10
trade_power = 5

sailors = 84

sprite_level = 3