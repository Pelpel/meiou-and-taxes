#50 - Lancers

type = cavalry
unit_type = soudantech
maneuver = 2

offensive_morale = 8
defensive_morale = 6
offensive_fire = 1
defensive_fire = 3
offensive_shock = 8
defensive_shock = 5

trigger = {
	any_owned_province = { #Tsetse flies and jungle
		NOT = { region = central_africa_region }
		NOT = { region = kongo_region }
		NOT = { region = south_africa_region }
		NOT = { region = east_africa_region }
		}
	}
