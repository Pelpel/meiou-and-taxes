#Uhlans (45)

type = cavalry
unit_type = chinese
maneuver = 2

offensive_morale = 7
defensive_morale = 4
offensive_fire = 4
defensive_fire = 3
offensive_shock = 7
defensive_shock = 3
