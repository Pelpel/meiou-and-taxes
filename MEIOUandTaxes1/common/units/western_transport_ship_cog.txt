#4 - Cog
type = transport

hull_size = 9 #140 tonnage
base_cannons = 1 #4 guns
sail_speed = 6 #?

sailors = 16

sprite_level = 1

trigger = { 
	NOT = { technology_group = eastern }
	NOT = { technology_group = muslim }
	NOT = { technology_group = turkishtech }
	NOT = { technology_group = high_turkishtech }
	NOT = { technology_group = nomad_group }
	NOT = { technology_group = steppestech }
	NOT = { technology_group = soudantech }
	NOT = { technology_group = sub_saharan }
	NOT = { technology_group = indian }
	NOT = { technology_group = hawaii_tech }
	NOT = { technology_group = chinese }
	NOT = { technology_group = austranesian }
	is_colonial_nation = no
	}
	