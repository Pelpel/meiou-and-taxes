# Western Gendarme Cavalry (14)

type = cavalry
unit_type = western
maneuver = 2

offensive_morale = 2
defensive_morale = 4
offensive_fire = 0
defensive_fire = 0
offensive_shock = 4
defensive_shock = 3
