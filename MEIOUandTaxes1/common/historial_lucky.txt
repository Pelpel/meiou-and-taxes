# Order here is the priority for taking them, if trigger is true.

ENR = {
   NOT = {
     exists = SPA
     OR = {
       exists = NED
       is_year = 1600
     }   
   }
}

CAS = {
   NOT = {
     exists = ENR
     exists = SPA
     OR = {
       exists = NED
       is_year = 1600
     }   
   }
}

SPA = {
   NOT = {
     exists = CAS
     OR = {
       exists = NED
       is_year = 1600
     }   
   }
}

TUR = {
   NOT = {
     exists = OTT
     is_year = 1650
   }
}

OTT = {
   NOT = {
     exists = TUR
     is_year = 1650
   }
}

QNG = {
   is_year = 1600
}

MCH = {
   NOT = {
     exists = QNG
   }
   is_year = 1600
}

RUS = {
   always = yes
}

MOS = {
   NOT = {
     exists = RUS
   }
}

GBR = {
   always = yes
}

ENG = {
   NOT = {
     exists = GBR
   }
}

FRA = {
   always = yes
}

HAB = {
   always = yes
}

BRA = {
   NOT = {
     exists = PRU
   }
   is_year = 1658
}


PRU = {
   NOT = {
     exists = BRA
   }
   is_year = 1658
}

NED = {
   always = yes
}

POR = {
   NOT = {
     is_year = 1700
   }
}

DEN = {
   NOT = {
     exists = DAN
     is_year = 1523
   }
}

MNG = {
	NOT = { is_year = 1600 }
}

SWE = {
   is_year = 1500
   NOT = {
     is_year = 1700
   }
}

PLC = {
   NOT = {
     is_year = 1640
     exists = ZAZ
   }
}
