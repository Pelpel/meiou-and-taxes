#Country Name: See the Name of this File.

graphical_culture = indiangfx

color = { 60  154  135 }

historical_idea_groups = {
	leadership_ideas
	trade_ideas
	quality_ideas
	diplomatic_ideas
	administrative_ideas
	naval_ideas
	popular_religion_ideas
	economic_ideas
	spy_ideas
}

historical_units = { #North Indian group
	indian_kashmir_slinger_infantry
	indian_war_elephant_cavalry
	indian_bow_infantry
	indian_heavy_elephant_cavalry
	indian_nayaka_infantry
	indian_armored_elephant_cavalry
	indian_fauj_infantry
	indian_tabinan_cavalry
	indian_banduqchi_infantry
	indian_silhedar_cavalry
	indian_mawle_infantry
	indian_musketeer_elephantry
	indian_saranjam_infantry
	indian_ekanda_cavalry
	indian_ganimi_kava_infantry
	indian_pindari_cavalry
	indian_westernized_infantry
	indian_uhlan_cavalry
	indian_rifled_infantry
	indian_lighthussar_cavalry
	indian_drill_infantry
	indian_hunter_cavalry
	indian_impulse_infantry
	indian_lancer_cavalry
	indian_breech_infantry
}

monarch_names = {
	"Hansdeo Pal #0" = 20
	"Sahaj Pal #0" = 20
	"Balbhadra Shah #0" = 10
	"Man Shah #0" = 10
	"Shyam Shah #0" = 10
	"Mahipat Sha #0" = 5
	"Prithvi Pat Shah #0" = 5
	"Fateh Shah#0" = 5
	"Upendra Shah#0" = 5
	"Pradip Shah#0" = 5
	"Lalit Shah#0" = 5
	"Jaikarat Shah#0" = 5
	"Pradyuman Shah#0" = 5
	"Sudarshan Shah#0" = 5
	
	"Saw Sit #0" = -1
	"Saw Pu Nyo #0" = -1
	"Saw Pyauk #0" = -1
	"Saw Pyinsa #0" = -1
	"Saw Yin Mi #0" = -1
	"Gita Chand #0" = -1
	"Uma #0" = -1
	"Krishna Devi #0" = -1
	"Mangaldahi #0" = -1
	"Jira #0" = -1
	"Hira #0" = -1
}

leader_names = {
	Bhumla Singh
	Sardul Singh
	Jodh Singh
	Chand Kaur
	Budh Singh
	Dip Singh
	Granth
	Ratan Kaur
	Kulendra Singh
	Jhotti
	Krishan
	Lohaar
	Parmanand
	Rajsingh
	Shergill
	Singh
	Sodhi
	Sohal
	Rai
	Udasi
}

ship_names = {
	Beas Chenab Devi Gayatri Guru Indus
	Jalsena Jhelum Lakshmi Nausena
	"Nav ka Yudh" Parvati Radha Ratri
	Ravi Sagar Sutlej Sarasvati Sita
}
