# Yuan.txt
# Tag : YUA
# Tech : steppestech
#2011-jun-30 FB	resequenced historical_idea_groups

graphical_culture = asiangfx

color = { 99  43  71 }

historical_idea_groups = {
	leadership_ideas
	popular_religion_ideas
	quantity_ideas
	trade_ideas
	aristocracy_ideas
	economic_ideas
	spy_ideas
	logistic_ideas
}

historical_units = {
	#Steppes Group
	steppes_spear_infantry
	steppes_swarm_cavalry
	steppes_archer_infantry
	steppes_heavy_swarm_cavalry
	steppes_divani_swarm_cavalry
	steppes_heavy_infantry
	steppes_tumandar_cavalry
	steppes_handgunner_infantry
	steppes_yikitlar_cavalry
	steppes_firelock_cavalry
	steppes_banner_infantry
	steppes_raid_cavalry
	steppes_green_standard_infantry
	steppes_shock_cavalry
	steppes_field_army_infantry
	steppes_armeblanche_cavalry
	steppes_hunter_cavalry
	steppes_drill_infantry
	steppes_impulse_infantry
	steppes_lancer_cavalry
	steppes_breech_infantry
}
monarch_names = {
	"Daye" = 100
	"Hongdao" = 100
	"Jinglong" = 100
	"Kaihuang" = 100
	"Kaiyao" = 100
	"Linde" = 100
	"Longshuo" = 100
	"Qianfeng" = 100
	"Renshou" = 100
	"Shangyuan" = 100
	"Sisheng" = 100
	"Shenlong" = 100
	"Tiaolu" = 100
	"Wude" = 100
	"Xianheng" = 100
	"Xianqing" = 100
	"Yifeng" = 100
	"Yonghui" = 100
	"Zhenguan" = 100
	"Zongzhang" = 100
	"Yonglong" = 100
	"Yongchun" = 100
	"Tianshou" = 100
	"Ruyi" = 100
	"Changshou" = 100
	"Yanzai" = 100
	"Zhengsheng" = 100
	"Shegong" = 100
	"Shengli" = 100
	"Jiushi" = 100
	"Dazu" = 100
	"Chang'an" = 100
	"Tanglong" = 100
	"Wenming" = 100
	"Guangzhai" = 100
	"Chuigong" = 100
	"Yongchang" = 100
	"Zaichu" = 100
	"Jingyun" = 100
	"Taiji" = 100
	"Yanhe" = 100
	"Xiantian" = 100
	"Kaiyuan" = 100
	"Tianbao" = 100
	"Shengwu" = 100
	"Tiancheng" = 100
	"Shuntian" = 100
	"Yingtian" = 100
	"Xiansheng" = 100
	"Zhide" = 100
	"Ganyuan" = 100
	"Guangde" = 100
	"Yongtai" = 100
	"Dali" = 100
	"Jianzhong" = 100
	"Xingyuan" = 100
	"Zhengyuan" = 100
	"Yongzhen" = 100
	"Yuanhe" = 100
	"Changqing" = 100
	"Baoli" = 100
	"Dahe" = 100
	"Kaicheng" = 100
	"Huichang" = 100
	"Dazhong" = 100
	"Xiantong" = 100
	"Qianfu" = 100
	"Guangming" = 100
	"Zhonghe" = 100
	"Guangqi" = 100
	"Wende" = 100
	"Longji" = 100
	"Dashun" = 100
	"Jingfu" = 100
	"Qianning" = 100
	"Guanghua" = 100
	"Tianfu" = 100
	"Tianyou" = 100
	"Shence" = 100
	"Tianzan" = 100
	"Tianxian" = 100
	"Huitong" = 100
	"Datong" = 100
	"Qianyou" = 100
	"Tianlu" = 100
	"Yingli" = 100
	"Baoning" = 100
	"Qianheng" = 100
	"Tonghe" = 100
	"Kaitai" = 100
	"Taiping" = 100
	"Chongxi" = 100
	"Qingning" = 100
	"Xianyong" = 100
	"Dakang" = 100
	"Da'an" = 100
	"Shouchang" = 100
	"Qiantong" = 100
	"Tiangqing" = 100
	"Baoda" = 100
	"Shouguo" = 100
	"Tianhui" = 100
	"Tianjuan" = 100
	"Huangtong" = 100
	"Tiande" = 100
	"Zhenyuan" = 100
	"Zhenlong" = 100
	"Dading" = 100
	"Mingchang" = 100
	"Cheng'an" = 100
	"Taihe" = 100
	"Chongqing" = 100
	"Zhining" = 100
	"Zhenyou" = 100
	"Xingding" = 100
	"Yuanguang" = 100
	"Zhengda" = 100
	"Kaixing" = 100
	"Tianxing" = 100
	"Shengchang" = 100
	"Baozheng" = 100
	"Fengli" = 100
	"Qianhua" = 100
	"Zhenming" = 100
	"Longde" = 100
	"Changxing" = 100
	"Yingshun" = 100
	"Qingtai" = 100
	"Kaiyun" = 100
	"Guangshun" = 100
	"Xiande" = 100
	"Jianlong" = 100
	"Qiande" = 100
	"Kaibao" = 100
	"Tongguang" = 100
	"Kaiping" = 100
	"Wucheng" = 100
	"Yongping" = 100
	"Tongzheng" = 100
	"Tianhan" = 100
	"Guangtian" = 100
	"Xiankang" = 100
	"Mingde" = 100
	"Guangzheng" = 100
	"Bailong" = 100
	"Dayou" = 100
	"Yingqian" = 100
	"Qianhe" = 100
	"Dabao" = 100
	"Wuyi" = 100
	"Shunyi" = 100
	"Qianzhen" = 100
	"Tianzuo" = 100
	"Shengyuan" = 100
	"Zhongxing" = 100
	"Yonghe" = 100
	"Longqi" = 100
	"Tongwen" = 100
	"Yongxi" = 100
	"Duangong" = 100
	"Chunhua" = 100
	"Zhidao" = 100
	"Xianping" = 100
	"Jingde" = 100
	"Tianxi" = 100
	"Qianxing" = 100
	"Tiansheng" = 100
	"Mingdao" = 100
	"Jingyou" = 100
	"Baoyuan" = 100
	"Kangding" = 100
	"Qingli" = 100
	"Huangyou" = 100
	"Zhihe" = 100
	"Jiayou" = 100
	"Zhiping" = 100
	"Jingkang" = 100
	"Jianyan" = 100
	"Shaoxing" = 100
	"Longxing" = 100
	"Qiandao" = 100
	"Chunxi" = 100
	"Shaoxi" = 100
	"Qingyuan" = 100
	"Jiatai" = 100
	"Kaixi" = 100
	"Jiading" = 100
	"Baoqing" = 100
	"Shaoding" = 100
	"Duanping" = 100
	"Jiaxi" = 100
	"Chunyou" = 100
	"Baoyou" = 100
	"Kaiqing" = 100
	"Jingding" = 100
	"Xianchun" = 100
	"Deyou" = 100
	"Jingyan" = 100
	"Xiangxing" = 100
	"Yong'an" = 100
	"Xiandao" = 100
	"Guangping" = 100
	"Daqing" = 100
	"Duodu" = 100
	"Gonghua" = 100
	"Zheguan" = 100
	"Yongning" = 100
	"Yuande" = 100
	"Zhengde" = 100
	"Dade" = 100
	"Renqing" = 100
	"Tianqing" = 100
	"Qingtian" = 100
	"Huangjian" = 100
	"Guangding" = 100
	"Qiangding" = 100
	"Baoyi" = 100
	"Zhongtong" = 100
	"Zhiyuan" = 100
	"Yuanzhen" = 100
	"Zhida" = 100
	"Huanqing" = 100
	"Yanyou" = 100
	"Zhizhi" = 100
	"Taiding" = 100
	"Tianshun" = 100
	"Tianli" = 100
	"Zhishun" = 100
	"Yuantong" = 100
	"Zhizheng" = 100
	"Xuanguang" = 100
	"Tianguang" = 100
	"Hongwu" = 100
	"Jianwen" = 100
	"Yongle" = 100
	"Hongxi" = 100
	"Jingtai" = 100
	"Chenghua" = 100
	"Hongzhi" = 100
	"Jiajing" = 100
	"Longqing" = 100
	"Wanli" = 100
	"Taichang" = 100
	"Tianqi" = 100
	"Chongzhen" = 100
	"Hongguang" = 100
	"Longwu" = 100
	"Shaowu" = 100
	"Yongli" = 100
	"Tianming" = 100
	"Tiancong" = 100
	"Chongde" = 100
	"Shunzhi" = 100
	"Kangxi" = 100
	"Yongzheng" = 100
	"Qianlong" = 100
	"Jiaqing" = 100
	"Daoguang" = 100
	"Xianfeng" = 100
	"Tongzhi" = 100
	"Guangxu" = 100
	"Xuantong" = 100

	"Qizhen" = 1
	"Qiyu" = 1
	"Jianjun" = 1
	"Youcheng" = 1
	"Houzhao" = 1
	"Houcong" = 1
	"Zaihou" = 1
	"Yijun" = 1
	"Changluo" = 1
	"Youjian" = 1
	"Yousong" = 1
	"Yujian" = 1
	"Youlang" = 1
	"Chenggong" = 1
	"Kehshuan" = 1
	"Biao" = 1
	"Shuang" = 1
	"Gang" = 1
	"Su" = 1
	"Zhen" = 1
	"Fu" = 1
	"Zi" = 1
	"Qi" = 1
	"Tan" = 1
	"Chun" = 1
	"Bai" = 1
	"Gui" = 1
	"Zhi" = 1
	"Quan" = 1
	"Pian" = 1
	"Hui" = 1
	"Song" = 1
	"Mo" = 1
	"Ying" = 1
	"Jing" = 1
	"Dang" = 1
	"Nan" = 1
	"Gaoxu" = 1
	"Caosui" = 1
	"Gauxi" = 1
	"Youyuan" = 1
	"Youlun" = 1
	"Youbin" = 1
	"Houwei" = 1
	"Youxiao" = 1
	"Youji" = 1
	"Youxue" = 1
	"Youyi" = 1
	"Youshan" = 1
	"Cixuan" = 1
	"Cijiang" = 1
	"Cizhao" = 1
	"Cihuan" = 1
	"Cican" = 1
	"He" = 1
	"Ji" = 1
	"Da" = 1
	"Yuchun" = 1

	"Heying" = -1
	"Chunmei" = -1
	"Kon" = -1
	"Mao" = -1
	"Aigiarm" = -1
	"Borte" = -1
	"Ho'elun" = -1
	"Orqina" = -1
	"Toregene" = -1
}

leader_names = {
	Borjigin Borjigin Borjigin Borjigin Borjigin Borjigin Borjigin Borjigin Borjigin Borjigin # Ghengisid supremacy among the Mongols
	Khalkha Chakhar Uriankhai Ordos T�med Yungshiyebu
	Yingchang Karakorum Hohhot Chagaan
	Noyan Darqan Wang Khorchin Onriut Abaga Abaganar Ast Kharachin
	Jurim Xilingol Ulanqab
}

ship_names = {
	Yingchang Karakorum Hohhot Chagaan
	"Ghengis Khan" J�chi �g�dei Tolui
	M�ngke Qubilai H�leg� B�ke
	Jinggim Kamala Dharmapala "�r�g Tem�r"
	Adai Elbeg Batu Mugali "Bo'orchu" Borokhula Chilaun
	Khubilai Jelme Jebe Subutai
	"Yel� Chucai" Shikhikhutug "Sorqan Shira" "Khar Khiruge"
	"D�rben K�l�'�d" "D�rben Noyas"
}
