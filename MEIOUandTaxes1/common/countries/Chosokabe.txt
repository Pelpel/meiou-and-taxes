#Country Name: Please see filename.

graphical_culture = asiangfx

color = { 204  126  177 }

historical_idea_groups = {
	trade_ideas
	logistic_ideas
	popular_religion_ideas
	naval_ideas
	aristocracy_ideas
	economic_ideas
	spy_ideas
	merchant_marine_ideas
}

#Japanese group
historical_units = {
	asian_light_foot_infantry
	asian_horse_archer_cavalry
	asian_bushi_cavalry
	asian_shashu_no_ashigaru_infantry
	asian_samurai_cavalry
	asian_samurai_infantry
	asian_yarigumi_infantry
	asian_late_samurai_cavalry
	asian_arquebusier_infantry
	asian_musketeer_infantry
	asian_horse_guard_cavalry
	asian_new_guard_infantry
	asian_volley_infantry
	asian_armeblanche_cavalry
	asian_bayonet_infantry
	asian_lighthussar_cavalry
	asian_drill_infantry
	asian_columnar_infantry
	asian_lancer_cavalry
	asian_breech_infantry
	}

monarch_names = {
	"Yoshitoshi #0" = 75
	"Toshimune #0" = 75
	"Tadatoshi #0" = 75
	"Shigeuji #0" = 75
	"Ujiyuki #0" = 75	
	"Mitsuyuki #0" = 75	
	"Kanemitsu #0" = 75	
	"Shigetoshi #0" = 75	
	"Shigetaka #0" = 75
	"Shigemune #0" = 75
	"Shigeyoshi #0" = 75
	"Kaneyoshi #0" = 75
	"Kanetsuna #0" = 75
	"Yoshishige #0" = 75
	 "Fumikane #0" = 75
	"Motokaddo #0" = 75
	"Katsuchika #0" = 75
	"Kanetsugu #0" = 75	
	"Kunichika #0" = 75
	 "Motochika #0" = 75	
	"Morichika #0" = 75	
	"Chikasada #0" = 75	
	"Chikazane #0" = 75
	"Chikayasu #0" = 75
	"Nobuchika #0" = 75
	"Chikakazu #0" = 75
	"Chikatada #0" = 75
	"Moritane #0" = 75
	 "Moritsugu #0" = 75
	
	"Ako #0" = -1
	"Asahi #0" = -1
	"Aya #0" = -1
	"Harukiri #0" = -1
	"Inuwaka #0" = -1
	"Itoito #0" = -1
	"Itsuitsu #0" = -1
	"Koneneme #0" = -1
	"Mitsu #0" = -1
	"Narime #0" = -1
	"Sakami #0" = -1
	"Shiro #0" = -1
	"Tatsuko #0" = -1
	"Tomiko #0" = -1
	"Toyome #0" = -1
	"Yamabukime #0" = -1
}

leader_names = {
	Asai Abe Adachi Akamatsu Akechi Akita Akiyama Akizuki Amago
	Ando Anayama Asakura Ashikaga Asahina
	Chosokabe
	Date
	Hara Hatakeyama Hatano Hayashi Honda Hojo Hosokawa
	Idaten Ii Ikeda Imagawa Inoue Ishida Ishikawa Ishimaki Ito
	Kikkawa Kiso Kitabatake
	Maeda Matsuda Matsudaira Miura Mikumo Miyoshi Mogami M�ri
	Nanbu Nitta Niwa
	Oda �tomo Ouchi
	Rokkaku
	Sakai Sakuma Shimazu Shiba Sanada Sogo Suwa
	Takeda Takigawa Toda Toki Tokugawa Toyotomi Tsutsui
	Uesugi Ukita
	Yagyu Yamana
	# Chosokabe Flavor
	Chosokabe Chosokabe Chosokabe Chosokabe Chosokabe
	Chosokabe Chosokabe Chosokabe Chosokabe Chosokabe
	# Vassals of Chosokabe
	Yoshida Hisatake Tani Kagawa Fukudome Emura Motoyama Aki Ichijo Kira Tsuno
	 Kosokabe
}

ship_names = {
	"Asai Maru" "Abe Maru" "Adachi Maru" "Akamatsu Maru" "Akechi Maru"
	"Akita Maru" "Akiyama Maru" "Akizuki Maru" "Amago Maru" "Ando Maru"
	"Anayama Maru" "Asakura Maru" "Ashikaga Maru" "Asano Maru" "Ashina Maru"
	"Atagi Maru" "Azai Maru"
	"Bito Maru" "Byakko Maru"
	"Chiba Maru" "Chousokabe Maru"
	"Date Maru" "Doi Maru"
	"Fujiwara Maru" "Fuji-san Maru"
	"Genbu maru"
	"Haga Maru" "Hatakeyama Maru" "Hatano Maru" "Honda Maru" "Hojo Maru"
	"Hosokawa Maru" "Hachisuka Maru" "Hayashi Maru" "Hiki Maru"
	"Idaten Maru" "Ikeda Maru" "Imagawa Maru" "Ishida Maru" "Ishikawa Maru"
	"Ishimaki Maru" "Ii Maru" "Inoue Maru" "Ito Maru"
	"Kikkawa Maru" "Kiso Maru" "Kisona Maru" "Kitabatake Maru" "Kyogoku Maru"
	"Maeda Maru" "Matsuda Maru" "Matsudaira Maru" "Miura Maru" "Mikumo Maru"
	"Miyoshi Maru" "Mogami Maru" "Mori Maru"
	"Nitta Maru" "Niwa Maru" "Nihon Maru" "Nanbu Maru"
	"Oda Maru" "Otomo Maru" "Ouchi Maru"
	"Rokkaku Maru"
	"Sakai Maru" "Sakuma Maru" "Satake Maru" "Shimazu Maru" "Shiba Maru"
	"Sanada Maru" "Sogo Maru" "Suwa Maru" "Seiryu Maru" "Suzaku Maru"
	"Takeda Maru" "Tokugawa Maru" "Taira Maru" "Toyotomi Maru" "Tada Maru"
	"Toki Maru" "Tsugaru Maru" "Tsutsui Maru" "Tenno Maru"
	"Uesugi Maru" "Ukita Maru" "Uchia Maru"
	"Yamana Maru" "Yagyu Maru"
}