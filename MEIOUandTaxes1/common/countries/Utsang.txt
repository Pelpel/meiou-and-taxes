#Country Name: Please see filename.

graphical_culture = asiangfx

color = {  103  89  24  }

historical_idea_groups = {
	popular_religion_ideas
	leadership_ideas
	trade_ideas
	logistic_ideas
	economic_ideas
	spy_ideas
	diplomatic_ideas
	administrative_ideas
}

#Tibetan group
historical_units = {
	asian_warrior_monk_infantry
	asian_horse_archer_cavalry
	#complete guesses follow
	asian_lancer_bow_cavalry
	asian_halberdier_infantry
	asian_light_cavalry
	asian_repeating_crossbow_infantry
	asian_rocketeer_infantry
	asian_northern_guard_cavalry
	asian_mandarin_infantry
	asian_banner_infantry
	asian_forbidden_soldier_cavalry
	asian_new_guard_infantry
	asian_platoon_infantry
	asian_mediumhussar_cavalry
	asian_skirmisher_infantry
	asian_lighthussar_cavalry
	asian_rifled_infantry
	asian_impulse_infantry
	asian_lancer_cavalry
	asian_breech_infantry
}

monarch_names = {
	"Gendun Drup #0" = 20
	"Trakpa Chunge #0" = 20
	"Sangye Gyantesen Pal Zangpo #0" = 20
	"D�n-y� Dorje #0" = 20
	"Ngawang Namgyal #0" = 20
	"T�ndup Tseten #0" = 20
	"Ph�ntso Namgye #0" = 20
	"Karma Tseten #0" = 20
	"Lhawang Dorje #0" = 20
	"Karma Ten-Kyong #0" = 20
	"Gusri #0" = 20
	"Dayan #0" = 20
	"Tenzin Dalai #0" = 20
	"Lhabzang #0" = 20
	"Tsewang Arabtan #0" = 20
	"Phola Sonam Topbgye #0" = 20
	"Gyurme Namgyal #0" = 20
	"Lozang Gyastro #0" = 20
	"Tsangyang Gyatso #0" = 20
	"Kelzang Gyatso #0" = 20
	"Jamphel Gyatso #0" = 20
	"Lungtok Gyatso #0" = 20
	"Tsultrim Gyatso #0" = 20
	"Gendun Gyastro #0" = 10
	"Sonam Gyastro #0" = 10
	"Desi Sangay Gyatso #0" = 5
	"Ngwaang Yeshi Gyatso #0" = 5
	"Chang-chub #0" = 1
	"Sakya Gyantsen #0" = 1
	"Trakpa Richen #0" = 1
	"Trakpa Chang-chub #0" = 1
	"Trakpa Chungne #0" = 1
	"Sangye Gyamtso #0" = 1
	"Lobsang Yeshe #0" = 1
	"Palden Yeshi #0" = 1
	"Ngapoi Takna #0" = 1
	"Jigme Wangyal #0" = 1
	"Tashi Chophel #0" = 1
	"Bapu Gendun #0" = 1
	"Rabtan #0" = 1
	"Sengge #0" = 1
	"Galdan #0" = 1
	"Gushi #0" = 1

	"Damo Nyetuma #0" = -1
	"Kunga Pemo #0" = -1
	"Kunga Lhanzi #0" = -1
	"Dondrub Dolma #0" = -1
	"Namkha Kyi #0" = -1
	"Kamala #0" = -1
	"Indira #0" = -1
	"Hoelun #0" = -1
	"Tam�l�n #0" = -1
	"Sorghaghtani #0" = -1
}

leader_names = {
	Se
	Rmu
	Ldong
	Stong
	Dbra
	Dru
	Nyang
	Sbas
	Gnon
	Chespong
	Thonmi
	Mgar
	Bri
	Mong
	Zhang
	Mchims
	Bal
	Lang
	Cogro
	Bro
	Dbas
	Shudpu
	Gnyags
	Mgus
	Mjang
	Snang
	Sgro
	Rma
	Snyiba
	Khu
	Khun
	Mkar
	Rlangs
	Rinspungs
	Zhingshag
	Phjongsrgjas
	Nganlam
	Sba
	Lig
	Khjungpo
	Dbas
	Mnyammed
}

ship_names = {
	Barkam
	Dartsendo
	Gyantse Gartse
	Lhasa Lhotse
	Makalu 
	Nagqu Nyingchi Nedong
	Pelbar
	Qamdo
	Shigatse Sakya
	Tinggri
}
