#Country Name: See the Name of this File.

graphical_culture = indiangfx

color = { 163  5  77 }

historical_idea_groups = {
	quantity_ideas
	trade_ideas
	leadership_ideas
	diplomatic_ideas
	quality_ideas
	administrative_ideas
	naval_ideas
	popular_religion_ideas
	economic_ideas
	spy_ideas
}

#Burmese group
historical_units = {
	asian_tribal_spear_infantry
	asian_heavy_elephant_cavalry
	asian_war_elephant_cavalry
	#guesses follow
	asian_phkap_infantry
	asian_tower_elephant_cavalry
	asian_repeating_crossbow_infantry
	asian_chong_and_crossbow_infantry
	asian_armored_elephant_cavalry
	asian_chong_infantry
	asian_hundred_son_infantry
	asian_war_elephant_artillery_cavalry
	asian_matchlock_musketeer_infantry
	asian_platoon_infantry
	asian_carabinier_cavalry
	asian_skirmisher_infantry
	asian_uhlan_cavalry
	asian_rifled_infantry
	asian_impulse_infantry
	asian_lancer_cavalry
	asian_breech_infantry
}

monarch_names = {
	"Sudangpha #0" = 20
	"Sujangpha #0" = 20
	"Siphakpha #0" = 20
	"Susenpha #0" = 20
	"Suhempha #0" = 20
	"Supimpha #0" = 20
	"Suhung #0" = 20
	"Sutamla #0" = 20
	"Supungmung #0" = 20
	"Sukhampha Khora Raja #0" = 5
	"Suklenmung Garghgaya Raja #0" = 5
	"Gobar #0" = 10
	"Brajnatha #0" = 10
	
	"Thou Umma #0" = -1
	"Thin Bohme #0" = -1
	"Aung #0" = -1
	"Chit #0" = -1
	"Htwe #0" = -1
	"Nyeine #0" = -1
	"Sandi #0" = -1
}

leader_names = {
	Baruah
	Borphukan
	Chandra
	Das
	Deka
	Kalita
	Mahanta
	Narayan
	Prithu
	Roy
	Prithu
}

ship_names = {
	Brahmaputra "Chingri Mach" Darrang
	Devi Dibang "Durga Ma" Ganga "Hilsa Mach"
	"Ilish Mach" Machli "Kali Ma" "Kali Meghana"
	"Kali Nauk" Kamrup Lakimpur "Lal Nauk"
	"Loki Devi" Luhit "Manasa Devi" Manjuli
	Meghna "Nil Nauk" "Nil Sagor" "Parvati Devi"
	"Rui Mach" Saraswoti "Sagorer Bahini" Shakti
	"Shaktir Nauk" "Shasti Ma" Sibsagar Trishool
	"Uma Devi"
}
