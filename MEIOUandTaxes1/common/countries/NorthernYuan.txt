#Country Name: Please see filename.

graphical_culture = asiangfx

color = { 224  104  83 }

historical_idea_groups = {
	leadership_ideas
	popular_religion_ideas
	quantity_ideas
	trade_ideas
	aristocracy_ideas
	economic_ideas
	spy_ideas
	logistic_ideas
}

historical_units = {
	#Steppes Group
	steppes_spear_infantry
	steppes_swarm_cavalry
	steppes_archer_infantry
	steppes_heavy_swarm_cavalry
	steppes_divani_swarm_cavalry
	steppes_heavy_infantry
	steppes_tumandar_cavalry
	steppes_handgunner_infantry
	steppes_yikitlar_cavalry
	steppes_firelock_cavalry
	steppes_banner_infantry
	steppes_raid_cavalry
	steppes_green_standard_infantry
	steppes_shock_cavalry
	steppes_field_army_infantry
	steppes_armeblanche_cavalry
	steppes_hunter_cavalry
	steppes_drill_infantry
	steppes_impulse_infantry
	steppes_lancer_cavalry
	steppes_breech_infantry
}

monarch_names = {
	"Agbarjin #0" = 20
	"Adai #0" = 20
	"Altan #0" = 20
	"Arigaba #0" = 10
	"Arugtai #0" = 10
	"Ayurparibhadra #0" = 10
	"Bodi #0" = 20
	"Bek #0" = 20
	"Bahamu #0" = 20
	"Bars #0" = 20
	"Buyan #0" = 20
	"Biligt� #0" = 10
	"Dayan #0" = 20
	"Darayisung #0" = 20
	"Delbeg #0" = 20
	"Ejei #0" = 10
	"Elbeg #0" = 10
	"Engke #0" = 10
	"G�y�k #0" = 10
	"G�n #0" = 20
	"Gulichi #0" = 20
	"Irinchibal #0" = 10
	"Jorightu #0" = 10
	"Kublai #0" = 10
	"K�ke #0" = 10
	"Ligdan #0" = 20
	"Mark�rgis #0" = 10
	"M�ngke #0" = 10
	"Mulan #0" = 20
	"Manduulun #0" = 20
	"�ljei #0" = 20
	"�r�g #0" = 15
	"Oyiradai #0" = 20
	"�g�dei #0" = 10
	"Qayshan #0" = 10
	"Qoshila #0" = 10
	"Sayn #0" = 20
	"Suddhipala #0" = 10
	"Tayisung #0" = 20
	"Temujin #0" = 10
	"Tem�r #0" = 10
	"Toghun #0" = 10
	"T�men #0" = 10
	"Tolui #0" = 10
	"Toq #0" = 10
	"Uskhal #0" = 10
	"Yes�n #0" = 10
	"Ariq #0" = 1
	"Batu #0" = 1
	"Eljigidei #0" = 1	
	"Joichi #0" = 1

	"Heying" = -1
	"Chunmei" = -1
	"Tianhui" = -1
	"Kon" = -1
	"Mao" = -1
	"Aigiarm" = -1
	"Borte" = -1
	"Ho'elun" = -1
	"Orqina" = -1
	"Toregene" = -1
}

leader_names = { 
	Borjigin
	Besud
	Jalair
	Hatagin
	Olkhonud
	Harnut
	Eljigin
	Bayaud
	Gorlos
	Darhad
	Sharnud
	Uriankhan
	Chinos
	Onkhod
	Mangud
	Ujeed
	Onniud
	Myangan
	Yunsheebuu
	Tumed
	Sartuul
	Tangud
	Merged
	Barga
	Daguur
	Tsakhar
	Kharchin
	Uuld
	Torguut
	Jongoor
	Khotogoid
	Avga
	Tugchin
	Guchid
	Khorchin
	Khitad
	Ikh
	Asud
	Baatud
	Barnud
	Khuuchid
	Khalbagad
	Khangad
	Jarangiinkhan
	Togoruutan
	Khar'd
	Khorkhoi-nudten
	Buurluud
	Khavchig
	Bulagachid
	Gozuul
	Uzuun
	Echeed
	Khavkhchin
	Orovgod
	Tavnag
	Khaakhar
	Khunguud
	Burged
	Khuitserleg
	Chutsuut
	Ulanguud
	Ezen
	Undgaa
	Gahan
	Aksagal
	Zuun-shuvuuchin
	Zaaruud
	Dalandaganad
	Tsagaan-zel
	Khar-zel
	Khariad
	Daar'tan
	Barchuul
	Gerchuud
	Baachuud
	Buuchuud
	Khuluud
	Basigid
	Tsookhor
	Khiitluud
	Gal
	Motoi
	Tele
	Sood
	Mankhilag
	Khamnigan
	Taijiud
	Naiman
	Soloon
	Khoid
	Khereed
	Tsoros
	Talas
	Baarin
	Alag-aduun
	Saljud
	Uvashi
	Uneged
	Asan
	Uuhan
	Uush
	Tsoor
	Jalaid
	Zaisanguud
	Zurchid
	Sunid
	Tatar
	Tuuhai
	Usun
	Khalkha
	Khandgai
	Khangin
	Khiad
	Khoton
	Khurlad
	Tsagaan
	Nirun
	Durved
	Baatar
	Zelme
	Tuved
	Tavuud
	Khoshuud
	Khasag
	Urad
	Yamaat
}

ship_names = {
	Yingchang Karakorum Hohhot Chagaan
	"Ghengis Khan" J�chi �g�dei Tolui
	M�ngke Qubilai H�leg� B�ke
	Jinggim Kamala Dharmapala "�r�g Tem�r"
	Adai Elbeg Batu Mugali "Bo'orchu" Borokhula Chilaun
	Khubilai Jelme Jebe Subutai
	"Yel� Chucai" Shikhikhutug "Sorqan Shira" "Khar Khiruge"
	"D�rben K�l�'�d" "D�rben Noyas"
}

army_names = {
	Kheshig
	"Left Wing Tumen"
	"Right Wing Tumen"
	"Central Tumen"
	"Buryat Tumen"
	"$PROVINCE$ Tumen"
}