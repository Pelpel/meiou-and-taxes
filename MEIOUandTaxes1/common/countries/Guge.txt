#Country Name: Please see filename.

graphical_culture = asiangfx

color = { 50  37  125 }

historical_idea_groups = {
	quantity_ideas
	leadership_ideas
	trade_ideas
	logistic_ideas
	diplomatic_ideas
	spy_ideas
	economic_ideas
	innovativeness_ideas
}

#Tibetan group
historical_units = {
	asian_warrior_monk_infantry
	asian_horse_archer_cavalry
	#complete guesses follow
	asian_lancer_bow_cavalry
	asian_halberdier_infantry
	asian_light_cavalry
	asian_repeating_crossbow_infantry
	asian_rocketeer_infantry
	asian_northern_guard_cavalry
	asian_mandarin_infantry
	asian_banner_infantry
	asian_forbidden_soldier_cavalry
	asian_new_guard_infantry
	asian_platoon_infantry
	asian_mediumhussar_cavalry
	asian_skirmisher_infantry
	asian_lighthussar_cavalry
	asian_rifled_infantry
	asian_impulse_infantry
	asian_lancer_cavalry
	asian_breech_infantry
}

monarch_names = {
	"rNam rgyal lde #0" = 20
	"Nam mkha'i dBang po Phun tshogs lde #0" = 20
	"rNam ri Sang rgyas lde #0" = 20
	"bLo bzang Rab brtan #0" = 20
	"sTod tsha 'Phags pa lha #0" = 20
	"Shakya 'od #0" = 20
	"Jig rten dBang phyug Pad kar lde #0" = 20
	"Ngag gi dBang phyug #0" = 20
	"Nam mkha dBang phyug #0" = 20
	"Khri Nyi ma dBang phyug #0" = 20
	"Khri Grags pa'i dBang phyug #0" = 20
	"Khri Nam rgyal Grags pa lde #0" = 20
	"Khri bKra shis Grags pa lde #0" = 20
	"Pratapamalla #0" = 20
	"Kalyanamalla #0" = 20
	"Ajitamalla #0" = 20
	"San gha sMal #0" = 20
	"Ri'u sMal #0" = 20
	"sPri ti sMal #0" = 20
	"Pu ni sMal #0" = 20

	"Damo Nyetuma #0" = -1
	"Kunga Pemo #0" = -1
	"Kunga Lhanzi #0" = -1
	"Dondrub Dolma #0" = -1
	"Namkha Kyi #0" = -1
	"Kamala #0" = -1
	"Indira #0" = -1
	"Hoelun #0" = -1
	"Tamülün #0" = -1
	"Sorghaghtani #0" = -1
}

leader_names = {
	Gampo
	Srong
	Sregs
	Thekchen
	Sonam
	Tashi
	Tsewang
	Wangdue
	Wangdak
	Phuntsok
}

ship_names = {
	Barkam
	Dartsendo
	Gyantse Gartse
	Lhasa Lhotse
	Makalu 
	Nagqu Nyingchi Nedong
	Pelbar
	Qamdo
	Shigatse Sakya
	Tinggri
}
