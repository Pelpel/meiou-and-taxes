#Country Name: Please see filename.

graphical_culture = africangfx

color = { 55  158  198 }

historical_idea_groups = {
	trade_ideas
	logistic_ideas
	leadership_ideas
	spy_ideas
	diplomatic_ideas
	economic_ideas
	innovativeness_ideas
	popular_religion_ideas
}

historical_units = { #Savannah Empires
	sudanese_ton_tigi_light_cavalry
	sudanese_free_skirmisher_infantry
	sudanese_farima_heavy_cavalry
	sudanese_formation_infantry
	sudanese_quilted_heavy_cavalry
	sudanese_ranked_infantry
	sudanese_armored_heavy_cavalry
	sudanese_elite_archer_infantry
	sudanese_angola_gun_infantry
	sudanese_eso_heavy_cavalry
	sudanese_spear_and_shot_infantry
	sudanese_slave_javelin_cavalry
	sudanese_savannah_musketeer_infantry
	sudanese_sofa_rifle_infantry
	sudanese_mass_infantry
	sudanese_lighthussar_cavalry
	sudanese_columnar_infantry
	sudanese_lancer_cavalry
	sudanese_jominian_infantry
}

monarch_names = {
	"Kunburu #2" = 30
	"Waybu'ali #0" = 20
	"Anakba'ali #0" = 20
	"Barama N'Golo #0" = 10
	"Danfasri #0" = 10
	"Isma'il #0" = 10
	"Muhammad #0" = 10
	"Muhammad Kinba #0" = 10
	"Muhammad Kunburu #0" = 10
	"Abd Allah #0" = 10
	"Abu Bakr #0" = 10
	"Fa Sine #0" = 1
	"Da Kaha #0" = 1
	"Tyefolo #0" = 1
	"Nga Ngolo #0" = 1
	"Sunsa #0" = 1
	"Massa #0" = 1
	"Sekolo #0" = 1
	"Forokolo #0" = 1
	"Seba Mana #0" = 1
	"Deni ba Bo #0" = 1
	"Sira Bo #0" = 1
	"Dessekoro #0" = 1
	"Saraba #0" = 1
	"Nntinkoro #0" = 1
	"Nanka #0" = 1
	"Daoula #0" = 1
	"Yadega #0" = 1
	"Yolomfaogoma #0" = 1
	"Kourita #0" = 1
	"Geba #0" = 1
	"Wobgho #0" = 1
	"Saaga #0" = 1
	"Kaongo #0" = 1
	"Dulugu #0" = 1
	"Sawadogo #0" = 1
	"Karfo #0" = 1
	"Baongo #0" = 1
	"Tontuoriba #0" = 1
	 
	"Masego #0" = -1
	"Sekai #0" = -1
	"Tsholofelo #0" = -1
	"Mathzaho #0" = -1
}

leader_names = {
	Mbazia
	Chichuana
	Cossa
	Machungo
	Sarea
	Machel
	Mutola
	Vashko
	Sanyang
	Mondlane
}

ship_names = {
	Heinyaho Ti�yaho Tigermaxo "Ti�ma ci�we"
	Sorogama Jeneema Sorko Pondori Kotya Korondugu Debo
}
