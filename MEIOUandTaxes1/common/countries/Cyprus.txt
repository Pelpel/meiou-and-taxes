# Kingdom of Cyprus
# Tag : CYP

graphical_culture = easterngfx

color = { 78  75  39 }

historical_idea_groups = {
	popular_religion_ideas
	naval_ideas
	logistic_ideas
	trade_ideas	
	spy_ideas
	economic_ideas
	plutocracy_ideas
	expansion_ideas
}

historical_units = { #Greek group
	eastern_toxotai_infantry
	eastern_horse_archer_cavalry
	eastern_skoutatoi_infantry
	eastern_pronoia_cavalry
	eastern_latinikon_infantry
	eastern_deli_cavalry
	eastern_draby_infantry
	eastern_pomeste_cavalry
	eastern_drabant_infantry
	eastern_light_cossack_cavalry
	eastern_slavic_western_infantry
	eastern_reiter_cavalry
	eastern_regular_infantry
	eastern_armeblanche_cavalry
	eastern_bayonet_infantry
	eastern_uhlan_cavalry
	eastern_drill_infantry
	eastern_columnar_infantry
	eastern_lancer_cavalry
	eastern_breech_infantry
}


monarch_names = {
	"Jacques #1" = 40
	"Jean #1" = 40
	"Henri #2" = 15
	"Charlotte #0" = -15
	"Hugues #4" = 10
	"Guy #1" = 10
	"Eudes #0" = 10
	"Philippe #0" = 10
	"Amalric #2" = 5
	"Pierre #1" = 5
	"Aiolos #0" = 1
	"Bartholomaios #0" = 1
	"Dionysios #0" = 1
	"Eusebios #0" = 1
	"Gregorios #0" = 1
	"Heliodoros #0" = 1
	"Hermogenes #0" = 1
	"Hieronymos #0" = 1
	"Ioannes #0" = 1
	"Isidoros #0" = 1
	"Konstantinos #0" = 1
	"Kreon #0" = 1
	"Lavrentios #0" = 1
	"Matthaios #0" = 1
	"Nikolaos #0" = 1
	"Nikomachos #0" = 1
	"Nomiki #0" = 1
	"Onesimos #0" = 1
	"Phamphilos #0" = 1
	"Pantaleon #0" = 1
	"Paris #0" = 1
	"Pelagios #0" = 1
	"Serafeim #0" = 1
	"Spyridon #0" = 1
	"Thaddeos #0" = 1
	"Theodoros #0" = 1
	"Urias #0" = 1
	"Xenocrates #0" = 1
	"Zacharias #0" = 1
	"Zenobios #0" = 1
	
	"Eirene #1" = -20
	"Theodora #1" = -20
	"Zoe #1" = -10
	"Alexandra #0" = -10
	"Antonia #0" = -5
	"Anna #0" = -5
	"Artemisia #0" = -1
	"Eirene #0" = -1
	"Euphrosyne #0" = -1
	"Laskarina #0" = -1
	"Manto #0" = -1
	"Thalia #0" = -1
	"Sevasti #0" = -1
	"Sophia #0" = -1
}

ship_names = {
	"Agios Georgios"
	"Kato Yialla"
	Lachi Leivadi Lempa 
	Maa Maurvoll
	Pakhyammos Pomos
	Symvoulos
}

leader_names = { 
	Bragadin 
	Crispo
	Doria	
	Fadrigue
	Lusignans
	Melissinos 
	Plethon
	Rendis
	Saraceno 
	Tocco
}
