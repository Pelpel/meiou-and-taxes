#Glossary
# cost = x #==> cost in $ to build (subject to other modifiers)
# time = x #==> number of months to build.
# modifier = m # a modifier on the province that the building gives
# trigger = t # an and trigger that needs to be fullfilled to build and keep the building
# one_per_country = yes/no # if yes, only one of these can exist in a country
# manufactory = { trade_good trade_good } # list of trade goods that get a production bonus
# onmap = yes/no # show as a sprite on the map

################################################
# Naval Buildings
################################################
#Shipyard
#Drydock
#Grand Shipyard

#Dock
#Naval Arsenal
#Naval Base

################################################
# Tier 1, 15th Century Buildings
################################################
dock = {
	cost = 100
	time = 12
	
	trigger = {
		has_port = yes
		NOT = {  has_building = naval_arsenal }
		NOT = {  has_building = naval_base }
	}
	
	modifier = {
		province_trade_power_value = 2
		local_sailors_modifier = 0.6
	}
	
	ai_will_do = {
		factor = 100	
		modifier = {
			factor = 0.4
			OR = {
				has_province_modifier = perfect_communication
				has_province_modifier = rapid_communication
				has_province_modifier = quick_communication
				has_province_modifier = fairly_quick_communication
				has_province_modifier = decent_communication
				has_province_modifier = average_communication
				has_province_modifier = moderate_communication
			}
		}
		modifier = {
			factor = 2.5
			OR = {
				has_province_modifier = difficult_communication
				has_province_modifier = poor_communication
				has_province_modifier = very_poor_communication
				has_province_modifier = terrible_communication
			}
		}
		modifier = {
			factor = 0.0
			not = { manpower = 2 }
		}
		modifier = {
			factor = 0.5
			not = { manpower = 4 }
		}
		modifier = {
			factor = 1.5
			manpower = 5
		}
		modifier = {
			factor = 1.5
			manpower = 7
		}			
	}
}

shipyard = {
	cost = 100
	time = 12
	
	trigger = {
		has_port = yes
		NOT = {  has_building = drydock }
		NOT = {  has_building = grand_shipyard }
	}
	
	modifier = {
		local_ship_repair = 0.1
		ship_recruit_speed = -0.2
		naval_forcelimit = 0.5
	}
	
	ai_will_do = {
		factor = 100	
		modifier = {
			factor = 0
			NOT = { development = 14 }
		}
		modifier = {
			factor = 2.0
			is_capital = yes
		}
		modifier = {
			factor = 6.0
			has_province_modifier = natural_harbour
		}		
		modifier = {
			factor = 1.5
			owner = {
				num_of_colonists = 1
			}
		}
		modifier = {
			factor = 0.25
			is_overseas = yes
		}		
	}
}

################################################
# Tier 2, 16th Century Buildings
################################################
naval_arsenal = {
	cost = 200
	time = 24
	
	make_obsolete = dock
	
	trigger = {
		has_port = yes
		NOT = {  has_building = naval_base }
	}
	
	modifier = {
		province_trade_power_value = 3
		local_sailors_modifier = 1.2
	}
	
	ai_will_do = {
		factor = 100	
		modifier = {
			factor = 0.4
			OR = {
				has_province_modifier = perfect_communication
				has_province_modifier = rapid_communication
				has_province_modifier = quick_communication
				has_province_modifier = fairly_quick_communication
				has_province_modifier = decent_communication
				has_province_modifier = average_communication
			}
		}
		modifier = {
			factor = 2.5
			OR = {
				has_province_modifier = difficult_communication
				has_province_modifier = poor_communication
				has_province_modifier = very_poor_communication
				has_province_modifier = terrible_communication
			}
		}
		modifier = {
			factor = 0.0
			not = { manpower = 2 }
		}
		modifier = {
			factor = 0.5
			not = { manpower = 4 }
		}
		modifier = {
			factor = 1.5
			manpower = 5
		}
		modifier = {
			factor = 1.5
			manpower = 7
		}			
	}
}

drydock = {
	cost = 200
	time = 24
	
	make_obsolete = shipyard
	
	trigger = {
		has_port = yes
		NOT = {  has_building = grand_shipyard }		
	}
	
	modifier = {
		local_ship_repair = 0.2
		ship_recruit_speed = -0.4
		naval_forcelimit = 1
	}
	
	ai_will_do = {
		factor = 100	
		modifier = {
			factor = 0
			NOT = { development = 10 }
		}
		modifier = {
			factor = 2.0
			is_capital = yes
		}
		modifier = {
			factor = 6.0
			has_province_modifier = natural_harbour
		}		
		modifier = {
			factor = 1.5
			owner = {
				num_of_colonists = 1
			}
		}
		modifier = {
			factor = 0.25
			is_overseas = yes
		}		
	}
}

################################################
# Tier 3, 17th Century Buildings
################################################
naval_base = {
	cost = 300
	time = 36
	
	make_obsolete = naval_arsenal
	
	trigger = {
		has_port = yes
	}
	
	modifier = {
		province_trade_power_value = 4
		local_sailors_modifier = 2.4
	}
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0.4
			OR = {
				has_province_modifier = perfect_communication
				has_province_modifier = rapid_communication
				has_province_modifier = quick_communication
				has_province_modifier = fairly_quick_communication
				has_province_modifier = decent_communication
			}
		}
		modifier = {
			factor = 2.5
			OR = {
				has_province_modifier = moderate_communication
				has_province_modifier = difficult_communication
				has_province_modifier = poor_communication
				has_province_modifier = very_poor_communication
				has_province_modifier = terrible_communication
			}
		}
		modifier = {
			factor = 0.0
			not = { manpower = 2 }
		}
		modifier = {
			factor = 0.5
			not = { manpower = 4 }
		}
		modifier = {
			factor = 1.5
			manpower = 5
		}
		modifier = {
			factor = 1.5
			manpower = 7
		}		
	}
}

grand_shipyard = {
	cost = 300
	time = 36
	
	make_obsolete = drydock
	
	trigger = {
		has_port = yes
		OR = {
			has_province_modifier = natural_harbour
			has_province_modifier = important_natural_harbour
		}
	}
	
	modifier = {
		local_ship_repair = 0.3
		ship_recruit_speed = -0.6
		naval_forcelimit = 3
	}
	
	ai_will_do = {
		factor = 100	
		modifier = {
			factor = 0
			NOT = { development = 10 }
		}
		modifier = {
			factor = 2.0
			is_capital = yes
		}
		modifier = {
			factor = 6.0
			has_province_modifier = natural_harbour
		}		
		modifier = {
			factor = 1.5
			owner = {
				num_of_colonists = 1
			}
		}
		modifier = {
			factor = 0.25
			is_overseas = yes
		}		
	}
}