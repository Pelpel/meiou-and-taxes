# XVIth Century Colonisation
colonisation_xvi_minority = {
	global_colonial_growth = 1
}
colonisation_xvi_small = {
	global_colonial_growth = 2
}
colonisation_xvi_medium = {
	global_colonial_growth = 2
	global_tariffs = -0.01
}
colonisation_xvi_significant = {
	global_colonial_growth = 2
	global_tariffs = -0.02
}
colonisation_xvi_large = {
	global_colonial_growth = 2
	global_tariffs = -0.03
}
colonisation_xvi_huge = {
	global_colonial_growth = 2
	global_tariffs = -0.04
}
colonisation_xvi_major = {
	global_colonial_growth = 2
	global_tariffs = -0.05
}
colonisation_xvi_massive = {
	global_colonial_growth = 3
	global_tariffs = -0.05
}

# XVIIth Century Colonisation
colonisation_xvii_minority = {
	global_colonial_growth = 1
}
colonisation_xvii_small = {
	global_colonial_growth = 2
}
colonisation_xvii_medium = {
	global_colonial_growth = 3
}
colonisation_xvii_significant = {
	global_colonial_growth = 4
}
colonisation_xvii_large = {
	global_colonial_growth = 5
}
colonisation_xvii_huge = {
	global_colonial_growth = 6
}
colonisation_xvii_major = {
	global_colonial_growth = 7
	colonists = 1
}
colonisation_xvii_massive = {
	global_colonial_growth = 8
	colonists = 1
}

# XVIIIth Century Colonisation
colonisation_xviii_minority = {
	global_colonial_growth = 1
	global_tariffs = 0.05
}
colonisation_xviii_small = {
	global_colonial_growth = 2
	global_tariffs = 0.05
}
colonisation_xviii_medium = {
	global_colonial_growth = 4
	global_tariffs = 0.05
}
colonisation_xviii_significant = {
	global_colonial_growth = 5
	global_tariffs = 0.05
}
colonisation_xviii_large = {
	global_colonial_growth = 6
	global_tariffs = 0.05
	colonists = 1
}
colonisation_xviii_huge = {
	global_colonial_growth = 7
	global_tariffs = 0.05
	colonists = 1
}
colonisation_xviii_major = {
	global_colonial_growth = 9
	global_tariffs = 0.05
	colonists = 1
}
colonisation_xviii_massive = {
	global_colonial_growth = 10
	global_tariffs = 0.05
	colonists = 1
}

# XIXth Century Colonisation
colonisation_xix_minority = {
	global_colonial_growth = 1
	global_tariffs = 0.20
}
colonisation_xix_small = {
	global_colonial_growth = 2
	global_tariffs = 0.20
}
colonisation_xix_medium = {
	global_colonial_growth = 4
	global_tariffs = 0.20
}
colonisation_xix_significant = {
	global_colonial_growth = 5
	global_tariffs = 0.20
}
colonisation_xix_large = {
	global_colonial_growth = 6
	global_tariffs = 0.20
	colonists = 1
}
colonisation_xix_huge = {
	global_colonial_growth = 7
	global_tariffs = 0.20
	colonists = 1
}
colonisation_xix_major = {
	global_colonial_growth = 9
	global_tariffs = 0.20
	colonists = 1
}
colonisation_xix_massive = {
	global_colonial_growth = 10
	global_tariffs = 0.20
	colonists = 1
}

# Other Colonisation Modifiers
tough_settlers = {
	global_colonial_growth = 2
}

naval_range = {
	range = 0.50
}
		