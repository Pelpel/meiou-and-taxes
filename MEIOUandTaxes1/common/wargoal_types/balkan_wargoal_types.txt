wg_de_jure_wallachia = {
	type = take_province
	
	badboy_factor = 0.2
	prestige_factor = 0.5
	peace_cost_factor = 0.2

	allowed_provinces = {
		or = {
			area = oltenia_area
			area = muntenia_area
			province_id = 159 
			province_id = 3782 
			province_id = 2380
		}
	}
	allowed_provinces_are_eligible = yes

	po_demand_provinces = yes
	po_annex = yes
	
	war_name = DE_JURE_WALLACHIA
}

wg_de_jure_moldavia = {
	type = take_province
	
	badboy_factor = 0.2
	prestige_factor = 0.5
	peace_cost_factor = 0.2

	allowed_provinces = {
		area = moldavia_area
	}
	allowed_provinces_are_eligible = yes

	po_demand_provinces = yes
	po_annex = yes
	
	war_name = DE_JURE_MOLDAVIA
}

wg_de_jure_hungary = {
	type = take_province
	
	badboy_factor = 0.2
	prestige_factor = 0.5
	peace_cost_factor = 0.2

	allowed_provinces = {
		province_group = kingdom_of_hungary_group
	}
	allowed_provinces_are_eligible = yes

	po_demand_provinces = yes
	po_annex = yes
	
	war_name = DE_JURE_HUNGARY
}

wg_de_jure_bulgaria = {
	type = take_province
	
	badboy_factor = 0.2
	prestige_factor = 0.5
	peace_cost_factor = 0.2

	allowed_provinces = {
		or = {
			area = bulgaria_area
			area = dobrogea_area
			area = vidin_area 
			area = macedonia_area 					
			province_id = 2501 
			province_id = 147
		}
	}
	allowed_provinces_are_eligible = yes

	po_demand_provinces = yes
	po_annex = yes
	
	war_name = DE_JURE_BULGARIA
}

wg_de_jure_serbia = {
	type = take_province
	
	badboy_factor = 0.2
	prestige_factor = 0.5
	peace_cost_factor = 0.2

	allowed_provinces = {
		or = {
			area = macedonia_area
			area = serbia_area	
			province_id = 147
			province_id = 3776
		}
	}
	allowed_provinces_are_eligible = yes

	po_demand_provinces = yes
	po_annex = yes
	
	war_name = DE_JURE_SERBIA
}

wg_de_jure_croatia = {
	type = take_province
	
	badboy_factor = 0.2
	prestige_factor = 0.5
	peace_cost_factor = 0.2

	allowed_provinces = {
		or = {
			area = croatia_area
			area = east_adriatic_coast_area	
			province_id = 2389
		}
	}
	allowed_provinces_are_eligible = yes

	po_demand_provinces = yes
	po_annex = yes
	
	war_name = DE_JURE_CROATIA
}

wg_de_jure_bosnia = {
	type = take_province
	
	badboy_factor = 0.2
	prestige_factor = 0.5
	peace_cost_factor = 0.2

	allowed_provinces = {
		area = bosnia_area
	}
	allowed_provinces_are_eligible = yes

	po_demand_provinces = yes
	po_annex = yes
	
	war_name = DE_JURE_BOSNIA
}

wg_de_jure_albania = {
	type = take_province
	
	badboy_factor = 0.2
	prestige_factor = 0.5
	peace_cost_factor = 0.2

	allowed_provinces = {
		area = albania_area
	}
	allowed_provinces_are_eligible = yes

	po_demand_provinces = yes
	po_annex = yes
	
	war_name = DE_JURE_ALBANIA
}