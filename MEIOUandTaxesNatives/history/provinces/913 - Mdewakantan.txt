# No previous file for Mdewakantan

owner = SIO
controller = SIO
add_core = SIO
is_city = yes
culture = ojibwa
religion = totemism
capital = "Mdewakantan"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 45
native_ferocity = 2
native_hostileness = 5

1664.1.1  = {  }
1763.2.10 = {  }
1763.10.9 = {	owner = OJI
		controller = OJI
		add_core = OJI
		culture = anishinabe
		religion = totemism
	    } # Royal Proclamation, land set aside for natives
1813.10.5 = {	owner = USA
		controller = USA
		culture = american
		religion = protestant } #The death of Tecumseh mark the end of organized native resistance 
