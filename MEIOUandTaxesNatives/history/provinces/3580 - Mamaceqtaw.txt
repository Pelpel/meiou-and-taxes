# No previous file for Mamaceqtaw

culture = ojibwa
religion = totemism
capital = "Mamaceqtaw"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 50
native_ferocity = 2
native_hostileness = 5

1650.1.1 = {
	owner = FOX
	controller = FOX 
	is_city = yes
	trade_goods = fur
	add_core = FOX
}
1813.10.5 = {
	owner = USA
	controller = USA
	culture = american
	religion = protestant
} #The death of Tecumseh mark the end of organized native resistance 
