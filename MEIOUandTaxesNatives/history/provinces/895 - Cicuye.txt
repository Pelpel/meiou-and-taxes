# No previous file for Cicuye

culture = comanche
religion = totemism
capital = "Cicuye"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 5
native_ferocity = 4
native_hostileness = 6

1710.1.1  = {
	owner = COM
	controller = COM
	add_core = COM
	trade_goods = maize
	is_city = yes
	culture = shoshone
} #Horses cause waves of migration on the Great Plains
