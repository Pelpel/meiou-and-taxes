# No previous file for Ho-Chunk

culture = illini
religion = totemism
capital = "Ho-Chunk"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 30
native_ferocity = 2
native_hostileness = 5

1650.1.1  = {	owner = ILN
		controller = ILN
		add_core = ILN
		is_city = yes }
1670.1.1  = {  } # Robert Cavelier de La Salle
1808.1.1 = {
	owner = USA
	controller = USA
	citysize = 350
	trade_goods = wheat
	religion = protestant
	culture = american
 } #Fort Madison
