# No previous file for Wenrohronon

culture = neutral
religion = totemism
capital = "Wenrohronon"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 10
native_ferocity = 1 
native_hostileness = 6

1615.1.1  = {  } # �tienne Br�l�
1629.1.1  = {  }
1638.1.1  = {
 	owner = IRO
	controller = IRO
	add_core = IRO
	is_city = yes
	culture = iroquois
} #Taken by Iroquois in Beaver Wars. Seneca migrate here.
1700.1.1  = { citysize = 2944 }
1707.5.12 = {  }
1726.1.1  = {	owner = FRA
		controller = FRA
		religion = catholic
		culture = francien	
	    } # Construction of Fort Niagara
1751.1.1  = { add_core = FRA }
1763.2.10 = {	owner = GBR
		controller = GBR
		remove_core = FRA
		culture = english
		religion = protestant
	    } # Treaty of Paris - ceded to Britain, France gave up its claim
1763.3.1  = { unrest = 6 } # Native discontent with the British rule
1763.10.9 = {	owner = IRO
		controller = IRO
		add_core = IRO
		culture = iroquois
		religion = totemism
	    } # Royal proclamation, Britan recognize native lands (as protectorates)
1784.10.22  = {	owner = USA
		controller = USA
		culture = american
		religion = protestant } #Second Treaty of Fort Stanwix, Iroquois confederacy no longer dominant
1794.6.1  = { unrest = 7 } # Whiskey rebellion, opposition to federal taxation
1794.9.7  = { unrest = 0 } # The revolt is suppressed
1799.1.1  = { unrest = 4 } # John Fries's "House tax rebellion" 
1800.1.1  = { unrest = 0 citysize = 3190 } # Fries is arrested
1809.10.22 = { 	add_core = USA }
