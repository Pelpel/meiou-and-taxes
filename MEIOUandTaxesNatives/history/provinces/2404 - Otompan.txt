# 2404 - Otompan

owner = OTO
controller = OTO
add_core = OTO
is_city = yes
culture = chichimecha
religion = aztec_reformed
capital = "Paxtitlan" 
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1.0
native_size = 25
native_ferocity = 4
native_hostileness = 8


1428.1.1  = {
	owner = AZT
	controller = AZT
	add_core = AZT
}
1519.3.13  = {
	
} # Hernán Cortés
1521.8.13  = {
	owner = SPA
	controller = SPA
	
} #Fall of Tenochtitlan
1546.1.1   = {
	add_core = SPA
}
1571.1.1   = {
	culture = castillian
	religion = catholic
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
}
1810.9.16  = {
	owner = MEX
	controller = MEX
} # Declaration of Independence
1821.8.24  = {
	remove_core = SPA
} # Treaty of Cordba
1832.10.1  = {
	unrest = 3
} # Convention of 1832
1833.4.1   = {
	unrest = 6
} # Convention of 1833
