# No previous file for Saugeen

culture = odawa
religion = totemism
capital = "Saugeen"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 5
native_ferocity = 2
native_hostileness = 5

1650.1.1 = {
	owner = OTW
	controller = OTW
	add_core = OTW
	is_city = yes
	trade_goods = fur
}
