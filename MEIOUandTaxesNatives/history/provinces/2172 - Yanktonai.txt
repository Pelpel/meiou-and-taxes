# No previous file for Yanktonai

culture = dakota
religion = totemism
capital = "Yanktonai"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 40
native_ferocity = 3
native_hostileness = 4

1650.1.1  = {
	owner = SIO
	controller = SIO
	add_core = SIO
	trade_goods = fur
	is_city = yes
} #Position of the natives circal the Beaver Wars
