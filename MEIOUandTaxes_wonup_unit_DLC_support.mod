name="M&T Trade Nations Unit DLC Support"
path="mod/MEIOUandTaxes_wonup_unit_DLC_support"
dependencies={
	"MEIOU and Taxes 1.27"
}
tags={
	"MEIOU and Taxes"
}
picture="MEIOUandTaxesTN.jpg"
supported_version="1.19.*.*"
